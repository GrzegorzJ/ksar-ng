ksar with fixed date issue causing not displaying on the chart statistics for the next days when sar created for more then 1 day.

Sample usage from 'samples' run:
java -jar ksar/kSar-5.0.7.jar -cpuFixedAxis -noEmptyDisk -input 8h-sar-sampled-every-10-seconds-from-two-days.sar.txt -addHTML -outputPNG png -outputCSV output.csv

See samples for more training data.
