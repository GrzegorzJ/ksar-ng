/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class VersionNumber {
    private static VersionNumber instance = new VersionNumber();
    private static String myversion;
    static Integer major;
    static Integer minor;
    static Integer micro;

    public static VersionNumber getInstance() {
        return instance;
    }

    VersionNumber() {
        this.setTo(this.readVersion("/kSar.ver"));
    }

    public String readVersion(String filename) {
        StringBuffer tmpstr = new StringBuffer();
        BufferedReader reader = null;
        try {
            InputStream is = this.getClass().getResourceAsStream(filename);
            InputStreamReader isr = new InputStreamReader(is);
            reader = new BufferedReader(isr);
            String line = "";
            while ((line = reader.readLine()) != null) {
                tmpstr.append(line);
            }
        }
        catch (IOException e) {
            return null;
        }
        try {
            reader.close();
            return tmpstr.toString();
        }
        catch (Exception e) {
            return null;
        }
    }

    public void setTo(String version) {
        this.setVersionNumber(version);
    }

    public void setVersionNumber(String version) {
        myversion = version;
        String[] tmp = version.split("\\.");
        if (tmp.length != 3) {
            return;
        }
        major = new Integer(tmp[0]);
        minor = new Integer(tmp[1]);
        micro = new Integer(tmp[2]);
    }

    public static String getVersionNumber() {
        return myversion;
    }

    public static Integer getVersionNumberint() {
        return major * 100 + minor * 10 + micro;
    }

    public static boolean isOlderThan(String version) {
        String[] tmp = version.split("\\.");
        if (tmp.length != 3) {
            return false;
        }
        Integer mymajor = new Integer(tmp[0]);
        Integer myminor = new Integer(tmp[1]);
        Integer mymicro = new Integer(tmp[2]);
        if (major < mymajor) {
            return true;
        }
        if (minor < myminor) {
            return true;
        }
        if (micro < mymicro) {
            return true;
        }
        return false;
    }
}

