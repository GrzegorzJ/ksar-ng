/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class CalendarSelection2
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private JButton ResetButton;
    private JButton cancelButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JList jList1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JButton oKButton;
    private JList startList;
    kSar mysar = null;
    DefaultListModel mytimeList = null;

    public CalendarSelection2(kSar hissar, DefaultListModel histimeList) {
        this.mysar = hissar;
        this.mytimeList = histimeList;
        this.initComponents();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jPanel3 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.startList = new JList();
        this.jLabel1 = new JLabel();
        this.jPanel4 = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this.jList1 = new JList();
        this.jLabel2 = new JLabel();
        this.jPanel2 = new JPanel();
        this.oKButton = new JButton();
        this.ResetButton = new JButton();
        this.cancelButton = new JButton();
        this.setTitle("Calendar");
        try {
            this.setSelected(true);
        }
        catch (PropertyVetoException e1) {
            e1.printStackTrace();
        }
        this.setVisible(true);
        this.jPanel1.setLayout(new GridLayout(1, 2));
        this.jPanel3.setLayout(new BorderLayout());
        this.startList.setModel(this.mytimeList);
        this.startList.setSelectionMode(0);
        this.jScrollPane1.setViewportView(this.startList);
        this.jPanel3.add((Component)this.jScrollPane1, "Center");
        this.jLabel1.setText("Start at:");
        this.jPanel3.add((Component)this.jLabel1, "First");
        this.jPanel1.add(this.jPanel3);
        this.jPanel4.setLayout(new BorderLayout());
        this.jList1.setModel(this.mytimeList);
        this.jList1.setSelectionMode(0);
        this.jList1.setVisibleRowCount(7);
        this.jScrollPane2.setViewportView(this.jList1);
        this.jPanel4.add((Component)this.jScrollPane2, "Center");
        this.jLabel2.setText("End at:");
        this.jPanel4.add((Component)this.jLabel2, "First");
        this.jPanel1.add(this.jPanel4);
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.oKButton.setText("Ok");
        this.oKButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CalendarSelection2.this.oKButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.oKButton);
        this.ResetButton.setText("Reset");
        this.jPanel2.add(this.ResetButton);
        this.cancelButton.setText("Cancel");
        this.jPanel2.add(this.cancelButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void oKButtonActionPerformed(ActionEvent evt) {
    }

}

