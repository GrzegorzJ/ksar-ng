/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

public class kSarDesktop
extends JFrame {
    public static final long serialVersionUID = 501;
    public String unified_user;
    public String unified_pass;
    public boolean unified_id = false;
    int totalframe = 0;
    public VersionNumber currentVersion = VersionNumber.getInstance();
    public static JMenuItem aboutMenuItem;
    public static JMenuItem alloptions;
    public static JMenuItem autoempty;
    public static JMenu automenu;
    public static JMenuItem autowinconfig;
    public static JMenuItem checkforupdate;
    public static JMenuItem colormenu;
    public JDesktopPane desktopPane;
    public static JMenuItem exitMenuItem;
    public static JMenu fileMenu;
    public static JMenu helpMenu;
    public static JMenuItem jMenuItem3;
    public static JMenuItem jMenuItem4;
    public static JSeparator jSeparator1;
    public static JSeparator jSeparator2;
    public static JSeparator jSeparator3;
    public static JSeparator jSeparator4;
    public static JMenuItem jmenulinuxtrigger;
    public static JMenuItem jmenusolaristrigger;
    public static JMenuBar menuBar;
    public static JMenuItem openMenuItem;
    public static JMenu optmenu;
    public static JMenuItem refreshopt;
    public static JMenuItem tileitem;
    public static JMenu triggermenu;
    public static JMenu windowmenu;

    public kSarDesktop() {
        kSarConfig.hasUI = true;
        int wmargins = 90;
        int hmargins = 60;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.initComponents();
        this.load_shortcut();
        this.setBounds(wmargins, hmargins, screenSize.width - wmargins * 2, screenSize.height - hmargins * 2);
        this.desktopPane.setDragMode(1);
        this.setVisible(true);
    }

    private void initComponents() {
        this.desktopPane = new JDesktopPane();
        menuBar = new JMenuBar();
        fileMenu = new JMenu();
        openMenuItem = new JMenuItem();
        automenu = new JMenu();
        autoempty = new JMenuItem();
        jSeparator2 = new JSeparator();
        exitMenuItem = new JMenuItem();
        triggermenu = new JMenu();
        jmenusolaristrigger = new JMenuItem();
        jmenulinuxtrigger = new JMenuItem();
        jMenuItem3 = new JMenuItem();
        jMenuItem4 = new JMenuItem();
        windowmenu = new JMenu();
        tileitem = new JMenuItem();
        optmenu = new JMenu();
        colormenu = new JMenuItem();
        jSeparator1 = new JSeparator();
        refreshopt = new JMenuItem();
        jSeparator3 = new JSeparator();
        alloptions = new JMenuItem();
        jSeparator4 = new JSeparator();
        autowinconfig = new JMenuItem();
        helpMenu = new JMenu();
        checkforupdate = new JMenuItem();
        aboutMenuItem = new JMenuItem();
        this.setDefaultCloseOperation(0);
        this.setTitle("kSar : a sar grapher");
        this.addWindowListener(new WindowAdapter(){

            public void windowClosing(WindowEvent evt) {
                kSarDesktop.this.formWindowClosing(evt);
            }
        });
        this.desktopPane.setPreferredSize(null);
        this.getContentPane().add((Component)this.desktopPane, "Center");
        fileMenu.setText("File");
        openMenuItem.setText("New window");
        openMenuItem.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);
        automenu.setText("Auto window");
        autoempty.setText("Empty window");
        autoempty.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.autoemptyActionPerformed(evt);
            }
        });
        automenu.add(autoempty);
        fileMenu.add(automenu);
        fileMenu.add(jSeparator2);
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);
        menuBar.add(fileMenu);
        triggermenu.setText("Triggers");
        jmenusolaristrigger.setText("Solaris");
        jmenusolaristrigger.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.jmenusolaristriggerActionPerformed(evt);
            }
        });
        triggermenu.add(jmenusolaristrigger);
        jmenulinuxtrigger.setText("Linux");
        jmenulinuxtrigger.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.jmenulinuxtriggerActionPerformed(evt);
            }
        });
        triggermenu.add(jmenulinuxtrigger);
        jMenuItem3.setText("AIX");
        triggermenu.add(jMenuItem3);
        jMenuItem4.setText("HP-UX");
        triggermenu.add(jMenuItem4);
        menuBar.add(triggermenu);
        windowmenu.setText("Window");
        tileitem.setText("Tile");
        tileitem.setAutoscrolls(true);
        tileitem.setDoubleBuffered(true);
        tileitem.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.tileitemActionPerformed(evt);
            }
        });
        windowmenu.add(tileitem);
        menuBar.add(windowmenu);
        optmenu.setText("Options");
        colormenu.setText("Setup lines colors");
        colormenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.colormenuActionPerformed(evt);
            }
        });
        optmenu.add(colormenu);
        optmenu.add(jSeparator1);
        refreshopt.setText("Refresh options");
        refreshopt.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.refreshoptActionPerformed(evt);
            }
        });
        optmenu.add(refreshopt);
        optmenu.add(jSeparator3);
        alloptions.setText("Preferences");
        alloptions.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.alloptionsActionPerformed(evt);
            }
        });
        optmenu.add(alloptions);
        optmenu.add(jSeparator4);
        autowinconfig.setText("Config window");
        autowinconfig.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.autowinconfigActionPerformed(evt);
            }
        });
        optmenu.add(autowinconfig);
        menuBar.add(optmenu);
        helpMenu.setText("Help");
        checkforupdate.setText("Check for update");
        checkforupdate.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.checkforupdateActionPerformed(evt);
            }
        });
        helpMenu.add(checkforupdate);
        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarDesktop.this.aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);
        menuBar.add(helpMenu);
        this.setJMenuBar(menuBar);
        this.pack();
    }

    private void exitMenuItemActionPerformed(ActionEvent evt) {
        this.tryToQuit();
    }

    private void aboutMenuItemActionPerformed(ActionEvent evt) {
        AboutBox myAboutBox = AboutBox.getInstance();
        myAboutBox.setVisible(true);
        Dimension d1 = myAboutBox.getSize();
        Dimension d2 = this.getSize();
        this.desktopPane.remove(myAboutBox);
        this.desktopPane.add(myAboutBox);
        int x = Math.max((d2.width - d1.width) / 2, 0);
        int y = Math.max((d2.height - d1.height) / 2, 0);
        myAboutBox.setBounds(x, y, d1.width, d1.height);
        try {
            myAboutBox.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    private void formWindowClosing(WindowEvent evt) {
        this.tryToQuit();
    }

    private void openMenuItemActionPerformed(ActionEvent evt) {
        this.make_new_window("Empty");
    }

    public kSar make_new_window(String title) {
        kSar newsar = new kSar(this, title);
        return newsar;
    }

    public void do_tile() {
        kSarDesktop.tile(this.desktopPane);
    }

    private void tileitemActionPerformed(ActionEvent evt) {
        this.do_tile();
    }

    private void checkforupdateActionPerformed(ActionEvent evt) {
        UpdateManager myUpdateManager = new UpdateManager();
        myUpdateManager.setVisible(true);
        Dimension d1 = myUpdateManager.getSize();
        Dimension d2 = this.getSize();
        this.desktopPane.add(myUpdateManager);
        int x = Math.max((d2.width - d1.width) / 2, 0);
        int y = Math.max((d2.height - d1.height) / 2, 0);
        myUpdateManager.setBounds(x, y, d1.width, d1.height);
        try {
            myUpdateManager.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    private void alloptionsActionPerformed(ActionEvent evt) {
        AllOptionsForm tmpoptions = new AllOptionsForm(this);
        tmpoptions.setVisible(true);
        this.desktopPane.add(tmpoptions);
        try {
            tmpoptions.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    private void jmenusolaristriggerActionPerformed(ActionEvent evt) {
        SolarisTrigger tmptrigger = new SolarisTrigger();
        tmptrigger.setVisible(true);
        this.desktopPane.add(tmptrigger);
        try {
            tmptrigger.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    private void jmenulinuxtriggerActionPerformed(ActionEvent evt) {
        LinuxTrigger tmptrigger = new LinuxTrigger();
        tmptrigger.setVisible(true);
        this.desktopPane.add(tmptrigger);
        try {
            tmptrigger.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    private void autoemptyActionPerformed(ActionEvent evt) {
        this.make_new_window("Empty");
    }

    private void autowinconfigActionPerformed(ActionEvent evt) {
        ConfigStartup tmpstart = new ConfigStartup(this);
        tmpstart.setVisible(true);
    }

    private void colormenuActionPerformed(ActionEvent evt) {
        LineColorChooser tmp = new LineColorChooser(this, true);
        tmp.setVisible(true);
    }

    private void refreshoptActionPerformed(ActionEvent evt) {
        ParserOptions tmp = new ParserOptions(this, true);
        tmp.setVisible(true);
    }

    public void load_shortcut() {
        if (kSarConfig.shortcut_window_list == null) {
            return;
        }
        for (String key : kSarConfig.shortcut_window_list.keySet()) {
            String value = kSarConfig.shortcut_window_list.get(key);
            this.add_shortcut(value);
        }
    }

    public void show_shortcut(String shortcut_desc) {
        String tmpcmd = null;
        for (String key : kSarConfig.shortcut_window_list.keySet()) {
            String value = kSarConfig.shortcut_window_list.get(key);
            if (!value.equals(shortcut_desc)) continue;
            tmpcmd = key;
        }
        if (tmpcmd != null) {
            this.make_new_window(tmpcmd);
        }
    }

    public void add_shortcut(String shortcut_desc) {
        JMenuItem tmp = new JMenuItem(shortcut_desc);
        tmp.setActionCommand(shortcut_desc);
        tmp.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent h) {
                kSarDesktop.this.show_shortcut(h.getActionCommand());
            }
        });
        automenu.add(tmp);
    }

    public void delete_shortcut(String shortcut_desc) {
        for (int i = 0; i < automenu.getMenuComponentCount(); ++i) {
            JMenuItem tmp = (JMenuItem)automenu.getMenuComponent(i);
            if (tmp == null || !tmp.getText().equals(shortcut_desc)) continue;
            automenu.remove(tmp);
        }
    }

    private static JInternalFrame[] filterFrame(JInternalFrame[] frames) {
        int n = 0;
        for (int i = 0; i < frames.length; ++i) {
            if (!frames[i].isVisible() || frames[i].isIcon()) continue;
            ++n;
        }
        JInternalFrame[] newfs = new JInternalFrame[n];
        int j = 0;
        for (int i2 = 0; i2 < frames.length; ++i2) {
            if (!frames[i2].isVisible() || frames[i2].isIcon()) continue;
            newfs[j++] = frames[i2];
        }
        return newfs;
    }

    public static void tile(JDesktopPane desktopPane) {
        JInternalFrame[] frames = kSarDesktop.filterFrame(desktopPane.getAllFrames());
        if (frames.length == 0) {
            return;
        }
        kSarDesktop.tile(frames, desktopPane.getBounds());
    }

    private static void tile(JInternalFrame[] frames, Rectangle dBounds) {
        int rows;
        int width;
        int height;
        int cols = (int)Math.sqrt(frames.length);
        int lastRow = frames.length - cols * ((rows = (int)Math.ceil((double)frames.length / (double)cols)) - 1);
        if (lastRow == 0) {
            height = dBounds.height / --rows;
        } else {
            height = dBounds.height / rows;
            if (lastRow < cols) {
                --rows;
                width = dBounds.width / lastRow;
                for (int i = 0; i < lastRow; ++i) {
                    frames[cols * rows + i].setBounds(i * width, rows * height, width, height);
                }
            }
        }
        width = dBounds.width / cols;
        for (int j = 0; j < rows; ++j) {
            for (int i = 0; i < cols; ++i) {
                frames[i + j * cols].setBounds(i * width, j * height, width, height);
            }
        }
    }

    public void showgraphselected(String s, kSarUI hisUI) {
        JInternalFrame[] frames = this.desktopPane.getAllFrames();
        if (frames.length <= 0) {
            return;
        }
        for (int i = 0; i < frames.length; ++i) {
            if (!(frames[i] instanceof kSarUI) || hisUI == frames[i] || hisUI.mysar.isparsing) continue;
            ((kSarUI)frames[i]).changeintoherframe(s);
        }
    }

    private void tryToQuit() {
        int i = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit kSar ?", "Confirm", 0);
        if (i == 0) {
            System.exit(0);
        }
    }

}

