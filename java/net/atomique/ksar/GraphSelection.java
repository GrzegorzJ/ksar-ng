/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class GraphSelection {
    private static final long serialVersionUID = 4;
    kSar mysar;
    Map<String, JCheckBox> toprinthash;
    Map<String, AllGraph> pdfList;
    String hostPrintPrefs = new String("");
    String pdffilename;
    String graphtype;

    public GraphSelection(kSar hissar, Map<String, AllGraph> hispdfList, String filename, String type) {
        this.mysar = hissar;
        this.pdffilename = filename;
        TreeMap<String, AllGraph> tmphash = new TreeMap<String, AllGraph>(hispdfList);
        this.toprinthash = new HashMap<String, JCheckBox>();
        this.graphtype = type;
        this.pdfList = hispdfList;
        this.mysar.okforprinting = false;
        boolean mustbeselected = true;
        this.mysar.printList.clear();
        JPanel panel0 = new JPanel();
        JPanel panel1 = new JPanel(new GridLayout(0, 1));
        JPanel panel3 = new JPanel(new GridLayout(5, 0));
        panel1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        final JDialog dialog = new JDialog();
        dialog.setVisible(true);
        JButton okButton = new JButton("Ok");
        JButton cancelButton = new JButton("Cancel");
        JButton saButton = new JButton("Select All");
        JButton uaButton = new JButton("Unselect All");
        JButton woButton = new JButton("Unselect Disks");
        String prefGraph = kSarConfig.readSpecial("PDF:" + this.mysar.hostName);
        Iterator<String> it = tmphash.keySet().iterator();
        while (it.hasNext()) {
            mustbeselected = true;
            String key = it.next();
            AllGraph value = tmphash.get(key);
            if (key.indexOf("-t2") > 0 || key.indexOf("-if2") > 0 || key.indexOf("Aixwait") > 0 || key.indexOf("Solariswait") > 0 || key.indexOf("Hpuxwait") > 0) continue;
            if (prefGraph != null && prefGraph.indexOf(" " + key + " ") < 0) {
                mustbeselected = false;
            }
            JCheckBox tmp = new JCheckBox(value.getcheckBoxTitle() + " ", mustbeselected);
            this.toprinthash.put(key, tmp);
            panel1.add(tmp);
        }
        JScrollPane jscroll = new JScrollPane(panel1);
        jscroll.setPreferredSize(new Dimension(300, 200));
        jscroll.setVerticalScrollBarPolicy(20);
        jscroll.setHorizontalScrollBarPolicy(31);
        panel3.add(okButton);
        panel3.add(cancelButton);
        panel3.add(uaButton);
        panel3.add(saButton);
        panel3.add(woButton);
        okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
                dialog.dispose();
                GraphSelection.this.mysar.okforprinting = true;
                GraphSelection.this.parsebox();
            }
        });
        cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
                dialog.dispose();
                GraphSelection.this.mysar.okforprinting = true;
            }
        });
        saButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                for (String key : GraphSelection.this.toprinthash.keySet()) {
                    JCheckBox value = GraphSelection.this.toprinthash.get(key);
                    value.setSelected(true);
                }
            }
        });
        uaButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                for (String key : GraphSelection.this.toprinthash.keySet()) {
                    JCheckBox value = GraphSelection.this.toprinthash.get(key);
                    value.setSelected(false);
                }
            }
        });
        woButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                for (String key : GraphSelection.this.toprinthash.keySet()) {
                    JCheckBox value = GraphSelection.this.toprinthash.get(key);
                    if (key.toString().indexOf("-t1") > 0) {
                        value.setSelected(false);
                    }
                    if (key.toString().indexOf("Solarisxfer") > 0) {
                        value.setSelected(false);
                    }
                    if (key.toString().indexOf("Hpuxxfer") > 0) {
                        value.setSelected(false);
                    }
                    if (key.toString().indexOf("Aixxfer") <= 0) continue;
                    value.setSelected(false);
                }
            }
        });
        panel0.add(jscroll);
        panel0.add(panel3);
        dialog.getContentPane().add(panel0);
        dialog.pack();
        dialog.setLocationRelativeTo(this.mysar.myUI);
    }

    public void parsebox() {
        for (String key : this.toprinthash.keySet()) {
            String diskn;
            JCheckBox value = this.toprinthash.get(key);
            if (!value.isSelected()) continue;
            if (key.indexOf("-t1") > 0) {
                diskn = key.substring(0, key.length() - 3);
                if (this.pdfList.get(diskn + "-t1") != null) {
                    this.mysar.printList.put(diskn + "-t1", this.pdfList.get(diskn + "-t1"));
                }
                if (this.pdfList.get(diskn + "-t2") == null) continue;
                this.mysar.printList.put(diskn + "-t2", this.pdfList.get(diskn + "-t2"));
                continue;
            }
            if (key.indexOf("-if1") > 0) {
                diskn = key.substring(0, key.length() - 4);
                if (this.pdfList.get(diskn + "-if1") != null) {
                    this.mysar.printList.put(diskn + "-if1", this.pdfList.get(diskn + "-if1"));
                }
                if (this.pdfList.get(diskn + "-if2") == null) continue;
                this.mysar.printList.put(diskn + "-if2", this.pdfList.get(diskn + "-if2"));
                continue;
            }
            if (key.indexOf("Solarisxfer") > 0) {
                diskn = key.substring(0, key.length() - 11);
                if (this.pdfList.get(diskn + "Solarisxfer") != null) {
                    this.mysar.printList.put(diskn + "Solarisxfer", this.pdfList.get(diskn + "Solarisxfer"));
                }
                if (this.pdfList.get(diskn + "Solariswait") == null) continue;
                this.mysar.printList.put(diskn + "Solariswait", this.pdfList.get(diskn + "Solariswait"));
                continue;
            }
            if (key.indexOf("Hpuxxfer") > 0) {
                diskn = key.substring(0, key.length() - 8);
                if (this.pdfList.get(diskn + "Hpuxxfer") != null) {
                    this.mysar.printList.put(diskn + "Hpuxxfer", this.pdfList.get(diskn + "Hpuxxfer"));
                }
                if (this.pdfList.get(diskn + "Hpuxwait") == null) continue;
                this.mysar.printList.put(diskn + "Hpuxwait", this.pdfList.get(diskn + "Hpuxwait"));
                continue;
            }
            if (key.indexOf("Aixxfer") > 0) {
                diskn = key.substring(0, key.length() - 7);
                if (this.pdfList.get(diskn + "Aixxfer") != null) {
                    this.mysar.printList.put(diskn + "Aixxfer", this.pdfList.get(diskn + "Aixxfer"));
                }
                if (this.pdfList.get(diskn + "Aixwait") == null) continue;
                this.mysar.printList.put(diskn + "Aixwait", this.pdfList.get(diskn + "Aixwait"));
                continue;
            }
            if (this.pdfList.get(key) == null) continue;
            this.mysar.printList.put(key, this.pdfList.get(key));
        }
        if (!this.mysar.okforprinting) {
            return;
        }
        if (this.mysar.printList.size() < 1) {
            return;
        }
        HashMap<String, AllGraph> tmphash = new HashMap<String, AllGraph>();
        tmphash.putAll(this.mysar.printList);
        for (String key2 : tmphash.keySet()) {
            AllGraph value = (AllGraph)tmphash.get(key2);
            this.hostPrintPrefs = this.hostPrintPrefs.concat(" " + key2 + " ");
        }
        kSarConfig.writeSpecial("PDF:" + this.mysar.hostName, this.hostPrintPrefs.toString());
        if ("PDF".equals(this.graphtype)) {
            this.mysar.myUI.exportPdf(this.pdffilename, this.mysar.printList);
        }
        if ("JPG".equals(this.graphtype)) {
            this.mysar.myUI.exportJpg(this.pdffilename, this.mysar.printList);
        }
        if ("PNG".equals(this.graphtype)) {
            this.mysar.myUI.exportPng(this.pdffilename, this.mysar.printList);
        }
    }

}

