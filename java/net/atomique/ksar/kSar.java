/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 */
package net.atomique.ksar;

import java.awt.Component;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.FileRead;
import net.atomique.ksar.JpgExport;
import net.atomique.ksar.Linux.Parser;
import net.atomique.ksar.LocalCommand;
import net.atomique.ksar.OSInfo;
import net.atomique.ksar.PdfExport;
import net.atomique.ksar.PngExport;
import net.atomique.ksar.Ps.ProcessList;
import net.atomique.ksar.SSHCommand;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;
import net.atomique.ksar.kSarUI;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.joda.time.LocalDate;

public class kSar {
    public boolean isparsing = false;
    int sarType = 0;
    public OSInfo myOS = null;
    public kSarUI myUI = null;
    public kSarDesktop mydesktop = null;
    public DefaultMutableTreeNode graphtree = new DefaultMutableTreeNode("kSar");
    public String hostName = null;
    public String osVersion = null;
    public String kernelVersion = null;
    public String cpuType = null;
    public String sarDate = null;
    public LocalDate sarStartDate = null;
    public int day = 0;
    public int month = 0;
    public int year = 0;
    public int underaverage = 0;
    public String statstart = null;
    public Second startofgraph = null;
    public ArrayList<Second> datefound = new ArrayList();
    public Second lastever = new Second(0, 0, 0, 1, 1, 1970);
    public String statend = null;
    public Second endofgraph = null;
    public HashMap<String, AllGraph> DetectedBounds = new HashMap();
    public boolean showtrigger = false;
    public boolean showstackedcpu = false;
    public boolean show100axiscpu = false;
    public boolean showemptydisk = true;
    public boolean showstackedmem = false;
    public HashMap<String, AllGraph> pdfList = new HashMap();
    public HashMap<String, TimeSeries> dispo = new HashMap();
    public HashMap othergraphlist = new HashMap();
    public int solarispagesize = -1;
    public boolean hasdisknode = false;
    public DefaultMutableTreeNode diskstreenode = new DefaultMutableTreeNode("Disk");
    public HashMap<String, AllGraph> disksSarList = new HashMap();
    public HashMap<String, diskName> AlternateDiskName = new HashMap();
    public HashMap<String, String> Adiskname = new HashMap();
    public boolean haspidnode = false;
    public DefaultMutableTreeNode pidstreenode = new DefaultMutableTreeNode("Pid");
    public HashMap<String, AllGraph> pidSarList = new HashMap();
    public boolean hascpunode = false;
    public DefaultMutableTreeNode cpustreenode = new DefaultMutableTreeNode("Cpu");
    public HashMap<String, AllGraph> cpuSarList = new HashMap();
    public boolean hasifnode = false;
    public DefaultMutableTreeNode ifacetreenode = new DefaultMutableTreeNode("Interface");
    public HashMap<String, AllGraph> ifaceSarList = new HashMap();
    static final String parser_err1 = "There was a problem while parsing stat";
    static final String parser_err2 = "Sorry i cannot parse this file using the command line";
    static final String parser_end = "Data import is finished";
    static final String parser_solarispagesize = "\nDon't forget to set PageSize (unders Option Menu)";
    public boolean showmemusedbuffersadjusted = false;
    public String reload_command = new String("Empty");
    public HashMap<String, AllGraph> printList = new HashMap();
    public boolean okforprinting = false;
    public boolean hasfilenode = false;
    public DefaultMutableTreeNode filetreenode = new DefaultMutableTreeNode("File");
    public HashMap<String, AllGraph> fileSarList = new HashMap();
    public boolean hasmsgnode = false;
    public DefaultMutableTreeNode msgtreenode = new DefaultMutableTreeNode("Message & Semaphore");
    public HashMap<String, AllGraph> msgSarList = new HashMap();
    public boolean hascswchnode = false;
    public DefaultMutableTreeNode cswchtreenode = new DefaultMutableTreeNode("Context");
    public HashMap<String, AllGraph> cswchSarList = new HashMap();
    public boolean hasscallnode = false;
    public DefaultMutableTreeNode scalltreenode = new DefaultMutableTreeNode("Syscalls");
    public HashMap<String, AllGraph> scallSarList = new HashMap();
    Thread launched_command = null;
    public boolean command_interrupted = false;
    public boolean hasbuffernode = false;
    public DefaultMutableTreeNode buffertreenode = new DefaultMutableTreeNode("Buffers");
    public HashMap<String, AllGraph> bufferSarList = new HashMap();
    public boolean haspaging1node = false;
    public DefaultMutableTreeNode paging1treenode = new DefaultMutableTreeNode("Paging1");
    public HashMap<String, AllGraph> paging1SarList = new HashMap();
    public boolean haspaging2node = false;
    public DefaultMutableTreeNode paging2treenode = new DefaultMutableTreeNode("Paging2");
    public HashMap<String, AllGraph> paging2SarList = new HashMap();
    public boolean hasswapingnode = false;
    public DefaultMutableTreeNode swapingtreenode = new DefaultMutableTreeNode("Swapping");
    public HashMap<String, AllGraph> swapingSarList = new HashMap();
    public boolean hasttynode = false;
    public DefaultMutableTreeNode ttytreenode = new DefaultMutableTreeNode("TTY");
    public HashMap<String, AllGraph> ttySarList = new HashMap();
    public boolean haspsetnode = false;
    public DefaultMutableTreeNode psettreenode = new DefaultMutableTreeNode("Pset");
    public HashMap<String, AllGraph> psetSarList = new HashMap();
    public boolean hasnfsnode = false;
    public DefaultMutableTreeNode nfstreenode = new DefaultMutableTreeNode("NFS");
    public boolean hasintrlistnode = false;
    public HashMap<String, AllGraph> intrSarlist = new HashMap();
    public DefaultMutableTreeNode intrtreenode = new DefaultMutableTreeNode("Interrupt");
    public boolean showstackedintrlist = false;
    net.atomique.ksar.Solaris.Parser sarParsersolaris = null;
    Parser sarParserlinux = null;
    net.atomique.ksar.AIX.Parser sarParserAix = null;
    net.atomique.ksar.Hpux.Parser sarParserHpux = null;
    net.atomique.ksar.Mac.Parser sarParserMac = null;
    net.atomique.ksar.Esar.Parser sarParserEsar = null;
    ProcessList pslist = null;
    public File tmpfile = null;
    public BufferedWriter tmpfile_out = null;
    public String previousOperationType = "";

    public kSar(String title) {
        if (!this.parse_mission(title)) {
            System.err.println("Cannot process input: " + title);
            return;
        }
        this.do_mission(title);
    }

    public kSar(kSarDesktop hisdesktop, String title) {
        this.mydesktop = hisdesktop;
        if (!this.parse_mission(title)) {
            System.err.println("Cannot process input: " + title);
            return;
        }
        this.addGUI(title);
        this.do_mission(title);
    }

    public void do_fileread(String filename) {
        this.resetInfo();
        this.launched_command = filename == null ? new FileRead(this) : new FileRead(this, filename);
        this.reload_command = ((FileRead)this.launched_command).get_action();
        this.launched_command.start();
    }

    public void do_sshread(String cmd) {
        this.resetInfo();
        this.launched_command = cmd == null ? new SSHCommand(this, null) : new SSHCommand(this, cmd);
        this.reload_command = ((SSHCommand)this.launched_command).get_action();
        this.launched_command.start();
    }

    public void do_localcommand(String cmd) {
        this.resetInfo();
        this.launched_command = cmd == null ? new LocalCommand(this) : new LocalCommand(this, cmd);
        this.reload_command = ((LocalCommand)this.launched_command).get_action();
        this.launched_command.start();
    }

    public void do_mission(String title) {
        if ("Empty".equals(title)) {
            return;
        }
        if (title.startsWith("file://")) {
            String filename = new String(title.substring(7));
            this.do_fileread(filename);
            return;
        }
        if (title.startsWith("cmd://")) {
            String commandname = new String(title.substring(6));
            this.get_processlist(title);
            this.do_localcommand(commandname);
            return;
        }
        if (title.startsWith("ssh://")) {
            String commandname = new String(title.substring(6));
            this.get_processlist(title);
            this.do_sshread(commandname);
            return;
        }
        this.do_fileread(title);
    }

    private void get_processlist(String orig) {
        if (kSarConfig.shortcut_window_processlist == null) {
            return;
        }
        String processlist_cmd = kSarConfig.shortcut_window_processlist.get(orig);
        if (processlist_cmd != null) {
            this.pslist = new ProcessList(this, processlist_cmd);
        }
    }

    private boolean testfile(String filepath) {
        if (!new File(filepath).exists()) {
            return false;
        }
        return true;
    }

    public boolean parse_mission(String mission) {
        if ("Empty".equals(mission)) {
            return true;
        }
        if (mission.startsWith("file://")) {
            return this.testfile(mission.substring(7));
        }
        if (mission.startsWith("ssh://")) {
            return true;
        }
        if (mission.startsWith("cmd://")) {
            return true;
        }
        return this.testfile(mission);
    }

    public void setPageSize() {
        String tmp = kSarConfig.readSpecial("PGSZ:" + this.hostName);
        if (tmp != null) {
            Integer i = new Integer(tmp);
            this.solarispagesize = i;
        }
    }

    public void getUserPref() {
        String prefGraph = new String(kSarConfig.readSpecial("PDF:" + this.hostName));
        if (prefGraph != null && prefGraph.length() > 3) {
            for (String key : this.pdfList.keySet()) {
                AllGraph value = this.pdfList.get(key);
                if (prefGraph.indexOf(" " + key.toString() + " ") <= 0) continue;
                this.printList.put(key.toString(), this.pdfList.get(key.toString()));
            }
        } else {
            this.printList = this.pdfList;
        }
    }

    public void getGraphList(String graphlist) {
        if (graphlist == null) {
            this.printList.putAll(this.pdfList);
            return;
        }
        if (graphlist.length() < 2) {
            this.printList.putAll(this.pdfList);
            return;
        }
        for (String key : this.pdfList.keySet()) {
            AllGraph value = this.pdfList.get(key);
            if (graphlist.indexOf(" " + key.toString() + " ") < 0) continue;
            this.printList.put(key.toString(), value);
        }
    }

    public int outputCsv(String csvfilename) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(csvfilename));
        }
        catch (IOException e) {
            out = null;
        }
        try {
            out.write("Date;");
            TreeMap<String, TimeSeries> tmphash = new TreeMap<String, TimeSeries>();
            tmphash.putAll(this.dispo);
            for (String key : tmphash.keySet()) {
                out.write(key + ";");
            }
            out.write("\n");
            ArrayList<Second> tmp2list = new ArrayList<Second>();
            tmp2list.addAll(this.datefound);
            for (Second value_time : tmp2list) {
                out.write(value_time.toString() + ";");
                for (String key2 : tmphash.keySet()) {
                    TimeSeries value = this.dispo.get(key2);
                    if (value.getValue((RegularTimePeriod)value_time) == null) {
                        out.write(";");
                        continue;
                    }
                    out.write(value.getValue((RegularTimePeriod)value_time) + ";");
                }
                out.write("\n");
            }
            out.close();
        }
        catch (IOException e) {
            out = null;
        }
        return 0;
    }

    public int outputPdf(String pdffilename, boolean usepref, String graphlist) {
        if (usepref) {
            this.getUserPref();
        } else {
            this.getGraphList(graphlist);
        }
        PdfExport t = new PdfExport(pdffilename, this.printList, null, null, this);
        t.run();
        return 0;
    }

    public int outputJpg(String basefilename, boolean usepref, boolean dohtml, String graphlist, int width, int height) {
        if (usepref) {
            this.getUserPref();
        } else {
            this.getGraphList(graphlist);
        }
        JpgExport t = new JpgExport(basefilename, this.printList, null, null, this, dohtml, width, height);
        t.run();
        return 0;
    }

    public int outputPng(String basefilename, boolean usepref, boolean dohtml, String graphlist, int width, int height) {
        if (usepref) {
            this.getUserPref();
        } else {
            this.getGraphList(graphlist);
        }
        PngExport t = new PngExport(basefilename, this.printList, null, null, this, dohtml, width, height);
        t.run();
        return 0;
    }

    public String calendarinfo() {
        if (this.statstart == null) {
            return "";
        }
        StringBuffer tmpstr = new StringBuffer("\nTime range information:\n");
        tmpstr.append("First data point: " + this.statstart + "\n");
        tmpstr.append("Last data point: " + this.statend + "\n");
        tmpstr.append("\nGraph range:\n");
        tmpstr.append("First data point: " + (Object)this.startofgraph + "\n");
        tmpstr.append("Last data point: " + (Object)this.endofgraph + "\n");
        return tmpstr.toString();
    }

    public void refreshdisktree() {
        this.parseAlternatediskname();
        Enumeration mydisklist = this.diskstreenode.children();
        while (mydisklist.hasMoreElements()) {
            DefaultMutableTreeNode curtree = (DefaultMutableTreeNode)mydisklist.nextElement();
            String tmp = this.Adiskname.get(curtree.toString());
            if (tmp == null) continue;
            curtree.setUserObject(tmp);
        }
    }

    public void selectrange(String _startdate, String _enddate) {
        int i;
        String tmp;
        if (_startdate != null) {
            for (i = 0; i < this.datefound.size(); ++i) {
                tmp = this.datefound.get(i).toString();
                if (tmp.indexOf(_startdate) < 0) continue;
                this.startofgraph = this.datefound.get(i);
                break;
            }
        }
        if (_enddate != null) {
            for (i = this.datefound.size() - 1; i > 0; --i) {
                tmp = this.datefound.get(i).toString();
                if (tmp.indexOf(_enddate) < 0) continue;
                this.endofgraph = this.datefound.get(i);
                break;
            }
        }
    }

    public void parseAlternatediskname() {
        String tmp = kSarConfig.readSpecial("ADISK:" + this.hostName);
        if (tmp == null) {
            return;
        }
        String[] tmp2 = tmp.split("!");
        for (int i = 0; i < tmp2.length; ++i) {
            String tmpstr = tmp2[i];
            String[] val = tmpstr.split("=");
            if (val.length != 2) continue;
            this.Adiskname.put(val[0], val[1]);
        }
    }

    public void resetInfo() {
        if (this.myUI != null) {
            this.myUI.setTitle("Empty");
            this.myUI.selecttimemenu.setEnabled(false);
            this.myUI.exportpdfmenu.setEnabled(false);
            this.myUI.exportjpgmenu.setEnabled(false);
            this.myUI.exportpngmenu.setEnabled(false);
            this.myUI.disknamemenu.setEnabled(false);
            this.myUI.menushowstackedmem.setEnabled(false);
            this.myUI.exportcsvmenu.setEnabled(false);
            this.myUI.obj2 = null;
            this.myUI.reset2tree();
            this.myUI.showtriggermenu.setSelected(this.showtrigger);
            this.myUI.chkbox_cpuused.setSelected(this.showstackedcpu);
            this.myUI.memusedbufadj.setSelected(this.showmemusedbuffersadjusted);
            this.myUI.redobutton.setEnabled(false);
        }
        this.myOS = null;
        this.sarType = 0;
        this.kernelVersion = null;
        this.sarDate = null;
        this.day = 0;
        this.month = 0;
        this.year = 0;
        this.haspidnode = false;
        this.pidSarList.clear();
        this.pidstreenode = new DefaultMutableTreeNode("Pid");
        this.DetectedBounds.clear();
        this.pdfList.clear();
        this.isparsing = false;
        this.othergraphlist.clear();
        this.disksSarList.clear();
        this.pdfList.clear();
        this.printList.clear();
        this.cpuSarList.clear();
        this.fileSarList.clear();
        this.msgSarList.clear();
        this.scallSarList.clear();
        this.cswchSarList.clear();
        this.ifaceSarList.clear();
        this.cpustreenode = new DefaultMutableTreeNode("CPU");
        this.hascpunode = false;
        this.diskstreenode = new DefaultMutableTreeNode("Disk");
        this.hasdisknode = false;
        this.ifacetreenode = new DefaultMutableTreeNode("Interface");
        this.hasifnode = false;
        this.filetreenode = new DefaultMutableTreeNode("File");
        this.hasfilenode = false;
        this.msgtreenode = new DefaultMutableTreeNode("Message & Semaphore");
        this.hasmsgnode = false;
        this.scalltreenode = new DefaultMutableTreeNode("Syscalls");
        this.hasscallnode = false;
        this.cswchtreenode = new DefaultMutableTreeNode("Context");
        this.hascswchnode = false;
        this.buffertreenode = new DefaultMutableTreeNode("Buffers");
        this.hasbuffernode = false;
        this.bufferSarList.clear();
        this.haspaging1node = false;
        this.paging1treenode = new DefaultMutableTreeNode("Paging1");
        this.paging1SarList.clear();
        this.haspaging2node = false;
        this.paging2treenode = new DefaultMutableTreeNode("Paging2");
        this.paging2SarList.clear();
        this.hasswapingnode = false;
        this.swapingtreenode = new DefaultMutableTreeNode("Swapping");
        this.swapingSarList.clear();
        this.hasttynode = false;
        this.ttytreenode = new DefaultMutableTreeNode("TTY");
        this.ttySarList.clear();
        this.haspsetnode = false;
        this.psettreenode = new DefaultMutableTreeNode("Pset");
        this.psetSarList.clear();
        this.hasnfsnode = false;
        this.nfstreenode = new DefaultMutableTreeNode("NFS");
        this.hasintrlistnode = false;
        this.intrtreenode = new DefaultMutableTreeNode("interrupt");
        this.intrSarlist.clear();
        this.hostName = null;
        this.sarType = 0;
        this.statstart = null;
        this.statend = null;
        this.datefound.clear();
        this.startofgraph = null;
        this.endofgraph = null;
        this.command_interrupted = false;
        this.isparsing = false;
        this.myOS = null;
        this.lastever = new Second(0, 0, 0, 1, 1, 1970);
        this.DetectedBounds.clear();
        this.solarispagesize = -1;
        this.othergraphlist.clear();
        this.command_interrupted = false;
        this.sarParsersolaris = null;
        this.sarParserlinux = null;
        this.sarParserAix = null;
        this.sarParserHpux = null;
        this.sarParserMac = null;
    }

    protected void finalize() throws Throwable {
        this.cleanup_temp();
        super.finalize();
    }

    public void cleanup_temp() {
        try {
            if (this.tmpfile != null) {
                this.tmpfile_out.close();
                this.tmpfile.delete();
            }
        }
        catch (IOException ioe) {
            // empty catch block
        }
    }

    public void make_temp() {
        try {
            if (this.tmpfile != null) {
                this.tmpfile_out.close();
                this.tmpfile.delete();
            }
            this.tmpfile = File.createTempFile("ksar", ".sartxt");
            this.tmpfile_out = new BufferedWriter(new FileWriter(this.tmpfile));
        }
        catch (IOException ioe) {
            // empty catch block
        }
        if (this.tmpfile != null) {
            this.tmpfile.deleteOnExit();
        }
    }

    public void parse(BufferedReader br) {
        block51 : {
            int parserreturn = 0;
            long num_lines = 0;
            this.make_temp();
            if (this.myUI != null) {
                this.changemenu(false);
            }
            try {
                String thisLine;
                this.tell_parsing(true);
                long start = System.currentTimeMillis();
                while ((thisLine = br.readLine()) != null && !this.command_interrupted) {
                    String[] dateSplit;
                    String tmpstr;
                    StringTokenizer matcher;
                    String[] dateSplit2;
                    ++num_lines;
                    if (this.tmpfile_out != null) {
                        this.tmpfile_out.write(thisLine + "\n");
                    }
                    if (thisLine.length() == 0 || (matcher = new StringTokenizer(thisLine)).countTokens() == 0) continue;
                    String first = matcher.nextToken();
                    if ("SunOS".equals(first)) {
                        this.sarType = 1;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("SunOS", "automatically");
                        }
                        this.hostName = matcher.nextToken();
                        this.myOS.setHostname(this.hostName);
                        this.osVersion = matcher.nextToken();
                        this.myOS.setOSversion(this.osVersion);
                        this.kernelVersion = matcher.nextToken();
                        this.myOS.setKernel(this.kernelVersion);
                        this.cpuType = matcher.nextToken();
                        this.myOS.setCpuType(this.cpuType);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit2 = this.sarDate.split("/");
                        if (dateSplit2.length == 3) {
                            this.day = Integer.parseInt(dateSplit2[1]);
                            this.month = Integer.parseInt(dateSplit2[0]);
                            this.year = Integer.parseInt(dateSplit2[2]);
                            if (this.year < 100) {
                                this.year += 2000;
                            }
                        }
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        this.setPageSize();
                        continue;
                    }
                    if ("Linux".equals(first)) {
                        this.sarType = 2;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("Linux", "automatically");
                        }
                        this.kernelVersion = matcher.nextToken();
                        this.myOS.setKernel(this.kernelVersion);
                        tmpstr = matcher.nextToken();
                        this.hostName = tmpstr.substring(1, tmpstr.length() - 1);
                        this.myOS.setHostname(this.hostName);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit = this.sarDate.split("/");
                        if (dateSplit.length == 3) {
                            this.day = Integer.parseInt(dateSplit[1]);
                            this.month = Integer.parseInt(dateSplit[0]);
                            this.year = Integer.parseInt(dateSplit[2]);
                            if (this.year < 100) {
                                this.year += 2000;
                            }
                        }
                        if ((dateSplit = this.sarDate.split("-")).length == 3) {
                            this.day = Integer.parseInt(dateSplit[2]);
                            this.month = Integer.parseInt(dateSplit[1]);
                            this.year = Integer.parseInt(dateSplit[0]);
                        }
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        this.solarispagesize = 0;
                        this.parseAlternatediskname();
                        continue;
                    }
                    if ("AIX".equals(first)) {
                        this.sarType = 3;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("AIX", "automatically");
                        }
                        this.hostName = matcher.nextToken();
                        this.myOS.setHostname(this.hostName);
                        tmpstr = matcher.nextToken();
                        this.osVersion = new String(matcher.nextToken() + "." + tmpstr);
                        this.myOS.setOSversion(this.osVersion);
                        tmpstr = matcher.nextToken();
                        this.myOS.setMacAddress(tmpstr);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit = this.sarDate.split("/");
                        if (dateSplit.length == 3) {
                            this.day = Integer.parseInt(dateSplit[1]);
                            this.month = Integer.parseInt(dateSplit[0]);
                            this.year = Integer.parseInt(dateSplit[2]);
                            if (this.year < 100) {
                                this.year += 2000;
                            }
                        }
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        this.parseAlternatediskname();
                        this.solarispagesize = 0;
                        continue;
                    }
                    if ("HP-UX".equals(first)) {
                        this.sarType = 4;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("HP-UX", "automatically");
                        }
                        this.hostName = matcher.nextToken();
                        this.myOS.setHostname(this.hostName);
                        this.osVersion = matcher.nextToken();
                        this.myOS.setOSversion(this.osVersion);
                        this.kernelVersion = matcher.nextToken();
                        this.myOS.setKernel(this.kernelVersion);
                        this.cpuType = matcher.nextToken();
                        this.myOS.setCpuType(this.cpuType);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit2 = this.sarDate.split("/");
                        if (dateSplit2.length == 3) {
                            this.day = Integer.parseInt(dateSplit2[1]);
                            this.month = Integer.parseInt(dateSplit2[0]);
                            this.year = Integer.parseInt(dateSplit2[2]);
                            if (this.year < 100) {
                                this.year += 2000;
                            }
                        }
                        this.parseAlternatediskname();
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        continue;
                    }
                    if ("Darwin".equals(first)) {
                        this.sarType = 5;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("Mac", "automatically");
                        }
                        this.hostName = matcher.nextToken();
                        this.myOS.setHostname(this.hostName);
                        this.osVersion = matcher.nextToken();
                        this.myOS.setOSversion(this.osVersion);
                        this.cpuType = matcher.nextToken();
                        this.myOS.setCpuType(this.cpuType);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit2 = this.sarDate.split("/");
                        if (dateSplit2.length == 3) {
                            this.day = Integer.parseInt(dateSplit2[1]);
                            this.month = Integer.parseInt(dateSplit2[0]);
                            this.year = Integer.parseInt(dateSplit2[2]);
                            if (this.year < 100) {
                                this.year += 2000;
                            }
                        }
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        this.parseAlternatediskname();
                        continue;
                    }
                    if ("Esar".equals(first)) {
                        this.sarType = 6;
                        if (this.myOS == null) {
                            this.myOS = new OSInfo("Esar SunOS", "automatically");
                        }
                        matcher.nextToken();
                        this.hostName = matcher.nextToken();
                        this.myOS.setHostname(this.hostName);
                        this.osVersion = matcher.nextToken();
                        this.myOS.setOSversion(this.osVersion);
                        this.kernelVersion = matcher.nextToken();
                        this.myOS.setKernel(this.kernelVersion);
                        this.cpuType = matcher.nextToken();
                        this.myOS.setCpuType(this.cpuType);
                        this.sarDate = matcher.nextToken();
                        this.myOS.setDate(this.sarDate);
                        dateSplit2 = this.sarDate.split("/");
                        if (dateSplit2.length != 3) continue;
                        this.day = Integer.parseInt(dateSplit2[1]);
                        this.month = Integer.parseInt(dateSplit2[0]);
                        this.year = Integer.parseInt(dateSplit2[2]);
                        if (this.year >= 100) continue;
                        this.year += 2000;
                        sarStartDate = new LocalDate(this.year, this.month, this.day);
                        continue;
                    }
                    if (first.equals("Average")) {
                        this.underaverage = 1;
                        continue;
                    }
                    if (thisLine.indexOf("unix restarts") >= 0 || thisLine.indexOf("LINUX RESTART") >= 0 || thisLine.indexOf(" unix restared") >= 0) {
                        this.underaverage = 0;
                        continue;
                    }
                    if (thisLine.indexOf("System Configuration") >= 0) continue;
                    if (thisLine.indexOf("State change") >= 0) {
                        this.underaverage = 0;
                        continue;
                    }
                    if (this.sarType == 0) {
                        if (this.myUI != null) break;
                        System.err.println("There was a problem while parsing stat");
                        System.exit(2);
                        break;
                    }
                    if (this.myUI != null && num_lines % 30 == 1 && !this.myUI.getTitle().equals(this.hostName + " : " + (Object)this.startofgraph + " -> " + (Object)this.endofgraph)) {
                        this.myUI.setTitle(this.hostName + " : " + (Object)this.startofgraph + " -> " + (Object)this.endofgraph);
                    }
                    if (this.sarType == 1) {
                        if (this.sarParsersolaris == null) {
                            this.sarParsersolaris = new net.atomique.ksar.Solaris.Parser(this);
                        }
                        parserreturn = this.sarParsersolaris.parse(thisLine, first, matcher);
                        continue;
                    }
                    if (this.sarType == 2) {
                        if (this.sarParserlinux == null) {
                            this.sarParserlinux = new Parser(this);
                        }
                        parserreturn = this.sarParserlinux.parse(thisLine, first, matcher);
                        continue;
                    }
                    if (this.sarType == 3) {
                        if (this.sarParserAix == null) {
                            this.sarParserAix = new net.atomique.ksar.AIX.Parser(this);
                        }
                        parserreturn = this.sarParserAix.parse(thisLine, first, matcher);
                        continue;
                    }
                    if (this.sarType == 4) {
                        if (this.sarParserHpux == null) {
                            this.sarParserHpux = new net.atomique.ksar.Hpux.Parser(this);
                        }
                        parserreturn = this.sarParserHpux.parse(thisLine, first, matcher);
                        continue;
                    }
                    if (this.sarType == 5) {
                        if (this.sarParserMac == null) {
                            this.sarParserMac = new net.atomique.ksar.Mac.Parser(this);
                        }
                        parserreturn = this.sarParserMac.parse(thisLine, first, matcher);
                        continue;
                    }
                    if (this.sarType != 6) continue;
                    if (this.sarParserEsar == null) {
                        this.sarParserEsar = new net.atomique.ksar.Esar.Parser(this);
                    }
                    parserreturn = this.sarParserEsar.parse(thisLine, first, matcher);
                }
                long elapsedTimeMillis = System.currentTimeMillis() - start;
                System.out.print("time to parse: " + elapsedTimeMillis + "ms ");
                System.out.print("number of line: " + num_lines + " ");
                System.out.println("line/msec: " + (float)(num_lines / elapsedTimeMillis));
            }
            catch (IOException ioe) {
                if (this.command_interrupted) break block51;
                System.err.println("ouch something bad has append");
            }
        }
        this.tell_parsing(false);
        if (this.sarType == 0) {
            if (this.myUI == null) {
                System.err.println("There was a problem while parsing stat");
                System.exit(2);
            } else {
                JOptionPane.showMessageDialog(this.myUI, "There was a problem while parsing stat", "Parser error", 0);
                this.resetInfo();
                this.myUI.setTitle("Empty");
            }
        } else {
            this.doclosetrigger();
            if (this.myUI != null) {
                this.myUI.home2tree();
                this.changemenu(true);
                this.myUI.setTitle(this.hostName + " : " + (Object)this.startofgraph + " -> " + (Object)this.endofgraph);
                if (this.solarispagesize == -1 && this.sarType == 0) {
                    JOptionPane.showMessageDialog(this.myUI, "Data import is finished\nDon't forget to set PageSize (unders Option Menu)", "Parser end", 1);
                } else {
                    JOptionPane.showMessageDialog(this.myUI, "Data import is finished", "Parser end", 1);
                }
            }
        }
    }

    private void tell_parsing(boolean val) {
        this.isparsing = val;
        if (this.myUI == null) {
            return;
        }
        if (val) {
            this.myUI.redobutton.setText("Stop");
            this.myUI.redobutton.setEnabled(true);
        } else {
            this.myUI.redobutton.setText("Redo");
            this.myUI.redobutton.setEnabled(true);
        }
    }

    public void changemenu(boolean val) {
        this.myUI.selecttimemenu.setEnabled(val);
        this.myUI.exportpdfmenu.setEnabled(val);
        this.myUI.exportjpgmenu.setEnabled(val);
        this.myUI.exportpngmenu.setEnabled(val);
        this.myUI.disknamemenu.setEnabled(val);
        this.myUI.menushowstackedmem.setEnabled(val);
        this.myUI.chkbox_stackintr.setEnabled(val);
        this.myUI.exportcsvmenu.setEnabled(val);
        this.myUI.redobutton.setEnabled(val);
        this.myUI.addtoauto.setEnabled(val);
        this.myUI.exporttxtmenu.setEnabled(val);
        this.command_interrupted = false;
    }

    private void doclosetrigger() {
        for (String key : this.pdfList.keySet()) {
            AllGraph value = this.pdfList.get(key);
            value.doclosetrigger();
        }
    }

    public void addGUI(String title) {
        this.myUI = new kSarUI(this);
        this.myUI.setTitle(title);
        this.myUI.toFront();
        this.myUI.setVisible(true);
        this.mydesktop.desktopPane.add(this.myUI);
        try {
            int num = this.mydesktop.desktopPane.getAllFrames().length;
            if (num != 1) {
                this.myUI.reshape(5 * num, 5 * num, 800, 600);
            } else {
                this.myUI.reshape(0, 0, 800, 600);
            }
            this.myUI.setSelected(true);
        }
        catch (PropertyVetoException vetoe) {
            // empty catch block
        }
    }

    public void add2tree(DefaultMutableTreeNode parent, DefaultMutableTreeNode newNode) {
        if (this.myUI != null) {
            this.myUI.add2tree(parent, newNode);
        }
    }

    public void remove2tree(DefaultMutableTreeNode oldNode) {
        if (this.myUI != null) {
            this.myUI.remove2tree(oldNode);
        }
    }

    public int showGraphName() {
        for (String key : this.pdfList.keySet()) {
            AllGraph value = this.pdfList.get(key);
        }
        return 0;
    }
}

