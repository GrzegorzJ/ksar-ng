/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class kbmiscSar
extends AllGraph {
    private TimeSeries t_shrd;
    private TimeSeries t_buff;
    private TimeSeries t_cach;
    private TimeSeries t_used_bufferadj;
    private String miscOpt = new String("");

    public kbmiscSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Memory Misc");
        this.t_shrd = new TimeSeries((Comparable)((Object)"memshrd"), (Class)Second.class);
        this.mysar.dispo.put("Memory shared", this.t_shrd);
        this.t_buff = new TimeSeries((Comparable)((Object)"buffers"), (Class)Second.class);
        this.mysar.dispo.put("Memory buffers", this.t_buff);
        this.t_cach = new TimeSeries((Comparable)((Object)"cached"), (Class)Second.class);
        this.mysar.dispo.put("Memory cached", this.t_cach);
        this.t_used_bufferadj = new TimeSeries((Comparable)((Object)"used(buffer adjusted)"), (Class)Second.class);
        this.mysar.dispo.put("Memory used (buffer adjusted)", this.t_used_bufferadj);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_shrd.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_buff.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_cach.add((RegularTimePeriod)now, val3.doubleValue() * 1024.0, this.do_notify());
        this.t_used_bufferadj.add((RegularTimePeriod)now, val4.doubleValue() * 1024.0, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        this.t_shrd.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_buff.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_cach.add((RegularTimePeriod)now, val3.doubleValue() * 1024.0, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2) {
        this.t_buff.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_cach.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        ++this.number_of_sample;
    }

    public void addused_bufferadj(Second now, Float v) {
        this.t_used_bufferadj.add((RegularTimePeriod)now, v.doubleValue() * 1024.0);
    }

    public XYDataset createbuff() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_buff);
        return graphcollection;
    }

    public XYDataset createcach() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_cach);
        return graphcollection;
    }

    public XYDataset createshrd() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_shrd);
        return graphcollection;
    }

    public XYDataset createused_bufferadj() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_used_bufferadj);
        return graphcollection;
    }

    public void setmiscOpt(String s) {
        this.miscOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXKBMISC", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset buffset = this.createbuff();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        NumberAxis numberaxis = new NumberAxis("buffers");
        IEEE1541Number decimalformat = new IEEE1541Number(1);
        numberaxis.setNumberFormatOverride((NumberFormat)decimalformat);
        XYPlot subplot1 = new XYPlot(buffset, null, (ValueAxis)numberaxis, (XYItemRenderer)minichart1);
        XYDataset cachset = this.createcach();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis1 = new NumberAxis("cached");
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat);
        XYPlot subplot2 = new XYPlot(cachset, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart2);
        XYPlot subplot3 = null;
        if (this.miscOpt.equals("kbmemshrd")) {
            XYDataset shrdset = this.createshrd();
            StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
            minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
            minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            NumberAxis numberaxis2 = new NumberAxis("memshrd");
            numberaxis2.setNumberFormatOverride((NumberFormat)decimalformat);
            subplot3 = new XYPlot(shrdset, null, (ValueAxis)numberaxis2, (XYItemRenderer)minichart3);
        }
        XYDataset memused_bufferadj = this.createused_bufferadj();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color4);
        NumberAxis numberaxis4 = new NumberAxis("memused (buffer adjusted)");
        numberaxis4.setNumberFormatOverride((NumberFormat)decimalformat);
        XYPlot subplot4 = new XYPlot(memused_bufferadj, null, (ValueAxis)numberaxis4, (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        if (this.miscOpt.equals("kbswpcad")) {
            plot.add(subplot3, 1);
        }
        if (this.mysar.showmemusedbuffersadjusted) {
            plot.add(subplot4, 1);
        }
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            if (this.miscOpt.equals("kbswpcad")) {
                subplot3.setBackgroundPaint(null);
            }
        }
        return mychart;
    }
}

