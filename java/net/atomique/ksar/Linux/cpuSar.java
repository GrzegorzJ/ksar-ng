/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class cpuSar
extends AllGraph {
    private Trigger cpuidletrigger;
    private Trigger cpusystemtrigger;
    private Trigger cpuwiotrigger;
    private Trigger cpuusrtrigger;
    private TimeTableXYDataset stacked_used;
    private TimeSeries t_usr;
    private TimeSeries t_sys;
    private TimeSeries t_wio;
    private TimeSeries t_idle;
    private TimeSeries t_steal;
    private TimeSeries t_nice;
    private TimeSeries t_guest;
    private TimeSeries t_irq;
    private TimeSeries t_soft;
    private String cpuOpt = new String("");
    private String cpuName = new String("");

    public cpuSar(kSar hissar, String cpuID) {
        super(hissar);
        this.Title = new String("CPU " + cpuID);
        this.cpuName = cpuID;
        this.t_usr = new TimeSeries((Comparable)((Object)"User"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " User", this.t_usr);
        this.t_sys = new TimeSeries((Comparable)((Object)"System"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " System", this.t_sys);
        this.t_wio = new TimeSeries((Comparable)((Object)"Waiting I/O"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Wait I/O", this.t_wio);
        this.t_idle = new TimeSeries((Comparable)((Object)"Idle"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Idle", this.t_idle);
        this.t_nice = new TimeSeries((Comparable)((Object)"Nice"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Nice", this.t_nice);
        this.t_steal = new TimeSeries((Comparable)((Object)"Steal"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Steal", this.t_steal);
        this.t_irq = new TimeSeries((Comparable)((Object)"%irq"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %irq", this.t_irq);
        this.t_soft = new TimeSeries((Comparable)((Object)"%soft"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %soft", this.t_soft);
        this.t_guest = new TimeSeries((Comparable)((Object)"%guest"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %guest", this.t_guest);
        this.stacked_used = new TimeTableXYDataset();
        this.cpuidletrigger = new Trigger(this.mysar, this, "idle", this.t_idle, "down");
        this.cpusystemtrigger = new Trigger(this.mysar, this, "system", this.t_sys, "up");
        this.cpuwiotrigger = new Trigger(this.mysar, this, "wio", this.t_wio, "up");
        this.cpuusrtrigger = new Trigger(this.mysar, this, "usr", this.t_usr, "up");
        this.cpuidletrigger.setTriggerValue(kSarConfig.linuxcpuidletrigger);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.linuxcpusystemtrigger);
        this.cpuwiotrigger.setTriggerValue(kSarConfig.linuxcpuwiotrigger);
        this.cpuusrtrigger.setTriggerValue(kSarConfig.linuxcpuusrtrigger);
    }

    public void doclosetrigger() {
        this.cpuidletrigger.doclose();
        this.cpusystemtrigger.doclose();
        this.cpuwiotrigger.doclose();
        this.cpuusrtrigger.doclose();
    }

    public void setcpuOpt(String s) {
        this.cpuOpt = s;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_usr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_idle.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_nice.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.cpuidletrigger.doMarker(now, val4);
        this.cpusystemtrigger.doMarker(now, val3);
        this.cpuusrtrigger.doMarker(now, val1);
        this.stacked_used.add((TimePeriod)now, (double)val1.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)val3.floatValue(), "System");
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        this.t_usr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_wio.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_idle.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_nice.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.cpuidletrigger.doMarker(now, val5);
        this.cpusystemtrigger.doMarker(now, val3);
        this.cpuwiotrigger.doMarker(now, val4);
        this.cpuusrtrigger.doMarker(now, val1);
        this.stacked_used.add((TimePeriod)now, (double)val1.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)val3.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)val4.floatValue(), "Waiting I/O");
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6) {
        this.t_usr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_wio.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_idle.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_nice.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_steal.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.cpuidletrigger.doMarker(now, val6);
        this.cpusystemtrigger.doMarker(now, val3);
        this.cpuwiotrigger.doMarker(now, val4);
        this.cpuusrtrigger.doMarker(now, val1);
        this.stacked_used.add((TimePeriod)now, (double)val1.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)val3.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)val4.floatValue(), "Waiting I/O");
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9) {
        this.t_usr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_wio.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_idle.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        this.t_nice.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_steal.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_irq.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_soft.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_guest.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        this.cpuidletrigger.doMarker(now, val6);
        this.cpusystemtrigger.doMarker(now, val3);
        this.cpuwiotrigger.doMarker(now, val4);
        this.cpuusrtrigger.doMarker(now, val1);
        this.stacked_used.add((TimePeriod)now, (double)val1.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)val3.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)val4.floatValue(), "Waiting I/O");
        ++this.number_of_sample;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_usr);
        graphcollection.addSeries(this.t_sys);
        if (this.cpuOpt.equals("%iowait")) {
            graphcollection.addSeries(this.t_wio);
        }
        if (this.cpuOpt.equals("%steal")) {
            graphcollection.addSeries(this.t_wio);
            graphcollection.addSeries(this.t_steal);
        }
        return graphcollection;
    }

    public XYDataset createnice() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_nice);
        return graphcollection;
    }

    public XYDataset createidle() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_idle);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXCPU", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYPlot subplot1;
        XYDataset xydataset1 = this.createused();
        NumberAxis usedaxis = new NumberAxis("% used cpu");
        if (this.mysar.show100axiscpu) {
            usedaxis.setRange(0.0, 100.0);
        }
        if (this.mysar.showstackedcpu) {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
            renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
            renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
            renderer.setSeriesPaint(3, (Paint)kSarConfig.color4);
            subplot1 = new XYPlot((XYDataset)this.stacked_used, (ValueAxis)new DateAxis(null), (ValueAxis)usedaxis, (XYItemRenderer)renderer);
        } else {
            StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
            minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
            minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
            minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
            minichart1.setSeriesPaint(2, (Paint)kSarConfig.color4);
            subplot1 = new XYPlot(xydataset1, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        }
        XYDataset idleset = this.createidle();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(idleset, null, (ValueAxis)new NumberAxis("% idle"), (XYItemRenderer)minichart2);
        XYDataset niceset = this.createnice();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color6);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(niceset, null, (ValueAxis)new NumberAxis("% niced"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 2);
        plot.add(subplot3, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        this.cpuidletrigger.setTriggerValue(kSarConfig.linuxcpuidletrigger);
        this.cpuidletrigger.tagMarker(subplot2);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.linuxcpusystemtrigger);
        this.cpusystemtrigger.tagMarker(subplot1);
        if (this.cpuOpt.equals("%iowait") || this.cpuOpt.equals("%steal")) {
            this.cpuwiotrigger.setTriggerValue(kSarConfig.linuxcpuwiotrigger);
            this.cpuwiotrigger.tagMarker(subplot1);
        }
        this.cpuusrtrigger.setTriggerValue(kSarConfig.linuxcpuusrtrigger);
        this.cpuusrtrigger.tagMarker(subplot1);
        return mychart;
    }
}

