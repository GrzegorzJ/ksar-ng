/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class sockSar
extends AllGraph {
    private TimeSeries t_totsck;
    private TimeSeries t_tcpsck;
    private TimeSeries t_udpsck;
    private TimeSeries t_rawsck;
    private TimeSeries t_ipfrag;
    private String ioOpt = new String("");

    public sockSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Sockets");
        this.t_totsck = new TimeSeries((Comparable)((Object)"totsck"), (Class)Second.class);
        this.mysar.dispo.put("Total Socket", this.t_totsck);
        this.t_tcpsck = new TimeSeries((Comparable)((Object)"tcpsck"), (Class)Second.class);
        this.mysar.dispo.put("TCP socket", this.t_tcpsck);
        this.t_udpsck = new TimeSeries((Comparable)((Object)"udpsck"), (Class)Second.class);
        this.mysar.dispo.put("UDP socket", this.t_udpsck);
        this.t_rawsck = new TimeSeries((Comparable)((Object)"rawsck"), (Class)Second.class);
        this.mysar.dispo.put("RAW socket", this.t_rawsck);
        this.t_ipfrag = new TimeSeries((Comparable)((Object)"ip-frag"), (Class)Second.class);
        this.mysar.dispo.put("IP Frag", this.t_ipfrag);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        this.t_totsck.add((RegularTimePeriod)now, (Number)val1);
        this.t_tcpsck.add((RegularTimePeriod)now, (Number)val2);
        this.t_udpsck.add((RegularTimePeriod)now, (Number)val3);
        this.t_rawsck.add((RegularTimePeriod)now, (Number)val4);
        this.t_ipfrag.add((RegularTimePeriod)now, (Number)val5);
        ++this.number_of_sample;
    }

    public XYDataset createtotsck() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_totsck);
        return graphcollection;
    }

    public XYDataset createsck() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_tcpsck);
        graphcollection.addSeries(this.t_udpsck);
        graphcollection.addSeries(this.t_rawsck);
        return graphcollection;
    }

    public XYDataset createipfrag() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_ipfrag);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXSOCK", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        CombinedDomainXYPlot plot = null;
        XYDataset xydataset1 = this.createtotsck();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("totsck"), (XYItemRenderer)minichart1);
        XYDataset sck = this.createsck();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(2, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(sck, null, (ValueAxis)new NumberAxis("udp/tcp/raw"), (XYItemRenderer)minichart2);
        XYDataset ipfrag = this.createipfrag();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(ipfrag, null, (ValueAxis)new NumberAxis("ip-frag"), (XYItemRenderer)minichart3);
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot3, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

