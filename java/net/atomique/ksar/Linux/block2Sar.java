/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class block2Sar
extends AllGraph {
    private TimeSeries t_avgrq;
    private TimeSeries t_avgqu;
    private TimeSeries t_await;
    private TimeSeries t_svctm;
    private TimeSeries t_util;
    private String blockName;
    private final diskName optdisk;
    private String blockOpt = new String("");

    public block2Sar(kSar hissar, String s1, diskName diskopt) {
        super(hissar);
        this.Title = new String("Block Wait");
        this.blockName = s1;
        this.optdisk = diskopt;
        this.datain = 0;
        this.t_avgrq = new TimeSeries((Comparable)((Object)"avgrq-sz"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " avgrq-sz", this.t_avgrq);
        this.t_avgqu = new TimeSeries((Comparable)((Object)"avgqu-sz"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " avgqu-sz", this.t_avgqu);
        this.t_await = new TimeSeries((Comparable)((Object)"await"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " await", this.t_await);
        this.t_svctm = new TimeSeries((Comparable)((Object)"svctm"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " svctm", this.t_svctm);
        this.t_util = new TimeSeries((Comparable)((Object)"util%"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " util%", this.t_util);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        Float zerof = new Float(0.0f);
        if (!(val1.equals(zerof) && val2.equals(zerof) && val3.equals(zerof) && val4.equals(zerof) && val5.equals(zerof) || this.datain != 0)) {
            this.datain = 1;
        }
        this.t_avgrq.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_avgqu.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_await.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_svctm.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_util.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        ++this.number_of_sample;
    }

    public void setioOpt(String s) {
        this.blockOpt = s;
    }

    public XYDataset createavgrq() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_avgrq);
        return graphcollection;
    }

    public XYDataset createavgqu() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_avgqu);
        return graphcollection;
    }

    public XYDataset createawait() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_await);
        return graphcollection;
    }

    public XYDataset createutil() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_util);
        return graphcollection;
    }

    public XYDataset createsvctm() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_svctm);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXBLOCK2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.blockName;
    }

    public String getGraphTitle() {
        return this.Title + " " + this.optdisk.showTitle() + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset avgrqset = this.createavgrq();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(avgrqset, null, (ValueAxis)new NumberAxis("avgrq-sz"), (XYItemRenderer)minichart1);
        XYDataset avgquset = this.createavgqu();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(avgquset, null, (ValueAxis)new NumberAxis("avgqu-sz"), (XYItemRenderer)minichart2);
        XYDataset awaitset = this.createawait();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(awaitset, null, (ValueAxis)new NumberAxis("await"), (XYItemRenderer)minichart3);
        XYDataset svctmset = this.createsvctm();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(svctmset, null, (ValueAxis)new NumberAxis("svctm"), (XYItemRenderer)minichart4);
        XYDataset utilset = this.createutil();
        StandardXYItemRenderer minichart5 = new StandardXYItemRenderer();
        minichart5.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart5.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot5 = new XYPlot(utilset, null, (ValueAxis)new NumberAxis("util%"), (XYItemRenderer)minichart5);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.add(subplot5, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
            subplot5.setBackgroundPaint(null);
        }
        return mychart;
    }
}

