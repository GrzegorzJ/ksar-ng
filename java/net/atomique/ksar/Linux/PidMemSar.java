/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class PidMemSar
extends AllGraph {
    private TimeSeries t_RSS;
    private TimeSeries t_VSZ;
    private TimeSeries t_percent;
    private TimeSeries t_minflt;
    private TimeSeries t_majflt;
    private String mypid;

    public PidMemSar(kSar hissar, String prog, String pid) {
        super(hissar);
        this.mypid = pid;
        this.Title = new String("Mem PID " + pid + " " + prog);
        this.t_VSZ = new TimeSeries((Comparable)((Object)"VSZ Memory"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " VSZ memory", this.t_VSZ);
        this.t_RSS = new TimeSeries((Comparable)((Object)"RSS Memory"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " RSS memory", this.t_RSS);
        this.t_percent = new TimeSeries((Comparable)((Object)"% Memory"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " %Memory", this.t_percent);
        this.t_minflt = new TimeSeries((Comparable)((Object)"minflt/s"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " minflt/s", this.t_minflt);
        this.t_majflt = new TimeSeries((Comparable)((Object)"majflt/s"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " majflt/s", this.t_majflt);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        this.t_minflt.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_majflt.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_VSZ.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_RSS.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_percent.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createflt() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_minflt);
        graphcollection.addSeries(this.t_majflt);
        return graphcollection;
    }

    public XYDataset createVSZ() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_VSZ);
        return graphcollection;
    }

    public XYDataset createRSS() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_RSS);
        return graphcollection;
    }

    public XYDataset createpct() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_percent);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXPIDMEM", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getMypid() {
        return this.mypid;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createflt();
        NumberAxis usedaxis = new NumberAxis("min/maj flt/s");
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        XYDataset idleset = this.createVSZ();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(idleset, null, (ValueAxis)new NumberAxis("VSZ"), (XYItemRenderer)minichart2);
        XYDataset niceset = this.createRSS();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(niceset, null, (ValueAxis)new NumberAxis("RSS"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 2);
        plot.add(subplot3, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

