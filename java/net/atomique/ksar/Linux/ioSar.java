/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ioSar
extends AllGraph {
    private TimeSeries t_tps;
    private TimeSeries t_rtps;
    private TimeSeries t_wtps;
    private TimeSeries t_bread;
    private TimeSeries t_bwrit;
    private TimeSeries t_sect;
    private String ioOpt = new String("");

    public ioSar(kSar hissar) {
        super(hissar);
        this.Title = new String("I/O");
        this.t_tps = new TimeSeries((Comparable)((Object)"Transfer/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Transfert/s", this.t_tps);
        this.t_rtps = new TimeSeries((Comparable)((Object)"Read/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Read/s", this.t_rtps);
        this.t_wtps = new TimeSeries((Comparable)((Object)"Write/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Write/s", this.t_wtps);
        this.t_bread = new TimeSeries((Comparable)((Object)"Block read/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Block read/s", this.t_bread);
        this.t_bwrit = new TimeSeries((Comparable)((Object)"Block write/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Block write/s", this.t_bwrit);
        this.t_sect = new TimeSeries((Comparable)((Object)"Sect/s"), (Class)Second.class);
        this.mysar.dispo.put("IO Sect/s", this.t_sect);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        this.t_tps.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_rtps.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_wtps.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_bread.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_bwrit.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2) {
        this.t_tps.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sect.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        ++this.number_of_sample;
    }

    public void setioOpt(String s) {
        this.ioOpt = s;
    }

    public XYDataset createtps() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_tps);
        return graphcollection;
    }

    public XYDataset createsect() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_sect);
        return graphcollection;
    }

    public XYDataset createrws() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_wtps);
        graphcollection.addSeries(this.t_rtps);
        return graphcollection;
    }

    public XYDataset createblock() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_bread);
        graphcollection.addSeries(this.t_bwrit);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXIO", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        CombinedDomainXYPlot plot = null;
        XYDataset xydataset1 = this.createtps();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("transfer/s"), (XYItemRenderer)minichart1);
        XYDataset rws = this.createrws();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(rws, null, (ValueAxis)new NumberAxis("Read/Write /s"), (XYItemRenderer)minichart2);
        XYDataset blockset = this.createblock();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color5);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(blockset, null, (ValueAxis)new NumberAxis("Blocks Read/Write"), (XYItemRenderer)minichart3);
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot3, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

