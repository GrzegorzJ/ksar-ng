/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class pgpSar
extends AllGraph {
    private TimeSeries t_pgpgin;
    private TimeSeries t_pgpgout;
    private TimeSeries t_fault;
    private TimeSeries t_majflt;
    private TimeSeries t_activepg;
    private TimeSeries t_inadtypg;
    private TimeSeries t_inaclnpg;
    private TimeSeries t_inatarpg;
    private TimeSeries t_pgfree;
    private TimeSeries t_pgscank;
    private TimeSeries t_pgscand;
    private TimeSeries t_pgsteal;
    private TimeSeries t_vmeff;
    private String pgpOpt = new String("");

    public pgpSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Paging Activity");
        this.t_pgpgin = new TimeSeries((Comparable)((Object)"pgpgin/s"), (Class)Second.class);
        this.mysar.dispo.put("pgpgin/s", this.t_pgpgin);
        this.t_pgpgout = new TimeSeries((Comparable)((Object)"pgpgout/s"), (Class)Second.class);
        this.mysar.dispo.put("pgpgout/s", this.t_pgpgout);
        this.t_fault = new TimeSeries((Comparable)((Object)"fault/s"), (Class)Second.class);
        this.mysar.dispo.put("fault/s", this.t_fault);
        this.t_majflt = new TimeSeries((Comparable)((Object)"majflt/s"), (Class)Second.class);
        this.mysar.dispo.put("majflt/s", this.t_majflt);
        this.t_activepg = new TimeSeries((Comparable)((Object)"activepg"), (Class)Second.class);
        this.mysar.dispo.put("activepg", this.t_activepg);
        this.t_inadtypg = new TimeSeries((Comparable)((Object)"inadtypg"), (Class)Second.class);
        this.mysar.dispo.put("inadtypg", this.t_inadtypg);
        this.t_inaclnpg = new TimeSeries((Comparable)((Object)"inaclnpg"), (Class)Second.class);
        this.mysar.dispo.put("inaclnpg", this.t_inaclnpg);
        this.t_inatarpg = new TimeSeries((Comparable)((Object)"inatargp"), (Class)Second.class);
        this.mysar.dispo.put("inatarpg", this.t_inatarpg);
        this.t_pgfree = new TimeSeries((Comparable)((Object)"pgfree/s"), (Class)Second.class);
        this.mysar.dispo.put("pgfree/s", this.t_pgfree);
        this.t_pgscank = new TimeSeries((Comparable)((Object)"pgscank/s"), (Class)Second.class);
        this.mysar.dispo.put("pgscank/s", this.t_pgscank);
        this.t_pgscand = new TimeSeries((Comparable)((Object)"pgscand/s"), (Class)Second.class);
        this.mysar.dispo.put("pgscand/s", this.t_pgscand);
        this.t_pgsteal = new TimeSeries((Comparable)((Object)"pgsteal/s"), (Class)Second.class);
        this.mysar.dispo.put("pgsteal/s", this.t_pgsteal);
        this.t_vmeff = new TimeSeries((Comparable)((Object)"%vmeff"), (Class)Second.class);
        this.mysar.dispo.put("%vmeff", this.t_vmeff);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_pgpgin.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_pgpgout.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_fault.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_majflt.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6) {
        this.t_pgpgin.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_pgpgout.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_activepg.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_inadtypg.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_inaclnpg.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_inatarpg.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9) {
        this.t_pgpgin.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_pgpgout.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_fault.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_majflt.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_pgfree.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_pgscank.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_pgscand.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_pgsteal.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_vmeff.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createpgpg() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_pgpgin);
        graphcollection.addSeries(this.t_pgpgout);
        return graphcollection;
    }

    public XYDataset createfault() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_fault);
        graphcollection.addSeries(this.t_majflt);
        return graphcollection;
    }

    public XYDataset createactive() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_activepg);
        return graphcollection;
    }

    public XYDataset createinact() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_inadtypg);
        graphcollection.addSeries(this.t_inaclnpg);
        graphcollection.addSeries(this.t_inatarpg);
        return graphcollection;
    }

    public void setpgpOpt(String s) {
        this.pgpOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXPGP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart2;
        CombinedDomainXYPlot plot = null;
        XYDataset pgpg = this.createpgpg();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(pgpg, null, (ValueAxis)new NumberAxis("pgpgin/pgpgout /s"), (XYItemRenderer)minichart1);
        XYPlot subplot2 = null;
        XYPlot subplot3 = null;
        if (!this.pgpOpt.equals("activepg")) {
            XYDataset faultset = this.createfault();
            minichart2 = new StandardXYItemRenderer();
            minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
            minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
            minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot2 = new XYPlot(faultset, null, (ValueAxis)new NumberAxis("fault/majflt /s"), (XYItemRenderer)minichart2);
        }
        if (this.pgpOpt.equals("activepg")) {
            XYDataset actset = this.createactive();
            minichart2 = new StandardXYItemRenderer();
            minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
            minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot2 = new XYPlot(actset, null, (ValueAxis)new NumberAxis("activepg"), (XYItemRenderer)minichart2);
            XYDataset inactset = this.createinact();
            StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
            minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
            minichart3.setSeriesPaint(1, (Paint)kSarConfig.color5);
            minichart3.setSeriesPaint(2, (Paint)kSarConfig.color6);
            minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot3 = new XYPlot(inactset, null, (ValueAxis)new NumberAxis("inadtypg/inaclnpg/inatarpg"), (XYItemRenderer)minichart3);
        }
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        if (this.pgpOpt.equals("activepg")) {
            plot.add(subplot3, 1);
        }
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        return mychart;
    }
}

