/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class PidCpuSar
extends AllGraph {
    private String mypid;
    private TimeSeries t_usr;
    private TimeSeries t_sys;
    private TimeSeries t_percent;

    public PidCpuSar(kSar hissar, String prog, String pid) {
        super(hissar);
        this.mypid = pid;
        this.Title = new String("Cpu PID " + pid + " " + prog);
        this.t_usr = new TimeSeries((Comparable)((Object)"%User"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " %User", this.t_usr);
        this.t_sys = new TimeSeries((Comparable)((Object)"%System"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " %System", this.t_sys);
        this.t_percent = new TimeSeries((Comparable)((Object)"%CPU"), (Class)Second.class);
        this.mysar.dispo.put("PID " + pid + " " + prog + " %CPU", this.t_percent);
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        this.t_usr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_percent.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_percent);
        return graphcollection;
    }

    public XYDataset createUser() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_usr);
        return graphcollection;
    }

    public XYDataset createSystem() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_sys);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXPIDCPU", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getMypid() {
        return this.mypid;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createused();
        NumberAxis usedaxis = new NumberAxis("% used cpu");
        if (this.mysar.show100axiscpu) {
            usedaxis.setRange(0.0, 100.0);
        }
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        XYDataset idleset = this.createUser();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(idleset, null, (ValueAxis)new NumberAxis("% User"), (XYItemRenderer)minichart2);
        XYDataset niceset = this.createSystem();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(niceset, null, (ValueAxis)new NumberAxis("% System"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 2);
        plot.add(subplot3, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

