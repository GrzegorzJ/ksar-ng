/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.Linux;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

public class Parser {
    kSar mysar;
    Float val1;
    Float val2;
    Float val3;
    Float val4;
    Float val5;
    Float val6;
    Float val7;
    Float val8;
    Float val9;
    Float val10;
    Float val11;
    int heure = 0;
    int minute = 0;
    int seconde = 0;
    Second now = null;
    String lastHeader;
    String statType = "none";
    int stattypenum = 0;
    int entreetype;
    int firstwastime;
    cpuSar sarCPU2 = null;
    procSar sarPROC = null;
    cswchSar sarCSWCH = null;
    intrSar sarINTR = null;
    swapSar sarSWAP2 = null;
    ioSar sarIO = null;
    loadSar sarLOAD = null;
    pageSar sarPAGE = null;
    sockSar sarSOCK = null;
    kbmemSar sarKBMEM = null;
    kbswpSar sarKBSWP = null;
    kbmiscSar sarKBMISC = null;
    pgpSar sarPGP = null;
    nfsSar sarNFS = null;
    nfsdSar sarNFSD = null;
    ArrayList<String> intrlist = new ArrayList();

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String first, StringTokenizer matcher) {
        String[] sarTime;
        this.val1 = null;
        this.val2 = null;
        this.val3 = null;
        this.val4 = null;
        this.val5 = null;
        this.val5 = null;
        this.val6 = null;
        this.val7 = null;
        this.val8 = null;
        this.val9 = null;
        this.val10 = null;
        this.val11 = null;
        boolean headerFound = false;
        if (thisLine.indexOf("proc/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("cswch/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("%user") > 0 || thisLine.indexOf("%usr") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
            this.lastHeader = matcher.nextToken();
        }
        if (thisLine.indexOf("intr/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
            this.lastHeader = matcher.nextToken();
        }
        if (thisLine.indexOf("CPU") > 0 && (thisLine.indexOf("%user") <= 0 || thisLine.indexOf("%usr") <= 0)) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pswpin/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("bread/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("blks/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("sect") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("bufpg/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rxpck/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rxerr/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rd_sec/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pgpgout/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("kbmemused") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("kbswpused") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("file-sz/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("tcpsck") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("runq-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("dentunusd") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("retrans/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("scall/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("plist-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("TTY") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("PID") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (isUnsupportedHeader(thisLine, headerFound)) {
            headerFound = true;
            this.mysar.underaverage = 0;
            System.err.println("There is no support for " + thisLine + ". The data/statistics for it will be skipped");
        }

        if ((sarTime = first.split(":")).length != 3) {
            return 1;
        }

        this.heure = Integer.parseInt(sarTime[0]);
        this.minute = Integer.parseInt(sarTime[1]);
        this.seconde = Integer.parseInt(sarTime[2]);

        if(isNewMetric(thisLine, headerFound)) {
            resetDateToTheOriginalTime(thisLine);
        }

        updateCurrentMetricTime();

        if (this.mysar.statstart == null) {
            this.mysar.statstart = new String(this.now.toString());
            this.mysar.startofgraph = this.now;
        }
        if (!this.mysar.datefound.contains((Object)this.now)) {
            this.mysar.datefound.add(this.now);
        }
        if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
            this.mysar.lastever = this.now;
            this.mysar.statend = new String(this.mysar.lastever.toString());
            this.mysar.endofgraph = this.mysar.lastever;
        }
        this.lastHeader = new String(matcher.nextToken());
        if (headerFound) {
            if (this.lastHeader.equals(this.statType)) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("pgpgin/s") && thisLine.indexOf("activepg") > 0 && this.statType.equals("activepg")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("pgpgin/s") && thisLine.indexOf("pgscank/s") > 0 && this.statType.equals("pgscank/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("dentunusd") && thisLine.indexOf("%file-sz") > 0 && this.statType.equals("%file-sz")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("kbmemfree") && thisLine.indexOf("kbmemshrd") > 0 && this.statType.equals("kbmemshrd")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("kbmemfree") && thisLine.indexOf("%commit") > 0 && this.statType.equals("newkmem")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%user") && thisLine.indexOf("%iowait") > 0 && this.statType.equals("%iowait")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%user") && thisLine.indexOf("%guest") > 0 && this.statType.equals("%guest")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%ser") && thisLine.indexOf("%steal") > 0 && this.statType.equals("%steal")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("DEV") && thisLine.indexOf("rd_sec/s") > 0 && this.statType.equals("rd_sec/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("IFACE") && thisLine.indexOf("rxpck/s") > 0 && this.statType.equals("rxpck/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("IFACE") && thisLine.indexOf("rxerr/s") > 0 && this.statType.equals("rxerr/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("DEV") && thisLine.indexOf("avgrq-sz") > 0 && this.statType.equals("avgrq-sz")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("runq-sz") && thisLine.indexOf("ldavg-15") > 0 && this.statType.equals("ldavg-15")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("bufpg/s") && thisLine.indexOf("shmpg/s") > 0 && this.statType.equals("shmpg/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("PID") && thisLine.indexOf("RSS") > 0 && this.statType.equals("RSS")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("PID") && thisLine.indexOf("kB_ccwr") > 0 && this.statType.equals("kB_ccwr")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%user") && thisLine.indexOf("Command") > 0 && this.statType.equals("Command")) {
                headerFound = false;
                return 1;
            }
            this.statType = this.lastHeader;
            if (this.statType.equals("%user") && thisLine.indexOf("Command") < 0) {
                if (thisLine.indexOf("%iowait") > 0) {
                    this.statType = "%iowait";
                }
                if (thisLine.indexOf("%steal") > 0) {
                    this.statType = "%steal";
                }
                if (!this.mysar.hascpunode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.cpustreenode);
                    }
                    this.mysar.hascpunode = true;
                }
                return 1;
            }
            if (this.statType.equals("%usr") && thisLine.indexOf("Command") < 0) {
                if (!this.mysar.hascpunode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.cpustreenode);
                    }
                    this.mysar.hascpunode = true;
                }
                return 1;
            }
            if (thisLine.indexOf("RSS") >= 0) {
                this.statType = "RSS";
                if (!this.mysar.haspidnode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.pidstreenode);
                    }
                    this.mysar.haspidnode = true;
                }
                return 1;
            }
            if (thisLine.indexOf("kB_ccwr") >= 0) {
                this.statType = "kB_ccwr";
                if (!this.mysar.haspidnode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.pidstreenode);
                    }
                    this.mysar.haspidnode = true;
                }
                return 1;
            }
            if (this.statType.equals("%user") && thisLine.indexOf("Command") >= 0) {
                if (thisLine.indexOf("Command") >= 0) {
                    this.statType = "Command";
                }
                if (!this.mysar.haspidnode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.pidstreenode);
                    }
                    this.mysar.haspidnode = true;
                }
                return 1;
            }
            if (this.statType.equals("proc/s")) {
                if (this.sarPROC == null) {
                    this.sarPROC = new procSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPROC.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxprocSar", this.sarPROC);
                    this.sarPROC.setGraphLink("LinuxprocSar");
                }
                if (thisLine.indexOf("cswch/s") >= 0) {
                    if (this.sarCSWCH == null) {
                        this.sarCSWCH = new cswchSar(this.mysar);
                        if (this.mysar.myUI != null) {
                            this.sarCSWCH.addtotree(this.mysar.graphtree);
                        }
                        this.mysar.pdfList.put("LinuxcswchSar", this.sarCSWCH);
                        this.sarCSWCH.setGraphLink("LinuxcswchSar");
                    }
                    this.statType = "proccswch";
                    return 1;
                }
                this.statType = "proc/s";
                return 1;
            }
            if (this.statType.equals("cswch/s")) {
                if (this.sarCSWCH == null) {
                    this.sarCSWCH = new cswchSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarCSWCH.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxcswchSar", this.sarCSWCH);
                    this.sarCSWCH.setGraphLink("LinuxcswchSar");
                }
                return 1;
            }
            if (this.statType.equals("intr/s")) {
                if (this.sarINTR == null) {
                    this.sarINTR = new intrSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarINTR.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxintrSar", this.sarINTR);
                    this.sarINTR.setGraphLink("LinuxintrSar");
                }
                return 1;
            }
            if (this.statType.equals("CPU")) {
                String intrheader = null;
                try {
                    while ((intrheader = matcher.nextToken()) != null) {
                        String intrname = intrheader.replaceFirst("/s", "");
                        this.intrlist.add(intrname);
                        intrlistSar mysarintr = (intrlistSar)this.mysar.intrSarlist.get(intrname + "-intr");
                        if (mysarintr != null) continue;
                        mysarintr = new intrlistSar(this.mysar, intrname);
                        this.mysar.intrSarlist.put(intrname + "-intr", mysarintr);
                        this.mysar.pdfList.put(intrname + "-intr", mysarintr);
                        mysarintr.setGraphLink("intr-" + intrname);
                        if (this.mysar.myUI == null) continue;
                        if (!this.mysar.hasintrlistnode) {
                            this.mysar.hasintrlistnode = true;
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.intrtreenode);
                        }
                        mysarintr.addtotree(this.mysar.intrtreenode);
                    }
                }
                catch (NoSuchElementException ee) {
                    // empty catch block
                }
                return 1;
            }
            if (this.statType.equals("pswpin/s")) {
                if (this.sarSWAP2 == null) {
                    this.sarSWAP2 = new swapSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSWAP2.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxswapSar", this.sarSWAP2);
                    this.sarSWAP2.setGraphLink("LinuxswapSar");
                }
                return 1;
            }
            if (this.statType.equals("tps")) {
                if (this.sarIO == null) {
                    this.sarIO = new ioSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarIO.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxioSar", this.sarIO);
                    this.sarIO.setGraphLink("LinuxioSar");
                }
                return 1;
            }
            if (this.statType.equals("DEV")) {
                if (thisLine.indexOf("rd_sec/s") > 0) {
                    this.statType = "rd_sec/s";
                }
                if (thisLine.indexOf("avgrq-sz") > 0) {
                    this.statType = "avgrq-sz";
                }
                if (!this.mysar.hasdisknode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                    }
                    this.mysar.hasdisknode = true;
                }
                return 1;
            }
            if (this.statType.equals("IFACE")) {
                if (thisLine.indexOf("rxpck/s") > 0) {
                    this.statType = "rxpck/s";
                }
                if (thisLine.indexOf("rxerr/s") > 0) {
                    this.statType = "rxerr/s";
                }
                if (!this.mysar.hasifnode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.ifacetreenode);
                    }
                    this.mysar.hasifnode = true;
                }
                return 1;
            }
            if (this.statType.equals("runq-sz")) {
                if (this.sarLOAD == null) {
                    this.sarLOAD = new loadSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarLOAD.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxloadSar", this.sarLOAD);
                    this.sarLOAD.setGraphLink("LinuxloadSar");
                }
                if (thisLine.indexOf("ldavg-15") > 0) {
                    this.statType = "ldavg-15";
                    this.sarLOAD.setloadOpt("ldavg-15");
                }
                return 1;
            }
            if (this.statType.equals("frmpg/s")) {
                if (this.sarPAGE == null) {
                    this.sarPAGE = new pageSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPAGE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxpageSar", this.sarPAGE);
                    this.sarPAGE.setGraphLink("LinuxpageSar");
                }
                if (thisLine.indexOf("shmpg/s") > 0) {
                    this.statType = "shmpg/s";
                    this.sarPAGE.setpageOpt("shmpg/s");
                }
                return 1;
            }
            if (this.statType.equals("totsck")) {
                if (this.sarSOCK == null) {
                    this.sarSOCK = new sockSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSOCK.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxsockSar", this.sarSOCK);
                    this.sarSOCK.setGraphLink("LinuxsockSar");
                }
                return 1;
            }
            if (this.statType.equals("kbmemfree") && thisLine.indexOf("%commit") < 0) {
                if (this.sarKBMEM == null) {
                    this.sarKBMEM = new kbmemSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBMEM.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbmemSar", this.sarKBMEM);
                    this.sarKBMEM.setGraphLink("LinuxkbmemSar");
                }
                if (this.sarKBSWP == null) {
                    this.sarKBSWP = new kbswpSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBSWP.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbswpSar", this.sarKBSWP);
                    this.sarKBSWP.setGraphLink("LinuxkbswpSar");
                }
                if (this.sarKBMISC == null) {
                    this.sarKBMISC = new kbmiscSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBMISC.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbmiscSar", this.sarKBMISC);
                    this.sarKBMISC.setGraphLink("LinuxkbmiscSar");
                }
                if (thisLine.indexOf("kbmemshrd") > 0) {
                    this.sarKBSWP.setswpOpt("kbmemshrd");
                    this.sarKBMISC.setmiscOpt("kbmemshrd");
                    this.statType = "kbmemshrd";
                }
                return 1;
            }
            if (this.statType.equals("kbswpfree")) {
                if (this.sarKBSWP == null) {
                    this.sarKBSWP = new kbswpSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBSWP.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbswpSar", this.sarKBSWP);
                    this.sarKBSWP.setGraphLink("LinuxkbswpSar");
                }
                return 1;
            }
            if (this.statType.equals("kbmemfree") && thisLine.indexOf("%commit") >= 0) {
                if (this.sarKBMEM == null) {
                    this.sarKBMEM = new kbmemSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBMEM.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbmemSar", this.sarKBMEM);
                    this.sarKBMEM.setGraphLink("LinuxkbmemSar");
                }
                if (this.sarKBMISC == null) {
                    this.sarKBMISC = new kbmiscSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKBMISC.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxkbmiscSar", this.sarKBMISC);
                    this.sarKBMISC.setGraphLink("LinuxkbmiscSar");
                }
                this.statType = "newkmem";
                return 1;
            }
            if (this.statType.equals("pgpgin/s")) {
                if (this.sarPGP == null) {
                    this.sarPGP = new pgpSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPGP.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxpgpSar", this.sarPGP);
                    this.sarPGP.setGraphLink("LinuxpgpSar");
                }
                if (thisLine.indexOf("activepg") > 0) {
                    this.sarPGP.setpgpOpt("activepg");
                    this.statType = "activepg";
                }
                if (thisLine.indexOf("pgscank/s") > 0) {
                    this.sarPGP.setpgpOpt("pgscank/s");
                    this.statType = "pgscank/s";
                }
                return 1;
            }
            if (this.statType.equals("call/s")) {
                if (this.sarNFS == null) {
                    this.sarNFS = new nfsSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarNFS.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("LinuxnfsSar", this.sarNFS);
                    this.sarNFS.setGraphLink("LinuxnfsSar");
                }
                return 1;
            }
            if (this.statType.equals("scall/s") && this.sarNFSD == null) {
                this.sarNFSD = new nfsdSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarNFSD.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("LinuxnfsdSar", this.sarNFSD);
                this.sarNFSD.setGraphLink("LinuxnfsdSar");
            }
            if (this.statType.equals("dentunusd")) {
                if (thisLine.indexOf("%file-sz") > 0) {
                    this.statType = "%file-sz";
                }
                return 1;
            }
            if (this.statType.equals("TTY")) {
                return 1;
            }
            headerFound = false;
            return 1;
        }
        try {
            DefaultMutableTreeNode mycpu;
            diskName tmp;
            iface2Sar mysarif2;
            blockSar mysarblock;
            cpuSar mysarcpu;
            DefaultMutableTreeNode mypid;
            iface1Sar mysarif1;
            if (thisLine.indexOf("nan") > 0) {
                return 1;
            }
            if (this.statType.equals("CPU")) {
                String intrname = null;
                String cpu = this.lastHeader;
                int i = 0;
                try {
                    String intr;
                    while ((intr = matcher.nextToken()) != null) {
                        intrname = this.intrlist.get(i);
                        intrlistSar intrList = (intrlistSar)this.mysar.intrSarlist.get(intrname + "-intr");
                        if (intrList != null) {
                            intrList.add(this.now, "CPU " + cpu, new Float(intr));
                        }
                        ++i;
                    }
                }
                catch (NoSuchElementException ee) {
                    return 1;
                }
            }
            if (this.statType.equals("proc/s")) {
                this.val1 = new Float(this.lastHeader);
                this.sarPROC.add(this.now, this.val1);
                return 1;
            }
            if (this.statType.equals("proccswch")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarPROC.add(this.now, this.val1);
                this.sarCSWCH.add(this.now, this.val2);
                return 1;
            }
            if (this.statType.equals("cswch/s")) {
                this.val1 = new Float(this.lastHeader);
                this.sarCSWCH.add(this.now, this.val1);
                return 1;
            }
            if (this.statType.equals("Command")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                String prog = matcher.nextToken();
                PidCpuSar tmppidcpu = (PidCpuSar)this.mysar.pidSarList.get(this.lastHeader + "-pidcpu");
                if (tmppidcpu == null) {
                    tmppidcpu = new PidCpuSar(this.mysar, prog, this.lastHeader);
                    mypid = new DefaultMutableTreeNode("pid-" + this.lastHeader);
                    this.mysar.pidSarList.put(this.lastHeader + "-pidcpu", tmppidcpu);
                    this.mysar.pdfList.put(this.lastHeader + "-pidcpu", tmppidcpu);
                    tmppidcpu.setGraphLink(this.lastHeader + "-pidcpu");
                    if (this.mysar.myUI != null) {
                        tmppidcpu.addtotree(this.mysar.pidstreenode);
                    }
                }
                tmppidcpu.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("RSS")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                String prog = matcher.nextToken();
                PidMemSar tmppidmem = (PidMemSar)this.mysar.pidSarList.get(this.lastHeader + "-pidmem");
                if (tmppidmem == null) {
                    tmppidmem = new PidMemSar(this.mysar, prog, this.lastHeader);
                    mypid = new DefaultMutableTreeNode("pid-" + this.lastHeader);
                    this.mysar.pidSarList.put(this.lastHeader + "-pidmem", tmppidmem);
                    this.mysar.pdfList.put(this.lastHeader + "-pidmem", tmppidmem);
                    tmppidmem.setGraphLink(this.lastHeader + "-pidmem");
                    if (this.mysar.myUI != null) {
                        tmppidmem.addtotree(this.mysar.pidstreenode);
                    }
                }
                tmppidmem.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("kB_ccwr")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                String prog = matcher.nextToken();
                PidIOSar tmppidio = (PidIOSar)this.mysar.pidSarList.get(this.lastHeader + "-pidkb");
                if (tmppidio == null) {
                    tmppidio = new PidIOSar(this.mysar, prog, this.lastHeader);
                    mypid = new DefaultMutableTreeNode("pid-" + this.lastHeader + "/" + prog);
                    this.mysar.pidSarList.put(this.lastHeader + "-pidkb", tmppidio);
                    this.mysar.pdfList.put(this.lastHeader + "-pidkb", tmppidio);
                    tmppidio.setGraphLink(this.lastHeader + "-pidkb");
                    if (this.mysar.myUI != null) {
                        tmppidio.addtotree(this.mysar.pidstreenode);
                    }
                } else {
                    tmppidio = (PidIOSar)this.mysar.pidSarList.get(prog + "/" + this.lastHeader + "-pidkb");
                }
                tmppidio.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("%user")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarcpu = (cpuSar)this.mysar.cpuSarList.get(this.lastHeader + "-cpu");
                if (mysarcpu == null) {
                    mysarcpu = new cpuSar(this.mysar, this.lastHeader);
                    mysarcpu.setcpuOpt(this.statType);
                    mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                    this.mysar.cpuSarList.put(this.lastHeader + "-cpu", mysarcpu);
                    this.mysar.pdfList.put(this.lastHeader + "-cpu", mysarcpu);
                    mysarcpu.setGraphLink("cpu-" + this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarcpu.addtotree(this.mysar.cpustreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                mysarcpu.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("%iowait")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarcpu = (cpuSar)this.mysar.cpuSarList.get(this.lastHeader + "-cpu");
                if (mysarcpu == null) {
                    mysarcpu = new cpuSar(this.mysar, this.lastHeader);
                    mysarcpu.setcpuOpt(this.statType);
                    mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                    this.mysar.cpuSarList.put(this.lastHeader + "-cpu", mysarcpu);
                    this.mysar.pdfList.put(this.lastHeader + "-cpu", mysarcpu);
                    mysarcpu.setGraphLink("cpu-" + this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarcpu.addtotree(this.mysar.cpustreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                mysarcpu.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("%steal")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarcpu = (cpuSar)this.mysar.cpuSarList.get(this.lastHeader + "-cpu");
                if (mysarcpu == null) {
                    mysarcpu = new cpuSar(this.mysar, this.lastHeader);
                    mysarcpu.setcpuOpt(this.statType);
                    mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                    this.mysar.cpuSarList.put(this.lastHeader + "-cpu", mysarcpu);
                    this.mysar.pdfList.put(this.lastHeader + "-cpu", mysarcpu);
                    mysarcpu.setGraphLink("cpu-" + this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarcpu.addtotree(this.mysar.cpustreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mysarcpu.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("%usr")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarcpu = (cpuSar)this.mysar.cpuSarList.get(this.lastHeader + "-cpu");
                if (mysarcpu == null) {
                    mysarcpu = new cpuSar(this.mysar, this.lastHeader);
                    mysarcpu.setcpuOpt(this.statType);
                    mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                    this.mysar.cpuSarList.put(this.lastHeader + "-cpu", mysarcpu);
                    this.mysar.pdfList.put(this.lastHeader + "-cpu", mysarcpu);
                    mysarcpu.setGraphLink("cpu-" + this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarcpu.addtotree(this.mysar.cpustreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                mysarcpu.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("intr/s")) {
                if (this.lastHeader.equals("sum")) {
                    this.lastHeader = new String(matcher.nextToken());
                    this.val1 = new Float(this.lastHeader);
                    this.sarINTR.add(this.now, this.val1);
                }
                return 1;
            }
            if (this.statType.equals("pswpin/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(new String(matcher.nextToken()));
                this.sarSWAP2.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("tps")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarIO.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("avgrq-sz")) {
                block2Sar mysarblock2;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarblock = (blockSar)this.mysar.disksSarList.get(this.lastHeader + "-t1");
                if (mysarblock == null) {
                    diskName tmp2 = new diskName(this.lastHeader);
                    this.mysar.AlternateDiskName.put(this.lastHeader, tmp2);
                    tmp2.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                    mysarblock = new blockSar(this.mysar, this.lastHeader, tmp2);
                    mysarblock2 = new block2Sar(this.mysar, this.lastHeader, tmp2);
                    mysarblock.setioOpt("avgrq-sz");
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp2.showTitle());
                    if (this.mysar.myUI != null) {
                        mysarblock.addtotree(mydisk);
                        mysarblock2.addtotree(mydisk);
                    }
                    this.mysar.disksSarList.put(this.lastHeader + "-t1", mysarblock);
                    mysarblock.setGraphLink(this.lastHeader + "-t1");
                    this.mysar.disksSarList.put(this.lastHeader + "-t2", mysarblock2);
                    mysarblock2.setGraphLink(this.lastHeader + "-t2");
                    this.mysar.pdfList.put(this.lastHeader + "-t1", mysarblock);
                    this.mysar.pdfList.put(this.lastHeader + "-t2", mysarblock2);
                    this.mysar.diskstreenode.add(mydisk);
                } else {
                    mysarblock2 = (block2Sar)this.mysar.disksSarList.get(this.lastHeader + "-t2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                mysarblock.add(this.now, this.val1, this.val2, this.val3);
                mysarblock2.add(this.now, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if (this.statType.equals("rd_sec/s")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarblock = (blockSar)this.mysar.disksSarList.get(this.lastHeader + "-t1");
                if (mysarblock == null) {
                    tmp = new diskName(this.lastHeader);
                    this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                    mysarblock = new blockSar(this.mysar, this.lastHeader, tmp);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    this.mysar.disksSarList.put(this.lastHeader + "-t1", mysarblock);
                    this.mysar.pdfList.put(this.lastHeader + "-t1", mysarblock);
                    mysarblock.setGraphLink(this.lastHeader + "-t1");
                    mysarblock.setioOpt("rd_sec/s");
                    if (this.mysar.myUI != null) {
                        mysarblock.addtotree(mydisk);
                        this.mysar.diskstreenode.add(mydisk);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                mysarblock.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("DEV")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarblock = (blockSar)this.mysar.disksSarList.get(this.lastHeader + "-t1");
                if (mysarblock == null) {
                    tmp = new diskName(this.lastHeader);
                    this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                    mysarblock = new blockSar(this.mysar, this.lastHeader, tmp);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    this.mysar.disksSarList.put(this.lastHeader + "-t1", mysarblock);
                    mysarblock.setGraphLink(this.lastHeader + "-t1");
                    this.mysar.pdfList.put(this.lastHeader + "-t1", mysarblock);
                    if (this.mysar.myUI != null) {
                        mysarblock.addtotree(mydisk);
                        this.mysar.diskstreenode.add(mydisk);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                mysarblock.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("ldavg-15")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarLOAD.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("runq-sz")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarLOAD.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("frmpg/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarPAGE.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("shmpg/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarPAGE.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("totsck")) {
                    this.val1 = new Float(this.lastHeader);
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.val5 = new Float(matcher.nextToken());
                    this.sarSOCK.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("newkmem")) {
                this.val1 = new Float(this.lastHeader);
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarKBMEM.add(this.now, this.val1, this.val2, this.val3);
                this.sarKBMISC.add(this.now, this.val4, this.val5);
                this.sarKBMISC.addused_bufferadj(this.now, new Float(this.val2.floatValue() - this.val4.floatValue() - this.val5.floatValue()));
                return 1;
            }
            if (this.statType.equals("kbmemfree")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.sarKBMEM.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val7);
                this.sarKBMISC.add(this.now, this.val4, this.val5);
                this.sarKBMISC.addused_bufferadj(this.now, new Float(this.val2.floatValue() - this.val4.floatValue() - this.val5.floatValue()));
                this.sarKBSWP.add(this.now, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("kbmemshrd")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.sarKBMEM.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val7);
                this.sarKBMISC.add(this.now, this.val4, this.val5, this.val6);
                this.sarKBMISC.addused_bufferadj(this.now, new Float(this.val2.floatValue() - this.val4.floatValue() - this.val5.floatValue()));
                this.sarKBSWP.add(this.now, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("kbswpfree")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarKBSWP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("pgpgin/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarPGP.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("activepg")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarPGP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("pgscank/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.sarPGP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("call/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarNFS.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("scall/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.val11 = new Float(matcher.nextToken());
                this.sarNFSD.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10, this.val11);
                return 1;
            }
            if (this.statType.equals("rxpck/s")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if1");
                if (mysarif1 == null) {
                    mysarif1 = new iface1Sar(this.mysar, this.lastHeader);
                    mysarif2 = new iface2Sar(this.mysar, this.lastHeader);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarif1.addtotree(myif);
                        mysarif2.addtotree(myif);
                    }
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if1", mysarif1);
                    mysarif1.setGraphLink(this.lastHeader + "-if1");
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if2", mysarif2);
                    mysarif2.setGraphLink(this.lastHeader + "-if2");
                    this.mysar.pdfList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.pdfList.put(this.lastHeader + "-if2", mysarif2);
                    this.mysar.ifacetreenode.add(myif);
                } else {
                    mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                mysarif1.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("rxerr/s")) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if1");
                if (mysarif1 == null) {
                    mysarif1 = new iface1Sar(this.mysar, this.lastHeader);
                    mysarif2 = new iface2Sar(this.mysar, this.lastHeader);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(this.lastHeader);
                    if (this.mysar.myUI != null) {
                        mysarif1.addtotree(myif);
                        mysarif2.addtotree(myif);
                    }
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if1", mysarif1);
                    mysarif1.setGraphLink(this.lastHeader + "-if1");
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if2", mysarif2);
                    mysarif2.setGraphLink(this.lastHeader + "-if2");
                    this.mysar.pdfList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.pdfList.put(this.lastHeader + "-if2", mysarif2);
                    this.mysar.ifacetreenode.add(myif);
                } else {
                    mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                mysarif2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("dentunusd")) {
                return 1;
            }
            if (this.statType.equals("%file-sz")) {
                return 1;
            }
            if (this.statType.equals("TTY")) {
                return 1;
            } else {
                /**Operations for unsupported metrics (filtered-out at the beginning based on the header value) are redirected to /dev/null.
                 *  Without this many exceptions as well as date range problems are observed. **/
                return 1;
            }
        }
        catch (SeriesException e) {
            System.out.println("Linux parser: " + (Object)e);
            return -1;
        }
    }

    /**
     * For some metrics like: irec/s, ihdrerr/s, imsg/s, ierr/s, active/s, idgm/s, tcp6sck, irec6/s, ihdrer6/s, imsg6/s, idgm6/s
     * there is no implemented support in ksar yet. These metrics are ignored and only supported statistics are processed.
     * Sample header:
     * 23:17:05       imsg/s    omsg/s    iech/s   iechr/s    oech/s   oechr/s     itm/s    itmr/s     otm/s    otmr/s  iadrmk/s iadrmkr/s  oadrmk/s oadrmkr/s.
     * The below condition was created based on the observation. First value is date, second and third contains basic description having
     * at least one character.
     */
    private boolean isUnsupportedHeader(String thisLine, boolean headerFound) {
        String firstMetricHeaderDescription = thisLine.split(" +")[1];
        String secondMetricHeaderDescription = thisLine.split(" +")[2];
        return !headerFound &&  firstMetricHeaderDescription.matches(".*[a-zA-Z]+.*") && secondMetricHeaderDescription.matches(".*[a-zA-Z]+.*");
    }

    /**
     * Each metric is processed separately based on the order define in sar file.
     * Sample metric data row: contains only time with hour accuracy, there is no info about the day ex:
     * 20:57:15          2     22.69      0.00      1.91      0.00      0.00      0.00      0.00      0.00     75.40
     * As a result when the new metric is processed then the date needs to be restarted to the original time, having exact time
     * - including day, month, year info.
     */
    private boolean isNewMetric(String thisLine, boolean headerFound) {
        return headerFound && !getDistinctOperationHeader(thisLine).equals(this.mysar.previousOperationType);
    }

    private void resetDateToTheOriginalTime(String thisLine) {
        this.mysar.previousOperationType = getDistinctOperationHeader(thisLine);
        LocalDate generalStartSarDate = this.mysar.sarStartDate;
        this.now = new Second(this.seconde, this.minute, this.heure, generalStartSarDate.getDayOfMonth(), generalStartSarDate.getMonthOfYear(), generalStartSarDate.getYear());
        this.mysar.day = generalStartSarDate.getDayOfMonth();
        this.mysar.month = generalStartSarDate.getMonthOfYear();
        this.mysar.year = generalStartSarDate.getYear();
    }

    /**
     * Update time for which metric actual metric data are processed.
     * Added JodaTime to handle time incrementation(like leap-year etc).
     * Not use Java 8 build in date to be compatible with Java < 8.
     * Note: To reuse org.jfree.data.time.Seconds frequently used in this project some casting between java.util.Date
     * has to be made.
     */
    private void updateCurrentMetricTime() {
        Date previousEndDate = this.now.getEnd();
        Calendar previousEndDateCalendar = GregorianCalendar.getInstance();
        previousEndDateCalendar.setTime(previousEndDate);
        DateTime nextDate = new DateTime(previousEndDate)
                .plusHours((this.heure - previousEndDateCalendar.get(Calendar.HOUR_OF_DAY) == -23) ?  1 : this.heure - previousEndDateCalendar.get(Calendar.HOUR_OF_DAY))
                .plusMinutes(this.minute - previousEndDateCalendar.get(Calendar.MINUTE))
                .plusSeconds(this.seconde - previousEndDateCalendar.get(Calendar.SECOND));
        this.now = new Second(nextDate.toDate());

        if(Days.daysBetween(new DateTime(previousEndDate).toLocalDate(), nextDate.toLocalDate()).getDays() == 1) {
            this.mysar.day = nextDate.getDayOfMonth();
            this.mysar.month = nextDate.getMonthOfYear();
            this.mysar.year = nextDate.getYear();
        }
    }

    private String getDistinctOperationHeader(String operationHeader) {
        String [] allOperationInHeader = operationHeader.split(" ");
        //remove date. Sometime sar split large data for a single metric into few smaller.
        //All of them have the same headers info -only date is different.
        // By removing the date we can differentiate if the new header appear.
        allOperationInHeader[0] = "";


        return Arrays.toString(allOperationInHeader);
    }
}

