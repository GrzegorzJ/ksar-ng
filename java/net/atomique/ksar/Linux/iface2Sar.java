/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class iface2Sar
extends AllGraph {
    private TimeSeries t_rxerr;
    private TimeSeries t_txerr;
    private TimeSeries t_coll;
    private TimeSeries t_rxdrop;
    private TimeSeries t_txdrop;
    private TimeSeries t_txcarr;
    private TimeSeries t_rxfram;
    private TimeSeries t_rxfifo;
    private TimeSeries t_txfifo;
    private String ifName;
    private String ifOpt = new String("");

    public iface2Sar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Interface errors");
        this.ifName = s1;
        this.t_rxerr = new TimeSeries((Comparable)((Object)"rxerr/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " rxerr/s", this.t_rxerr);
        this.t_txerr = new TimeSeries((Comparable)((Object)"txerr/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " txerr/s", this.t_txerr);
        this.t_coll = new TimeSeries((Comparable)((Object)"coll/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " coll/s", this.t_coll);
        this.t_rxdrop = new TimeSeries((Comparable)((Object)"rxdrop/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " rxdrop/s", this.t_rxdrop);
        this.t_txdrop = new TimeSeries((Comparable)((Object)"txdrop/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " txdrop/s", this.t_txdrop);
        this.t_txcarr = new TimeSeries((Comparable)((Object)"txcarr/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " txcarr/s", this.t_txcarr);
        this.t_rxfram = new TimeSeries((Comparable)((Object)"rxfram/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " rxfram/s", this.t_rxfram);
        this.t_rxfifo = new TimeSeries((Comparable)((Object)"rxfifo/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " rxfifo/s", this.t_rxfifo);
        this.t_txfifo = new TimeSeries((Comparable)((Object)"txfifo/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " txfifo/s", this.t_txfifo);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9) {
        this.t_rxerr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_txerr.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_coll.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_rxdrop.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_txdrop.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_txcarr.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_rxfram.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_rxfifo.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_txfifo.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        ++this.number_of_sample;
    }

    public void setifOpt(String s) {
        this.ifOpt = s;
    }

    public XYDataset createerr() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxerr);
        graphcollection.addSeries(this.t_txerr);
        return graphcollection;
    }

    public XYDataset createcoll() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_coll);
        graphcollection.addSeries(this.t_txcarr);
        graphcollection.addSeries(this.t_rxfram);
        return graphcollection;
    }

    public XYDataset createfifo() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxfifo);
        graphcollection.addSeries(this.t_txfifo);
        return graphcollection;
    }

    public XYDataset createdrop() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxdrop);
        graphcollection.addSeries(this.t_txdrop);
        return graphcollection;
    }

    public String getcheckBoxTitle() {
        return "Interface " + this.ifName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXIFACE2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " " + this.ifName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createerr();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("rxer/txerr /s"), (XYItemRenderer)minichart1);
        XYDataset dropset = this.createdrop();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(dropset, null, (ValueAxis)new NumberAxis("txdrop/rxdrop /s"), (XYItemRenderer)minichart2);
        XYDataset collset = this.createcoll();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setSeriesPaint(2, (Paint)kSarConfig.color7);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(collset, null, (ValueAxis)new NumberAxis("coll/txcarr/rxfram /s"), (XYItemRenderer)minichart3);
        XYDataset mcstset = this.createfifo();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color8);
        minichart4.setSeriesPaint(1, (Paint)kSarConfig.color9);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(mcstset, null, (ValueAxis)new NumberAxis("rxdrop/txdrop /s"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        return mychart;
    }
}

