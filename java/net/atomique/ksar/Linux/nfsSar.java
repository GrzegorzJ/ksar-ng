/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class nfsSar
extends AllGraph {
    private TimeSeries t_call;
    private TimeSeries t_retrans;
    private TimeSeries t_read;
    private TimeSeries t_write;
    private TimeSeries t_access;
    private TimeSeries t_getatt;
    private String nfsOpt = new String("");

    public nfsSar(kSar hissar) {
        super(hissar);
        this.Title = new String("NFS client");
        this.t_call = new TimeSeries((Comparable)((Object)"call/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS call/s", this.t_call);
        this.t_retrans = new TimeSeries((Comparable)((Object)"retrans/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS retrans/s", this.t_retrans);
        this.t_read = new TimeSeries((Comparable)((Object)"read/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS read/s", this.t_read);
        this.t_write = new TimeSeries((Comparable)((Object)"write/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS write/s", this.t_write);
        this.t_access = new TimeSeries((Comparable)((Object)"access/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS access/s", this.t_access);
        this.t_getatt = new TimeSeries((Comparable)((Object)"getatt/s"), (Class)Second.class);
        this.mysar.dispo.put("NFS getatt/s", this.t_getatt);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6) {
        this.t_call.add((RegularTimePeriod)now, (Number)val1);
        this.t_retrans.add((RegularTimePeriod)now, (Number)val2);
        this.t_read.add((RegularTimePeriod)now, (Number)val3);
        this.t_write.add((RegularTimePeriod)now, (Number)val4);
        this.t_access.add((RegularTimePeriod)now, (Number)val5);
        this.t_getatt.add((RegularTimePeriod)now, (Number)val6);
        ++this.number_of_sample;
    }

    public XYDataset createcall() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_call);
        graphcollection.addSeries(this.t_retrans);
        return graphcollection;
    }

    public XYDataset createrw() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_read);
        graphcollection.addSeries(this.t_write);
        return graphcollection;
    }

    public XYDataset createatt() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_access);
        graphcollection.addSeries(this.t_getatt);
        return graphcollection;
    }

    public void setnfsOpt(String s) {
        this.nfsOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXNFS", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createcall();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("call/retrans /s"), (XYItemRenderer)minichart1);
        XYDataset rwset = this.createrw();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(rwset, null, (ValueAxis)new NumberAxis("write/read /s"), (XYItemRenderer)minichart2);
        XYDataset attset = this.createatt();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(attset, null, (ValueAxis)new NumberAxis("access/getattr /s"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

