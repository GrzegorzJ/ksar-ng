/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Color;
import java.awt.Paint;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class intrlistSar
extends AllGraph {
    private TimeTableXYDataset stacked_intr;
    private String intrId;

    public intrlistSar(kSar hissar, String intrId) {
        super(hissar);
        this.intrId = intrId;
        this.Title = new String("Interrupt " + intrId);
        this.stacked_intr = new TimeTableXYDataset();
    }

    public void add(Second now, String cpu, Float val1) {
        this.stacked_intr.add((TimePeriod)now, val1.doubleValue(), cpu);
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXINTRLIST", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYPlot subplot1;
        NumberAxis intraxis = new NumberAxis("intr/s");
        if (!this.mysar.showstackedintrlist) {
            StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
            subplot1 = new XYPlot((XYDataset)this.stacked_intr, null, (ValueAxis)intraxis, (XYItemRenderer)minichart1);
        } else {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            subplot1 = new XYPlot((XYDataset)this.stacked_intr, (ValueAxis)new DateAxis(null), (ValueAxis)intraxis, (XYItemRenderer)renderer);
        }
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 2);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), JFreeChart.DEFAULT_TITLE_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        mychart.setBackgroundPaint((Paint)Color.white);
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
        }
        return mychart;
    }
}

