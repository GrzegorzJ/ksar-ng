/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class nfsdSar
extends AllGraph {
    private TimeSeries t_scall;
    private TimeSeries t_badcall;
    private TimeSeries t_packet;
    private TimeSeries t_udp;
    private TimeSeries t_tcp;
    private TimeSeries t_hit;
    private TimeSeries t_miss;
    private TimeSeries t_sread;
    private TimeSeries t_swrite;
    private TimeSeries t_saccess;
    private TimeSeries t_sgetatt;
    private String nfsOpt = new String("");

    public nfsdSar(kSar hissar) {
        super(hissar);
        this.Title = new String("NFS Server");
        this.t_scall = new TimeSeries((Comparable)((Object)"scall/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD scall/s", this.t_scall);
        this.t_badcall = new TimeSeries((Comparable)((Object)"badcall/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD badcall/s", this.t_badcall);
        this.t_packet = new TimeSeries((Comparable)((Object)"packet/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD packet/s", this.t_packet);
        this.t_udp = new TimeSeries((Comparable)((Object)"udp/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD udp/s", this.t_udp);
        this.t_tcp = new TimeSeries((Comparable)((Object)"tcp/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD tcp/s", this.t_tcp);
        this.t_hit = new TimeSeries((Comparable)((Object)"hit/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD hit/s", this.t_hit);
        this.t_miss = new TimeSeries((Comparable)((Object)"miss/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD miss/s", this.t_miss);
        this.t_sread = new TimeSeries((Comparable)((Object)"sread/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD sread/s", this.t_sread);
        this.t_swrite = new TimeSeries((Comparable)((Object)"swrite/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD swrite/s", this.t_swrite);
        this.t_saccess = new TimeSeries((Comparable)((Object)"saccess/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD saccess/s", this.t_saccess);
        this.t_sgetatt = new TimeSeries((Comparable)((Object)"sgetatt/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD sgetatt/s", this.t_sgetatt);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9, Float val10, Float val11) {
        this.t_scall.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_badcall.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_packet.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_udp.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_tcp.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_hit.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_miss.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_sread.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_swrite.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        this.t_saccess.add((RegularTimePeriod)now, (Number)val10, this.do_notify());
        this.t_sgetatt.add((RegularTimePeriod)now, (Number)val11, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createcall() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_scall);
        graphcollection.addSeries(this.t_packet);
        return graphcollection;
    }

    public XYDataset createnet() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_packet);
        graphcollection.addSeries(this.t_udp);
        graphcollection.addSeries(this.t_tcp);
        return graphcollection;
    }

    public XYDataset createcache() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_hit);
        graphcollection.addSeries(this.t_miss);
        return graphcollection;
    }

    public XYDataset createrw() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_sread);
        graphcollection.addSeries(this.t_swrite);
        return graphcollection;
    }

    public XYDataset createatt() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_saccess);
        graphcollection.addSeries(this.t_sgetatt);
        return graphcollection;
    }

    public void setnfsOpt(String s) {
        this.nfsOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXNFSD", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createcall();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("scall/badcall /s"), (XYItemRenderer)minichart1);
        XYDataset netset = this.createnet();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color5);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(netset, null, (ValueAxis)new NumberAxis("packet/udp/tcp /s"), (XYItemRenderer)minichart2);
        XYDataset cacheset = this.createcache();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color6);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color7);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(cacheset, null, (ValueAxis)new NumberAxis("hit/miss /s"), (XYItemRenderer)minichart3);
        XYDataset rwset = this.createrw();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color8);
        minichart4.setSeriesPaint(1, (Paint)kSarConfig.color9);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(rwset, null, (ValueAxis)new NumberAxis("sread/swrite /s"), (XYItemRenderer)minichart4);
        XYDataset attset = this.createatt();
        StandardXYItemRenderer minichart5 = new StandardXYItemRenderer();
        minichart5.setSeriesPaint(0, (Paint)kSarConfig.color10);
        minichart5.setSeriesPaint(1, (Paint)kSarConfig.color11);
        minichart5.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot5 = new XYPlot(attset, null, (ValueAxis)new NumberAxis("saccess/sgetatt /s"), (XYItemRenderer)minichart5);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.add(subplot5, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
            subplot5.setBackgroundPaint(null);
        }
        return mychart;
    }
}

