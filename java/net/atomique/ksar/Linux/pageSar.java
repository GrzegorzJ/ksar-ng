/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class pageSar
extends AllGraph {
    private TimeSeries t_shmpg;
    private TimeSeries t_bufpg;
    private TimeSeries t_campg;
    private TimeSeries t_frmpg;
    private String pageOpt = new String("");

    public pageSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Page");
        this.t_frmpg = new TimeSeries((Comparable)((Object)"frmpg/s"), (Class)Second.class);
        this.mysar.dispo.put("Page frmpg/s", this.t_frmpg);
        this.t_shmpg = new TimeSeries((Comparable)((Object)"shmpg/s"), (Class)Second.class);
        this.mysar.dispo.put("Page shmpg/s", this.t_shmpg);
        this.t_bufpg = new TimeSeries((Comparable)((Object)"bufpg/s"), (Class)Second.class);
        this.mysar.dispo.put("Page bufpg/s", this.t_bufpg);
        this.t_campg = new TimeSeries((Comparable)((Object)"campg/s"), (Class)Second.class);
        this.mysar.dispo.put("Page campg/s", this.t_campg);
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        this.t_frmpg.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_bufpg.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_campg.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_frmpg.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_shmpg.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_bufpg.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_campg.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createfrmpg() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_frmpg);
        return graphcollection;
    }

    public XYDataset createshmpg() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_shmpg);
        return graphcollection;
    }

    public XYDataset createbufpg() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_bufpg);
        return graphcollection;
    }

    public XYDataset createcampg() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_campg);
        return graphcollection;
    }

    public void setpageOpt(String s) {
        this.pageOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXPAGE", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createfrmpg();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("frmpg/s"), (XYItemRenderer)minichart1);
        XYDataset bufpgset = this.createbufpg();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(bufpgset, null, (ValueAxis)new NumberAxis("bufpg/s"), (XYItemRenderer)minichart2);
        XYDataset campgset = this.createcampg();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(campgset, null, (ValueAxis)new NumberAxis("campg/s"), (XYItemRenderer)minichart3);
        XYPlot subplot4 = null;
        if (this.pageOpt.equals("shmpg/s")) {
            XYDataset shmpgset = this.createshmpg();
            StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
            minichart4.setSeriesPaint(0, (Paint)kSarConfig.color4);
            minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            NumberAxis axis = new NumberAxis("shmpg/s");
            axis.setAutoRangeIncludesZero(true);
            subplot4 = new XYPlot(shmpgset, null, (ValueAxis)axis, (XYItemRenderer)minichart4);
        }
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        if (this.pageOpt.equals("shmpg/s")) {
            plot.add(subplot4, 1);
        }
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            if (this.pageOpt.equals("shmpg/s")) {
                subplot4.setBackgroundPaint(null);
            }
        }
        return mychart;
    }
}

