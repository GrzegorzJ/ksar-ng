/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class kbmemSar
extends AllGraph {
    private TimeSeries t_free;
    private TimeSeries t_used;
    private TimeSeries t_perc;
    private String loadOpt = new String("");
    private TimeTableXYDataset stacked_used;

    public kbmemSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Memory Usage");
        this.t_free = new TimeSeries((Comparable)((Object)"memfree"), (Class)Second.class);
        this.mysar.dispo.put("Memory free", this.t_free);
        this.t_used = new TimeSeries((Comparable)((Object)"memused"), (Class)Second.class);
        this.mysar.dispo.put("Memory used", this.t_used);
        this.t_perc = new TimeSeries((Comparable)((Object)"%memused"), (Class)Second.class);
        this.mysar.dispo.put("%Memory used", this.t_perc);
        this.stacked_used = new TimeTableXYDataset();
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float kbbuffers, Float kbcached, Float kbswpused) {
        this.t_free.add((RegularTimePeriod)now, (double)(val1.floatValue() * 1024.0f), this.do_notify());
        this.t_used.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_perc.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        double kbusedbuffadj = val2.floatValue() - kbbuffers.floatValue() - kbcached.floatValue();
        this.stacked_used.add((TimePeriod)now, (Number)(kbusedbuffadj * 1024.0), "used (buffer adjusted)", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)(kbbuffers.doubleValue() * 1024.0), "buffers", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)(kbcached.doubleValue() * 1024.0), "cached", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)(val1.doubleValue() * 1024.0), "free", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)(kbswpused.doubleValue() * 1024.0), "swap", this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        this.t_free.add((RegularTimePeriod)now, (double)(val1.floatValue() * 1024.0f), this.do_notify());
        this.t_used.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_perc.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createfree() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_free);
        return graphcollection;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_used);
        return graphcollection;
    }

    public XYDataset createperc() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_perc);
        return graphcollection;
    }

    public void setloadOpt(String s) {
        this.loadOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXKBMEM", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYPlot subplot2;
        XYDataset freeset = this.createfree();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        NumberAxis numberaxis1 = new NumberAxis("memfree");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot1 = new XYPlot(freeset, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        NumberAxis numberaxis = new NumberAxis("memused");
        IEEE1541Number decimalformat = new IEEE1541Number(1);
        numberaxis.setNumberFormatOverride((NumberFormat)decimalformat);
        if (this.mysar.showstackedmem) {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            renderer.setSeriesPaint(0, (Paint)kSarConfig.color3);
            renderer.setSeriesPaint(1, (Paint)kSarConfig.color4);
            renderer.setSeriesPaint(2, (Paint)kSarConfig.color5);
            renderer.setSeriesPaint(3, (Paint)kSarConfig.color6);
            renderer.setSeriesPaint(4, (Paint)kSarConfig.color7);
            subplot2 = new XYPlot((XYDataset)this.stacked_used, (ValueAxis)new DateAxis(null), (ValueAxis)numberaxis, (XYItemRenderer)renderer);
        } else {
            XYDataset usedset = this.createused();
            StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
            minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
            minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot2 = new XYPlot(usedset, null, (ValueAxis)numberaxis, (XYItemRenderer)minichart2);
        }
        XYDataset percset = this.createperc();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color8);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(percset, null, (ValueAxis)new NumberAxis("%memused"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

