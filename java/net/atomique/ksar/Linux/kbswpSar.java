/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class kbswpSar
extends AllGraph {
    private TimeSeries t_free;
    private TimeSeries t_used;
    private TimeSeries t_perc;
    private TimeSeries t_adc;
    private TimeSeries t_swpcad;
    private String swpOpt = new String("");

    public kbswpSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Swap usage");
        this.t_free = new TimeSeries((Comparable)((Object)"kbswpfree"), (Class)Second.class);
        this.mysar.dispo.put("Swap free", this.t_free);
        this.t_used = new TimeSeries((Comparable)((Object)"kbswpused"), (Class)Second.class);
        this.mysar.dispo.put("Swap used", this.t_used);
        this.t_perc = new TimeSeries((Comparable)((Object)"%swpused"), (Class)Second.class);
        this.mysar.dispo.put("% Swap used", this.t_perc);
        this.t_adc = new TimeSeries((Comparable)((Object)"kbswpcad"), (Class)Second.class);
        this.mysar.dispo.put("Swap cad", this.t_adc);
        this.t_swpcad = new TimeSeries((Comparable)((Object)"%swpcad"), (Class)Second.class);
        this.mysar.dispo.put("Swap %swpcad", this.t_swpcad);
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        this.t_free.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_used.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_perc.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_free.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_used.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_perc.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_adc.add((RegularTimePeriod)now, val4.doubleValue() * 1024.0, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5) {
        this.t_free.add((RegularTimePeriod)now, val1.doubleValue() * 1024.0, this.do_notify());
        this.t_used.add((RegularTimePeriod)now, val2.doubleValue() * 1024.0, this.do_notify());
        this.t_perc.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_adc.add((RegularTimePeriod)now, val4.doubleValue() * 1024.0, this.do_notify());
        this.t_swpcad.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createfree() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_free);
        return graphcollection;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_used);
        return graphcollection;
    }

    public XYDataset createperc() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_perc);
        return graphcollection;
    }

    public XYDataset createadc() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_adc);
        return graphcollection;
    }

    public void setswpOpt(String s) {
        this.swpOpt = s;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXKBSWP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset freeset = this.createfree();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        NumberAxis numberaxis1 = new NumberAxis("swpfree");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot1 = new XYPlot(freeset, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        XYDataset usedset = this.createused();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis2 = new NumberAxis("swpused");
        numberaxis2.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot2 = new XYPlot(usedset, null, (ValueAxis)numberaxis2, (XYItemRenderer)minichart2);
        XYDataset percset = this.createperc();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(percset, null, (ValueAxis)new NumberAxis("%swpused"), (XYItemRenderer)minichart3);
        XYPlot subplot4 = null;
        if (!this.swpOpt.equals("kbmemshrd")) {
            XYDataset adcset = this.createadc();
            StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
            minichart4.setSeriesPaint(0, (Paint)kSarConfig.color4);
            minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            NumberAxis numberaxis3 = new NumberAxis("swpcad");
            numberaxis3.setNumberFormatOverride((NumberFormat)decimalformat1);
            subplot4 = new XYPlot(adcset, null, (ValueAxis)numberaxis3, (XYItemRenderer)minichart4);
        }
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        if (!this.swpOpt.equals("kbmemshrd")) {
            plot.add(subplot4, 1);
        }
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            if (!this.swpOpt.equals("kbmemshrd")) {
                subplot4.setBackgroundPaint(null);
            }
        }
        return mychart;
    }
}

