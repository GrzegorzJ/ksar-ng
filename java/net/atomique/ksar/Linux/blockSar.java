/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Linux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class blockSar
extends AllGraph {
    private TimeSeries t_tps;
    private TimeSeries t_rdsec;
    private TimeSeries t_wrsec;
    private TimeSeries t_sect;
    private String blockName;
    private String blockOpt = new String("");
    private final diskName optdisk;

    public blockSar(kSar hissar, String s1, diskName diskopt) {
        super(hissar);
        this.Title = new String("Block Transfer");
        this.blockName = s1;
        this.datain = 0;
        this.optdisk = diskopt;
        this.t_tps = new TimeSeries((Comparable)((Object)"Transfer/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "Transfer/s", this.t_tps);
        this.t_rdsec = new TimeSeries((Comparable)((Object)"Read/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "Read/s", this.t_rdsec);
        this.t_wrsec = new TimeSeries((Comparable)((Object)"Write/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "Write/s", this.t_wrsec);
        this.t_sect = new TimeSeries((Comparable)((Object)"Bytes read/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "Bytes read/s", this.t_sect);
    }

    public void add(Second now, Float val1, Float val2, Float val3) {
        Float newval;
        Number tmpInt;
        Float zerof = new Float(0.0f);
        if (!(val1.equals(zerof) && val2.equals(zerof) && val3.equals(zerof) || this.datain != 0)) {
            this.datain = 1;
        }
        if ((tmpInt = this.t_tps.getValue((RegularTimePeriod)now)) != null) {
            newval = new Float(tmpInt.floatValue() + val1.floatValue());
            this.t_tps.update((RegularTimePeriod)now, (Number)newval);
        } else {
            this.t_tps.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        }
        if (tmpInt != null) {
            newval = new Float(tmpInt.floatValue() + val2.floatValue());
            this.t_rdsec.update((RegularTimePeriod)now, (Number)newval);
        } else {
            this.t_rdsec.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        }
        if (tmpInt != null) {
            newval = new Float(tmpInt.floatValue() + val3.floatValue());
            this.t_wrsec.update((RegularTimePeriod)now, (Number)newval);
        } else {
            this.t_wrsec.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        }
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2) {
        Float zerof = new Float(0.0f);
        if (!(val1.equals(zerof) && val2.equals(zerof) || this.datain != 0)) {
            this.datain = 1;
        }
        this.t_tps.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_sect.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        ++this.number_of_sample;
    }

    public void setioOpt(String s) {
        this.blockOpt = s;
    }

    public XYDataset createtps() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_tps);
        return graphcollection;
    }

    public XYDataset createsect() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_sect);
        return graphcollection;
    }

    public XYDataset createrws() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_wrsec);
        graphcollection.addSeries(this.t_rdsec);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = this.blockOpt.equals("rd_sec/s") || this.blockOpt.equals("avgrq-sz") ? new DefaultMutableTreeNode(new GraphDescription(this, "LINUXBLOCK", this.Title, null)) : new DefaultMutableTreeNode(new GraphDescription(this, "LINUXBLOCK", this.blockName, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.blockName;
    }

    public String getGraphTitle() {
        return this.Title + " " + this.optdisk.showTitle() + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart2;
        CombinedDomainXYPlot plot = null;
        XYPlot subplot2 = null;
        XYDataset xydataset1 = this.createtps();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("tps/s"), (XYItemRenderer)minichart1);
        if (this.blockOpt.equals("rd_sec/s") || this.blockOpt.equals("avgrq-sz")) {
            XYDataset rws = this.createrws();
            minichart2 = new StandardXYItemRenderer();
            minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
            minichart2.setSeriesPaint(1, (Paint)kSarConfig.color3);
            minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot2 = new XYPlot(rws, null, (ValueAxis)new NumberAxis("Read/Write /s"), (XYItemRenderer)minichart2);
        } else {
            XYDataset sectset = this.createsect();
            minichart2 = new StandardXYItemRenderer();
            minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
            minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot2 = new XYPlot(sectset, null, (ValueAxis)new NumberAxis("Sect/s"), (XYItemRenderer)minichart2);
        }
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        return mychart;
    }
}

