/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Mac;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class cpuSar
extends AllGraph {
    private TimeTableXYDataset stacked_used;
    private TimeSeries t_usr;
    private TimeSeries t_sys;
    private TimeSeries t_wio;
    private TimeSeries t_idle;

    public cpuSar(kSar hissar) {
        super(hissar);
        this.Title = new String("CPU");
        this.t_usr = new TimeSeries((Comparable)((Object)"User"), (Class)Second.class);
        this.mysar.dispo.put("CPU user", this.t_usr);
        this.t_sys = new TimeSeries((Comparable)((Object)"System"), (Class)Second.class);
        this.mysar.dispo.put("CPU System", this.t_sys);
        this.t_idle = new TimeSeries((Comparable)((Object)"Idle"), (Class)Second.class);
        this.mysar.dispo.put("CPU Idle", this.t_idle);
        this.stacked_used = new TimeTableXYDataset();
    }

    public void add(Second now, Float usrInit, Float sysInit, Float idleInit) {
        this.t_usr.add((RegularTimePeriod)now, (Number)usrInit);
        this.t_sys.add((RegularTimePeriod)now, (Number)sysInit);
        this.t_idle.add((RegularTimePeriod)now, (Number)idleInit);
        this.stacked_used.add((TimePeriod)now, (double)usrInit.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)sysInit.floatValue(), "System");
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_usr);
        graphcollection.addSeries(this.t_sys);
        return graphcollection;
    }

    public XYDataset createidle() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_idle);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "MACCPU", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYPlot subplot1 = null;
        XYDataset xydataset1 = this.createused();
        NumberAxis usedaxis = new NumberAxis("% used cpu");
        if (this.mysar.show100axiscpu) {
            usedaxis.setRange(0.0, 100.0);
        }
        if (this.mysar.showstackedcpu) {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
            renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
            renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
            subplot1 = new XYPlot((XYDataset)this.stacked_used, (ValueAxis)new DateAxis(null), (ValueAxis)usedaxis, (XYItemRenderer)renderer);
        } else {
            StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
            minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
            minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
            minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
            subplot1 = new XYPlot(xydataset1, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        }
        XYDataset idleset = this.createidle();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(idleset, null, (ValueAxis)new NumberAxis("%"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 3);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }
}

