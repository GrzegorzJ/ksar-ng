/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.Mac;

import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;

public class Parser {
    kSar mysar;
    Float val1;
    Float val2;
    Float val3;
    Float val4;
    Float val5;
    Float val6;
    Float val7;
    Float val8;
    Float val9;
    Float val10;
    Float val11;
    int heure = 0;
    int minute = 0;
    int seconde = 0;
    Second now = new Second(0, 0, 0, 1, 1, 1970);
    String lastHeader;
    String statType = "none";
    cpuSar sarCPU4 = null;
    pgoutSar sarPGOUT4 = null;
    pginSar sarPGIN4 = null;

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String first, StringTokenizer matcher) {
        boolean headerFound = false;
        if (thisLine.indexOf("%usr") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pgout/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pgin/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("device") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("IFACE") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("New Disk: ") > 0) {
            return 1;
        }
        String[] sarTime = first.split(":");
        if (sarTime.length != 3) {
            return 1;
        }
        if (sarTime.length == 3) {
            this.heure = Integer.parseInt(sarTime[0]);
            this.minute = Integer.parseInt(sarTime[1]);
            this.seconde = Integer.parseInt(sarTime[2]);
            this.now = new Second(this.seconde, this.minute, this.heure, this.mysar.day, this.mysar.month, this.mysar.year);
            if (this.mysar.statstart == null) {
                this.mysar.statstart = new String(this.now.toString());
                this.mysar.startofgraph = this.now;
            }
            if (!this.mysar.datefound.contains((Object)this.now)) {
                this.mysar.datefound.add(this.now);
            }
            if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
                this.mysar.lastever = this.now;
                this.mysar.statend = new String(this.mysar.lastever.toString());
                this.mysar.endofgraph = this.now;
            }
        }
        this.lastHeader = matcher.nextToken();
        if (headerFound) {
            if (this.lastHeader.equals(this.statType)) {
                headerFound = false;
                return 1;
            }
            this.statType = this.lastHeader;
            if (this.lastHeader.equals("%usr")) {
                if (this.sarCPU4 == null) {
                    this.sarCPU4 = new cpuSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarCPU4.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("MaccpuSar", this.sarCPU4);
                    this.sarCPU4.setGraphLink("MaccpuSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("pgout/s")) {
                if (this.sarPGOUT4 == null) {
                    this.sarPGOUT4 = new pgoutSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPGOUT4.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("MacpgoutSar", this.sarPGOUT4);
                    this.sarPGOUT4.setGraphLink("MacpgoutSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("pgin/s")) {
                if (this.sarPGIN4 == null) {
                    this.sarPGIN4 = new pginSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPGIN4.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("MacpginSar", this.sarPGIN4);
                    this.sarPGIN4.setGraphLink("MacpginSar");
                }
                return 1;
            }
            if (this.statType.equals("device")) {
                if (!this.mysar.hasdisknode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                    }
                    this.mysar.hasdisknode = true;
                }
                return 1;
            }
            if (this.statType.equals("IFACE")) {
                if (thisLine.indexOf("Ipkts/s") > 0) {
                    this.statType = "Ipkts/s";
                }
                if (thisLine.indexOf("Ierrs/s") > 0) {
                    this.statType = "Ierrs/s";
                }
                if (!this.mysar.hasifnode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.ifacetreenode);
                    }
                    this.mysar.hasifnode = true;
                }
                return 1;
            }
            headerFound = false;
            return 1;
        }
        try {
            if (this.statType.equals("%usr")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarCPU4.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("pgout/s")) {
                this.val1 = new Float(this.lastHeader);
                this.sarPGOUT4.add(this.now, this.val1);
                return 1;
            }
            if (this.statType.equals("pgin/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarPGIN4.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("device")) {
                blockSar mysarblock;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (!this.mysar.disksSarList.containsKey(this.lastHeader + "-t1")) {
                    diskName tmp = new diskName(this.lastHeader);
                    this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                    mysarblock = new blockSar(this.mysar, this.lastHeader);
                    this.mysar.disksSarList.put(this.lastHeader + "-t1", mysarblock);
                    mysarblock.setGraphLink(this.lastHeader + "-t1");
                    this.mysar.pdfList.put(this.lastHeader + "-t1", mysarblock);
                    mysarblock.addtotree(this.mysar.diskstreenode);
                } else {
                    mysarblock = (blockSar)this.mysar.disksSarList.get(this.lastHeader + "-t1");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                mysarblock.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("Ipkts/s")) {
                iface1Sar mysarif1;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (!this.mysar.ifaceSarList.containsKey(this.lastHeader + "-if1")) {
                    mysarif1 = new iface1Sar(this.mysar, this.lastHeader);
                    iface2Sar mysarif2 = new iface2Sar(this.mysar, this.lastHeader);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(this.lastHeader);
                    mysarif1.addtotree(myif);
                    mysarif2.addtotree(myif);
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if2", mysarif2);
                    mysarif1.setGraphLink(this.lastHeader + "-if1");
                    mysarif2.setGraphLink(this.lastHeader + "-if2");
                    this.mysar.pdfList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.pdfList.put(this.lastHeader + "-if2", mysarif2);
                    this.mysar.add2tree(this.mysar.ifacetreenode, myif);
                } else {
                    mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if1");
                    iface2Sar mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                mysarif1.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("Ierrs/s")) {
                iface2Sar mysarif2;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (!this.mysar.ifaceSarList.containsKey(this.lastHeader + "-if1")) {
                    iface1Sar mysarif1 = new iface1Sar(this.mysar, this.lastHeader);
                    mysarif2 = new iface2Sar(this.mysar, this.lastHeader);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(this.lastHeader);
                    mysarif1.addtotree(myif);
                    mysarif2.addtotree(myif);
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.ifaceSarList.put(this.lastHeader + "-if2", mysarif2);
                    this.mysar.pdfList.put(this.lastHeader + "-if1", mysarif1);
                    this.mysar.pdfList.put(this.lastHeader + "-if2", mysarif2);
                    mysarif1.setGraphLink(this.lastHeader + "-if1");
                    mysarif2.setGraphLink(this.lastHeader + "-if2");
                    this.mysar.add2tree(this.mysar.ifacetreenode, myif);
                } else {
                    iface1Sar mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if1");
                    mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(this.lastHeader + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                mysarif2.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
        }
        catch (SeriesException e) {
            System.out.println("Mac parser: " + (Object)e);
            return -1;
        }
        return 0;
    }
}

