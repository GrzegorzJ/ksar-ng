/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Mac;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class iface2Sar
extends AllGraph {
    private TimeSeries t_rxerr;
    private TimeSeries t_txerr;
    private TimeSeries t_coll;
    private TimeSeries t_drop;
    private kSar hissar;
    private String ifName;
    private String ifOpt = new String("");

    public iface2Sar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Interface errors");
        this.ifName = s1;
        this.t_rxerr = new TimeSeries((Comparable)((Object)"Ierrs/s"), (Class)Second.class);
        this.mysar.dispo.put("Intreface " + s1 + " ierr/s", this.t_rxerr);
        this.t_txerr = new TimeSeries((Comparable)((Object)"Oerrs/s"), (Class)Second.class);
        this.mysar.dispo.put("Intreface " + s1 + " oerr/s", this.t_txerr);
        this.t_coll = new TimeSeries((Comparable)((Object)"Coll/s"), (Class)Second.class);
        this.mysar.dispo.put("Intreface " + s1 + " coll/s", this.t_coll);
        this.t_drop = new TimeSeries((Comparable)((Object)"Drop/s"), (Class)Second.class);
        this.mysar.dispo.put("Intreface " + s1 + " drop/s", this.t_drop);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_rxerr.add((RegularTimePeriod)now, (Number)val1);
        this.t_txerr.add((RegularTimePeriod)now, (Number)val2);
        this.t_coll.add((RegularTimePeriod)now, (Number)val3);
        this.t_drop.add((RegularTimePeriod)now, (Number)val4);
    }

    public void setifOpt(String s) {
        this.ifOpt = s;
    }

    public XYDataset createerr() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxerr);
        graphcollection.addSeries(this.t_txerr);
        return graphcollection;
    }

    public XYDataset createcoll() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_coll);
        graphcollection.addSeries(this.t_drop);
        return graphcollection;
    }

    public String getcheckBoxTitle() {
        return "Interface " + this.ifName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "MACIFACE2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " " + this.ifName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createerr();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("Ierrs/Oerrs /s"), (XYItemRenderer)minichart1);
        XYDataset dropset = this.createcoll();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(dropset, null, (ValueAxis)new NumberAxis("Coll/Drop /s"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }
}

