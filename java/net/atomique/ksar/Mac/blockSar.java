/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Mac;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class blockSar
extends AllGraph {
    private TimeSeries t_rw;
    private TimeSeries t_blks;
    private kSar hissar;
    private String blockName;
    private String blockOpt = new String("");

    public blockSar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Block");
        this.blockName = s1;
        this.datain = 0;
        this.t_rw = new TimeSeries((Comparable)((Object)"r+w/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + " r+w/s", this.t_rw);
        this.t_blks = new TimeSeries((Comparable)((Object)"Block/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "block/s", this.t_blks);
    }

    public void add(Second now, Float val1, Float val2) {
        Float zerof = new Float(0.0f);
        if ((val1 != zerof || val2 != zerof) && this.datain == 0) {
            this.datain = 1;
        }
        this.t_rw.add((RegularTimePeriod)now, (Number)val1);
        this.t_blks.add((RegularTimePeriod)now, (Number)val2);
    }

    public void setioOpt(String s) {
        this.blockOpt = s;
    }

    public XYDataset createtps() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rw);
        return graphcollection;
    }

    public XYDataset createsect() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_blks);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "MACBLOCK", this.blockName, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.blockName;
    }

    public String getGraphTitle() {
        return this.Title + " " + this.blockName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        CombinedDomainXYPlot plot = null;
        XYPlot subplot2 = null;
        XYDataset xydataset1 = this.createtps();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("r+w/s"), (XYItemRenderer)minichart1);
        XYDataset sectset = this.createsect();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        subplot2 = new XYPlot(sectset, null, (ValueAxis)new NumberAxis("Block/s"), (XYItemRenderer)minichart2);
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }
}

