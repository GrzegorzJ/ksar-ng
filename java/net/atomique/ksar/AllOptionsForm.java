/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import net.atomique.ksar.BackgroundImage;
import net.atomique.ksar.SSHIdentity;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;

public class AllOptionsForm
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private JButton cancelButton;
    private JCheckBox jCheckBox1;
    private JCheckBox jCheckBox2;
    private JComboBox jComboBox1;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JPanel jPanel10;
    private JPanel jPanel11;
    private JPanel jPanel12;
    private JPanel jPanel13;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JPanel jPanel9;
    private JScrollPane jScrollPane1;
    private JSpinner jSpinner1;
    private JSpinner jSpinner2;
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JButton okButton;
    private JButton resetButton;
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    kSarDesktop mydesktop;

    public AllOptionsForm(kSarDesktop hisdesktop) {
        this.mydesktop = hisdesktop;
        this.initComponents();
        ActionListener actionListener = new ActionListener(){

            public void actionPerformed(ActionEvent actionEvent) {
                Object source = actionEvent.getSource();
                String lafClassName = null;
                if (source instanceof JComboBox) {
                    JComboBox comboBox = (JComboBox)source;
                    lafClassName = (String)comboBox.getSelectedItem();
                }
                if (lafClassName != null) {
                    String finalLafClassName = lafClassName;
                    for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
                        if (!lafClassName.equals(laf.getName())) continue;
                        try {
                            UIManager.setLookAndFeel(laf.getClassName());
                            SwingUtilities.updateComponentTreeUI(AllOptionsForm.this.mydesktop);
                            continue;
                        }
                        catch (Exception e) {
                            // empty catch block
                        }
                    }
                }
            }
        };
        this.load_landf();
        this.resetButtonActionPerformed(null);
        this.jComboBox1.addActionListener(actionListener);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jPanel3 = new JPanel();
        this.jPanel5 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jSpinner1 = new JSpinner();
        this.jPanel4 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jSpinner2 = new JSpinner();
        this.jPanel6 = new JPanel();
        this.jLabel3 = new JLabel();
        this.jCheckBox1 = new JCheckBox();
        this.jPanel7 = new JPanel();
        this.jLabel4 = new JLabel();
        this.jTextField3 = new JTextField();
        this.jPanel8 = new JPanel();
        this.jLabel5 = new JLabel();
        this.jTextField4 = new JTextField();
        this.jPanel9 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jTextField5 = new JTextField();
        this.jPanel10 = new JPanel();
        this.jLabel7 = new JLabel();
        this.jTextField1 = new JTextField();
        this.jPanel11 = new JPanel();
        this.jLabel8 = new JLabel();
        this.jTextField2 = new JTextField();
        this.jPanel12 = new JPanel();
        this.jLabel9 = new JLabel();
        this.jComboBox1 = new JComboBox();
        this.jPanel13 = new JPanel();
        this.jLabel10 = new JLabel();
        this.jCheckBox2 = new JCheckBox();
        this.jPanel2 = new JPanel();
        this.okButton = new JButton();
        this.cancelButton = new JButton();
        this.resetButton = new JButton();
        this.setClosable(true);
        this.setTitle("Options");
        this.setPreferredSize(new Dimension(430, 438));
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new BoxLayout(this.jPanel3, 1));
        this.jPanel5.setLayout(new FlowLayout(0));
        this.jLabel1.setLabelFor(this.jTextField1);
        this.jLabel1.setText("Image Width: ");
        this.jPanel5.add(this.jLabel1);
        this.jSpinner1.setValue(new Integer(kSarConfig.imagewidth));
        this.jPanel5.add(this.jSpinner1);
        this.jPanel3.add(this.jPanel5);
        this.jPanel4.setLayout(new FlowLayout(0));
        this.jLabel2.setText("Image Height: ");
        this.jPanel4.add(this.jLabel2);
        this.jSpinner2.setValue(new Integer(kSarConfig.imageheight));
        this.jPanel4.add(this.jSpinner2);
        this.jPanel3.add(this.jPanel4);
        this.jPanel6.setLayout(new FlowLayout(0));
        this.jLabel3.setText("make HTML index");
        this.jPanel6.add(this.jLabel3);
        this.jPanel6.add(this.jCheckBox1);
        this.jPanel3.add(this.jPanel6);
        this.jPanel7.setLayout(new FlowLayout(0));
        this.jLabel4.setText("PDF bottom left text:");
        this.jPanel7.add(this.jLabel4);
        this.jTextField3.setText(kSarConfig.pdfbottomleft);
        this.jTextField3.setMinimumSize(new Dimension(150, 22));
        this.jTextField3.setPreferredSize(new Dimension(150, 22));
        this.jPanel7.add(this.jTextField3);
        this.jPanel3.add(this.jPanel7);
        this.jPanel8.setLayout(new FlowLayout(0));
        this.jLabel5.setText("PDF upper right text: ");
        this.jPanel8.add(this.jLabel5);
        this.jTextField4.setText(kSarConfig.pdfupperright);
        this.jTextField4.setMinimumSize(new Dimension(150, 22));
        this.jTextField4.setPreferredSize(new Dimension(150, 22));
        this.jPanel8.add(this.jTextField4);
        this.jPanel3.add(this.jPanel8);
        this.jPanel9.setLayout(new FlowLayout(0));
        this.jLabel6.setText("PDF index page text: ");
        this.jPanel9.add(this.jLabel6);
        this.jTextField5.setText(kSarConfig.pdfindexpage);
        this.jTextField5.setMinimumSize(new Dimension(150, 22));
        this.jTextField5.setPreferredSize(new Dimension(150, 22));
        this.jPanel9.add(this.jTextField5);
        this.jPanel3.add(this.jPanel9);
        this.jPanel10.setLayout(new FlowLayout(0));
        this.jLabel7.setText("Background image File: ");
        this.jPanel10.add(this.jLabel7);
        this.jTextField1.setEditable(false);
        this.jTextField1.setText(kSarConfig.getBackground_image());
        this.jTextField1.setMinimumSize(new Dimension(150, 22));
        this.jTextField1.setPreferredSize(new Dimension(150, 22));
        this.jTextField1.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                AllOptionsForm.this.jTextField1MouseClicked(evt);
            }
        });
        this.jPanel10.add(this.jTextField1);
        this.jPanel3.add(this.jPanel10);
        this.jPanel11.setLayout(new FlowLayout(0));
        this.jLabel8.setText("SSH key file");
        this.jPanel11.add(this.jLabel8);
        this.jTextField2.setEditable(false);
        this.jTextField2.setText(kSarConfig.getSSHidentity());
        this.jTextField2.setMinimumSize(new Dimension(150, 22));
        this.jTextField2.setPreferredSize(new Dimension(150, 22));
        this.jTextField2.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                AllOptionsForm.this.jTextField2MouseClicked(evt);
            }
        });
        this.jPanel11.add(this.jTextField2);
        this.jPanel3.add(this.jPanel11);
        this.jPanel12.setLayout(new FlowLayout(0));
        this.jLabel9.setText("Look&Feel");
        this.jPanel12.add(this.jLabel9);
        this.jComboBox1.setModel(this.model);
        this.jPanel12.add(this.jComboBox1);
        this.jPanel3.add(this.jPanel12);
        this.jPanel13.setLayout(new FlowLayout(0));
        this.jLabel10.setText("SSH strict host checking ");
        this.jPanel13.add(this.jLabel10);
        this.jCheckBox2.setSelected(true);
        this.jPanel13.add(this.jCheckBox2);
        this.jPanel3.add(this.jPanel13);
        this.jScrollPane1.setViewportView(this.jPanel3);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                AllOptionsForm.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                AllOptionsForm.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.resetButton.setText("Reset");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                AllOptionsForm.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.setVisible(true);
        this.dispose();
    }

    private void jTextField1MouseClicked(MouseEvent evt) {
        BackgroundImage tmpimg = new BackgroundImage(null);
        if (kSarConfig.background_image != null) {
            this.jTextField1.setText(kSarConfig.background_image.toString());
        }
    }

    private void jTextField2MouseClicked(MouseEvent evt) {
        SSHIdentity tmpident = new SSHIdentity(null);
        if (kSarConfig.sshidentity != null) {
            this.jTextField2.setText(kSarConfig.sshidentity.toString());
        }
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        Integer tmp = new Integer(this.jSpinner1.getValue().toString());
        kSarConfig.imagewidth = tmp;
        tmp = new Integer(this.jSpinner2.getValue().toString());
        kSarConfig.imageheight = tmp;
        kSarConfig.imagehtml = this.jCheckBox1.isSelected();
        kSarConfig.pdfbottomleft = this.jTextField3.getText();
        kSarConfig.pdfupperright = this.jTextField4.getText();
        kSarConfig.pdfindexpage = this.jTextField5.getText();
        kSarConfig.landf = this.jComboBox1.getSelectedItem().toString();
        kSarConfig.ssh_stricthostchecking = this.jCheckBox2.isSelected();
        kSarConfig.writeDefault();
        this.dispose();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.jSpinner1.setValue(new Integer(kSarConfig.imagewidth));
        this.jSpinner2.setValue(new Integer(kSarConfig.imageheight));
        this.jCheckBox1.setSelected(kSarConfig.imagehtml);
        this.jCheckBox2.setSelected(kSarConfig.ssh_stricthostchecking);
        this.jTextField3.setText(kSarConfig.pdfbottomleft);
        this.jTextField4.setText(kSarConfig.pdfupperright);
        this.jTextField5.setText(kSarConfig.pdfindexpage);
        this.jTextField1.setText(kSarConfig.getBackground_image());
        this.jTextField2.setText(kSarConfig.getBackground_image());
        this.jComboBox1.setSelectedItem(kSarConfig.landf);
    }

    private void load_landf() {
        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        int n = looks.length;
        for (int i = 0; i < n; ++i) {
            String tmp = looks[i].getName();
            this.model.addElement(tmp);
            if (!kSarConfig.landf.equals(tmp)) continue;
            this.jComboBox1.setSelectedItem(tmp);
        }
    }

}

