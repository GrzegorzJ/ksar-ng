/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.JOptionPane;

public class LocalCommand
extends Thread {
    kSar mysar = null;
    InputStream in = null;
    public String command = null;

    public LocalCommand(kSar hissar) {
        this.mysar = hissar;
        try {
            String[] envvar = new String[]{new String("LC_ALL=C")};
            this.command = JOptionPane.showInputDialog("Enter local command ", (Object)"sar -A");
            if (this.command == null) {
                return;
            }
            Process p = Runtime.getRuntime().exec(this.command, envvar);
            this.in = p.getInputStream();
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "There was a problem while running the command ", "Local error", 0);
            this.in = null;
        }
    }

    public LocalCommand(kSar hissar, String hiscommand) {
        this.mysar = hissar;
        this.command = hiscommand;
        try {
            String[] envvar = new String[]{new String("LC_ALL=C")};
            Process p = Runtime.getRuntime().exec(this.command, envvar);
            this.in = p.getInputStream();
        }
        catch (Exception e) {
            if (this.mysar.myUI != null) {
                JOptionPane.showMessageDialog(null, "There was a problem while running the command " + this.command, "Local error", 0);
            } else {
                System.err.println("There was a problem while running the command " + this.command);
            }
            this.in = null;
        }
    }

    public void run() {
        block3 : {
            try {
                if (this.in != null) {
                    BufferedReader myfile = new BufferedReader(new InputStreamReader(this.in));
                    this.mysar.parse(myfile);
                    break block3;
                }
                return;
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public String get_action() {
        return "cmd://" + this.command;
    }
}

