/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartPanel
 *  org.jfree.chart.ChartUtilities
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar;

import java.awt.Color;
import java.awt.Paint;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Second;

public abstract class AllGraph {
    protected JFreeChart mygraph = null;
    protected String Title = "Not Defined";
    protected String GraphLink = null;
    protected kSar mysar = null;
    protected int isEmpty = 1;
    protected int datain = 0;
    protected DefaultMutableTreeNode mynode = null;
    protected boolean notifygraph = false;
    protected long number_of_sample = 0;

    public AllGraph(kSar hissar) {
        this.mysar = hissar;
    }

    public JPanel run(Second g_start, Second g_end) {
        return new ChartPanel(this.getgraph(g_start, g_end));
    }

    public int savePNG(Second g_start, Second g_end, String filename, int width, int height) {
        try {
            ChartUtilities.saveChartAsPNG((File)new File(filename), (JFreeChart)this.makegraph(g_start, g_end), (int)width, (int)height);
        }
        catch (IOException e) {
            System.err.println("Unable to write to : " + filename);
            return -1;
        }
        return 0;
    }

    public int saveJPG(Second g_start, Second g_end, String filename, int width, int height) {
        try {
            ChartUtilities.saveChartAsJPEG((File)new File(filename), (JFreeChart)this.makegraph(g_start, g_end), (int)width, (int)height);
        }
        catch (IOException e) {
            System.err.println("Unable to write to : " + filename);
            return -1;
        }
        return 0;
    }

    public String getGraphTitle() {
        return this.Title + " for " + this.mysar.hostName;
    }

    public String getTitle() {
        return this.Title;
    }

    public int hasdata() {
        return this.datain;
    }

    public String getcheckBoxTitle() {
        return this.Title;
    }

    public String getToolTipText() {
        return this.Title;
    }

    public void setGraphLink(String val) {
        this.GraphLink = val;
    }

    public void setnotifygraph(boolean val) {
        this.notifygraph = val;
    }

    public String getGraphLink() {
        return this.GraphLink;
    }

    public void doclosetrigger() {
    }

    public void cleargraph() {
        this.mygraph = null;
    }

    public JFreeChart getgraph(Second g_start, Second g_end) {
        return this.makegraph(g_start, g_end);
    }

    protected boolean do_notify() {
        if (!this.notifygraph) {
            return false;
        }
        if (this.number_of_sample <= (long)kSarConfig.alwaysrefresh) {
            return true;
        }
        if (this.number_of_sample <= (long)kSarConfig.somerefresh) {
            if (this.number_of_sample % (long)kSarConfig.somerefresh_time == 1) {
                return true;
            }
            return false;
        }
        if (this.number_of_sample <= (long)kSarConfig.lessrefresh) {
            if (this.number_of_sample % (long)kSarConfig.lessrefresh_time == 1) {
                return true;
            }
            return false;
        }
        if (this.number_of_sample >= (long)kSarConfig.norefresh) {
            return false;
        }
        return false;
    }

    public int setbackgroundimage(JFreeChart mychart) {
        if (kSarConfig.background_image != null) {
            ImageIcon icon = new ImageIcon(kSarConfig.background_image.toString());
            if (icon == null) {
                mychart.setBackgroundPaint((Paint)Color.white);
            } else {
                mychart.setBackgroundImage(icon.getImage());
            }
            return 1;
        }
        mychart.setBackgroundPaint((Paint)Color.white);
        return 0;
    }

    public int setbackgroundimage(XYPlot myplot) {
        if (kSarConfig.background_image != null) {
            ImageIcon icon = new ImageIcon(kSarConfig.background_image.toString());
            if (icon == null) {
                myplot.setBackgroundPaint((Paint)Color.white);
            } else {
                myplot.setBackgroundImage(icon.getImage());
            }
            return 1;
        }
        myplot.setBackgroundPaint((Paint)Color.white);
        return 0;
    }

    public abstract JFreeChart makegraph(Second var1, Second var2);
}

