/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.jcraft.jsch.JSch
 *  com.jcraft.jsch.JSchException
 *  com.jcraft.jsch.Session
 *  com.jcraft.jsch.UserInfo
 */
package net.atomique.ksar;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;

public class Wizard
extends JDialog {
    public static final long serialVersionUID = 501;
    private JLabel Errormsg;
    private JButton Ok;
    private JButton Skip;
    private JTextField hostfield;
    private JLabel hostlabel;
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    public JCheckBox openinigraph;
    private JPasswordField passfield;
    private JLabel passlabel;
    private JTextField userfield;
    private JLabel userlabel;
    static final Properties systemprops = System.getProperties();
    kSarDesktop mydesktop;

    public Wizard(Frame parent, boolean modal) {
        super(parent, modal);
        this.mydesktop = (kSarDesktop)parent;
        this.initComponents();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width / 2 - 225, screenSize.height / 2 - 150);
        this.passfield.requestFocus();
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jPanel1 = new JPanel();
        this.jPanel4 = new JPanel();
        this.userlabel = new JLabel();
        this.userfield = new JTextField();
        this.jPanel5 = new JPanel();
        this.passlabel = new JLabel();
        this.passfield = new JPasswordField();
        this.jPanel6 = new JPanel();
        this.hostlabel = new JLabel();
        this.hostfield = new JTextField();
        this.openinigraph = new JCheckBox();
        this.Errormsg = new JLabel();
        this.jPanel2 = new JPanel();
        this.Ok = new JButton();
        this.Skip = new JButton();
        this.setDefaultCloseOperation(2);
        this.setTitle("Unified Authentication");
        this.setModal(true);
        this.jPanel3.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.jPanel3.setLayout(new BorderLayout());
        this.jLabel1.setFont(new Font("Lucida Grande", 0, 24));
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText("Unified Authentication");
        this.jPanel3.add((Component)this.jLabel1, "Center");
        this.getContentPane().add((Component)this.jPanel3, "North");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.jPanel1.setLayout(new GridLayout(5, 0));
        this.userlabel.setText("Username:");
        this.jPanel4.add(this.userlabel);
        this.userfield.setText(kSarConfig.unified_user);
        this.userfield.setPreferredSize(new Dimension(158, 22));
        this.jPanel4.add(this.userfield);
        this.jPanel1.add(this.jPanel4);
        this.passlabel.setText("Password:");
        this.jPanel5.add(this.passlabel);
        this.passfield.setPreferredSize(new Dimension(158, 22));
        this.jPanel5.add(this.passfield);
        this.jPanel1.add(this.jPanel5);
        this.hostlabel.setText("       Host: ");
        this.jPanel6.add(this.hostlabel);
        this.hostfield.setText(kSarConfig.unified_host);
        this.hostfield.setPreferredSize(new Dimension(158, 22));
        this.jPanel6.add(this.hostfield);
        this.jPanel1.add(this.jPanel6);
        this.openinigraph.setSelected(true);
        this.openinigraph.setText("Startup default graph");
        this.openinigraph.setHorizontalAlignment(0);
        this.jPanel1.add(this.openinigraph);
        this.Errormsg.setForeground(Color.red);
        this.Errormsg.setHorizontalAlignment(0);
        this.jPanel1.add(this.Errormsg);
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.Ok.setText("Ok");
        this.Ok.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                Wizard.this.OkActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.Ok);
        this.Skip.setText("Skip");
        this.Skip.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                Wizard.this.SkipActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.Skip);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void SkipActionPerformed(ActionEvent evt) {
        this.mydesktop.unified_user = null;
        this.mydesktop.unified_pass = null;
        this.mydesktop.unified_id = false;
        this.dispose();
    }

    private void OkActionPerformed(ActionEvent evt) {
        boolean retval = this.verifUser();
        if (retval) {
            kSarConfig.unified_user = this.userfield.getText();
            kSarConfig.unified_host = this.hostfield.getText();
            kSarConfig.tile_at_startup = this.openinigraph.isShowing();
            kSarConfig.writeDefault();
            this.mydesktop.unified_user = this.userfield.getText();
            this.mydesktop.unified_pass = new String(this.passfield.getPassword());
            this.mydesktop.unified_id = true;
            this.dispose();
        }
    }

    public boolean verifUser() {
        String userResp = this.userfield.getText();
        String userPass = new String(this.passfield.getPassword());
        String userHost = this.hostfield.getText();
        int port = 22;
        Session session = null;
        String userhome = (String)systemprops.get("user.home") + (String)systemprops.get("file.separator");
        boolean goodLog = false;
        if (this.hostfield.getText().indexOf(58) != -1) {
            userHost = this.hostfield.getText().substring(0, this.hostfield.getText().indexOf(58));
            String s_port = this.hostfield.getText().substring(this.hostfield.getText().indexOf(58) + 1);
            port = Integer.parseInt(s_port);
        }
        JSch jsch = new JSch();
        try {
            session = jsch.getSession(userResp, userHost, port);
        }
        catch (JSchException e) {
            JOptionPane.showInternalMessageDialog(this, "Unable to connect", "Bad user/password identification", 0);
        }
        if (new File(userhome + ".ssh/known_hosts").exists()) {
            try {
                jsch.setKnownHosts(userhome + ".ssh/known_hosts");
            }
            catch (JSchException e) {
                JOptionPane.showInternalMessageDialog(this, "Unable to connect", "Bad user/password identification", 0);
            }
        }
        MyUserInfo ui = new MyUserInfo();
        session.setUserInfo((UserInfo)ui);
        try {
            session.connect();
            if (session.isConnected()) {
                goodLog = true;
                session.disconnect();
            }
        }
        catch (JSchException ee) {
            this.Errormsg.setText("Authentication Error");
            this.Errormsg.setForeground(Color.RED);
            this.Errormsg.setVisible(true);
        }
        return goodLog;
    }

    public class MyUserInfo
    implements UserInfo {
        String password;
        String passphrase;

        public String getPasswd() {
            return this.password;
        }

        public void setPassphrase(String passphrase) {
            this.passphrase = passphrase;
        }

        public boolean promptYesNo(String str) {
            return true;
        }

        public String getPassphrase() {
            return this.passphrase;
        }

        public boolean promptPassphrase(String message) {
            this.passphrase = new String(Wizard.this.passfield.getPassword());
            return true;
        }

        public String getPassword() {
            return this.password;
        }

        public boolean promptPassword(String message) {
            this.password = new String(Wizard.this.passfield.getPassword());
            return true;
        }

        public void showMessage(String message) {
        }
    }

}

