/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OSInfo {
    private String ostype = null;
    private String Hostname = null;
    private String OSversion = null;
    private String Kernel = null;
    private String CpuType = null;
    private String sarStartDate = null;
    private String sarEndDate = null;
    private String MacAddress = null;
    private String Memory = null;
    private String NBDisk = null;
    private String NBCpu = null;
    private String ENT = null;
    private String Detect = null;

    public OSInfo(String s1, String s2) {
        this.ostype = new String(s1);
        this.Detect = new String(s2);
    }

    public void setHostname(String s) {
        this.Hostname = new String(s);
    }

    public void setOSversion(String s) {
        this.OSversion = new String(s);
    }

    public void setKernel(String s) {
        this.Kernel = new String(s);
    }

    public void setCpuType(String s) {
        this.CpuType = new String(s);
    }

    public void setDate(String s) {
        Date dateSimple3;
        Date dateSimple2;
        Date dateSimple1;
        if (this.sarStartDate == null) {
            this.sarStartDate = new String(s);
        }
        if (this.sarEndDate == null) {
            this.sarEndDate = new String(s);
        }
        try {
            dateSimple1 = new SimpleDateFormat("MM/dd/yy").parse(s);
            dateSimple2 = new SimpleDateFormat("MM/dd/yy").parse(this.sarStartDate);
            dateSimple3 = new SimpleDateFormat("MM/dd/yy").parse(this.sarEndDate);
        }
        catch (ParseException e) {
            return;
        }
        if (dateSimple1.compareTo(dateSimple2) < 0) {
            this.sarStartDate = new String(s);
        }
        if (dateSimple1.compareTo(dateSimple3) > 0) {
            this.sarEndDate = new String(s);
        }
    }

    public void setMacAddress(String s) {
        this.MacAddress = new String(s);
    }

    public void setMemory(String s) {
        this.Memory = new String(s);
    }

    public void setNBDisk(String s) {
        this.NBDisk = new String(s);
    }

    public void setNBCpu(String s) {
        this.NBCpu = new String(s);
    }

    public void setENT(String s) {
        this.ENT = new String(s);
    }

    public String getDate() {
        if (this.sarStartDate.equals(this.sarEndDate)) {
            return this.sarStartDate;
        }
        return this.sarStartDate + " to " + this.sarEndDate;
    }

    public String getOSInfo() {
        StringBuffer tmpstr = new StringBuffer();
        tmpstr.append("OS Type: " + this.ostype + " (" + this.Detect + " detected)\n");
        if (this.OSversion != null) {
            tmpstr.append("OS Version: " + this.OSversion + "\n");
        }
        if (this.Kernel != null) {
            tmpstr.append("Kernel Release: " + this.Kernel + "\n");
        }
        if (this.CpuType != null) {
            tmpstr.append("CPU Type: " + this.CpuType + "\n");
        }
        if (this.Hostname != null) {
            tmpstr.append("Hostname: " + this.Hostname + "\n");
        }
        if (this.MacAddress != null) {
            tmpstr.append("Mac Address: " + this.MacAddress + "\n");
        }
        if (this.Memory != null) {
            tmpstr.append("Memory: " + this.Memory + "\n");
        }
        if (this.NBDisk != null) {
            tmpstr.append("Number of disks: " + this.NBDisk + "\n");
        }
        if (this.NBCpu != null) {
            tmpstr.append("Number of CPU: " + this.NBCpu + "\n");
        }
        if (this.ENT != null) {
            tmpstr.append("Ent: " + this.ENT + "\n");
        }
        if (this.sarStartDate != null) {
            tmpstr.append("Start of SAR: " + this.sarStartDate + "\n");
        }
        if (this.sarEndDate != null) {
            tmpstr.append("End of SAR: " + this.sarEndDate + "\n");
        }
        tmpstr.append("\n");
        return tmpstr.toString();
    }
}

