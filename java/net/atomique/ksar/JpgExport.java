/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.time.Second
 *  org.jfree.text.TextUtilities
 */
package net.atomique.ksar;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JDialog;
import javax.swing.JProgressBar;

import org.jfree.text.TextUtilities;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class JpgExport
implements Runnable {
    kSar mysar;
    JProgressBar progressbar;
    Map<String, AllGraph> printMap;
    JDialog mydial;
    String basefilename;
    int width;
    int height;
    boolean dohtml;

    public JpgExport(String p, Map<String, AllGraph> m, JProgressBar g, JDialog d, kSar s, boolean hisdohtml, int hisheight, int hiswidth) {
        this.mysar = s;
        this.mydial = d;
        this.progressbar = g;
        this.printMap = m;
        this.basefilename = p;
        this.width = hiswidth;
        this.height = hisheight;
        this.dohtml = hisdohtml;
    }

    public String crackfilename(String filename) {
        String tmp = new String(filename);
        tmp.replace('/', '_');
        tmp.replace('\\', '_');
        return tmp;
    }

    @Override
    public void run() {
        if (this.basefilename == null) {
            return;
        }
        if (this.printMap.size() < 1) {
            return;
        }
        BufferedWriter out = null;
        if (this.dohtml) {
            try {
                out = new BufferedWriter(new FileWriter(this.basefilename + "_index.html"));
            }
            catch (IOException e) {
                System.err.println("Unable to open file: " + this.basefilename + "_index.html");
                out = null;
            }
        }
        if (out != null) {
            try {
                out.write("<HTML><HEAD><title>kSar : " + this.mysar.hostName + "</title></HEAD><body bgcolor='#ffffff' link='#000000' vlink='#000000'><div align='center' valign='center'>");
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        TextUtilities.setUseDrawRotatedStringWorkaround((boolean)false);
        TreeMap<String, AllGraph> tmphash = new TreeMap<String, AllGraph>();
        tmphash.putAll(this.printMap);
        int progressint = 0;
        for (String key : tmphash.keySet()) {
            AllGraph value = (AllGraph)tmphash.get(key);
            if (this.progressbar != null) {
                this.progressbar.setValue(++progressint);
                this.progressbar.repaint();
            }
            String name = new File(this.basefilename + "_" + this.crackfilename(new StringBuilder().append(key).append(".jpg").toString())).getName();
            value.saveJPG(this.mysar.startofgraph, this.mysar.endofgraph, this.basefilename + "_" + this.crackfilename(new StringBuilder().append(key).append(".jpg").toString()), this.width, this.height);
            if (out == null) continue;
            try {
                out.write("<img src='" + name + "'>");
            }
            catch (IOException e) {}
        }
        if (out != null) {
            try {
                out.write("</div></body></html>");
                out.close();
            }
            catch (IOException e) {
                // empty catch block
            }
        }
        if (this.mydial != null) {
            this.mydial.dispose();
        }
    }
}

