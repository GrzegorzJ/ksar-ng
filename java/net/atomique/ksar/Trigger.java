/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.plot.IntervalMarker
 *  org.jfree.chart.plot.Marker
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.ui.Layer
 */
package net.atomique.ksar;

import java.awt.Color;
import java.awt.Paint;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.ui.Layer;

public class Trigger {
    Double triggervalue = new Double(0.0);
    kSar mysar;
    AllGraph mygraph;
    TimeSeries myseries;
    double tmpnum;
    String myway = new String("none");
    Double lasttriggervalue = new Double(0.0);
    Second tmpsec;
    Second debutmarker = null;
    Second finmarker = null;
    String comment = "";
    List<IntervalMarker> WarningList = new ArrayList<IntervalMarker>();

    public Trigger(kSar hissar, AllGraph hisgraph, TimeSeries hisseries, String hisway) {
        this.mysar = hissar;
        this.mygraph = hisgraph;
        this.myseries = hisseries;
        this.myway = hisway;
    }

    public Trigger(kSar hissar, AllGraph hisgraph, String s, TimeSeries hisseries, String hisway) {
        this.mysar = hissar;
        this.mygraph = hisgraph;
        this.comment = s;
        this.myseries = hisseries;
        this.myway = hisway;
    }

    public void doMarker(Second now, Number v) {
        if ("up".equals(this.myway)) {
            this.doMarkerUP(now, v);
        }
        if ("down".equals(this.myway)) {
            this.doMarkerDOWN(now, v);
        }
    }

    public void doclose() {
        if ("up".equals(this.myway)) {
            this.closeup();
        }
        if ("down".equals(this.myway)) {
            this.closedown();
        }
    }

    private void doMarkerUP(Second now, Number v) {
        this.lasttriggervalue = this.triggervalue;
        if (v.doubleValue() >= this.triggervalue) {
            if (this.debutmarker == null) {
                this.debutmarker = now;
                this.finmarker = now;
            } else {
                this.finmarker = now;
            }
        } else {
            if (this.debutmarker == null) {
                this.debutmarker = null;
                this.finmarker = null;
                return;
            }
            this.closeup();
        }
    }

    private void closeup() {
        if (this.debutmarker != this.finmarker) {
            IntervalMarker intervalobj = new IntervalMarker((double)this.debutmarker.getFirstMillisecond(), (double)this.finmarker.getLastMillisecond());
            intervalobj.setPaint((Paint)new Color(200, 200, 200));
            this.WarningList.add(intervalobj);
            this.mysar.DetectedBounds.put(this.mygraph.getcheckBoxTitle() + " " + this.comment + " over " + this.triggervalue, this.mygraph);
        }
        this.debutmarker = null;
        this.finmarker = null;
    }

    private void doMarkerDOWN(Second now, Number v) {
        this.lasttriggervalue = this.triggervalue;
        if (v.doubleValue() <= this.triggervalue) {
            if (this.debutmarker == null) {
                this.debutmarker = now;
                this.finmarker = now;
            } else {
                this.finmarker = now;
            }
        } else {
            this.closedown();
        }
    }

    private void closedown() {
        if (this.debutmarker != this.finmarker) {
            IntervalMarker intervalobj = new IntervalMarker((double)this.debutmarker.getFirstMillisecond(), (double)this.finmarker.getLastMillisecond());
            intervalobj.setPaint((Paint)new Color(200, 200, 200));
            this.WarningList.add(intervalobj);
            this.mysar.DetectedBounds.put(this.mygraph.getcheckBoxTitle() + " " + this.comment + " under " + this.triggervalue, this.mygraph);
        }
        this.debutmarker = null;
        this.finmarker = null;
    }

    public void tagMarker(XYPlot myplot) {
        if (!this.mysar.showtrigger) {
            return;
        }
        if (this.WarningList.size() > 0) {
            ListIterator<IntervalMarker> listItr = this.WarningList.listIterator();
            while (listItr.hasNext()) {
                IntervalMarker value = listItr.next();
                myplot.addDomainMarker((Marker)value, Layer.BACKGROUND);
            }
        }
    }

    private void refresh() {
        if (!this.mysar.showtrigger) {
            return;
        }
        if (this.lasttriggervalue.doubleValue() == this.triggervalue.doubleValue()) {
            return;
        }
        this.debutmarker = null;
        this.finmarker = null;
        this.mysar.DetectedBounds.remove(this.mygraph.getcheckBoxTitle() + " " + this.comment + " under " + this.lasttriggervalue);
        this.mysar.DetectedBounds.remove(this.mygraph.getcheckBoxTitle() + " " + this.comment + " over " + this.lasttriggervalue);
        this.lasttriggervalue = this.triggervalue;
        this.WarningList.clear();
        int count = this.myseries.getItemCount();
        for (int i = 0; i < count; ++i) {
            this.tmpnum = this.myseries.getValue(i).doubleValue();
            this.tmpsec = new Second(this.myseries.getTimePeriod(i).getStart());
            if (this.myway.equals("down")) {
                if (this.tmpnum <= this.triggervalue) {
                    if (this.debutmarker == null) {
                        this.debutmarker = this.tmpsec;
                        this.finmarker = this.tmpsec;
                    } else {
                        this.finmarker = this.tmpsec;
                    }
                } else {
                    if (this.debutmarker == null) {
                        this.debutmarker = null;
                        this.finmarker = null;
                        continue;
                    }
                    this.closedown();
                }
            }
            if (!this.myway.equals("up")) continue;
            if (this.tmpnum >= this.triggervalue) {
                if (this.debutmarker == null) {
                    this.debutmarker = this.tmpsec;
                    this.finmarker = this.tmpsec;
                    continue;
                }
                this.finmarker = this.tmpsec;
                continue;
            }
            if (this.debutmarker == null) {
                this.debutmarker = null;
                this.finmarker = null;
                continue;
            }
            this.closeup();
        }
        if (this.myway.equals("down")) {
            if (this.debutmarker == null) {
                this.debutmarker = null;
                this.finmarker = null;
            } else {
                this.closedown();
            }
        }
        if (this.myway.equals("up")) {
            if (this.debutmarker == null) {
                this.debutmarker = null;
                this.finmarker = null;
            } else {
                this.closeup();
            }
        }
    }

    public void setComment(String s) {
        this.comment = s;
    }

    public void setTriggerValue(double d) {
        this.triggervalue = new Double(d);
        this.refresh();
    }

    public void setTriggerValue(Double d) {
        this.triggervalue = d;
        this.refresh();
    }
}

