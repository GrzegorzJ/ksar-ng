/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;

public class BackgroundImage {
    private final kSarDesktop mydesktop;

    public BackgroundImage(kSarDesktop hisdesktop) {
        int returnVal;
        this.mydesktop = hisdesktop;
        String filename = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose your background image file");
        chooser.setFileHidingEnabled(true);
        if (kSarConfig.background_image != null) {
            chooser.setCurrentDirectory(kSarConfig.background_image);
        }
        if ((returnVal = chooser.showOpenDialog(this.mydesktop)) == 0) {
            filename = chooser.getSelectedFile().getAbsolutePath();
            File tmp = new File(filename);
            if (tmp.exists()) {
                kSarConfig.background_image = tmp;
                kSarConfig.writeDefault();
            } else {
                filename = null;
            }
        } else {
            kSarConfig.background_image = null;
            kSarConfig.writeDefault();
        }
    }
}

