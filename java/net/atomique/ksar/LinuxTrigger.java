/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;

public class LinuxTrigger
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private JButton cancelButton;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JPanel jPanel1;
    private JPanel jPanel10;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JPanel jPanel9;
    private JScrollPane jScrollPane1;
    private JTextField jTextField1;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JButton okButton;
    private JButton resetButton;

    public LinuxTrigger() {
        this.initComponents();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jPanel3 = new JPanel();
        this.jPanel7 = new JPanel();
        this.jLabel4 = new JLabel();
        this.jTextField3 = new JTextField();
        this.jPanel8 = new JPanel();
        this.jLabel5 = new JLabel();
        this.jTextField4 = new JTextField();
        this.jPanel9 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jTextField5 = new JTextField();
        this.jPanel10 = new JPanel();
        this.jLabel7 = new JLabel();
        this.jTextField1 = new JTextField();
        this.jPanel2 = new JPanel();
        this.okButton = new JButton();
        this.cancelButton = new JButton();
        this.resetButton = new JButton();
        this.setClosable(true);
        this.setTitle("Options");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new BoxLayout(this.jPanel3, 1));
        this.jPanel7.setLayout(new FlowLayout(0));
        this.jLabel4.setText("CPU idle minimum:");
        this.jPanel7.add(this.jLabel4);
        this.jTextField3.setText(kSarConfig.linuxcpuidletrigger.toString());
        this.jTextField3.setMinimumSize(new Dimension(150, 22));
        this.jTextField3.setPreferredSize(new Dimension(150, 22));
        this.jPanel7.add(this.jTextField3);
        this.jPanel3.add(this.jPanel7);
        this.jPanel8.setLayout(new FlowLayout(0));
        this.jLabel5.setText("CPU system maximum: ");
        this.jPanel8.add(this.jLabel5);
        this.jTextField4.setText(kSarConfig.linuxcpusystemtrigger.toString());
        this.jTextField4.setMinimumSize(new Dimension(150, 22));
        this.jTextField4.setPreferredSize(new Dimension(150, 22));
        this.jPanel8.add(this.jTextField4);
        this.jPanel3.add(this.jPanel8);
        this.jPanel9.setLayout(new FlowLayout(0));
        this.jLabel6.setText("CPU I/O maximum");
        this.jPanel9.add(this.jLabel6);
        this.jTextField5.setText(kSarConfig.linuxcpuwiotrigger.toString());
        this.jTextField5.setMinimumSize(new Dimension(150, 22));
        this.jTextField5.setPreferredSize(new Dimension(150, 22));
        this.jPanel9.add(this.jTextField5);
        this.jPanel3.add(this.jPanel9);
        this.jPanel10.setLayout(new FlowLayout(0));
        this.jLabel7.setText("CPU user maximum");
        this.jPanel10.add(this.jLabel7);
        this.jTextField1.setEditable(false);
        this.jTextField1.setText(kSarConfig.linuxcpuusrtrigger.toString());
        this.jTextField1.setMinimumSize(new Dimension(150, 22));
        this.jTextField1.setPreferredSize(new Dimension(150, 22));
        this.jPanel10.add(this.jTextField1);
        this.jPanel3.add(this.jPanel10);
        this.jScrollPane1.setViewportView(this.jPanel3);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LinuxTrigger.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LinuxTrigger.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.resetButton.setText("Reset");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LinuxTrigger.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.setVisible(true);
        this.dispose();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.jTextField3.setText(kSarConfig.linuxcpuidletrigger.toString());
        this.jTextField4.setText(kSarConfig.linuxcpusystemtrigger.toString());
        this.jTextField5.setText(kSarConfig.linuxcpuwiotrigger.toString());
        this.jTextField1.setText(kSarConfig.linuxcpuusrtrigger.toString());
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        kSarConfig.linuxcpuidletrigger = new Double(this.jTextField3.getText());
        kSarConfig.linuxcpusystemtrigger = new Double(this.jTextField4.getText());
        kSarConfig.linuxcpuwiotrigger = new Double(this.jTextField5.getText());
        kSarConfig.linuxcpuusrtrigger = new Double(this.jTextField1.getText());
        kSarConfig.writeDefault();
        this.setVisible(true);
        this.dispose();
    }

}

