/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.lowagie.text.Document
 *  com.lowagie.text.DocumentException
 *  com.lowagie.text.Element
 *  com.lowagie.text.ExceptionConverter
 *  com.lowagie.text.FontFactory
 *  com.lowagie.text.Image
 *  com.lowagie.text.PageSize
 *  com.lowagie.text.Paragraph
 *  com.lowagie.text.Rectangle
 *  com.lowagie.text.pdf.BaseFont
 *  com.lowagie.text.pdf.DefaultFontMapper
 *  com.lowagie.text.pdf.FontMapper
 *  com.lowagie.text.pdf.MultiColumnText
 *  com.lowagie.text.pdf.PdfContentByte
 *  com.lowagie.text.pdf.PdfDestination
 *  com.lowagie.text.pdf.PdfGState
 *  com.lowagie.text.pdf.PdfOutline
 *  com.lowagie.text.pdf.PdfPTable
 *  com.lowagie.text.pdf.PdfPageEvent
 *  com.lowagie.text.pdf.PdfPageEventHelper
 *  com.lowagie.text.pdf.PdfTemplate
 *  com.lowagie.text.pdf.PdfWriter
 *  org.jfree.chart.ChartRenderingInfo
 *  org.jfree.chart.JFreeChart
 *  org.jfree.data.time.Second
 *  org.jfree.text.TextUtilities
 */
package net.atomique.ksar;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.FontMapper;
import com.lowagie.text.pdf.MultiColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfOutline;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JDialog;
import javax.swing.JProgressBar;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.text.TextUtilities;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class PdfExport
extends PdfPageEventHelper
implements Runnable {
    private int pdfheight = 842;
    private int pdfwidth = 595;
    private int pdfmargins = 10;
    int height = this.pdfheight - 2 * this.pdfmargins;
    int width = this.pdfwidth - 2 * this.pdfmargins;
    PdfTemplate tp;
    Graphics2D g2d;
    Rectangle2D r2d;
    kSar mysar;
    JProgressBar progressbar;
    String pdffilename;
    Map<String, AllGraph> printMap;
    JDialog mydial;
    private PdfContentByte cb;
    public Image headerImage;
    public PdfPTable table;
    public PdfGState gstate;
    public PdfTemplate tpl;
    public BaseFont helv;
    public int totalpages = 0;
    ChartRenderingInfo chartinfo = null;
    FontMapper mapper = new DefaultFontMapper();
    BaseFont bf = FontFactory.getFont((String)"Courier").getCalculatedBaseFont(false);

    public PdfExport(String p, Map<String, AllGraph> m, JProgressBar g, JDialog d, kSar s) {
        this.mysar = s;
        this.mydial = d;
        this.progressbar = g;
        this.printMap = m;
        this.pdffilename = p;
    }

    @Override
    public void run() {
        PdfWriter writer = null;
        if (this.pdffilename == null) {
            return;
        }
        if (this.printMap.size() < 1) {
            return;
        }
        TextUtilities.setUseDrawRotatedStringWorkaround((boolean)true);
        Document document = new Document(PageSize.A4.rotate());
        try {
            PdfOutline kerneltree = null;
            PdfOutline disktree = null;
            PdfOutline iftree = null;
            PdfOutline cputree = null;
            PdfOutline filetree = null;
            PdfOutline scalltree = null;
            PdfOutline msgtree = null;
            PdfOutline cswchtree = null;
            TreeMap<String, AllGraph> tmphash = new TreeMap<String, AllGraph>();
            tmphash.putAll(this.printMap);
            writer = PdfWriter.getInstance((Document)document, (OutputStream)new FileOutputStream(this.pdffilename));
            writer.setPageEvent((PdfPageEvent)this);
            writer.setCompressionLevel(0);
            document.addTitle("kSar Grapher");
            document.addSubject("Sar output of " + this.mysar.hostName);
            document.addKeywords("http://ksar.atomique.net/ ");
            document.addKeywords(this.mysar.hostName);
            document.addCreator("kSar Version:" + VersionNumber.getVersionNumber());
            document.addAuthor("Xavier cherif");
            document.open();
            this.cb = writer.getDirectContent();
            PdfOutline root = this.cb.getRootOutline();
            this.totalpages = tmphash.size() + 1;
            this.IndexPage(writer, document);
            this.TriggerPage(writer, document);
            int progressint = 0;
            for (String key : tmphash.keySet()) {
                PdfOutline thisdiskxf;
                String diskn;
                PdfOutline thisdiskwt;
                PdfOutline thisdisk;
                AllGraph value2;
                PdfOutline thisfile;
                AllGraph value = (AllGraph)tmphash.get(key);
                if (this.progressbar != null) {
                    this.progressbar.setValue(++progressint);
                    this.progressbar.repaint();
                }
                if (key.indexOf("wait") >= 0) {
                    value = null;
                    continue;
                }
                if (key.indexOf("-t2") >= 0) {
                    value = null;
                    continue;
                }
                if (key.indexOf("-if2") >= 0) {
                    value = null;
                    continue;
                }
                if (key.indexOf("-t1") >= 0) {
                    diskn = key.substring(0, key.length() - 3);
                    value2 = (AllGraph)tmphash.get(diskn + "-t2");
                    if (!this.mysar.showemptydisk) {
                        if (value2 != null) {
                            if (value2.hasdata() == 0 && value.hasdata() == 0) {
                                value2 = null;
                                value = null;
                                continue;
                            }
                        } else if (value.hasdata() == 0) {
                            value2 = null;
                            value = null;
                            continue;
                        }
                    }
                    if (disktree == null) {
                        disktree = new PdfOutline(root, new PdfDestination(1), "Disks");
                    }
                    thisdisk = null;
                    thisdisk = this.mysar.Adiskname.get(diskn) != null ? new PdfOutline(disktree, new PdfDestination(1), this.mysar.Adiskname.get(diskn)) : new PdfOutline(disktree, new PdfDestination(1), diskn);
                    this.addchart(writer, value);
                    thisdiskxf = new PdfOutline(thisdisk, new PdfDestination(1), value.getTitle());
                    if (value2 != null) {
                        document.newPage();
                        thisdiskwt = new PdfOutline(thisdisk, new PdfDestination(1), value2.getTitle());
                        this.addchart(writer, value2);
                    }
                    value2 = null;
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("-if1") >= 0) {
                    String ifn = key.substring(0, key.length() - 4);
                    if (iftree == null) {
                        iftree = new PdfOutline(root, new PdfDestination(1), "Interfaces");
                    }
                    PdfOutline thisif = new PdfOutline(iftree, new PdfDestination(1), ifn);
                    this.addchart(writer, value);
                    PdfOutline thisif1 = new PdfOutline(thisif, new PdfDestination(1), value.getTitle());
                    AllGraph value22 = (AllGraph)tmphash.get(ifn + "-if2");
                    if (value22 != null) {
                        document.newPage();
                        PdfOutline thisif2 = new PdfOutline(thisif, new PdfDestination(1), value22.getTitle());
                        this.addchart(writer, value22);
                    }
                    value22 = null;
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("-cpu") >= 0) {
                    String cpun = key.substring(0, key.length() - 4);
                    if (cputree == null) {
                        cputree = new PdfOutline(root, new PdfDestination(1), "Cpus");
                    }
                    PdfOutline thiscpu = new PdfOutline(cputree, new PdfDestination(1), cpun);
                    this.addchart(writer, value);
                    document.newPage();
                    value = null;
                    continue;
                }
                if (key.indexOf("-file") >= 0) {
                    String filen = key.substring(0, key.length() - 5);
                    if (filetree == null) {
                        filetree = new PdfOutline(root, new PdfDestination(1), "Files");
                    }
                    thisfile = new PdfOutline(filetree, new PdfDestination(1), filen);
                    this.addchart(writer, value);
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("-scall") >= 0) {
                    String scalln = key.substring(0, key.length() - 6);
                    if (scalltree == null) {
                        scalltree = new PdfOutline(root, new PdfDestination(1), "Syscalls");
                    }
                    thisfile = new PdfOutline(scalltree, new PdfDestination(1), scalln);
                    this.addchart(writer, value);
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("-cswch") >= 0) {
                    String cswchn = key.substring(0, key.length() - 6);
                    if (cswchtree == null) {
                        cswchtree = new PdfOutline(root, new PdfDestination(1), "Contexts");
                    }
                    thisfile = new PdfOutline(cswchtree, new PdfDestination(1), cswchn);
                    this.addchart(writer, value);
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("-msg") >= 0) {
                    String msgn = key.substring(0, key.length() - 4);
                    if (msgtree == null) {
                        msgtree = new PdfOutline(root, new PdfDestination(1), "Messages & Semaphores");
                    }
                    thisfile = new PdfOutline(msgtree, new PdfDestination(1), msgn);
                    this.addchart(writer, value);
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("Solarisxfer") >= 0) {
                    diskn = key.substring(0, key.length() - 11);
                    value2 = (AllGraph)tmphash.get(diskn + "Solariswait");
                    if (!this.mysar.showemptydisk && value2.hasdata() == 0 && value.hasdata() == 0) {
                        value2 = null;
                        value = null;
                        continue;
                    }
                    if (disktree == null) {
                        disktree = new PdfOutline(root, new PdfDestination(1), "Disks");
                    }
                    thisdisk = null;
                    thisdisk = this.mysar.Adiskname.get(diskn) != null ? new PdfOutline(disktree, new PdfDestination(1), this.mysar.Adiskname.get(diskn)) : new PdfOutline(disktree, new PdfDestination(1), diskn);
                    this.addchart(writer, value);
                    thisdiskxf = new PdfOutline(thisdisk, new PdfDestination(1), value.getTitle());
                    if (value2 != null) {
                        document.newPage();
                        thisdiskwt = new PdfOutline(thisdisk, new PdfDestination(1), value2.getTitle());
                        this.addchart(writer, value2);
                    }
                    value2 = null;
                    value = null;
                    document.newPage();
                    continue;
                }
                if (key.indexOf("Hpuxxfer") >= 0) {
                    diskn = key.substring(0, key.length() - 8);
                    value2 = (AllGraph)tmphash.get(diskn + "Hpuxwait");
                    if (!this.mysar.showemptydisk && value2.hasdata() == 0 && value.hasdata() == 0) {
                        value2 = null;
                        value = null;
                        continue;
                    }
                    if (disktree == null) {
                        disktree = new PdfOutline(root, new PdfDestination(1), "Disks");
                    }
                    thisdisk = null;
                    thisdisk = this.mysar.Adiskname.get(diskn) != null ? new PdfOutline(disktree, new PdfDestination(1), this.mysar.Adiskname.get(diskn)) : new PdfOutline(disktree, new PdfDestination(1), diskn);
                    this.addchart(writer, value);
                    thisdiskxf = new PdfOutline(thisdisk, new PdfDestination(1), value.getTitle());
                    if (value2 != null) {
                        document.newPage();
                        thisdiskwt = new PdfOutline(thisdisk, new PdfDestination(1), value2.getTitle());
                        this.addchart(writer, value2);
                    }
                    document.newPage();
                    value2 = null;
                    value = null;
                    continue;
                }
                if (key.indexOf("Aixxfer") >= 0) {
                    diskn = key.substring(0, key.length() - 7);
                    value2 = (AllGraph)tmphash.get(diskn + "Aixwait");
                    if (!this.mysar.showemptydisk && value2.hasdata() == 0 && value.hasdata() == 0) {
                        value2 = null;
                        value = null;
                        continue;
                    }
                    if (disktree == null) {
                        disktree = new PdfOutline(root, new PdfDestination(1), "Disks");
                    }
                    thisdisk = null;
                    thisdisk = this.mysar.Adiskname.get(diskn) != null ? new PdfOutline(disktree, new PdfDestination(1), this.mysar.Adiskname.get(diskn)) : new PdfOutline(disktree, new PdfDestination(1), diskn);
                    this.addchart(writer, value);
                    thisdiskxf = new PdfOutline(thisdisk, new PdfDestination(1), value.getTitle());
                    if (value2 != null) {
                        document.newPage();
                        thisdiskwt = new PdfOutline(thisdisk, new PdfDestination(1), value2.getTitle());
                        this.addchart(writer, value2);
                    }
                    document.newPage();
                    value2 = null;
                    value = null;
                    continue;
                }
                if (key.equals("SolariskmasmlSar")) {
                    this.addchart(writer, value);
                    if (kerneltree == null) {
                        kerneltree = new PdfOutline(root, new PdfDestination(1), "Kernel");
                    }
                    PdfOutline out13 = new PdfOutline(kerneltree, new PdfDestination(1), value.getTitle());
                    document.newPage();
                    value = null;
                    continue;
                }
                if (key.equals("SolariskmalgSar")) {
                    this.addchart(writer, value);
                    if (kerneltree == null) {
                        kerneltree = new PdfOutline(root, new PdfDestination(1), "Kernel");
                    }
                    PdfOutline out14 = new PdfOutline(kerneltree, new PdfDestination(1), value.getTitle());
                    document.newPage();
                    value = null;
                    continue;
                }
                if (key.equals("SolariskmaovzSar")) {
                    this.addchart(writer, value);
                    if (kerneltree == null) {
                        kerneltree = new PdfOutline(root, new PdfDestination(1), "Kernel");
                    }
                    PdfOutline out15 = new PdfOutline(kerneltree, new PdfDestination(1), value.getTitle());
                    document.newPage();
                    value = null;
                    continue;
                }
                this.addchart(writer, value);
                PdfOutline out1 = new PdfOutline(root, new PdfDestination(1), value.getTitle());
                document.newPage();
                value = null;
            }
        }
        catch (DocumentException de) {
            System.err.println("Unable to write to : " + this.pdffilename);
            return;
        }
        catch (IOException ioe) {
            System.err.println("Unable to write to : " + this.pdffilename);
            return;
        }
        document.close();
        if (this.mydial != null) {
            this.mydial.dispose();
        }
    }

    public void onEndPage(PdfWriter writer, Document document) {
        try {
            String text = "Page " + writer.getPageNumber() + "/" + this.totalpages;
            this.cb.beginText();
            this.cb.setFontAndSize(this.bf, 10.0f);
            this.cb.setColorFill(new Color(0, 0, 0));
            this.cb.showTextAligned(2, text, (float)(this.pdfheight - this.pdfmargins - 10), (float)(10 + this.pdfmargins), 0.0f);
            this.cb.showTextAligned(0, kSarConfig.pdfbottomleft, (float)(10 + this.pdfmargins), (float)(10 + this.pdfmargins), 0.0f);
            this.cb.showTextAligned(2, kSarConfig.pdfupperright, (float)(this.pdfheight - this.pdfmargins - 10), (float)(this.pdfwidth - this.pdfmargins - 10), 0.0f);
            this.cb.endText();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public void IndexPage(PdfWriter writer, Document document) {
        try {
            String title = new String("SAR Statistics");
            String t_host = new String("For " + this.mysar.hostName);
            String t_date = new String("On " + this.mysar.myOS.getDate());
            this.cb.beginText();
            this.cb.setFontAndSize(this.bf, 48.0f);
            this.cb.setColorFill(new Color(0, 0, 0));
            this.cb.showTextAligned(1, title, (float)((this.pdfheight - this.pdfmargins) / 2), 500.0f, 0.0f);
            this.cb.setFontAndSize(this.bf, 36.0f);
            this.cb.showTextAligned(1, t_host, (float)((this.pdfheight - this.pdfmargins) / 2), 400.0f, 0.0f);
            this.cb.showTextAligned(1, t_date, (float)((this.pdfheight - this.pdfmargins) / 2), 300.0f, 0.0f);
            this.cb.showTextAligned(1, kSarConfig.pdfindexpage, (float)((this.pdfheight - this.pdfmargins) / 2), 150.0f, 0.0f);
            this.cb.endText();
            document.newPage();
        }
        catch (Exception de) {
            return;
        }
    }

    public void TriggerPage(PdfWriter writer, Document document) {
        if (!this.mysar.showtrigger) {
            return;
        }
        if (this.mysar.DetectedBounds.size() < 1) {
            return;
        }
        try {
            MultiColumnText mct = new MultiColumnText();
            mct.addRegularColumns(document.left(), document.right(), 10.0f, 4);
            String title = "Detected Bottlenecks: ";
            this.cb.beginText();
            this.cb.setFontAndSize(this.bf, 20.0f);
            this.cb.setColorFill(new Color(0, 0, 0));
            this.cb.showTextAligned(1, title, (float)((this.pdfheight - this.pdfmargins) / 2), (float)(this.pdfwidth - this.pdfmargins - 10), 0.0f);
            for (String key : this.mysar.DetectedBounds.keySet()) {
                mct.addElement((Element)new Paragraph(key));
            }
            document.add((Element)mct);
            document.newPage();
        }
        catch (DocumentException de) {
            return;
        }
    }

    public int addchart(PdfWriter writer, AllGraph graph) {
        JFreeChart chart = graph.getgraph(this.mysar.startofgraph, this.mysar.endofgraph);
        this.tp = this.cb.createTemplate((float)this.height, (float)this.width);
        this.g2d = this.tp.createGraphics((float)this.height, (float)this.width, this.mapper);
        this.r2d = new Rectangle2D.Double(0.0, 0.0, this.height, this.width);
        chart.draw(this.g2d, this.r2d, this.chartinfo);
        graph.cleargraph();
        this.g2d.dispose();
        this.cb.addTemplate(this.tp, (float)this.pdfmargins, (float)this.pdfmargins);
        try {
            writer.releaseTemplate(this.tp);
        }
        catch (IOException ioe) {
            System.err.println("Unable to write to : " + this.pdffilename);
        }
        return 0;
    }
}

