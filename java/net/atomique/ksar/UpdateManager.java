/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.atomique.ksar.VersionNumber;

public class UpdateManager
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private String updateVersion = new String();
    private String updateString = new String();
    private String str = null;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;
    private JTextArea jTextArea1;
    private JButton okButton;

    public UpdateManager() {
        this.loadVersion();
        if (this.updateString == null) {
            return;
        }
        this.initComponents();
        this.jLabel1.setText("your version: " + VersionNumber.getVersionNumber());
        this.jLabel2.setText("Available version: " + this.updateVersion);
        if (VersionNumber.isOlderThan(this.updateVersion)) {
            this.jLabel3.setText("A new version is available");
            this.jLabel3.setForeground(Color.red);
        } else {
            this.jLabel3.setText("Cool, you are update to date");
        }
        this.jTextArea1.setText(this.updateString);
        this.jTextArea1.setEditable(false);
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jTextArea1 = new JTextArea();
        this.okButton = new JButton();
        this.setMinimumSize(new Dimension(200, 150));
        this.setName("Check for update");
        this.setPreferredSize(new Dimension(400, 325));
        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), 3));
        this.jPanel3.setLayout(new BoxLayout(this.jPanel3, 3));
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText("jLabel1");
        this.jLabel1.setHorizontalTextPosition(0);
        this.jPanel3.add(this.jLabel1);
        this.jLabel2.setHorizontalAlignment(0);
        this.jLabel2.setText("jLabel2");
        this.jPanel3.add(this.jLabel2);
        this.jLabel3.setHorizontalAlignment(0);
        this.jLabel3.setText("jLabel3");
        this.jPanel3.add(this.jLabel3);
        this.getContentPane().add(this.jPanel3);
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel2.setLayout(new BorderLayout());
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setRows(5);
        this.jTextArea1.setText("test");
        this.jScrollPane1.setViewportView(this.jTextArea1);
        this.jPanel2.add((Component)this.jScrollPane1, "Center");
        this.jPanel1.add((Component)this.jPanel2, "Center");
        this.okButton.setText("OK");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                UpdateManager.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel1.add((Component)this.okButton, "South");
        this.getContentPane().add(this.jPanel1);
        this.pack();
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    private void loadVersion() {
        String patternStr = "^Version:(.*)$";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher("");
        try {
            URL url = new URL("http://ksar.atomique.net/updater2?" + VersionNumber.getVersionNumber());
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((this.str = in.readLine()) != null) {
                matcher.reset(this.updateString);
                if (matcher.find()) {
                    this.updateVersion = matcher.group(1);
                }
                this.updateString = this.updateString.concat(this.str + "\n");
            }
            in.close();
        }
        catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "There was a problem while checking for updates", "Update error", 0);
            return;
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "There was a problem while checking for updates", "Update error", 0);
            return;
        }
    }

}

