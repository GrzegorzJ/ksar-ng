/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.ChartPanel
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.LegendItemSource
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.block.Arrangement
 *  org.jfree.chart.block.Block
 *  org.jfree.chart.block.BlockContainer
 *  org.jfree.chart.block.BorderArrangement
 *  org.jfree.chart.block.EmptyBlock
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.chart.title.CompositeTitle
 *  org.jfree.chart.title.LegendTitle
 *  org.jfree.chart.title.Title
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 *  org.jfree.ui.RectangleEdge
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.Arrangement;
import org.jfree.chart.block.Block;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.block.EmptyBlock;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

public class OtherGraph
extends AllGraph {
    TimeSeriesCollection xydataset1 = new TimeSeriesCollection();
    TimeSeriesCollection xydataset2 = new TimeSeriesCollection();
    JComboBox jComboBox1 = new JComboBox();
    JComboBox jComboBox2 = new JComboBox();
    JButton jButton1 = new JButton();
    String oldseries1 = new String("None");
    String oldseries2 = new String("None");
    JPanel mypanel = null;

    public OtherGraph(kSar hissar) {
        super(hissar);
        this.Title = new String("Specific Graph");
        this.initComponents();
        this.initCombo();
    }

    public void initComponents() {
        this.jComboBox1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                OtherGraph.this.jComboBox1ActionPerformed(evt);
            }
        });
        this.jComboBox2.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                OtherGraph.this.jComboBox2ActionPerformed(evt);
            }
        });
        this.jButton1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                OtherGraph.this.jButton1ActionPerformed(evt);
            }
        });
    }

    private void jComboBox1ActionPerformed(ActionEvent evt) {
        JComboBox combo = (JComboBox)evt.getSource();
        String key = (String)combo.getSelectedItem();
        if ("None".equals(key)) {
            if (!"None".equals(this.oldseries1)) {
                this.xydataset1.removeSeries(this.mysar.dispo.get(this.oldseries1));
            }
            return;
        }
        if (!"None".equals(this.oldseries1)) {
            this.xydataset1.removeSeries(this.mysar.dispo.get(this.oldseries1));
        }
        this.xydataset1.addSeries(this.mysar.dispo.get(key));
        this.oldseries1 = key;
    }

    private void jComboBox2ActionPerformed(ActionEvent evt) {
        JComboBox combo = (JComboBox)evt.getSource();
        String key = (String)combo.getSelectedItem();
        if ("None".equals(key)) {
            if (!"None".equals(this.oldseries2)) {
                this.xydataset2.removeSeries(this.mysar.dispo.get(this.oldseries2));
            }
            return;
        }
        if (!"None".equals(this.oldseries2)) {
            this.xydataset2.removeSeries(this.mysar.dispo.get(this.oldseries2));
        }
        this.xydataset2.addSeries(this.mysar.dispo.get(key));
        this.oldseries2 = key;
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        this.mysar.myUI.remove2tree(this.mynode);
        this.mysar.myUI.home2tree();
        this.mysar.pdfList.remove("xXx");
    }

    public void initCombo() {
        this.jComboBox1.addItem("None");
        this.jComboBox2.addItem("None");
        TreeMap<String, TimeSeries> tmphash = new TreeMap<String, TimeSeries>();
        tmphash.putAll(this.mysar.dispo);
        for (String key : tmphash.keySet()) {
            this.jComboBox1.addItem(key);
            this.jComboBox2.addItem(key);
        }
    }

    public JPanel run(Second g_start, Second g_end) {
        this.mypanel = new JPanel();
        JPanel jPanel2 = new JPanel();
        ChartPanel Xgraphpanel = new ChartPanel(this.getgraph(g_start, g_end));
        this.mypanel.setLayout(new BorderLayout());
        jPanel2.add(this.jComboBox1);
        this.jButton1.setText("Delete");
        jPanel2.add(this.jButton1);
        jPanel2.add(this.jComboBox2);
        this.mypanel.add((Component)jPanel2, "South");
        this.mypanel.add((Component)Xgraphpanel, "Center");
        return this.mypanel;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "XxX", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        JFreeChart mychart = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"", (XYDataset)this.xydataset1, (boolean)false, (boolean)true, (boolean)false);
        this.setbackgroundimage(mychart);
        XYPlot xyplot = (XYPlot)mychart.getPlot();
        NumberAxis numberaxis = new NumberAxis("");
        numberaxis.setAutoRangeIncludesZero(false);
        xyplot.setRangeAxis(1, (ValueAxis)numberaxis);
        xyplot.setDataset(1, (XYDataset)this.xydataset2);
        xyplot.mapDatasetToRangeAxis(1, 1);
        XYItemRenderer xyitemrenderer = xyplot.getRenderer();
        xyitemrenderer.setSeriesPaint(0, (Paint)kSarConfig.color3);
        xyitemrenderer.setSeriesPaint(1, (Paint)kSarConfig.color4);
        if (xyitemrenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyitemrenderer;
            xylineandshaperenderer.setBaseShapesVisible(false);
        }
        XYLineAndShapeRenderer xylineandshaperenderer1 = new XYLineAndShapeRenderer();
        xylineandshaperenderer1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        xylineandshaperenderer1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        xylineandshaperenderer1.setBaseShapesVisible(false);
        LegendTitle legendtitle = new LegendTitle((LegendItemSource)xyitemrenderer);
        BlockContainer blockcontainer = new BlockContainer((Arrangement)new BorderArrangement());
        blockcontainer.add((Block)legendtitle, (Object)RectangleEdge.LEFT);
        xyplot.setRenderer(1, (XYItemRenderer)xylineandshaperenderer1);
        LegendTitle legendtitle1 = new LegendTitle((LegendItemSource)xylineandshaperenderer1);
        blockcontainer.add((Block)legendtitle1, (Object)RectangleEdge.RIGHT);
        blockcontainer.add((Block)new EmptyBlock(2000.0, 0.0));
        CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
        compositetitle.setPosition(RectangleEdge.BOTTOM);
        mychart.addSubtitle((Title)compositetitle);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }

}

