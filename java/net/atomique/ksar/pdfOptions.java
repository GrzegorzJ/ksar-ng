/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;

public class pdfOptions
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private JButton cancelButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JButton okButton;
    private JPanel optionpanel;
    private JPanel panelinfo;
    private JPanel panelname;
    private JPanel panelvalue;
    private JButton resetButton;

    public pdfOptions() {
        this.initComponents();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.optionpanel = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.panelinfo = new JPanel();
        this.panelname = new JPanel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.panelvalue = new JPanel();
        this.jTextField3 = new JTextField();
        this.jTextField2 = new JTextField();
        this.jTextField1 = new JTextField();
        this.jPanel2 = new JPanel();
        this.okButton = new JButton();
        this.resetButton = new JButton();
        this.cancelButton = new JButton();
        this.setClosable(true);
        this.setTitle("PDF Options");
        this.setPreferredSize(new Dimension(450, 280));
        this.jPanel1.setLayout(new BorderLayout());
        this.optionpanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.optionpanel.setLayout(new BorderLayout());
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setPreferredSize(new Dimension(300, 200));
        this.panelname.setLayout(new GridLayout(0, 1));
        this.jLabel1.setLabelFor(this.jTextField1);
        this.jLabel1.setText("PDF Bottom Left: ");
        this.panelname.add(this.jLabel1);
        this.jLabel2.setLabelFor(this.jTextField2);
        this.jLabel2.setText("PDF Upper Right: ");
        this.panelname.add(this.jLabel2);
        this.jLabel3.setLabelFor(this.jTextField3);
        this.jLabel3.setText("PDF Index Page: ");
        this.panelname.add(this.jLabel3);
        this.panelinfo.add(this.panelname);
        this.panelvalue.setLayout(new GridLayout(0, 1));
        this.jTextField3.setColumns(30);
        this.jTextField3.setText(kSarConfig.pdfbottomleft);
        this.panelvalue.add(this.jTextField3);
        this.jTextField2.setColumns(30);
        this.jTextField2.setText(kSarConfig.pdfupperright);
        this.panelvalue.add(this.jTextField2);
        this.jTextField1.setColumns(30);
        this.jTextField1.setText(kSarConfig.pdfindexpage);
        this.panelvalue.add(this.jTextField1);
        this.panelinfo.add(this.panelvalue);
        this.jScrollPane1.setViewportView(this.panelinfo);
        this.optionpanel.add((Component)this.jScrollPane1, "Center");
        this.jPanel1.add((Component)this.optionpanel, "First");
        this.okButton.setText("OK");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                pdfOptions.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.resetButton.setText("Reset");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                pdfOptions.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                pdfOptions.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.jPanel1.add((Component)this.jPanel2, "South");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.pack();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.jTextField1.setText(kSarConfig.pdfindexpage);
        this.jTextField2.setText(kSarConfig.pdfupperright);
        this.jTextField3.setText(kSarConfig.pdfbottomleft);
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.setVisible(false);
        this.dispose();
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        boolean tosave = false;
        String tmp = this.jTextField1.getText();
        if (!kSarConfig.pdfindexpage.equals(tmp)) {
            kSarConfig.pdfindexpage = tmp;
            tosave = true;
        }
        if (!kSarConfig.pdfbottomleft.equals(tmp = this.jTextField3.getText())) {
            kSarConfig.pdfbottomleft = tmp;
            tosave = true;
        }
        if (!kSarConfig.pdfupperright.equals(tmp = this.jTextField2.getText())) {
            kSarConfig.pdfupperright = tmp;
            tosave = true;
        }
        this.setVisible(false);
        this.dispose();
        if (tosave) {
            kSarConfig.writeDefault();
        }
    }

}

