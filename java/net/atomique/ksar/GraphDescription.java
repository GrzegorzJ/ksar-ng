/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import net.atomique.ksar.AllGraph;

public class GraphDescription {
    private AllGraph objectPointer;
    private String className;
    private String description;
    private String option;

    public GraphDescription(AllGraph o, String s, String s1, String s2) {
        this.objectPointer = o;
        this.className = s;
        this.description = s1;
        this.option = s2;
    }

    public AllGraph getobjectPointer() {
        return this.objectPointer;
    }

    public String getClassName() {
        return this.className;
    }

    public String getDescription() {
        return this.description;
    }

    public String getOption() {
        return this.option;
    }

    public String toString() {
        return this.description;
    }
}

