/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;

public class CommandChooser
extends JDialog {
    public static final long serialVersionUID = 501;
    private JButton cancelButton;
    private JComboBox cmdproccombo;
    private ButtonGroup commandbtngrp;
    private JComboBox commandcombo;
    private JRadioButton commandradio;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JButton okButton;
    private JCheckBox procshow;
    private JComboBox shortcutcombo;
    private JRadioButton shortcutradio;
    private DefaultComboBoxModel shortcutcommandmodel = new DefaultComboBoxModel();
    private DefaultComboBoxModel commandmodel = new DefaultComboBoxModel();

    public CommandChooser(Frame parent, boolean modal) {
        super(parent, modal);
        this.initComponents();
        this.load_data();
    }

    private void initComponents() {
        this.commandbtngrp = new ButtonGroup();
        this.jPanel1 = new JPanel();
        this.jPanel8 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jPanel4 = new JPanel();
        this.shortcutradio = new JRadioButton();
        this.shortcutcombo = new JComboBox();
        this.jPanel3 = new JPanel();
        this.commandradio = new JRadioButton();
        this.commandcombo = new JComboBox();
        this.procshow = new JCheckBox();
        this.jPanel7 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jPanel6 = new JPanel();
        this.cmdproccombo = new JComboBox();
        this.jPanel2 = new JPanel();
        this.cancelButton = new JButton();
        this.okButton = new JButton();
        this.setDefaultCloseOperation(2);
        this.setTitle("Choose command");
        this.setAlwaysOnTop(true);
        this.setName("choosecommanddlg");
        this.setResizable(false);
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel8.setLayout(new GridLayout(3, 1));
        this.jLabel2.setFont(new Font("Lucida Grande", 1, 14));
        this.jLabel2.setHorizontalAlignment(0);
        this.jLabel2.setText("Retrieving sar text file");
        this.jPanel8.add(this.jLabel2);
        this.jPanel4.setLayout(new FlowLayout(2));
        this.commandbtngrp.add(this.shortcutradio);
        this.shortcutradio.setSelected(true);
        this.shortcutradio.setText("Shortcut Command");
        this.shortcutradio.setHorizontalTextPosition(10);
        this.shortcutradio.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CommandChooser.this.shortcutradioActionPerformed(evt);
            }
        });
        this.jPanel4.add(this.shortcutradio);
        this.shortcutcombo.setModel(this.shortcutcommandmodel);
        this.shortcutcombo.setMinimumSize(new Dimension(200, 27));
        this.shortcutcombo.setPreferredSize(new Dimension(200, 27));
        this.jPanel4.add(this.shortcutcombo);
        this.jPanel8.add(this.jPanel4);
        this.jPanel3.setLayout(new FlowLayout(2));
        this.commandbtngrp.add(this.commandradio);
        this.commandradio.setText("Command");
        this.commandradio.setHorizontalTextPosition(10);
        this.commandradio.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CommandChooser.this.commandradioActionPerformed(evt);
            }
        });
        this.jPanel3.add(this.commandradio);
        this.commandcombo.setEditable(true);
        this.commandcombo.setModel(this.commandmodel);
        this.commandcombo.setEnabled(false);
        this.commandcombo.setMinimumSize(new Dimension(200, 27));
        this.commandcombo.setPreferredSize(new Dimension(200, 27));
        this.jPanel3.add(this.commandcombo);
        this.jPanel8.add(this.jPanel3);
        this.jPanel1.add((Component)this.jPanel8, "North");
        this.procshow.setText("Collect processlist (pidstat only)");
        this.procshow.setHorizontalAlignment(0);
        this.procshow.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CommandChooser.this.procshowActionPerformed(evt);
            }
        });
        this.jPanel1.add((Component)this.procshow, "Last");
        this.jPanel7.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        this.jPanel7.setEnabled(false);
        this.jPanel7.setFocusable(false);
        this.jPanel7.setOpaque(false);
        this.jPanel7.setVisible(false);
        this.jPanel7.setLayout(new GridLayout(2, 1));
        this.jLabel1.setFont(new Font("Lucida Grande", 1, 14));
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText("Process listing method");
        this.jPanel7.add(this.jLabel1);
        this.jPanel6.setFocusable(false);
        this.cmdproccombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.cmdproccombo.setMinimumSize(new Dimension(200, 27));
        this.cmdproccombo.setPreferredSize(new Dimension(200, 27));
        this.jPanel6.add(this.cmdproccombo);
        this.jPanel7.add(this.jPanel6);
        this.jPanel1.add((Component)this.jPanel7, "Center");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CommandChooser.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CommandChooser.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        this.dispose();
        System.exit(0);
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.dispose();
        System.exit(0);
    }

    private void procshowActionPerformed(ActionEvent evt) {
        this.jPanel7.setVisible(this.procshow.isSelected());
        this.pack();
    }

    private void shortcutradioActionPerformed(ActionEvent evt) {
        this.tooglecommandbutton();
    }

    private void commandradioActionPerformed(ActionEvent evt) {
        this.tooglecommandbutton();
    }

    private void load_data() {
        if (kSarConfig.association_list.isEmpty()) {
            this.jPanel8.remove(this.jPanel4);
            ((GridLayout)this.jPanel8.getLayout()).setRows(2);
            this.commandradio.setSelected(true);
            this.commandcombo.setEnabled(true);
            this.pack();
        } else {
            for (String key : kSarConfig.association_list.keySet()) {
                this.shortcutcommandmodel.addElement(key);
            }
        }
        for (String key : kSarConfig.sshconnectioncmd) {
            this.commandmodel.addElement(key);
        }
    }

    public String get_command() {
        if (this.shortcutradio.isSelected()) {
            return this.shortcutcombo.getSelectedItem().toString();
        }
        if (this.commandradio.isSelected()) {
            return this.commandcombo.getSelectedItem().toString();
        }
        return null;
    }

    public String get_proccommand() {
        if (this.procshow.isSelected()) {
            return this.cmdproccombo.getSelectedItem().toString();
        }
        return null;
    }

    private void tooglecommandbutton() {
        this.shortcutcombo.setEnabled(this.shortcutradio.isSelected());
        this.commandcombo.setEnabled(this.commandradio.isSelected());
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            public void run() {
                CommandChooser dialog = new CommandChooser(new JFrame(), true);
                dialog.addWindowListener(new WindowAdapter(){

                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }

        });
    }

}

