/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class SplashPicture
extends JPanel {
    public static final long serialVersionUID = 501;
    Image img = null;

    public SplashPicture(String file) {
        this.img = new ImageIcon(this.getClass().getResource(file)).getImage();
        this.repaint();
    }

    public void paintComponent(Graphics g) {
        boolean zoom;
        super.paintComponent(g);
        if (this.img == null) {
            return;
        }
        int w = this.img.getWidth(this);
        int h = this.img.getHeight(this);
        boolean bl = zoom = w > this.getWidth() || h > this.getHeight();
        if (zoom) {
            g.drawImage(this.img, 0, 0, this.getWidth(), this.getHeight(), this);
        } else {
            g.drawImage(this.img, (this.getWidth() - w) / 2, (this.getHeight() - h) / 2, this);
        }
    }
}

