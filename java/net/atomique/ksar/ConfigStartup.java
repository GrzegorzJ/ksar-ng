/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;

public class ConfigStartup
extends JFrame {
    public static final long serialVersionUID = 501;
    private JButton jButton1;
    private JButton jButton2;
    private JCheckBox jCheckBox1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel5;
    private JScrollPane jScrollPane1;
    kSarDesktop mydesktop;
    JCheckBox atstartup;

    public ConfigStartup(kSarDesktop hisdesktop) {
        this.mydesktop = hisdesktop;
        this.initComponents();
        this.init_winlist();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jPanel3 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jPanel5 = new JPanel();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jCheckBox1 = new JCheckBox();
        this.setDefaultCloseOperation(0);
        this.setTitle("Configure shortcut");
        this.jPanel1.setMinimumSize(new Dimension(300, 200));
        this.jPanel1.setPreferredSize(new Dimension(300, 200));
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new BoxLayout(this.jPanel3, 1));
        this.jScrollPane1.setViewportView(this.jPanel3);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.jPanel2.setLayout(new BorderLayout());
        this.jPanel5.setLayout(new FlowLayout(1, 1, 1));
        this.jButton1.setText("Cancel");
        this.jButton1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ConfigStartup.this.jButton1ActionPerformed(evt);
            }
        });
        this.jPanel5.add(this.jButton1);
        this.jButton2.setText("Ok");
        this.jButton2.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ConfigStartup.this.jButton2ActionPerformed(evt);
            }
        });
        this.jPanel5.add(this.jButton2);
        this.jPanel2.add((Component)this.jPanel5, "Last");
        this.jCheckBox1.setSelected(true);
        this.jCheckBox1.setText("Tile window at startup");
        this.jCheckBox1.setHorizontalAlignment(0);
        this.jCheckBox1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ConfigStartup.this.jCheckBox1ActionPerformed(evt);
            }
        });
        this.jPanel2.add((Component)this.jCheckBox1, "First");
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    private void jButton2ActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    private void jCheckBox1ActionPerformed(ActionEvent evt) {
        kSarConfig.tile_at_startup = this.jCheckBox1.isSelected();
        kSarConfig.writeDefault();
    }

    private void delete_shortcut(String temp) {
        boolean hasdeleted = false;
        String keytoremove = null;
        for (String key : kSarConfig.shortcut_window_list.keySet()) {
            String value = kSarConfig.shortcut_window_list.get(key);
            if (!temp.equals(value)) continue;
            keytoremove = key;
            hasdeleted = true;
        }
        if (hasdeleted) {
            kSarConfig.shortcut_window_list.remove(keytoremove);
            kSarConfig.writeDefault();
            this.mydesktop.delete_shortcut(temp);
            this.jPanel3.removeAll();
            this.jPanel3.repaint();
            this.init_winlist();
        }
    }

    private void change_atstartup(boolean sel, String shortcut) {
        if (sel) {
            kSarConfig.startup_windows_list.add(shortcut);
        } else {
            kSarConfig.startup_windows_list.remove(shortcut);
        }
        kSarConfig.writeDefault();
    }

    private void init_winlist() {
        for (String key : kSarConfig.shortcut_window_list.keySet()) {
            String value = kSarConfig.shortcut_window_list.get(key);
            JPanel tempwin = new JPanel();
            tempwin.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            tempwin.setMaximumSize(new Dimension(32767, 41));
            tempwin.setLayout(new FlowLayout(0));
            JButton Delete = new JButton();
            Delete.setText("Delete");
            Delete.setActionCommand(value);
            Delete.addActionListener(new ActionListener(){

                public void actionPerformed(ActionEvent h) {
                    ConfigStartup.this.delete_shortcut(h.getActionCommand());
                }
            });
            tempwin.add(Delete);
            this.atstartup = new JCheckBox();
            this.atstartup.setText(value);
            this.atstartup.setActionCommand(value);
            this.atstartup.setSelected(false);
            if (kSarConfig.startup_windows_list != null && kSarConfig.startup_windows_list.contains(value)) {
                this.atstartup.setSelected(true);
            }
            this.atstartup.addActionListener(new ActionListener(){

                public void actionPerformed(ActionEvent h) {
                    ConfigStartup.this.change_atstartup(ConfigStartup.this.atstartup.isSelected(), h.getActionCommand());
                }
            });
            tempwin.add(this.atstartup);
            this.jPanel3.add(tempwin);
        }
    }

}

