/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.atomique.ksar.kSarConfig;

public class ParserOptions
extends JDialog {
    public static final long serialVersionUID = 501;
    private JButton closeButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JSpinner jSpinner1;
    private JSpinner jSpinner2;
    private JSpinner jSpinner3;
    private JSpinner jSpinner4;
    private JSpinner jSpinner5;
    private JSpinner jSpinner6;
    private JButton okButton;
    private JButton resetButton;

    public ParserOptions(Frame parent, boolean modal) {
        super(parent, modal);
        this.initComponents();
        this.load_value();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jPanel4 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jSpinner2 = new JSpinner();
        this.jPanel7 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jSpinner1 = new JSpinner();
        this.jLabel1 = new JLabel();
        this.jSpinner5 = new JSpinner();
        this.jPanel6 = new JPanel();
        this.jLabel7 = new JLabel();
        this.jSpinner6 = new JSpinner();
        this.jLabel5 = new JLabel();
        this.jSpinner4 = new JSpinner();
        this.jPanel5 = new JPanel();
        this.jLabel4 = new JLabel();
        this.jSpinner3 = new JSpinner();
        this.jPanel2 = new JPanel();
        this.resetButton = new JButton();
        this.okButton = new JButton();
        this.closeButton = new JButton();
        this.setDefaultCloseOperation(2);
        this.jPanel1.setLayout(new BoxLayout(this.jPanel1, 3));
        this.jLabel2.setText("always refresh until datapoint grow up to");
        this.jPanel4.add(this.jLabel2);
        this.jSpinner2.setModel(new SpinnerNumberModel(100, 1, 4997, 1));
        this.jSpinner2.addChangeListener(new ChangeListener(){

            public void stateChanged(ChangeEvent evt) {
                ParserOptions.this.jSpinner2StateChanged(evt);
            }
        });
        this.jPanel4.add(this.jSpinner2);
        this.jPanel1.add(this.jPanel4);
        this.jLabel6.setText("refresh every ");
        this.jPanel7.add(this.jLabel6);
        this.jSpinner1.setModel(new SpinnerNumberModel(10, 1, 1000, 1));
        this.jPanel7.add(this.jSpinner1);
        this.jLabel1.setText("when datapoint is under");
        this.jPanel7.add(this.jLabel1);
        this.jSpinner5.setModel(new SpinnerNumberModel(500, 2, 4998, 1));
        this.jSpinner5.addChangeListener(new ChangeListener(){

            public void stateChanged(ChangeEvent evt) {
                ParserOptions.this.jSpinner5StateChanged(evt);
            }
        });
        this.jPanel7.add(this.jSpinner5);
        this.jPanel1.add(this.jPanel7);
        this.jLabel7.setText("refresh every ");
        this.jPanel6.add(this.jLabel7);
        this.jSpinner6.setModel(new SpinnerNumberModel(100, 1, 1000, 1));
        this.jPanel6.add(this.jSpinner6);
        this.jLabel5.setText("when datapoint is under");
        this.jPanel6.add(this.jLabel5);
        this.jSpinner4.setModel(new SpinnerNumberModel(1000, 3, 4999, 1));
        this.jSpinner4.addChangeListener(new ChangeListener(){

            public void stateChanged(ChangeEvent evt) {
                ParserOptions.this.jSpinner4StateChanged(evt);
            }
        });
        this.jPanel6.add(this.jSpinner4);
        this.jPanel1.add(this.jPanel6);
        this.jLabel4.setText("don't refresh graph if datapoint is over");
        this.jPanel5.add(this.jLabel4);
        this.jSpinner3.setModel(new SpinnerNumberModel((Number)5000, Integer.valueOf(4), null, (Number)1));
        this.jSpinner3.setMinimumSize(new Dimension(83, 24));
        this.jSpinner3.setOpaque(false);
        this.jSpinner3.setPreferredSize(new Dimension(83, 24));
        this.jSpinner3.addChangeListener(new ChangeListener(){

            public void stateChanged(ChangeEvent evt) {
                ParserOptions.this.jSpinner3StateChanged(evt);
            }
        });
        this.jPanel5.add(this.jSpinner3);
        this.jPanel1.add(this.jPanel5);
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.resetButton.setText("Reset to defaults");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ParserOptions.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ParserOptions.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.closeButton.setText("Close");
        this.closeButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                ParserOptions.this.closeButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.closeButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        this.save_value();
        this.dispose();
    }

    private void closeButtonActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.load_value();
    }

    private void jSpinner2StateChanged(ChangeEvent evt) {
        Integer tmp = (Integer)this.jSpinner2.getValue();
        Integer nexttmp = (Integer)this.jSpinner5.getValue();
        if (tmp >= nexttmp) {
            this.jSpinner5.setValue(new Integer(tmp + 1));
        }
    }

    private void jSpinner5StateChanged(ChangeEvent evt) {
        Integer tmp = (Integer)this.jSpinner5.getValue();
        Integer prevtmp = (Integer)this.jSpinner2.getValue();
        Integer nexttmp = (Integer)this.jSpinner4.getValue();
        if (tmp >= nexttmp) {
            this.jSpinner4.setValue(new Integer(tmp + 1));
        }
        if (tmp <= prevtmp) {
            this.jSpinner2.setValue(new Integer(tmp - 1));
        }
    }

    private void jSpinner4StateChanged(ChangeEvent evt) {
        Integer tmp = (Integer)this.jSpinner4.getValue();
        Integer prevtmp = (Integer)this.jSpinner5.getValue();
        Integer nexttmp = (Integer)this.jSpinner3.getValue();
        if (tmp >= nexttmp) {
            this.jSpinner3.setValue(new Integer(tmp + 1));
        }
        if (tmp <= prevtmp) {
            this.jSpinner5.setValue(new Integer(tmp - 1));
        }
    }

    private void jSpinner3StateChanged(ChangeEvent evt) {
        Integer tmp = (Integer)this.jSpinner3.getValue();
        Integer prevtmp = (Integer)this.jSpinner4.getValue();
        if (tmp <= prevtmp) {
            this.jSpinner4.setValue(new Integer(tmp - 1));
        }
    }

    private void save_value() {
        kSarConfig.alwaysrefresh = (Integer)this.jSpinner2.getValue();
        kSarConfig.somerefresh = (Integer)this.jSpinner5.getValue();
        kSarConfig.lessrefresh = (Integer)this.jSpinner4.getValue();
        kSarConfig.norefresh = (Integer)this.jSpinner3.getValue();
        kSarConfig.somerefresh_time = (Integer)this.jSpinner1.getValue();
        kSarConfig.lessrefresh_time = (Integer)this.jSpinner6.getValue();
        kSarConfig.writeDefault();
    }

    private void load_value() {
        this.jSpinner2.setValue(new Integer(kSarConfig.alwaysrefresh));
        this.jSpinner5.setValue(new Integer(kSarConfig.somerefresh));
        this.jSpinner4.setValue(new Integer(kSarConfig.lessrefresh));
        this.jSpinner3.setValue(new Integer(kSarConfig.norefresh));
        this.jSpinner1.setValue(new Integer(kSarConfig.somerefresh_time));
        this.jSpinner6.setValue(new Integer(kSarConfig.lessrefresh_time));
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            public void run() {
                ParserOptions dialog = new ParserOptions(new JFrame(), true);
                dialog.addWindowListener(new WindowAdapter(){

                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }

        });
    }

}

