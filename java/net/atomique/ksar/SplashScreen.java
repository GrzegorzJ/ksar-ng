/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import net.atomique.ksar.SplashPicture;

public class SplashScreen
extends JWindow {
    private static final long serialVersionUID = 8;

    public SplashScreen(String filename, Frame f, int waitTime) {
        super(f);
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(new SplashPicture(filename));
        p.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
        this.getContentPane().add(p);
        this.setSize(450, 300);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width / 2 - 225, screenSize.height / 2 - 150);
        this.addMouseListener(new MouseAdapter(){

            public void mousePressed(MouseEvent e) {
                SplashScreen.this.setVisible(false);
                SplashScreen.this.dispose();
            }
        });
        final int pause = waitTime;
        final Runnable closerRunner = new Runnable(){

            public void run() {
                SplashScreen.this.setVisible(false);
                SplashScreen.this.dispose();
            }
        };
        Runnable waitRunner = new Runnable(){

            public void run() {
                try {
                    Thread.sleep(pause);
                    SwingUtilities.invokeAndWait(closerRunner);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        this.setVisible(true);
        Thread splashThread = new Thread(waitRunner, "SplashThread");
        splashThread.start();
    }

}

