/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import javax.swing.JTextField;

public class diskName {
    private final JTextField myfield = new JTextField();
    private final String name;
    private String userTitle;

    public diskName(String val) {
        this.myfield.setColumns(25);
        this.myfield.setText(val);
        this.name = val;
    }

    public JTextField getField() {
        return this.myfield;
    }

    public String getName() {
        return this.name;
    }

    public String getfieldtext() {
        return this.myfield.getText();
    }

    public void setTitle(String val) {
        if (val != null) {
            this.userTitle = val;
            this.myfield.setText(val);
        }
    }

    public String getTitle() {
        if (this.userTitle != null) {
            return this.userTitle;
        }
        return this.name;
    }

    public String showTitle() {
        if (this.userTitle != null) {
            return this.userTitle + " (" + this.name + ")";
        }
        return this.name;
    }

    public String showrecord() {
        if (this.userTitle != null) {
            return this.name + "=" + this.userTitle;
        }
        return null;
    }

    public int fieldtouch() {
        if (this.myfield.getText().equals(this.getTitle())) {
            return 0;
        }
        return 1;
    }

    public void resetfield() {
        this.myfield.setText(this.getTitle());
    }
}

