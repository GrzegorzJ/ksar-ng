/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.jcraft.jsch.Channel
 *  com.jcraft.jsch.ChannelExec
 *  com.jcraft.jsch.JSch
 *  com.jcraft.jsch.JSchException
 *  com.jcraft.jsch.Logger
 *  com.jcraft.jsch.Session
 *  com.jcraft.jsch.UIKeyboardInteractive
 *  com.jcraft.jsch.UserInfo
 */
package net.atomique.ksar;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Logger;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class SSHCommand
extends Thread {
    static final Properties systemprops = System.getProperties();
    int port = 22;
    String host = null;
    kSar mysar = null;
    InputStream in = null;
    InputStream err = null;
    Channel channel = null;
    Session session = null;
    String cnx = null;
    String cmd = null;
    String cmd_password = null;
    private DefaultComboBoxModel comboModel = new DefaultComboBoxModel();
    private JComboBox combo = new JComboBox(this.comboModel);
    private DefaultComboBoxModel comboModel2 = new DefaultComboBoxModel();
    private JComboBox combo2 = new JComboBox(this.comboModel2);
    String shortcut_command = new String();
    private boolean debug = false;
    int num_try = 0;
    JSch jsch = null;

    public SSHCommand(kSar hissar, String command) {
        Object temp = null;
        this.num_try = 0;
        String username = null;
        this.mysar = hissar;
        String passed_user = null;
        String passed_host = null;
        String passed_cmd = null;
        int passed_port = 22;
        JInternalFrame dialog = new JInternalFrame("");
        if (command != null) {
            String[] cmd_splitted = command.split("@", 2);
            if (cmd_splitted.length != 2) {
                return;
            }
            String[] user_part = cmd_splitted[0].split(":", 2);
            if (user_part.length == 2) {
                passed_user = user_part[0];
                this.cmd_password = user_part[1];
            } else {
                passed_user = cmd_splitted[0];
                this.cmd_password = null;
            }
            String[] cmd_part = cmd_splitted[1].split("/", 2);
            if (cmd_part.length != 2) {
                return;
            }
            String[] host_part = cmd_part[0].split(":", 2);
            if (host_part.length == 2) {
                passed_host = host_part[0];
                try {
                    passed_port = Integer.parseInt(host_part[1]);
                }
                catch (NumberFormatException e) {
                    return;
                }
            } else {
                passed_host = host_part[0];
            }
            passed_cmd = cmd_part[1];
        }
        try {
            this.jsch = new JSch();
            if (this.debug) {
                JSch.setLogger((Logger)new MyLogger());
            }
            if (kSarConfig.sshidentity != null) {
                try {
                    this.jsch.addIdentity(kSarConfig.sshidentity.toString());
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            String userhome = (String)systemprops.get("user.home") + (String)systemprops.get("file.separator");
            if (kSarConfig.sshconnectionmap == null || kSarConfig.sshconnectionmap.size() < 1) {
                this.comboModel.addElement(System.getProperty("user.name") + "@localhost");
            } else {
                Iterator<String> it = kSarConfig.sshconnectionmap.iterator();
                while (it.hasNext()) {
                    this.comboModel.addElement(it.next());
                }
                if (!kSarConfig.sshconnectionmap.contains(System.getProperty("user.name") + "@localhost")) {
                    this.comboModel.addElement(System.getProperty("user.name") + "@localhost");
                }
            }
            this.combo.setEditable(true);
            if (passed_user == null) {
                int ret = JOptionPane.showConfirmDialog(this.mysar.myUI, this.combo, "SSH Connection", 1);
                if (ret != 0) {
                    return;
                }
                this.cnx = new String((String)this.combo.getSelectedItem());
                if (this.cnx == null) {
                    return;
                }
                if (this.cnx.indexOf(64) == -1) {
                    JOptionPane.showMessageDialog(this.mysar.myUI, "You must enter login@host", "SSH error", 0);
                    return;
                }
                username = this.cnx.substring(0, this.cnx.indexOf(64));
                if (this.cnx.indexOf(58) != -1) {
                    this.host = this.cnx.substring(this.cnx.indexOf(64) + 1, this.cnx.indexOf(58));
                    String s_port = this.cnx.substring(this.cnx.indexOf(58) + 1);
                    if (s_port == null || s_port.length() == 0 || s_port.equals("") || (this.port = Integer.parseInt(s_port)) <= 0) {
                        JOptionPane.showMessageDialog(this.mysar.myUI, "You must enter login@host:port", "SSH error", 0);
                        return;
                    }
                } else {
                    this.host = this.cnx.substring(this.cnx.indexOf(64) + 1);
                }
            } else {
                this.host = passed_host;
                username = passed_user;
                this.port = passed_port;
                this.cnx = this.port != 22 ? username + "@" + this.host + ":" + this.port : username + "@" + this.host;
            }
            this.session = this.jsch.getSession(username, this.host, this.port);
            if (!kSarConfig.ssh_stricthostchecking) {
                Properties config = new Properties();
                config.put("StrictHostKeyChecking", "no");
                this.session.setConfig(config);
            }
            if (new File(userhome + ".ssh" + (String)systemprops.get("file.separator") + "known_hosts").exists()) {
                this.jsch.setKnownHosts(userhome + ".ssh" + (String)systemprops.get("file.separator") + "known_hosts");
            }
            if (this.mysar.mydesktop.unified_id && this.mysar.mydesktop.unified_user.equals(username)) {
                this.session.setPassword(this.mysar.mydesktop.unified_pass);
            }
            if (this.cmd_password != null) {
                this.session.setPassword(this.cmd_password);
            }
            MyUserInfo ui = new MyUserInfo();
            this.session.setUserInfo((UserInfo)ui);
            try {
                this.session.connect();
            }
            catch (JSchException ee) {
                JOptionPane.showMessageDialog(this.mysar.myUI, "Unable to connect", "SSH error", 0);
                return;
            }
            if (kSarConfig.sshconnectionmap == null || kSarConfig.sshconnectionmap.size() == 0) {
                kSarConfig.sshconnectionmap.add(this.cnx);
                kSarConfig.writeDefault();
            } else if (!kSarConfig.sshconnectionmap.contains(this.cnx)) {
                kSarConfig.sshconnectionmap.add(this.cnx);
                kSarConfig.writeDefault();
            }
            if (kSarConfig.sshconnectioncmd == null || kSarConfig.sshconnectioncmd.size() < 1) {
                this.comboModel2.addElement("sar -A");
            } else {
                Iterator<String> it = kSarConfig.sshconnectioncmd.iterator();
                while (it.hasNext()) {
                    this.comboModel2.addElement(it.next());
                }
                if (!kSarConfig.sshconnectioncmd.contains("sar -A")) {
                    this.comboModel2.addElement("sar -A");
                }
            }
            this.combo2.setEditable(true);
            if (passed_cmd == null) {
                int ret2 = JOptionPane.showConfirmDialog(this.mysar.myUI, this.combo2, "SSH Command", 1);
                if (ret2 != 0) {
                    return;
                }
                this.cmd = new String((String)this.combo2.getSelectedItem());
                if (this.cmd == null) {
                    return;
                }
            } else {
                this.cmd = passed_cmd;
            }
            this.channel = this.session.openChannel("exec");
            ((ChannelExec)this.channel).setCommand("LC_ALL=C " + this.cmd + "\n");
            this.channel.setInputStream(null);
            this.channel.setXForwarding(false);
            ((ChannelExec)this.channel).setErrStream((OutputStream)System.err);
            this.in = this.channel.getInputStream();
            this.err = ((ChannelExec)this.channel).getErrStream();
            this.channel.connect();
            if (this.channel.isClosed() && this.channel.getExitStatus() != -1) {
                JOptionPane.showMessageDialog(this.mysar.myUI, "There was a problem while retrieving stat", "SSH error", 0);
                System.exit(2);
            }
            if (kSarConfig.sshconnectioncmd == null || kSarConfig.sshconnectioncmd.size() == 0) {
                kSarConfig.sshconnectioncmd.add(this.cmd);
                kSarConfig.writeDefault();
            } else if (!kSarConfig.sshconnectioncmd.contains(this.cmd)) {
                kSarConfig.sshconnectioncmd.add(this.cmd);
                kSarConfig.writeDefault();
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
        this.shortcut_command = this.cmd_password != null ? (this.port != 22 ? "ssh://" + username + ":" + this.cmd_password + "@" + this.host + ":" + this.port + "/" + this.cmd : "ssh://" + username + ":" + this.cmd_password + "@" + this.host + "/" + this.cmd) : "ssh://" + this.cnx + "/" + this.cmd;
    }

    public String get_action() {
        return this.shortcut_command;
    }

    public void run() {
        int max_waitdata = 10;
        try {
            if (this.in == null) {
                return;
            }
            InputStreamReader tmpin = new InputStreamReader(this.in);
            InputStreamReader tmperr = new InputStreamReader(this.err);
            while (max_waitdata > 0 && !tmpin.ready()) {
                try {
                    Thread.sleep(1000);
                }
                catch (Exception ee) {}
            }
            BufferedReader myfile = new BufferedReader(tmpin);
            this.mysar.parse(myfile);
            myfile.close();
            tmpin.close();
            this.in.close();
            this.err.close();
            this.channel.disconnect();
            this.session.disconnect();
            this.channel = null;
            this.session = null;
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    public static class MyLogger
    implements Logger {
        static Hashtable<Integer, String> name = new Hashtable();

        public boolean isEnabled(int level) {
            return true;
        }

        public void log(int level, String message) {
            System.err.print(name.get(new Integer(level)));
            System.err.println(message);
        }

        static {
            name.put(new Integer(0), "DEBUG: ");
            name.put(new Integer(1), "INFO: ");
            name.put(new Integer(2), "WARN: ");
            name.put(new Integer(3), "ERROR: ");
            name.put(new Integer(4), "FATAL: ");
        }
    }

    public class MyUserInfo
    implements UserInfo,
    UIKeyboardInteractive {
        String passwd;
        JTextField passwordField;
        String passphrase;
        JTextField passphraseField;
        final GridBagConstraints gbc;
        private Container panel;

        public MyUserInfo() {
            this.passwordField = new JPasswordField(20);
            this.passphraseField = new JPasswordField(20);
            this.gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 18, 0, new Insets(0, 0, 0, 0), 0, 0);
        }

        public boolean promptYesNo(String str) {
            Object[] options = new String[]{"yes", "no"};
            if (SSHCommand.this.mysar.myUI != null) {
                int foo = JOptionPane.showOptionDialog(SSHCommand.this.mysar.myUI, str, "Warning", -1, 2, null, options, options[0]);
                return foo == 0;
            }
            return true;
        }

        public String getPassphrase() {
            if (SSHCommand.this.mysar.mydesktop.unified_id) {
                return SSHCommand.this.mysar.mydesktop.unified_pass;
            }
            if (SSHCommand.this.cmd_password != null) {
                return SSHCommand.this.cmd_password;
            }
            return this.passphrase;
        }

        public boolean promptPassphrase(String message) {
            if (!SSHCommand.this.mysar.mydesktop.unified_id && SSHCommand.this.cmd_password == null || SSHCommand.this.num_try > 0) {
                Object[] ob = new Object[]{this.passphraseField};
                int result = JOptionPane.showConfirmDialog(SSHCommand.this.mysar.myUI, ob, message, 2);
                if (result == 0) {
                    this.passphrase = this.passphraseField.getText();
                    return true;
                }
                return false;
            }
            ++SSHCommand.this.num_try;
            return true;
        }

        public String getPassword() {
            if (SSHCommand.this.mysar.mydesktop.unified_id) {
                return SSHCommand.this.mysar.mydesktop.unified_pass;
            }
            if (SSHCommand.this.cmd_password != null) {
                return SSHCommand.this.cmd_password;
            }
            return this.passwd;
        }

        public boolean promptPassword(String message) {
            if (!SSHCommand.this.mysar.mydesktop.unified_id && SSHCommand.this.cmd_password == null || SSHCommand.this.num_try > 0) {
                Object[] ob = new Object[]{this.passwordField};
                if (SSHCommand.this.mysar.myUI != null) {
                    int result = JOptionPane.showConfirmDialog(SSHCommand.this.mysar.myUI, ob, message, 2);
                    if (result == 0) {
                        this.passwd = this.passwordField.getText();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            ++SSHCommand.this.num_try;
            return true;
        }

        public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {
            if (SSHCommand.this.mysar.mydesktop.unified_id && SSHCommand.this.num_try == 0) {
                ++SSHCommand.this.num_try;
                return SSHCommand.this.mysar.mydesktop.unified_pass.split("[.]");
            }
            if (SSHCommand.this.cmd_password != null) {
                ++SSHCommand.this.num_try;
                return SSHCommand.this.cmd_password.split("[.]");
            }
            this.panel = new JPanel();
            this.panel.setLayout(new GridBagLayout());
            this.gbc.weightx = 1.0;
            this.gbc.gridwidth = 0;
            this.gbc.gridx = 0;
            this.panel.add((Component)new JLabel(instruction), this.gbc);
            ++this.gbc.gridy;
            this.gbc.gridwidth = -1;
            JTextField[] texts = new JTextField[prompt.length];
            for (int i = 0; i < prompt.length; ++i) {
                this.gbc.fill = 0;
                this.gbc.gridx = 0;
                this.gbc.weightx = 1.0;
                this.panel.add((Component)new JLabel(prompt[i]), this.gbc);
                this.gbc.gridx = 1;
                this.gbc.fill = 2;
                this.gbc.weighty = 1.0;
                texts[i] = echo[i] ? new JTextField(20) : new JPasswordField(20);
                this.panel.add((Component)texts[i], this.gbc);
                ++this.gbc.gridy;
            }
            if (JOptionPane.showConfirmDialog(SSHCommand.this.mysar.myUI, this.panel, destination + " : " + name, 2, 3) == 0) {
                String[] response = new String[prompt.length];
                StringBuffer t = new StringBuffer();
                for (int i2 = 0; i2 < prompt.length; ++i2) {
                    response[i2] = texts[i2].getText();
                    t.append(response[i2]);
                }
                SSHCommand.this.cmd_password = t.toString();
                return response;
            }
            return null;
        }

        public void showMessage(String message) {
            if (SSHCommand.this.mysar.myUI == null) {
                return;
            }
            JOptionPane.showMessageDialog(SSHCommand.this.mysar.myUI, message);
        }
    }

}

