/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.BufferedReader;
import java.io.FileReader;
import javax.swing.JFileChooser;

public class FileRead
extends Thread {
    kSar mysar = null;
    public String sarfilename = null;

    public FileRead(kSar hissar) {
        this.mysar = hissar;
        try {
            this.sarfilename = this.getLocalFile();
            if (this.sarfilename == null) {
                // empty if block
            }
            return;
        }
        catch (Exception e) {
            return;
        }
    }

    public FileRead(kSar hissar, String filename) {
        this.mysar = hissar;
        this.sarfilename = filename;
    }

    public String getLocalFile() {
        int returnVal;
        String filename = null;
        JFileChooser fc = new JFileChooser();
        if (kSarConfig.lastReadDirectory != null) {
            fc.setCurrentDirectory(kSarConfig.lastReadDirectory);
        }
        if ((returnVal = fc.showDialog(null, "Open")) == 0) {
            filename = fc.getSelectedFile().getAbsolutePath();
            kSarConfig.lastReadDirectory = fc.getSelectedFile();
            if (!kSarConfig.lastReadDirectory.isDirectory()) {
                kSarConfig.lastReadDirectory = kSarConfig.lastReadDirectory.getParentFile();
                kSarConfig.writeDefault();
            }
        }
        return filename;
    }

    public String get_action() {
        return "file://" + this.sarfilename;
    }

    public void run() {
        try {
            if (this.sarfilename == null) {
                return;
            }
            FileReader tmpfile = new FileReader(this.sarfilename);
            BufferedReader myfile = new BufferedReader(tmpfile);
            this.mysar.parse(myfile);
            myfile.close();
            tmpfile.close();
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
}

