/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;

public class LineColorChooser
extends JDialog {
    public static final long serialVersionUID = 501;
    private JTextField boxcolor1;
    private JTextField boxcolor10;
    private JTextField boxcolor11;
    private JTextField boxcolor2;
    private JTextField boxcolor3;
    private JTextField boxcolor4;
    private JTextField boxcolor5;
    private JTextField boxcolor6;
    private JTextField boxcolor7;
    private JTextField boxcolor8;
    private JTextField boxcolor9;
    private JButton cancelButton;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JLabel labelcolor1;
    private JLabel labelcolor10;
    private JLabel labelcolor11;
    private JLabel labelcolor2;
    private JLabel labelcolor3;
    private JLabel labelcolor4;
    private JLabel labelcolor5;
    private JLabel labelcolor6;
    private JLabel labelcolor7;
    private JLabel labelcolor8;
    private JLabel labelcolor9;
    private JButton okButton;
    private JPanel panelcolor1;
    private JPanel panelcolor10;
    private JPanel panelcolor11;
    private JPanel panelcolor2;
    private JPanel panelcolor3;
    private JPanel panelcolor4;
    private JPanel panelcolor5;
    private JPanel panelcolor6;
    private JPanel panelcolor7;
    private JPanel panelcolor8;
    private JPanel panelcolor9;
    private JButton resetButton;

    public LineColorChooser(Frame parent, boolean modal) {
        super(parent, modal);
        this.initComponents();
        this.load_color();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.panelcolor1 = new JPanel();
        this.labelcolor1 = new JLabel();
        this.boxcolor1 = new JTextField();
        this.panelcolor2 = new JPanel();
        this.labelcolor2 = new JLabel();
        this.boxcolor2 = new JTextField();
        this.panelcolor3 = new JPanel();
        this.labelcolor3 = new JLabel();
        this.boxcolor3 = new JTextField();
        this.panelcolor4 = new JPanel();
        this.labelcolor4 = new JLabel();
        this.boxcolor4 = new JTextField();
        this.panelcolor5 = new JPanel();
        this.labelcolor5 = new JLabel();
        this.boxcolor5 = new JTextField();
        this.panelcolor6 = new JPanel();
        this.labelcolor6 = new JLabel();
        this.boxcolor6 = new JTextField();
        this.panelcolor7 = new JPanel();
        this.labelcolor7 = new JLabel();
        this.boxcolor7 = new JTextField();
        this.panelcolor8 = new JPanel();
        this.labelcolor8 = new JLabel();
        this.boxcolor8 = new JTextField();
        this.panelcolor9 = new JPanel();
        this.labelcolor9 = new JLabel();
        this.boxcolor9 = new JTextField();
        this.panelcolor10 = new JPanel();
        this.labelcolor10 = new JLabel();
        this.boxcolor10 = new JTextField();
        this.panelcolor11 = new JPanel();
        this.labelcolor11 = new JLabel();
        this.boxcolor11 = new JTextField();
        this.jPanel2 = new JPanel();
        this.resetButton = new JButton();
        this.cancelButton = new JButton();
        this.okButton = new JButton();
        this.setDefaultCloseOperation(2);
        this.setTitle("Choose Line Color");
        this.setAlwaysOnTop(true);
        this.setName("colordialog");
        this.setResizable(false);
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.setLayout(new BoxLayout(this.jPanel1, 3));
        this.panelcolor1.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor1.setText("Line Type 1");
        this.panelcolor1.add(this.labelcolor1);
        this.boxcolor1.setBackground(new Color(255, 51, 0));
        this.boxcolor1.setColumns(5);
        this.boxcolor1.setEditable(false);
        this.boxcolor1.setText("     ");
        this.boxcolor1.setEnabled(false);
        this.boxcolor1.setFocusable(false);
        this.boxcolor1.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor1.add(this.boxcolor1);
        this.jPanel1.add(this.panelcolor1);
        this.panelcolor2.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor2.setText("Line Type 2");
        this.panelcolor2.add(this.labelcolor2);
        this.boxcolor2.setBackground(new Color(255, 51, 0));
        this.boxcolor2.setColumns(5);
        this.boxcolor2.setEditable(false);
        this.boxcolor2.setText("     ");
        this.boxcolor2.setEnabled(false);
        this.boxcolor2.setFocusable(false);
        this.boxcolor2.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor2.add(this.boxcolor2);
        this.jPanel1.add(this.panelcolor2);
        this.panelcolor3.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor3.setText("Line Type 3");
        this.panelcolor3.add(this.labelcolor3);
        this.boxcolor3.setBackground(new Color(255, 51, 0));
        this.boxcolor3.setColumns(5);
        this.boxcolor3.setEditable(false);
        this.boxcolor3.setText("     ");
        this.boxcolor3.setEnabled(false);
        this.boxcolor3.setFocusable(false);
        this.boxcolor3.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor3.add(this.boxcolor3);
        this.jPanel1.add(this.panelcolor3);
        this.panelcolor4.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor4.setText("Line Type 4");
        this.panelcolor4.add(this.labelcolor4);
        this.boxcolor4.setBackground(new Color(255, 51, 0));
        this.boxcolor4.setColumns(5);
        this.boxcolor4.setEditable(false);
        this.boxcolor4.setText("     ");
        this.boxcolor4.setEnabled(false);
        this.boxcolor4.setFocusable(false);
        this.boxcolor4.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor4.add(this.boxcolor4);
        this.jPanel1.add(this.panelcolor4);
        this.panelcolor5.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor5.setText("Line Type 5");
        this.panelcolor5.add(this.labelcolor5);
        this.boxcolor5.setBackground(new Color(255, 51, 0));
        this.boxcolor5.setColumns(5);
        this.boxcolor5.setEditable(false);
        this.boxcolor5.setText("     ");
        this.boxcolor5.setEnabled(false);
        this.boxcolor5.setFocusable(false);
        this.boxcolor5.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LineColorChooser.this.boxcolor5ActionPerformed(evt);
            }
        });
        this.boxcolor5.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor5.add(this.boxcolor5);
        this.jPanel1.add(this.panelcolor5);
        this.panelcolor6.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor6.setText("Line Type 6");
        this.panelcolor6.add(this.labelcolor6);
        this.boxcolor6.setBackground(new Color(255, 51, 0));
        this.boxcolor6.setColumns(5);
        this.boxcolor6.setEditable(false);
        this.boxcolor6.setText("     ");
        this.boxcolor6.setEnabled(false);
        this.boxcolor6.setFocusable(false);
        this.boxcolor6.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor6.add(this.boxcolor6);
        this.jPanel1.add(this.panelcolor6);
        this.panelcolor7.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor7.setText("Line Type 7");
        this.panelcolor7.add(this.labelcolor7);
        this.boxcolor7.setBackground(new Color(255, 51, 0));
        this.boxcolor7.setColumns(5);
        this.boxcolor7.setEditable(false);
        this.boxcolor7.setText("     ");
        this.boxcolor7.setEnabled(false);
        this.boxcolor7.setFocusable(false);
        this.boxcolor7.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor7.add(this.boxcolor7);
        this.jPanel1.add(this.panelcolor7);
        this.panelcolor8.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor8.setText("Line Type 8");
        this.panelcolor8.add(this.labelcolor8);
        this.boxcolor8.setBackground(new Color(255, 51, 0));
        this.boxcolor8.setColumns(5);
        this.boxcolor8.setEditable(false);
        this.boxcolor8.setText("     ");
        this.boxcolor8.setEnabled(false);
        this.boxcolor8.setFocusable(false);
        this.boxcolor8.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor8.add(this.boxcolor8);
        this.jPanel1.add(this.panelcolor8);
        this.panelcolor9.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor9.setText("Line Type 9");
        this.panelcolor9.add(this.labelcolor9);
        this.boxcolor9.setBackground(new Color(255, 51, 0));
        this.boxcolor9.setColumns(5);
        this.boxcolor9.setEditable(false);
        this.boxcolor9.setText("     ");
        this.boxcolor9.setEnabled(false);
        this.boxcolor9.setFocusable(false);
        this.boxcolor9.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor9.add(this.boxcolor9);
        this.jPanel1.add(this.panelcolor9);
        this.panelcolor10.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor10.setText("Line Type 10");
        this.panelcolor10.add(this.labelcolor10);
        this.boxcolor10.setBackground(new Color(255, 51, 0));
        this.boxcolor10.setColumns(5);
        this.boxcolor10.setEditable(false);
        this.boxcolor10.setText("     ");
        this.boxcolor10.setEnabled(false);
        this.boxcolor10.setFocusable(false);
        this.boxcolor10.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.colorchanger(evt);
            }
        });
        this.panelcolor10.add(this.boxcolor10);
        this.jPanel1.add(this.panelcolor10);
        this.panelcolor11.setLayout(new GridLayout(1, 5, 5, 5));
        this.labelcolor11.setText("Line Type 11");
        this.panelcolor11.add(this.labelcolor11);
        this.boxcolor11.setBackground(new Color(255, 51, 0));
        this.boxcolor11.setColumns(5);
        this.boxcolor11.setEditable(false);
        this.boxcolor11.setText("     ");
        this.boxcolor11.setEnabled(false);
        this.boxcolor11.setFocusable(false);
        this.boxcolor11.addMouseListener(new MouseAdapter(){

            public void mouseClicked(MouseEvent evt) {
                LineColorChooser.this.boxcolor11colorchanger(evt);
            }
        });
        this.panelcolor11.add(this.boxcolor11);
        this.jPanel1.add(this.panelcolor11);
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.resetButton.setText("Reset to defaults");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LineColorChooser.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LineColorChooser.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                LineColorChooser.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void boxcolor5ActionPerformed(ActionEvent evt) {
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        this.save_color();
        this.dispose();
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    private void colorchanger(MouseEvent evt) {
        JTextField tmp;
        Color tmpcolor;
        if (evt == null) {
            return;
        }
        if (evt.getSource() instanceof JTextField && (tmpcolor = JColorChooser.showDialog(this, "Choose Background Color", (tmp = (JTextField)evt.getSource()).getBackground())) != null) {
            tmp.setBackground(tmpcolor);
        }
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.reset_color();
    }

    private void boxcolor11colorchanger(MouseEvent evt) {
    }

    private void reset_color() {
        kSarConfig.color1 = Color.BLUE;
        kSarConfig.color2 = Color.GREEN;
        kSarConfig.color3 = Color.RED;
        kSarConfig.color4 = Color.BLACK;
        kSarConfig.color5 = Color.MAGENTA;
        kSarConfig.color6 = Color.YELLOW;
        kSarConfig.color7 = Color.CYAN;
        kSarConfig.color8 = Color.GRAY;
        kSarConfig.color9 = Color.ORANGE;
        kSarConfig.color10 = Color.PINK;
        kSarConfig.color11 = Color.DARK_GRAY;
        kSarConfig.writeDefault();
        this.load_color();
    }

    private void load_color() {
        this.boxcolor1.setBackground(kSarConfig.color1);
        this.boxcolor2.setBackground(kSarConfig.color2);
        this.boxcolor3.setBackground(kSarConfig.color3);
        this.boxcolor4.setBackground(kSarConfig.color4);
        this.boxcolor5.setBackground(kSarConfig.color5);
        this.boxcolor6.setBackground(kSarConfig.color6);
        this.boxcolor7.setBackground(kSarConfig.color7);
        this.boxcolor8.setBackground(kSarConfig.color8);
        this.boxcolor9.setBackground(kSarConfig.color9);
        this.boxcolor10.setBackground(kSarConfig.color10);
        this.boxcolor11.setBackground(kSarConfig.color11);
    }

    private void save_color() {
        kSarConfig.color1 = this.boxcolor1.getBackground();
        kSarConfig.color2 = this.boxcolor2.getBackground();
        kSarConfig.color3 = this.boxcolor3.getBackground();
        kSarConfig.color4 = this.boxcolor4.getBackground();
        kSarConfig.color5 = this.boxcolor5.getBackground();
        kSarConfig.color6 = this.boxcolor6.getBackground();
        kSarConfig.color7 = this.boxcolor7.getBackground();
        kSarConfig.color8 = this.boxcolor8.getBackground();
        kSarConfig.color9 = this.boxcolor9.getBackground();
        kSarConfig.color10 = this.boxcolor10.getBackground();
        kSarConfig.color11 = this.boxcolor11.getBackground();
        kSarConfig.writeDefault();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){

            public void run() {
                LineColorChooser dialog = new LineColorChooser(new JFrame(), true);
                dialog.addWindowListener(new WindowAdapter(){

                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }

        });
    }

}

