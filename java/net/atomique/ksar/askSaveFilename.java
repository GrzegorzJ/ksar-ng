/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class askSaveFilename {
    private final kSar mysar;

    public askSaveFilename(kSar hissar) {
        this.mysar = hissar;
    }

    public String run() {
        int returnVal;
        String fileName = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Save sar text file");
        if (kSarConfig.lastExportDirectory != null) {
            chooser.setCurrentDirectory(kSarConfig.lastExportDirectory);
        }
        if ((returnVal = chooser.showSaveDialog(this.mysar.myUI)) == 0) {
            fileName = chooser.getSelectedFile().getAbsolutePath();
        }
        if (fileName == null) {
            return null;
        }
        if (new File(fileName).exists()) {
            Object[] choix = new String[]{"Yes", "No"};
            int resultat = JOptionPane.showOptionDialog(null, "Overwrite " + fileName + " ?", "File Exist", 1, 3, null, choix, choix[1]);
            if (resultat != 0) {
                return null;
            }
        }
        return fileName;
    }
}

