/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.AIX;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ttySar
extends AllGraph {
    final TimeSeries t_rawch;
    final TimeSeries t_canch;
    final TimeSeries t_outch;
    final TimeSeries t_rcvin;
    final TimeSeries t_xmtin;
    final TimeSeries t_mdmin;

    public ttySar(kSar hissar) {
        super(hissar);
        this.Title = new String("TTY");
        this.t_rawch = new TimeSeries((Comparable)((Object)"rawch/s"), (Class)Second.class);
        this.mysar.dispo.put("rawch/s", this.t_rawch);
        this.t_canch = new TimeSeries((Comparable)((Object)"canch/s"), (Class)Second.class);
        this.mysar.dispo.put("canch/s", this.t_canch);
        this.t_outch = new TimeSeries((Comparable)((Object)"outch/s"), (Class)Second.class);
        this.mysar.dispo.put("outch/s", this.t_outch);
        this.t_rcvin = new TimeSeries((Comparable)((Object)"rcvin/s"), (Class)Second.class);
        this.mysar.dispo.put("rcvin/s", this.t_rcvin);
        this.t_xmtin = new TimeSeries((Comparable)((Object)"xmtin/s"), (Class)Second.class);
        this.mysar.dispo.put("xmtin/s", this.t_xmtin);
        this.t_mdmin = new TimeSeries((Comparable)((Object)"mdmin/s"), (Class)Second.class);
        this.mysar.dispo.put("mdmin/s", this.t_mdmin);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init) {
        this.t_rawch.add((RegularTimePeriod)now, (Number)val1Init);
        this.t_canch.add((RegularTimePeriod)now, (Number)val2Init);
        this.t_outch.add((RegularTimePeriod)now, (Number)val3Init);
        this.t_rcvin.add((RegularTimePeriod)now, (Number)val4Init);
        this.t_xmtin.add((RegularTimePeriod)now, (Number)val5Init);
        this.t_mdmin.add((RegularTimePeriod)now, (Number)val6Init);
    }

    public XYDataset create() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_rawch);
        timeseriescollection.addSeries(this.t_canch);
        timeseriescollection.addSeries(this.t_outch);
        timeseriescollection.addSeries(this.t_rcvin);
        timeseriescollection.addSeries(this.t_xmtin);
        timeseriescollection.addSeries(this.t_mdmin);
        return timeseriescollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "AIXTTY", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset = this.create();
        JFreeChart mychart = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)xydataset, (boolean)true, (boolean)true, (boolean)false);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.setbackgroundimage(mychart);
        XYPlot xyplot = (XYPlot)mychart.getPlot();
        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        xylineandshaperenderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        xylineandshaperenderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        xylineandshaperenderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        xylineandshaperenderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        xylineandshaperenderer.setSeriesPaint(3, (Paint)kSarConfig.color4);
        xylineandshaperenderer.setSeriesPaint(4, (Paint)kSarConfig.color5);
        xylineandshaperenderer.setSeriesPaint(5, (Paint)kSarConfig.color6);
        xylineandshaperenderer.setSeriesPaint(6, (Paint)kSarConfig.color7);
        return mychart;
    }
}

