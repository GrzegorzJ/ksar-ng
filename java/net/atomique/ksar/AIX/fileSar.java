/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.AIX;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class fileSar
extends AllGraph {
    final TimeSeries iget;
    final TimeSeries namei;
    final TimeSeries dirbk;
    final String cpuName;

    public fileSar(kSar hissar, String cpuID) {
        super(hissar);
        this.cpuName = new String(cpuID);
        this.Title = new String("File " + cpuID);
        this.iget = new TimeSeries((Comparable)((Object)"iget"), (Class)Second.class);
        this.mysar.dispo.put("iget", this.iget);
        this.namei = new TimeSeries((Comparable)((Object)"lookuppn"), (Class)Second.class);
        this.mysar.dispo.put("lookuppn", this.namei);
        this.dirbk = new TimeSeries((Comparable)((Object)"dirblk"), (Class)Second.class);
        this.mysar.dispo.put("dirblk", this.dirbk);
    }

    public void add(Second now, Float val1Int, Float val2Int, Float val3Int) {
        this.iget.add((RegularTimePeriod)now, (Number)val1Int);
        this.namei.add((RegularTimePeriod)now, (Number)val2Int);
        this.dirbk.add((RegularTimePeriod)now, (Number)val3Int);
    }

    public XYDataset create() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.iget);
        timeseriescollection.addSeries(this.namei);
        timeseriescollection.addSeries(this.dirbk);
        return timeseriescollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "AIXFILE", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset = this.create();
        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)xydataset, (boolean)true, (boolean)true, (boolean)false);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)jfreechart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.setbackgroundimage(jfreechart);
        XYPlot xyplot = (XYPlot)jfreechart.getPlot();
        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        xylineandshaperenderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        xylineandshaperenderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        xylineandshaperenderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        xylineandshaperenderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        return jfreechart;
    }
}

