/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.AIX;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class bufferSar
extends AllGraph {
    final Trigger bufferrcachetrigger;
    final TimeSeries t_bread;
    final TimeSeries t_lread;
    final TimeSeries t_rcache;
    final TimeSeries t_bwrit;
    final TimeSeries t_lwrit;
    final TimeSeries t_wcache;
    final TimeSeries t_pread;
    final TimeSeries t_pwrit;

    public bufferSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Buffers");
        this.t_bread = new TimeSeries((Comparable)((Object)"bread/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers bread/s", this.t_bread);
        this.t_lread = new TimeSeries((Comparable)((Object)"lread/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers lread/s", this.t_lread);
        this.t_rcache = new TimeSeries((Comparable)((Object)"%rcache"), (Class)Second.class);
        this.mysar.dispo.put("Buffers %rcache", this.t_rcache);
        this.t_bwrit = new TimeSeries((Comparable)((Object)"bwrit/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers bwrit/s", this.t_bwrit);
        this.t_lwrit = new TimeSeries((Comparable)((Object)"lwrit/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers lwrit/s", this.t_lwrit);
        this.t_wcache = new TimeSeries((Comparable)((Object)"%wcache"), (Class)Second.class);
        this.mysar.dispo.put("Buffers %wcache", this.t_wcache);
        this.t_pread = new TimeSeries((Comparable)((Object)"pread/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers pread/s", this.t_pread);
        this.t_pwrit = new TimeSeries((Comparable)((Object)"pwrit/s"), (Class)Second.class);
        this.mysar.dispo.put("Buffers pwrit/s", this.t_pwrit);
        this.bufferrcachetrigger = new Trigger(this.mysar, this, "read cache", this.t_rcache, "down");
        this.bufferrcachetrigger.setTriggerValue(kSarConfig.aixbufferrcachetrigger);
    }

    public void doclosetrigger() {
        this.bufferrcachetrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init, Float val7Init, Float val8Init) {
        this.t_bread.add((RegularTimePeriod)now, (Number)val1Init);
        this.t_lread.add((RegularTimePeriod)now, (Number)val2Init);
        this.t_rcache.add((RegularTimePeriod)now, (Number)val3Init);
        this.t_bwrit.add((RegularTimePeriod)now, (Number)val4Init);
        this.t_lwrit.add((RegularTimePeriod)now, (Number)val5Init);
        this.t_wcache.add((RegularTimePeriod)now, (Number)val6Init);
        this.t_pread.add((RegularTimePeriod)now, (Number)val7Init);
        this.t_pwrit.add((RegularTimePeriod)now, (Number)val8Init);
        this.bufferrcachetrigger.doMarker(now, val3Init);
    }

    public XYDataset createread() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_bread);
        timeseriescollection.addSeries(this.t_lread);
        timeseriescollection.addSeries(this.t_pread);
        return timeseriescollection;
    }

    public XYDataset createwrit() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_bwrit);
        timeseriescollection.addSeries(this.t_lwrit);
        timeseriescollection.addSeries(this.t_pwrit);
        return timeseriescollection;
    }

    public XYDataset creatercache() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_rcache);
        return timeseriescollection;
    }

    public XYDataset createwcache() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_wcache);
        return timeseriescollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "AIXBUFFER", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset readset = this.createread();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot(readset, null, (ValueAxis)new NumberAxis("Read"), (XYItemRenderer)minichart1);
        XYDataset writset = this.createwrit();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart2.setSeriesPaint(2, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(writset, null, (ValueAxis)new NumberAxis("Write"), (XYItemRenderer)minichart2);
        XYDataset wcacheset = this.createwcache();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(wcacheset, null, (ValueAxis)new NumberAxis("%wcache"), (XYItemRenderer)minichart3);
        XYDataset rcacheset = this.creatercache();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(rcacheset, null, (ValueAxis)new NumberAxis("%rcache"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart jfreechart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(jfreechart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)jfreechart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.bufferrcachetrigger.setTriggerValue(kSarConfig.aixbufferrcachetrigger);
        this.bufferrcachetrigger.tagMarker(subplot4);
        return jfreechart;
    }
}

