/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.AIX;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class cpuSar
extends AllGraph {
    final Trigger cpuidletrigger;
    final Trigger cpusystemtrigger;
    final Trigger cpuwiotrigger;
    final TimeTableXYDataset stacked_used;
    final TimeSeries t_usr;
    final TimeSeries t_sys;
    final TimeSeries t_wio;
    final TimeSeries t_idle;
    final TimeSeries t_physc;
    final TimeSeries t_entc;
    private String cpuOpt = new String("");
    final String cpuName;

    public cpuSar(kSar hissar, String cpuID) {
        super(hissar);
        this.Title = new String("CPU " + cpuID);
        this.cpuName = new String(cpuID);
        this.t_usr = new TimeSeries((Comparable)((Object)"User"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " User", this.t_usr);
        this.t_sys = new TimeSeries((Comparable)((Object)"System"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " System", this.t_sys);
        this.t_wio = new TimeSeries((Comparable)((Object)"Waiting I/O"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Wait I/O", this.t_wio);
        this.t_idle = new TimeSeries((Comparable)((Object)"Idle"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Idle", this.t_idle);
        this.t_physc = new TimeSeries((Comparable)((Object)"physc"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " pysc", this.t_physc);
        this.t_entc = new TimeSeries((Comparable)((Object)"%entc"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " %entc", this.t_entc);
        this.stacked_used = new TimeTableXYDataset();
        this.cpuidletrigger = new Trigger(this.mysar, this, "idle", this.t_idle, "down");
        this.cpusystemtrigger = new Trigger(this.mysar, this, "system", this.t_sys, "up");
        this.cpuwiotrigger = new Trigger(this.mysar, this, "wio", this.t_wio, "up");
        this.cpuidletrigger.setTriggerValue(kSarConfig.aixcpuidletrigger);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.aixcpusystemtrigger);
        this.cpuwiotrigger.setTriggerValue(kSarConfig.aixcpuwiotrigger);
    }

    public void doclosetrigger() {
        this.cpuidletrigger.doclose();
        this.cpusystemtrigger.doclose();
        this.cpuwiotrigger.doclose();
    }

    public void add(Second now, Float usrInit, Float sysInit, Float wioInit, Float idleInit) {
        this.t_usr.add((RegularTimePeriod)now, (Number)usrInit);
        this.t_sys.add((RegularTimePeriod)now, (Number)sysInit);
        this.t_wio.add((RegularTimePeriod)now, (Number)wioInit);
        this.t_idle.add((RegularTimePeriod)now, (Number)idleInit);
        this.cpuidletrigger.doMarker(now, idleInit);
        this.cpusystemtrigger.doMarker(now, sysInit);
        this.cpuwiotrigger.doMarker(now, wioInit);
        this.stacked_used.add((TimePeriod)now, (double)usrInit.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)sysInit.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)wioInit.floatValue(), "Waiting I/O");
    }

    public void add(Second now, Float usrInit, Float sysInit, Float wioInit, Float idleInit, Float physcInit) {
        this.t_usr.add((RegularTimePeriod)now, (Number)usrInit);
        this.t_sys.add((RegularTimePeriod)now, (Number)sysInit);
        this.t_wio.add((RegularTimePeriod)now, (Number)wioInit);
        this.t_idle.add((RegularTimePeriod)now, (Number)idleInit);
        this.t_physc.add((RegularTimePeriod)now, (Number)physcInit);
        this.cpuidletrigger.doMarker(now, idleInit);
        this.cpusystemtrigger.doMarker(now, sysInit);
        this.cpuwiotrigger.doMarker(now, wioInit);
        this.stacked_used.add((TimePeriod)now, (double)usrInit.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)sysInit.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)wioInit.floatValue(), "Waiting I/O");
    }

    public void add(Second now, Float usrInit, Float sysInit, Float wioInit, Float idleInit, Float physcInit, Float entcInit) {
        this.t_usr.add((RegularTimePeriod)now, (Number)usrInit);
        this.t_sys.add((RegularTimePeriod)now, (Number)sysInit);
        this.t_wio.add((RegularTimePeriod)now, (Number)wioInit);
        this.t_idle.add((RegularTimePeriod)now, (Number)idleInit);
        this.t_physc.add((RegularTimePeriod)now, (Number)physcInit);
        this.t_entc.add((RegularTimePeriod)now, (Number)entcInit);
        this.cpuidletrigger.doMarker(now, idleInit);
        this.cpusystemtrigger.doMarker(now, sysInit);
        this.cpuwiotrigger.doMarker(now, wioInit);
        this.stacked_used.add((TimePeriod)now, (double)usrInit.floatValue(), "User");
        this.stacked_used.add((TimePeriod)now, (double)sysInit.floatValue(), "System");
        this.stacked_used.add((TimePeriod)now, (double)wioInit.floatValue(), "Waiting I/O");
    }

    public void setcpuOpt(String s) {
        this.cpuOpt = s;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_usr);
        graphcollection.addSeries(this.t_sys);
        graphcollection.addSeries(this.t_wio);
        return graphcollection;
    }

    public XYDataset createidle() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_idle);
        return graphcollection;
    }

    public XYDataset createphysc() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_physc);
        return graphcollection;
    }

    public XYDataset createentc() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_entc);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "AIXCPU", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset physcset;
        StandardXYItemRenderer minichart3;
        XYPlot subplot3 = null;
        XYPlot subplot4 = null;
        XYPlot subplot1 = null;
        XYDataset xydataset1 = this.createused();
        NumberAxis usedaxis = new NumberAxis("% used cpu");
        if (this.mysar.show100axiscpu) {
            usedaxis.setRange(0.0, 100.0);
        }
        if (this.mysar.showstackedcpu) {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
            renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
            renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
            subplot1 = new XYPlot((XYDataset)this.stacked_used, (ValueAxis)new DateAxis(null), (ValueAxis)usedaxis, (XYItemRenderer)renderer);
        } else {
            StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
            minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
            minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
            minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
            subplot1 = new XYPlot(xydataset1, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        }
        XYDataset idleset = this.createidle();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(idleset, null, (ValueAxis)new NumberAxis("% idle"), (XYItemRenderer)minichart2);
        if ("physc".equals(this.cpuOpt)) {
            physcset = this.createphysc();
            minichart3 = new StandardXYItemRenderer();
            minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
            minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot3 = new XYPlot(physcset, null, (ValueAxis)new NumberAxis("physc"), (XYItemRenderer)minichart3);
        }
        if ("%entc".equals(this.cpuOpt)) {
            physcset = this.createphysc();
            minichart3 = new StandardXYItemRenderer();
            minichart3.setSeriesPaint(0, (Paint)kSarConfig.color6);
            minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot3 = new XYPlot(physcset, null, (ValueAxis)new NumberAxis("physc"), (XYItemRenderer)minichart3);
            XYDataset entcset = this.createentc();
            StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
            minichart4.setSeriesPaint(0, (Paint)kSarConfig.color7);
            minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            subplot4 = new XYPlot(entcset, null, (ValueAxis)new NumberAxis("%entc"), (XYItemRenderer)minichart4);
        }
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 3);
        if ("physc".equals(this.cpuOpt)) {
            plot.add(subplot3, 1);
        }
        if ("%entc".equals(this.cpuOpt)) {
            plot.add(subplot4, 1);
            plot.add(subplot3, 1);
        }
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            if ("physc".equals(this.cpuOpt)) {
                subplot3.setBackgroundPaint(null);
            }
            if ("%entc".equals(this.cpuOpt)) {
                subplot3.setBackgroundPaint(null);
                subplot4.setBackgroundPaint(null);
            }
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.cpuidletrigger.setTriggerValue(kSarConfig.aixcpuidletrigger);
        this.cpuidletrigger.tagMarker(subplot2);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.aixcpusystemtrigger);
        this.cpusystemtrigger.tagMarker(subplot1);
        this.cpuwiotrigger.setTriggerValue(kSarConfig.aixcpuwiotrigger);
        this.cpuwiotrigger.tagMarker(subplot1);
        return mychart;
    }
}

