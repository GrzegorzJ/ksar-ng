/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.AIX;

import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;

public class Parser {
    private final kSar mysar;
    Float val1;
    Float val2;
    Float val3;
    Float val4;
    Float val5;
    Float val6;
    Float val7;
    Float val8;
    Float val9;
    Float val10;
    Float val11;
    int heure = 0;
    int minute = 0;
    int seconde = 0;
    Second now = new Second(0, 0, 0, 1, 1, 1970);
    String lastHeader;
    String statType = "none";
    int entreetype;
    int firstwastime;
    cpuSar sarCPU3 = null;
    bufferSar sarBUFFER3 = null;
    squeueSar sarSQUEUE3 = null;
    rqueueSar sarRQUEUE3 = null;
    ttySar sarTTY3 = null;
    syscallSar sarSYSCALL3 = null;
    msgSar sarMSG3 = null;
    cswchSar sarCSWCH3 = null;
    fileSar sarFILE3 = null;
    kernelSar sarKERNEL3 = null;
    slotSar sarSLOT3 = null;

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String prems, StringTokenizer matcher) {
        String[] sarTime;
        String first = prems;
        boolean headerFound = false;
        if (thisLine.indexOf("lookuppn") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("bread/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("scall/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("device") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("ksched") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("msg") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("runq-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("slots") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("%usr") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("proc-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("cswch") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rawch") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (!((sarTime = first.split(":")).length == 3 || "device".equals(this.statType) || "Kbs/s".equals(this.statType) || "cpu-iget/s".equals(this.statType) || "cpu-%usr".equals(this.statType) || "cpu-scall/s".equals(this.statType) || "cpu-cswch/s".equals(this.statType) || "cpu-msg/s".equals(this.statType) || "cpu-physc".equals(this.statType) || "cpu-%entc".equals(this.statType))) {
            return 1;
        }
        if (sarTime.length == 3) {
            this.heure = Integer.parseInt(sarTime[0]);
            this.minute = Integer.parseInt(sarTime[1]);
            this.seconde = Integer.parseInt(sarTime[2]);
            this.now = new Second(this.seconde, this.minute, this.heure, this.mysar.day, this.mysar.month, this.mysar.year);
            if (this.mysar.statstart == null) {
                this.mysar.statstart = new String(this.now.toString());
                this.mysar.startofgraph = this.now;
            }
            if (!this.mysar.datefound.contains((Object)this.now)) {
                this.mysar.datefound.add(this.now);
            }
            if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
                this.mysar.lastever = this.now;
                this.mysar.statend = new String(this.mysar.lastever.toString());
                this.mysar.endofgraph = this.mysar.lastever;
            }
            this.firstwastime = 1;
        } else {
            this.firstwastime = 0;
        }
        if (!matcher.hasMoreElements()) {
            return 1;
        }
        this.lastHeader = matcher.nextToken();
        if (headerFound) {
            if (this.lastHeader.equals(this.statType)) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("device") && thisLine.indexOf("Kbs/s") > 0 && this.statType.equals("Kbs/s")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%usr") && thisLine.indexOf("%entc") > 0 && this.statType.equals("%entc")) {
                headerFound = false;
                return 1;
            }
            if (this.lastHeader.equals("%usr") && thisLine.indexOf("physc") > 0 && this.statType.equals("physc")) {
                headerFound = false;
                return 1;
            }
            this.statType = this.lastHeader;
            if ("%usr".equals(this.lastHeader)) {
                if (this.sarCPU3 == null) {
                    this.sarCPU3 = new cpuSar(this.mysar, "");
                    if (this.mysar.myUI != null) {
                        this.sarCPU3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixcpuSar", this.sarCPU3);
                    this.sarCPU3.setGraphLink("AixcpuSar");
                }
                if (thisLine.indexOf("physc") > 0) {
                    this.statType = "physc";
                    this.sarCPU3.setcpuOpt("physc");
                }
                if (thisLine.indexOf("%entc") > 0) {
                    this.statType = "%entc";
                    this.sarCPU3.setcpuOpt("%entc");
                }
                return 1;
            }
            if ("bread/s".equals(this.lastHeader)) {
                if (this.sarBUFFER3 == null) {
                    this.sarBUFFER3 = new bufferSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarBUFFER3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixbufferSar", this.sarBUFFER3);
                    this.sarBUFFER3.setGraphLink("AixbufferSar");
                }
                return 1;
            }
            if ("runq-sz".equals(this.lastHeader)) {
                if (this.sarRQUEUE3 == null) {
                    this.sarRQUEUE3 = new rqueueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarRQUEUE3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixRqueueSar", this.sarRQUEUE3);
                    this.sarRQUEUE3.setGraphLink("AixRqueueSar");
                }
                if (this.sarSQUEUE3 == null) {
                    this.sarSQUEUE3 = new squeueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSQUEUE3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SqueueSar", this.sarSQUEUE3);
                    this.sarSQUEUE3.setGraphLink("AixSqueueSar");
                }
                return 1;
            }
            if ("ksched/s".equals(this.lastHeader)) {
                if (this.sarKERNEL3 == null) {
                    this.sarKERNEL3 = new kernelSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKERNEL3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("kernelSar", this.sarKERNEL3);
                    this.sarKERNEL3.setGraphLink("AixkernelSar");
                }
                return 1;
            }
            if ("rawch/s".equals(this.lastHeader)) {
                if (this.sarTTY3 == null) {
                    this.sarTTY3 = new ttySar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarTTY3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("ttySar", this.sarTTY3);
                    this.sarTTY3.setGraphLink("AixttySar");
                }
                return 1;
            }
            if ("msg/s".equals(this.lastHeader)) {
                if (this.sarMSG3 == null) {
                    this.sarMSG3 = new msgSar(this.mysar, "");
                    if (this.mysar.myUI != null) {
                        this.sarMSG3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixmsgSar", this.sarMSG3);
                    this.sarMSG3.setGraphLink("AixmsgSar");
                }
                return 1;
            }
            if ("scall/s".equals(this.lastHeader)) {
                if (this.sarSYSCALL3 == null) {
                    this.sarSYSCALL3 = new syscallSar(this.mysar, "");
                    if (this.mysar.myUI != null) {
                        this.sarSYSCALL3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixsyscalSar", this.sarSYSCALL3);
                    this.sarSYSCALL3.setGraphLink("AixsyscallSar");
                }
                return 1;
            }
            if ("cswch/s".equals(this.lastHeader)) {
                if (this.sarCSWCH3 == null) {
                    this.sarCSWCH3 = new cswchSar(this.mysar, "");
                    if (this.mysar.myUI != null) {
                        this.sarCSWCH3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixcswchSar", this.sarCSWCH3);
                    this.sarCSWCH3.setGraphLink("AixcswchSar");
                }
                return 1;
            }
            if ("iget/s".equals(this.lastHeader)) {
                if (this.sarFILE3 == null) {
                    this.sarFILE3 = new fileSar(this.mysar, "");
                    if (this.mysar.myUI != null) {
                        this.sarFILE3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixfileSar", this.sarFILE3);
                    this.sarFILE3.setGraphLink("AixfileSar");
                }
                return 1;
            }
            if ("slots".equals(this.lastHeader)) {
                if (this.sarSLOT3 == null) {
                    this.sarSLOT3 = new slotSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSLOT3.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("AixslotSar", this.sarSLOT3);
                    this.sarSLOT3.setGraphLink("AixslotSar");
                }
                return 1;
            }
            if ("device".equals(this.lastHeader)) {
                if (!this.mysar.hasdisknode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                    }
                    this.mysar.hasdisknode = true;
                }
                if (thisLine.indexOf("Kbs/s") > 0) {
                    this.statType = new String("Kbs/s");
                }
                if (thisLine.indexOf("scall/s") > 0) {
                    this.statType = new String("scall/s");
                }
                return 1;
            }
            if ("cpu".equals(this.lastHeader)) {
                if (thisLine.indexOf("%usr") > 0) {
                    if (!this.mysar.hascpunode) {
                        if (this.mysar.myUI != null) {
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.cpustreenode);
                        }
                        this.mysar.hascpunode = true;
                    }
                    this.statType = new String("cpu-%usr");
                    if (thisLine.indexOf("physc") > 0) {
                        this.statType = new String("cpu-physc");
                    }
                    if (thisLine.indexOf("%entc") > 0) {
                        this.statType = new String("cpu-%entc");
                    }
                    return 1;
                }
                if (thisLine.indexOf("iget") > 0) {
                    if (!this.mysar.hasfilenode) {
                        if (this.mysar.myUI != null) {
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.filetreenode);
                        }
                        this.mysar.hasfilenode = true;
                    }
                    this.statType = new String("cpu-iget/s");
                    return 1;
                }
                if (thisLine.indexOf("msg") > 0) {
                    if (!this.mysar.hasmsgnode) {
                        if (this.mysar.myUI != null) {
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.msgtreenode);
                        }
                        this.mysar.hasmsgnode = true;
                    }
                    this.statType = new String("cpu-msg/s");
                    return 1;
                }
                if (thisLine.indexOf("cswch") > 0) {
                    if (!this.mysar.hascswchnode) {
                        if (this.mysar.myUI != null) {
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.cswchtreenode);
                        }
                        this.mysar.hascswchnode = true;
                    }
                    this.statType = new String("cpu-cswch/s");
                    return 1;
                }
                if (thisLine.indexOf("scall") > 0) {
                    if (!this.mysar.hasscallnode) {
                        if (this.mysar.myUI != null) {
                            this.mysar.add2tree(this.mysar.graphtree, this.mysar.scalltreenode);
                        }
                        this.mysar.hasscallnode = true;
                    }
                    this.statType = new String("cpu-scall/s");
                    return 1;
                }
                return 1;
            }
            if ("proc-sz".equals(this.lastHeader)) {
                return 1;
            }
            headerFound = false;
            return 1;
        }
        try {
            if ("%usr".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarCPU3.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if ("physc".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarCPU3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if ("%entc".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarCPU3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if ("bread/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.sarBUFFER3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if ("runq-sz".equals(this.statType)) {
                if (!matcher.hasMoreElements()) {
                    return 1;
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarRQUEUE3.add(this.now, this.val1, this.val2);
                if (matcher.hasMoreElements()) {
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.sarSQUEUE3.add(this.now, this.val3, this.val4);
                }
                return 1;
            }
            if ("scall/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarSYSCALL3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if ("msg/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarMSG3.add(this.now, this.val1, this.val2);
                return 1;
            }
            if ("rawch/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarTTY3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if ("slots".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarSLOT3.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if ("proc-sz".equals(this.statType)) {
                return 1;
            }
            if ("cswch/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.sarCSWCH3.add(this.now, this.val1);
                return 1;
            }
            if ("iget/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarFILE3.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if ("cpu-%usr".equals(this.statType) || "cpu-%entc".equals(this.statType) || "cpu-physc".equals(this.statType)) {
                DefaultMutableTreeNode mycpu;
                cpuSar mycpusar;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    if ("-".equals(this.lastHeader)) {
                        this.lastHeader = "all";
                    }
                    if (this.mysar.cpuSarList.containsKey(this.lastHeader + "-cpu")) {
                        mycpusar = (cpuSar)this.mysar.cpuSarList.get(this.lastHeader + "-cpu");
                    } else {
                        mycpusar = new cpuSar(this.mysar, this.lastHeader);
                        mycpusar.setcpuOpt(this.statType);
                        mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                        this.mysar.cpuSarList.put(this.lastHeader + "-cpu", mycpusar);
                        this.mysar.pdfList.put(this.lastHeader + "-cpu", mycpusar);
                        mycpusar.setGraphLink(this.lastHeader + "-cpu");
                        if (this.mysar.myUI != null) {
                            mycpusar.addtotree(this.mysar.cpustreenode);
                        }
                    }
                    if ("cpu-%usr".equals(this.statType)) {
                        this.val1 = new Float(matcher.nextToken());
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4);
                        return 1;
                    }
                    if ("cpu-physc".equals(this.statType)) {
                        this.val1 = new Float(matcher.nextToken());
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        this.val5 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                        return 1;
                    }
                    if ("cpu-%entc".equals(this.statType)) {
                        this.val1 = new Float(matcher.nextToken());
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        this.val5 = new Float(matcher.nextToken());
                        this.val6 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                        return 1;
                    }
                } else {
                    if ("-".equals(first)) {
                        first = "all";
                    }
                    if (this.mysar.cpuSarList.containsKey(first + "-cpu")) {
                        mycpusar = (cpuSar)this.mysar.cpuSarList.get(first + "-cpu");
                    } else {
                        mycpusar = new cpuSar(this.mysar, first);
                        mycpusar.setcpuOpt(this.statType);
                        mycpu = new DefaultMutableTreeNode("cpu-" + this.lastHeader);
                        this.mysar.cpuSarList.put(first + "-cpu", mycpusar);
                        this.mysar.pdfList.put(first + "-cpu", mycpusar);
                        mycpusar.setGraphLink(first + "-cpu");
                        if (this.mysar.myUI != null) {
                            mycpusar.addtotree(this.mysar.cpustreenode);
                        }
                    }
                    if ("cpu-%usr".equals(this.statType)) {
                        this.val1 = new Float(this.lastHeader);
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4);
                        return 1;
                    }
                    if ("cpu-physc".equals(this.statType)) {
                        this.val1 = new Float(this.lastHeader);
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        this.val5 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                        return 1;
                    }
                    if ("cpu-%entc".equals(this.statType)) {
                        this.val1 = new Float(this.lastHeader);
                        this.val2 = new Float(matcher.nextToken());
                        this.val3 = new Float(matcher.nextToken());
                        this.val4 = new Float(matcher.nextToken());
                        this.val5 = new Float(matcher.nextToken());
                        this.val6 = new Float(matcher.nextToken());
                        mycpusar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                        return 1;
                    }
                }
            }
            if ("cpu-iget/s".equals(this.statType)) {
                fileSar myfilesar;
                DefaultMutableTreeNode myfile;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    if ("-".equals(this.lastHeader)) {
                        this.lastHeader = "all";
                    }
                    if (this.mysar.fileSarList.containsKey(this.lastHeader + "-file")) {
                        myfilesar = (fileSar)this.mysar.fileSarList.get(this.lastHeader + "-file");
                    } else {
                        myfilesar = new fileSar(this.mysar, this.lastHeader);
                        myfile = new DefaultMutableTreeNode("file-" + this.lastHeader);
                        this.mysar.fileSarList.put(this.lastHeader + "-file", myfilesar);
                        this.mysar.pdfList.put(this.lastHeader + "-file", myfilesar);
                        myfilesar.setGraphLink(this.lastHeader + "-file");
                        if (this.mysar.myUI != null) {
                            myfilesar.addtotree(this.mysar.filetreenode);
                        }
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    myfilesar.add(this.now, this.val1, this.val2, this.val3);
                    return 1;
                }
                if ("-".equals(first)) {
                    first = "all";
                }
                if (this.mysar.fileSarList.containsKey(first + "-file")) {
                    myfilesar = (fileSar)this.mysar.fileSarList.get(first + "-file");
                } else {
                    myfilesar = new fileSar(this.mysar, first);
                    myfile = new DefaultMutableTreeNode("file-" + this.lastHeader);
                    this.mysar.fileSarList.put(first + "-file", myfilesar);
                    this.mysar.pdfList.put(first + "-file", myfilesar);
                    myfilesar.setGraphLink(first + "-file");
                    if (this.mysar.myUI != null) {
                        myfilesar.addtotree(this.mysar.filetreenode);
                    }
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                myfilesar.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if ("cpu-msg/s".equals(this.statType)) {
                msgSar mymsgsar;
                DefaultMutableTreeNode mymsg;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    if ("-".equals(this.lastHeader)) {
                        this.lastHeader = "all";
                    }
                    if (this.mysar.msgSarList.containsKey(this.lastHeader + "-msg")) {
                        mymsgsar = (msgSar)this.mysar.msgSarList.get(this.lastHeader + "-msg");
                    } else {
                        mymsgsar = new msgSar(this.mysar, this.lastHeader);
                        mymsg = new DefaultMutableTreeNode("msg-" + this.lastHeader);
                        this.mysar.msgSarList.put(this.lastHeader + "-msg", mymsgsar);
                        this.mysar.pdfList.put(this.lastHeader + "-msg", mymsgsar);
                        mymsgsar.setGraphLink(this.lastHeader + "-msg");
                        if (this.mysar.myUI != null) {
                            mymsgsar.addtotree(this.mysar.msgtreenode);
                        }
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    mymsgsar.add(this.now, this.val1, this.val2);
                    return 1;
                }
                if ("-".equals(first)) {
                    first = "all";
                }
                if (this.mysar.msgSarList.containsKey(first + "-msg")) {
                    mymsgsar = (msgSar)this.mysar.msgSarList.get(first + "-msg");
                } else {
                    mymsgsar = new msgSar(this.mysar, first);
                    mymsg = new DefaultMutableTreeNode("msg-" + this.lastHeader);
                    this.mysar.msgSarList.put(first + "-msg", mymsgsar);
                    this.mysar.pdfList.put(first + "-msg", mymsgsar);
                    mymsgsar.setGraphLink(first + "-msg");
                    if (this.mysar.myUI != null) {
                        mymsgsar.addtotree(this.mysar.msgtreenode);
                    }
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                mymsgsar.add(this.now, this.val1, this.val2);
                return 1;
            }
            if ("cpu-cswch/s".equals(this.statType)) {
                cswchSar mycswchsar;
                DefaultMutableTreeNode mycswch;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    if ("-".equals(this.lastHeader)) {
                        this.lastHeader = "all";
                    }
                    if (this.mysar.cswchSarList.containsKey(this.lastHeader + "-cswch")) {
                        mycswchsar = (cswchSar)this.mysar.cswchSarList.get(this.lastHeader + "-cswch");
                    } else {
                        mycswchsar = new cswchSar(this.mysar, this.lastHeader);
                        mycswch = new DefaultMutableTreeNode("cswch-" + this.lastHeader);
                        this.mysar.cswchSarList.put(this.lastHeader + "-cswch", mycswchsar);
                        this.mysar.pdfList.put(this.lastHeader + "-cswch", mycswchsar);
                        mycswchsar.setGraphLink(this.lastHeader + "-cswch");
                        if (this.mysar.myUI != null) {
                            mycswchsar.addtotree(this.mysar.cswchtreenode);
                        }
                    }
                    this.val1 = new Float(matcher.nextToken());
                    mycswchsar.add(this.now, this.val1);
                    return 1;
                }
                if ("-".equals(first)) {
                    first = "all";
                }
                if (this.mysar.cswchSarList.containsKey(first + "-cswch")) {
                    mycswchsar = (cswchSar)this.mysar.cswchSarList.get(first + "-cswch");
                } else {
                    mycswchsar = new cswchSar(this.mysar, first);
                    mycswch = new DefaultMutableTreeNode("cswch-" + this.lastHeader);
                    this.mysar.cswchSarList.put(first + "-cswch", mycswchsar);
                    this.mysar.pdfList.put(first + "-cswch", mycswchsar);
                    mycswchsar.setGraphLink(first + "-cswch");
                    if (this.mysar.myUI != null) {
                        mycswchsar.addtotree(this.mysar.cswchtreenode);
                    }
                }
                this.val1 = new Float(this.lastHeader);
                mycswchsar.add(this.now, this.val1);
                return 1;
            }
            if ("cpu-scall/s".equals(this.statType)) {
                DefaultMutableTreeNode myscall;
                syscallSar myscallsar;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    if ("-".equals(this.lastHeader)) {
                        this.lastHeader = "all";
                    }
                    if (this.mysar.scallSarList.containsKey(this.lastHeader + "-scall")) {
                        myscallsar = (syscallSar)this.mysar.scallSarList.get(this.lastHeader + "-scall");
                    } else {
                        myscallsar = new syscallSar(this.mysar, this.lastHeader);
                        myscall = new DefaultMutableTreeNode("scall-" + this.lastHeader);
                        this.mysar.scallSarList.put(this.lastHeader + "-scall", myscallsar);
                        this.mysar.pdfList.put(this.lastHeader + "-scall", myscallsar);
                        myscallsar.setGraphLink(this.lastHeader + "-scall");
                        if (this.mysar.myUI != null) {
                            myscallsar.addtotree(this.mysar.scalltreenode);
                        }
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.val5 = new Float(matcher.nextToken());
                    this.val6 = new Float(matcher.nextToken());
                    this.val7 = new Float(matcher.nextToken());
                    myscallsar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                    return 1;
                }
                if ("-".equals(first)) {
                    first = "all";
                }
                if (this.mysar.scallSarList.containsKey(first + "-scall")) {
                    myscallsar = (syscallSar)this.mysar.scallSarList.get(first + "-scall");
                } else {
                    myscallsar = new syscallSar(this.mysar, first);
                    myscall = new DefaultMutableTreeNode("scall-" + this.lastHeader);
                    this.mysar.scallSarList.put(first + "-scall", myscallsar);
                    this.mysar.pdfList.put(first + "-scall", myscallsar);
                    myscallsar.setGraphLink(first + "-scall");
                    if (this.mysar.myUI != null) {
                        myscallsar.addtotree(this.mysar.scalltreenode);
                    }
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                myscallsar.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if ("device".equals(this.statType) || "Kbs/s".equals(this.statType)) {
                diskwaitSar mydiskwait;
                diskxferSar mydiskxfer;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(this.lastHeader + "Aixxfer");
                    if (mydiskxfer == null) {
                        diskName tmp = new diskName(this.lastHeader);
                        this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                        tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                        mydiskxfer = new diskxferSar(this.mysar, this.lastHeader);
                        mydiskxfer.setdiskOpt(this.statType);
                        this.mysar.disksSarList.put(this.lastHeader + "Aixxfer", mydiskxfer);
                        mydiskwait = new diskwaitSar(this.mysar, this.lastHeader);
                        this.mysar.disksSarList.put(this.lastHeader + "Aixwait", mydiskwait);
                        DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                        mydisk.add(new DefaultMutableTreeNode(new GraphDescription(mydiskxfer, "DISKXFER", "Disk Xfer", null)));
                        this.mysar.pdfList.put(this.lastHeader + "Aixxfer", mydiskxfer);
                        mydiskxfer.setGraphLink(this.lastHeader + "Aixxfer");
                        mydisk.add(new DefaultMutableTreeNode(new GraphDescription(mydiskwait, "DISKWAIT", "Disk Wait", null)));
                        this.mysar.pdfList.put(this.lastHeader + "Aixwait", mydiskwait);
                        mydiskwait.setGraphLink(this.lastHeader + "Aixwait");
                        this.mysar.diskstreenode.add(mydisk);
                    } else {
                        mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(this.lastHeader + "Aixwait");
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.val5 = new Float(matcher.nextToken());
                    this.val6 = new Float(matcher.nextToken());
                    mydiskxfer.add(this.now, this.val4, this.val3, this.val6);
                    mydiskwait.add(this.now, this.val2, this.val5, this.val1);
                    return 1;
                }
                mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(first + "Aixxfer");
                if (mydiskxfer == null) {
                    diskName tmp = new diskName(first);
                    this.mysar.AlternateDiskName.put(first, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(first));
                    mydiskxfer = new diskxferSar(this.mysar, first);
                    mydiskxfer.setdiskOpt(this.statType);
                    this.mysar.disksSarList.put(first + "Aixxfer", mydiskxfer);
                    mydiskwait = new diskwaitSar(this.mysar, first);
                    this.mysar.disksSarList.put(first + "Aixwait", mydiskwait);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    mydisk.add(new DefaultMutableTreeNode(new GraphDescription(mydiskxfer, "DISKXFER", "Disk Xfer", null)));
                    this.mysar.pdfList.put(first + "Aixxfer", mydiskxfer);
                    mydiskxfer.setGraphLink(first + "Aixxfer");
                    mydisk.add(new DefaultMutableTreeNode(new GraphDescription(mydiskwait, "DISKWAIT", "Disk Wait", null)));
                    this.mysar.pdfList.put(first + "Aixwait", mydiskwait);
                    mydiskwait.setGraphLink(first + "Aixwait");
                    this.mysar.diskstreenode.add(mydisk);
                } else {
                    mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(first + "Aixwait");
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mydiskxfer.add(this.now, this.val4, this.val3, this.val6);
                mydiskwait.add(this.now, this.val2, this.val5, this.val1);
                return 1;
            }
            if ("ksched/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarKERNEL3.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
        }
        catch (SeriesException e) {
            System.out.println("Aix parser: " + (Object)e);
            return -1;
        }
        return 0;
    }
}

