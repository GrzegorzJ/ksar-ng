/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.AIX;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class diskwaitSar
extends AllGraph {
    final Trigger diskbusytrigger;
    final Trigger diskavquetrigger;
    final TimeSeries t_avque;
    final TimeSeries t_avwait;
    final TimeSeries t_busy;
    final String diskName;

    public diskwaitSar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Disk wait");
        this.diskName = new String(s1);
        this.datain = 0;
        this.t_avque = new TimeSeries((Comparable)((Object)"avque"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "avque", this.t_avque);
        this.t_avwait = new TimeSeries((Comparable)((Object)"avwait"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "avwait", this.t_avwait);
        this.t_busy = new TimeSeries((Comparable)((Object)"%busy"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + s1 + "%busy", this.t_busy);
        this.diskbusytrigger = new Trigger(this.mysar, this, "%busy", this.t_busy, "up");
        this.diskbusytrigger.setTriggerValue(kSarConfig.aixdiskbusytrigger);
        this.diskavquetrigger = new Trigger(this.mysar, this, "avque", this.t_avque, "up");
        this.diskavquetrigger.setTriggerValue(kSarConfig.aixdiskavquetrigger);
    }

    public void doclosetrigger() {
        this.diskbusytrigger.doclose();
        this.diskavquetrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init) {
        Float newval;
        Number tmpInt;
        Float zerof = new Float(0.0f);
        if (!(val1Init.equals(zerof) && val2Init.equals(zerof) && val3Init.equals(zerof) || this.datain != 0)) {
            this.datain = 1;
        }
        if ((tmpInt = this.t_avque.getValue((RegularTimePeriod)now)) == null) {
            this.t_avque.add((RegularTimePeriod)now, (Number)val1Init);
            this.diskavquetrigger.doMarker(now, val1Init);
        } else {
            newval = new Float(tmpInt.floatValue() + val1Init.floatValue());
            this.t_avque.update((RegularTimePeriod)now, (Number)newval);
            this.diskavquetrigger.doMarker(now, newval);
        }
        tmpInt = this.t_avwait.getValue((RegularTimePeriod)now);
        if (tmpInt == null) {
            this.t_avwait.add((RegularTimePeriod)now, (Number)val2Init);
        } else {
            newval = new Float((tmpInt.floatValue() + val2Init.floatValue()) / 2.0f);
            this.t_avwait.update((RegularTimePeriod)now, (Number)newval);
        }
        tmpInt = this.t_busy.getValue((RegularTimePeriod)now);
        if (tmpInt == null) {
            this.t_busy.add((RegularTimePeriod)now, (Number)val3Init);
            this.diskbusytrigger.doMarker(now, val3Init);
        } else {
            newval = new Float((tmpInt.floatValue() + val3Init.floatValue()) / 2.0f);
            this.t_busy.update((RegularTimePeriod)now, (Number)newval);
            this.diskbusytrigger.doMarker(now, newval);
        }
    }

    public XYDataset createavque() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_avque);
        return timeseriescollection;
    }

    public XYDataset createavwait() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_avwait);
        return timeseriescollection;
    }

    public XYDataset createbusy() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_busy);
        return timeseriescollection;
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.diskName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "AIXDISKWAIT", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " on " + this.diskName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset avqueset = this.createavque();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(avqueset, null, (ValueAxis)new NumberAxis("avque"), (XYItemRenderer)minichart1);
        XYDataset avwaitset = this.createavwait();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(avwaitset, null, (ValueAxis)new NumberAxis("avwait"), (XYItemRenderer)minichart2);
        XYDataset busyset = this.createbusy();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(busyset, null, (ValueAxis)new NumberAxis("%busy"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart jfreechart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(jfreechart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)jfreechart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.diskbusytrigger.setTriggerValue(kSarConfig.aixdiskbusytrigger);
        this.diskbusytrigger.tagMarker(subplot3);
        this.diskavquetrigger.setTriggerValue(kSarConfig.aixdiskavquetrigger);
        this.diskavquetrigger.tagMarker(subplot1);
        return jfreechart;
    }
}

