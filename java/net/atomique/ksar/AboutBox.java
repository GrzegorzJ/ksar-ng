/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AboutBox
extends JInternalFrame
implements ActionListener {
    public static final long serialVersionUID = 501;
    private static AboutBox instance = new AboutBox();
    private final JPanel panel1 = new JPanel();
    private final JPanel panel2 = new JPanel();
    private final JPanel insetsPanel1 = new JPanel();
    private final JPanel insetsPanel2 = new JPanel();
    private final JButton button1 = new JButton();
    private final JLabel label1 = new JLabel();
    private final JLabel label2 = new JLabel();
    private final JLabel label3 = new JLabel();
    private final JLabel label4 = new JLabel();
    private final JLabel label5 = new JLabel();
    private final BorderLayout borderLayout1 = new BorderLayout();
    private final BorderLayout borderLayout2 = new BorderLayout();
    private final GridLayout gridLayout1 = new GridLayout();

    AboutBox() {
        super("About");
        this.setResizable(false);
        this.panel1.setLayout(this.borderLayout1);
        this.panel2.setLayout(this.borderLayout2);
        this.gridLayout1.setRows(6);
        this.gridLayout1.setColumns(1);
        this.label1.setText("kSar " + VersionNumber.getVersionNumber());
        this.label2.setText("Author: xavier cherif");
        this.label3.setText("Copyright (c) 2006 ");
        this.label4.setText("ARS LONGA, VITA BREVIS");
        this.label5.setText("Licence: BSD");
        JEditorPane copyright = new JEditorPane("text/html", "<html><span style='font-size: 10px;'><a href='http://ksar.atomique.net/'>http://ksar.atomique.net/</a></span></html>");
        copyright.setEditable(false);
        this.insetsPanel2.setLayout(this.gridLayout1);
        this.insetsPanel2.setBorder(BorderFactory.createEmptyBorder(10, 60, 10, 10));
        this.button1.setText("Ok");
        this.button1.addActionListener(this);
        this.panel1.add((Component)this.insetsPanel1, "South");
        this.insetsPanel1.add((Component)this.button1, (Object)null);
        this.panel2.add((Component)this.insetsPanel2, "Center");
        this.insetsPanel2.add((Component)this.label1, (Object)null);
        this.insetsPanel2.add((Component)this.label2, (Object)null);
        this.insetsPanel2.add((Component)this.label3, (Object)null);
        this.insetsPanel2.add((Component)this.label4, (Object)null);
        this.insetsPanel2.add((Component)this.label5, (Object)null);
        this.insetsPanel2.add((Component)copyright, (Object)null);
        this.getContentPane().add((Component)this.panel1, (Object)null);
        this.panel1.add((Component)this.panel2, "North");
        this.validate();
        this.pack();
    }

    public void actionPerformed(ActionEvent evt) {
        this.dispose();
    }

    public static AboutBox getInstance() {
        return instance;
    }
}

