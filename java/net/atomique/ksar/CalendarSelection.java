/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.data.time.Second;

public class CalendarSelection
extends JFrame {
    public static final long serialVersionUID = 501;
    private JButton OkButton;
    private JButton cancelButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JList listend;
    private JList liststart;
    private JButton resetButton;
    kSar mysar;
    DefaultListModel mytimeList = new DefaultListModel();

    public CalendarSelection(kSar hissar) {
        this.mysar = hissar;
        this.load_datefound();
        this.initComponents();
        this.liststart.setSelectedValue((Object)this.mysar.startofgraph, true);
        this.listend.setSelectedValue((Object)this.mysar.endofgraph, true);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jPanel5 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.liststart = new JList();
        this.jPanel4 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jScrollPane2 = new JScrollPane();
        this.listend = new JList();
        this.jPanel3 = new JPanel();
        this.OkButton = new JButton();
        this.resetButton = new JButton();
        this.cancelButton = new JButton();
        this.setDefaultCloseOperation(2);
        this.setTitle("Calendar");
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel2.setLayout(new GridLayout(1, 2));
        this.jPanel5.setLayout(new BoxLayout(this.jPanel5, 3));
        this.jLabel1.setText("start at:");
        this.jPanel5.add(this.jLabel1);
        this.liststart.setModel(this.mytimeList);
        this.liststart.setSelectionMode(0);
        this.liststart.addListSelectionListener(new ListSelectionListener(){

            public void valueChanged(ListSelectionEvent evt) {
                CalendarSelection.this.liststartValueChanged(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.liststart);
        this.jPanel5.add(this.jScrollPane1);
        this.jPanel2.add(this.jPanel5);
        this.jPanel4.setLayout(new BoxLayout(this.jPanel4, 3));
        this.jLabel2.setText("end at:");
        this.jPanel4.add(this.jLabel2);
        this.listend.setModel(this.mytimeList);
        this.listend.addListSelectionListener(new ListSelectionListener(){

            public void valueChanged(ListSelectionEvent evt) {
                CalendarSelection.this.listendValueChanged(evt);
            }
        });
        this.jScrollPane2.setViewportView(this.listend);
        this.jPanel4.add(this.jScrollPane2);
        this.jPanel2.add(this.jPanel4);
        this.jPanel1.add((Component)this.jPanel2, "Center");
        this.OkButton.setText("Ok");
        this.OkButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CalendarSelection.this.OkButtonActionPerformed(evt);
            }
        });
        this.jPanel3.add(this.OkButton);
        this.resetButton.setText("Reset");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CalendarSelection.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel3.add(this.resetButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                CalendarSelection.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel3.add(this.cancelButton);
        this.jPanel1.add((Component)this.jPanel3, "South");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.pack();
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.setVisible(false);
        this.dispose();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.liststart.setSelectedValue(this.mysar.statstart, true);
        this.listend.setSelectedValue(this.mysar.statend, true);
        if (this.mysar.myUI.obj2 != null) {
            this.mysar.myUI.chartContainer.removeAll();
            this.mysar.myUI.chartContainer.add(this.mysar.myUI.obj2.run(this.mysar.startofgraph, this.mysar.endofgraph));
        }
    }

    private void OkButtonActionPerformed(ActionEvent evt) {
        this.mysar.startofgraph = (Second)this.liststart.getSelectedValue();
        this.mysar.endofgraph = (Second)this.listend.getSelectedValue();
        this.setVisible(false);
        this.dispose();
        if (this.mysar.myUI.obj2 != null) {
            this.mysar.isparsing = false;
            this.mysar.myUI.chartContainer.removeAll();
            this.mysar.myUI.chartContainer.add(this.mysar.myUI.obj2.run(this.mysar.startofgraph, this.mysar.endofgraph));
            this.mysar.myUI.chartContainer.validate();
        }
    }

    private void liststartValueChanged(ListSelectionEvent evt) {
        if (!evt.getValueIsAdjusting()) {
            Second a = (Second)this.listend.getSelectedValue();
            Second b = (Second)this.liststart.getSelectedValue();
            if (a == null || b == null) {
                return;
            }
            if (a.compareTo((Object)b) <= 0) {
                this.liststart.setSelectedValue((Object)this.mysar.startofgraph, true);
            } else {
                this.mysar.startofgraph = (Second)this.liststart.getSelectedValue();
            }
        }
    }

    private void listendValueChanged(ListSelectionEvent evt) {
        if (!evt.getValueIsAdjusting()) {
            Second a = Second.parseSecond((String)this.listend.getSelectedValue().toString());
            Second b = Second.parseSecond((String)this.liststart.getSelectedValue().toString());
            if (a == null || b == null) {
                return;
            }
            if (b.compareTo((Object)a) >= 0) {
                this.listend.setSelectedValue((Object)this.mysar.endofgraph, true);
            } else {
                this.mysar.endofgraph = (Second)this.listend.getSelectedValue();
            }
        }
    }

    private void load_datefound() {
        for (int i = 0; i < this.mysar.datefound.size(); ++i) {
            this.mytimeList.addElement(this.mysar.datefound.get(i));
        }
    }

}

