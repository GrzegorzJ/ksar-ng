/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar.Ps;

public class ProcessInfo {
    private String command;
    private String mypid;

    public ProcessInfo(String hispid, String cmd) {
        int len = 50;
        StringBuffer tmp2 = new StringBuffer();
        for (int i = 0; i < cmd.length(); i += 50) {
            len = cmd.length() - i >= 50 ? 50 : cmd.length() - i;
            tmp2.append(cmd.substring(i, i + len) + "<br>");
        }
        this.command = new String("<html>" + tmp2.toString() + "</html>");
        this.mypid = hispid;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getMypid() {
        return this.mypid;
    }

    public void setMypid(String mypid) {
        this.mypid = mypid;
    }
}

