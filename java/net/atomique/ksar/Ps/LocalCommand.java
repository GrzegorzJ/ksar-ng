/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar.Ps;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.JOptionPane;

import net.atomique.ksar.kSar;

public class LocalCommand
extends Thread {
    private final ProcessList myprocesslist;
    private final kSar mysar;
    private InputStream infile;
    public String command;
    private static final String errmsg = "There was a problem while running the command ";

    public LocalCommand(kSar hissar, ProcessList hisprocesslist) {
        this.mysar = hissar;
        this.myprocesslist = hisprocesslist;
        try {
            String[] envvar = new String[]{"LC_ALL=C"};
            this.command = JOptionPane.showInputDialog("Enter local command ", (Object)"ps -ef");
            if (this.command == null) {
                return;
            }
            Process proc = Runtime.getRuntime().exec(this.command, envvar);
            this.infile = proc.getInputStream();
        }
        catch (Exception e) {
            if (this.mysar.myUI == null) {
                System.err.println("There was a problem while running the command " + this.command);
            }
            JOptionPane.showMessageDialog(null, "There was a problem while running the command " + this.command, "Local error", 0);
        }
    }

    public LocalCommand(kSar hissar, String hiscommand, ProcessList hisprocesslist) {
        this.mysar = hissar;
        this.command = hiscommand;
        this.myprocesslist = hisprocesslist;
        try {
            String[] envvar = new String[]{"LC_ALL=C"};
            Process proc = Runtime.getRuntime().exec(this.command, envvar);
            this.infile = proc.getInputStream();
        }
        catch (Exception e) {
            if (this.mysar.myUI == null) {
                System.err.println("There was a problem while running the command " + this.command);
            }
            JOptionPane.showMessageDialog(null, "There was a problem while running the command " + this.command, "Local error", 0);
        }
    }

    public void run() {
        try {
            if (this.infile == null) {
                return;
            }
            BufferedReader myfile = new BufferedReader(new InputStreamReader(this.infile));
            this.myprocesslist.parse(myfile);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    public String getAction() {
        return "cmd://" + this.command;
    }
}

