/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.jcraft.jsch.Channel
 *  com.jcraft.jsch.ChannelExec
 *  com.jcraft.jsch.JSch
 *  com.jcraft.jsch.JSchException
 *  com.jcraft.jsch.Logger
 *  com.jcraft.jsch.Session
 *  com.jcraft.jsch.UIKeyboardInteractive
 *  com.jcraft.jsch.UserInfo
 */
package net.atomique.ksar.Ps;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Logger;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;

public class SSHCommand
extends Thread {
    private static final Properties systemprops = System.getProperties();
    private int port = 22;
    private String host = null;
    private final kSar mysar;
    private InputStream infile;
    private Channel channel;
    private Session session;
    private String cnx;
    private String cmd;
    private String cmd_password;
    private final boolean debug = false;
    private int num_try = 0;
    private final ProcessList myprocesslist;

    public SSHCommand(kSar hissar, String command, ProcessList hisprocesslist) {
        String username = null;
        this.mysar = hissar;
        this.myprocesslist = hisprocesslist;
        String passed_user = null;
        String passed_host = null;
        String passed_cmd = null;
        int passed_port = 22;
        if (command != null) {
            String[] cmd_splitted = command.split("@", 2);
            if (cmd_splitted.length != 2) {
                return;
            }
            String[] user_part = cmd_splitted[0].split(":", 2);
            if (user_part.length == 2) {
                passed_user = user_part[0];
                this.cmd_password = user_part[1];
            } else {
                passed_user = cmd_splitted[0];
            }
            String[] cmd_part = cmd_splitted[1].split("/", 2);
            if (cmd_part.length != 2) {
                return;
            }
            String[] host_part = cmd_part[0].split(":", 2);
            if (host_part.length == 2) {
                passed_host = host_part[0];
                try {
                    passed_port = Integer.parseInt(host_part[1]);
                }
                catch (NumberFormatException e) {
                    return;
                }
            } else {
                passed_host = host_part[0];
            }
            passed_cmd = cmd_part[1];
        }
        try {
            JSch jsch = new JSch();
            if (kSarConfig.sshidentity != null) {
                try {
                    jsch.addIdentity(kSarConfig.sshidentity.toString());
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            String userhome = (String)systemprops.get("user.home") + (String)systemprops.get("file.separator");
            if (passed_user == null) {
                return;
            }
            this.host = passed_host;
            username = passed_user;
            this.port = passed_port;
            this.cnx = this.port == 22 ? username + "@" + this.host : username + "@" + this.host + ":" + this.port;
            this.session = jsch.getSession(username, this.host, this.port);
            if (new File(userhome + ".ssh" + (String)systemprops.get("file.separator") + "known_hosts").exists()) {
                jsch.setKnownHosts(userhome + ".ssh" + (String)systemprops.get("file.separator") + "known_hosts");
            }
            if (this.cmd_password != null) {
                this.session.setPassword(this.cmd_password);
            }
            MyUserInfo ui = new MyUserInfo();
            this.session.setUserInfo((UserInfo)ui);
            try {
                this.session.connect();
            }
            catch (JSchException ee) {
                return;
            }
            if (passed_cmd == null) {
                return;
            }
            this.cmd = passed_cmd;
            this.channel = this.session.openChannel("exec");
            ((ChannelExec)this.channel).setCommand("LC_ALL=C " + this.cmd + "\n");
            this.channel.setInputStream(null);
            this.channel.setXForwarding(false);
            ((ChannelExec)this.channel).setErrStream((OutputStream)System.err);
            this.infile = this.channel.getInputStream();
            this.channel.connect();
            if (this.channel.isClosed() && this.channel.getExitStatus() != -1) {
                return;
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    public void run() {
        int maxwaitdata = 10;
        try {
            if (this.infile == null) {
                return;
            }
            InputStreamReader tmpin = new InputStreamReader(this.infile);
            while (maxwaitdata > 0 && !tmpin.ready()) {
                try {
                    Thread.sleep(1000);
                }
                catch (Exception ee) {}
            }
            BufferedReader myfile = new BufferedReader(tmpin);
            this.myprocesslist.parse(myfile);
            myfile.close();
            tmpin.close();
            this.infile.close();
            this.channel.disconnect();
            this.session.disconnect();
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    static /* synthetic */ kSar access$000(SSHCommand x0) {
        return x0.mysar;
    }

    public static class MyLogger
    implements Logger {
        private static Map<Integer, String> name = new Hashtable<Integer, String>();

        public boolean isEnabled(int level) {
            return true;
        }

        public void log(int level, String message) {
            System.err.print(name.get(new Integer(level)));
            System.err.println(message);
        }

        static {
            name.put(new Integer(0), "DEBUG: ");
            name.put(new Integer(1), "INFO: ");
            name.put(new Integer(2), "WARN: ");
            name.put(new Integer(3), "ERROR: ");
            name.put(new Integer(4), "FATAL: ");
        }
    }

    public class MyUserInfo
    implements UserInfo,
    UIKeyboardInteractive {
        private String passwd;
        private String passphrase;

        public boolean promptYesNo(String str) {
            return true;
        }

        public String getPassphrase() {
            if (SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_id) {
                return SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_pass;
            }
            if (SSHCommand.this.cmd_password != null) {
                return SSHCommand.this.cmd_password;
            }
            return this.passphrase;
        }

        public boolean promptPassphrase(String message) {
            SSHCommand.this.num_try++;
            return true;
        }

        public String getPassword() {
            if (SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_id) {
                return SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_pass;
            }
            if (SSHCommand.this.cmd_password != null) {
                return SSHCommand.this.cmd_password;
            }
            return this.passwd;
        }

        public boolean promptPassword(String message) {
            SSHCommand.this.num_try++;
            return true;
        }

        public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {
            if (SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_id && SSHCommand.this.num_try == 0) {
                SSHCommand.this.num_try++;
                return SSHCommand.access$000((SSHCommand)SSHCommand.this).mydesktop.unified_pass.split("[.]");
            }
            if (SSHCommand.this.cmd_password != null) {
                SSHCommand.this.num_try++;
                return SSHCommand.this.cmd_password.split("[.]");
            }
            return null;
        }

        public void showMessage(String message) {
        }
    }

}

