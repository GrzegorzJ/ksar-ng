/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar.Ps;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.atomique.ksar.kSar;

public class ProcessList {
    public Map<String, ProcessInfo> listproc = new HashMap<String, ProcessInfo>();
    private Thread launched_command = null;
    private final kSar mysar;

    public ProcessList(kSar hissar, String command) {
        String commandname;
        this.mysar = hissar;
        if (command.startsWith("cmd://")) {
            commandname = new String(command.substring(6));
            this.dolocalcommand(commandname);
        }
        if (command.startsWith("ssh://")) {
            commandname = new String(command.substring(6));
            this.dosshread(commandname);
        }
    }

    private void dosshread(String cmd) {
        if (cmd != null) {
            this.launched_command = new SSHCommand(this.mysar, cmd, this);
        }
        this.launched_command.start();
    }

    private void dolocalcommand(String cmd) {
        if (cmd != null) {
            this.launched_command = new LocalCommand(this.mysar, cmd, this);
        }
        this.launched_command.start();
    }

    public void parse(BufferedReader infile) {
        try {
            String thisLine;
            while ((thisLine = infile.readLine()) != null) {
                String[] tempbuf = thisLine.split("\\s+", 8);
                if (tempbuf.length != 8) continue;
                ProcessInfo tmp = new ProcessInfo(tempbuf[1], tempbuf[7]);
                this.listproc.put(tempbuf[1], tmp);
            }
            infile.close();
        }
        catch (IOException ioe) {
            // empty catch block
        }
    }
}

