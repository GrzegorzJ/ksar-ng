/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import net.atomique.ksar.kSarConfig;
import net.atomique.ksar.kSarDesktop;

public class SSHIdentity {
    kSarDesktop mydesktop;

    public SSHIdentity(kSarDesktop hisdesktop) {
        int returnVal;
        this.mydesktop = hisdesktop;
        String filename = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose your identity file");
        chooser.setFileHidingEnabled(false);
        if (kSarConfig.sshidentity != null) {
            chooser.setCurrentDirectory(kSarConfig.sshidentity);
        }
        if ((returnVal = chooser.showOpenDialog(this.mydesktop)) == 0) {
            filename = chooser.getSelectedFile().getAbsolutePath();
            File tmp = new File(filename);
            if (tmp.exists()) {
                kSarConfig.sshidentity = tmp;
                kSarConfig.writeDefault();
            } else {
                filename = null;
            }
        } else {
            kSarConfig.sshidentity = null;
            kSarConfig.writeDefault();
        }
    }
}

