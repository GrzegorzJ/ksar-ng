/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import net.atomique.ksar.kSarConfig;

public class SolarisTrigger
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    private JButton cancelButton;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel13;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JPanel jPanel10;
    private JPanel jPanel11;
    private JPanel jPanel12;
    private JPanel jPanel13;
    private JPanel jPanel14;
    private JPanel jPanel15;
    private JPanel jPanel16;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JPanel jPanel9;
    private JScrollPane jScrollPane1;
    private JTextField jTextField1;
    private JTextField jTextField10;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JTextField jTextField6;
    private JTextField jTextField7;
    private JTextField jTextField8;
    private JTextField jTextField9;
    private JButton okButton;
    private JButton resetButton;

    public SolarisTrigger() {
        this.initComponents();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.jPanel3 = new JPanel();
        this.jPanel7 = new JPanel();
        this.jLabel4 = new JLabel();
        this.jTextField3 = new JTextField();
        this.jPanel8 = new JPanel();
        this.jLabel5 = new JLabel();
        this.jTextField4 = new JTextField();
        this.jPanel9 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jTextField5 = new JTextField();
        this.jPanel10 = new JPanel();
        this.jLabel7 = new JLabel();
        this.jTextField1 = new JTextField();
        this.jPanel11 = new JPanel();
        this.jLabel8 = new JLabel();
        this.jTextField2 = new JTextField();
        this.jPanel12 = new JPanel();
        this.jLabel9 = new JLabel();
        this.jTextField6 = new JTextField();
        this.jPanel13 = new JPanel();
        this.jLabel10 = new JLabel();
        this.jTextField7 = new JTextField();
        this.jPanel14 = new JPanel();
        this.jLabel11 = new JLabel();
        this.jTextField8 = new JTextField();
        this.jPanel15 = new JPanel();
        this.jLabel12 = new JLabel();
        this.jTextField9 = new JTextField();
        this.jPanel16 = new JPanel();
        this.jLabel13 = new JLabel();
        this.jTextField10 = new JTextField();
        this.jPanel2 = new JPanel();
        this.okButton = new JButton();
        this.cancelButton = new JButton();
        this.resetButton = new JButton();
        this.setClosable(true);
        this.setTitle("Options");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new BoxLayout(this.jPanel3, 1));
        this.jPanel7.setLayout(new FlowLayout(0));
        this.jLabel4.setText("CPU idle minimum:");
        this.jPanel7.add(this.jLabel4);
        this.jTextField3.setText(kSarConfig.solariscpuidletrigger.toString());
        this.jTextField3.setMinimumSize(new Dimension(150, 22));
        this.jTextField3.setPreferredSize(new Dimension(150, 22));
        this.jPanel7.add(this.jTextField3);
        this.jPanel3.add(this.jPanel7);
        this.jPanel8.setLayout(new FlowLayout(0));
        this.jLabel5.setText("CPU system maximum: ");
        this.jPanel8.add(this.jLabel5);
        this.jTextField4.setText(kSarConfig.solariscpusystemtrigger.toString());
        this.jTextField4.setMinimumSize(new Dimension(150, 22));
        this.jTextField4.setPreferredSize(new Dimension(150, 22));
        this.jPanel8.add(this.jTextField4);
        this.jPanel3.add(this.jPanel8);
        this.jPanel9.setLayout(new FlowLayout(0));
        this.jLabel6.setText("CPU I/O maximum");
        this.jPanel9.add(this.jLabel6);
        this.jTextField5.setText(kSarConfig.solariscpuwiotrigger.toString());
        this.jTextField5.setMinimumSize(new Dimension(150, 22));
        this.jTextField5.setPreferredSize(new Dimension(150, 22));
        this.jPanel9.add(this.jTextField5);
        this.jPanel3.add(this.jPanel9);
        this.jPanel10.setLayout(new FlowLayout(0));
        this.jLabel7.setText("CPU user maximum");
        this.jPanel10.add(this.jLabel7);
        this.jTextField1.setEditable(false);
        this.jTextField1.setText(kSarConfig.solariscpuusrtrigger.toString());
        this.jTextField1.setMinimumSize(new Dimension(150, 22));
        this.jTextField1.setPreferredSize(new Dimension(150, 22));
        this.jPanel10.add(this.jTextField1);
        this.jPanel3.add(this.jPanel10);
        this.jPanel11.setLayout(new FlowLayout(0));
        this.jLabel8.setText("Disk %busy maximum: ");
        this.jPanel11.add(this.jLabel8);
        this.jTextField2.setEditable(false);
        this.jTextField2.setText(kSarConfig.solarisdiskbusytrigger.toString());
        this.jTextField2.setMinimumSize(new Dimension(150, 22));
        this.jTextField2.setPreferredSize(new Dimension(150, 22));
        this.jPanel11.add(this.jTextField2);
        this.jPanel3.add(this.jPanel11);
        this.jPanel12.setLayout(new FlowLayout(0));
        this.jLabel9.setText("Disk avserv maximum:");
        this.jPanel12.add(this.jLabel9);
        this.jTextField6.setEditable(false);
        this.jTextField6.setText(kSarConfig.solarisdiskavservtrigger.toString());
        this.jTextField6.setMinimumSize(new Dimension(150, 22));
        this.jTextField6.setPreferredSize(new Dimension(150, 22));
        this.jPanel12.add(this.jTextField6);
        this.jPanel3.add(this.jPanel12);
        this.jPanel13.setLayout(new FlowLayout(0));
        this.jLabel10.setText("Disk avque maximum:");
        this.jPanel13.add(this.jLabel10);
        this.jTextField7.setEditable(false);
        this.jTextField7.setText(kSarConfig.solarisdiskavquetrigger.toString());
        this.jTextField7.setMinimumSize(new Dimension(150, 22));
        this.jTextField7.setPreferredSize(new Dimension(150, 22));
        this.jPanel13.add(this.jTextField7);
        this.jPanel3.add(this.jPanel13);
        this.jPanel14.setLayout(new FlowLayout(0));
        this.jLabel11.setText("Buffer %read cache minimum:");
        this.jPanel14.add(this.jLabel11);
        this.jTextField8.setEditable(false);
        this.jTextField8.setText(kSarConfig.solarisbufferrcachetrigger.toString());
        this.jTextField8.setMinimumSize(new Dimension(150, 22));
        this.jTextField8.setPreferredSize(new Dimension(150, 22));
        this.jPanel14.add(this.jTextField8);
        this.jPanel3.add(this.jPanel14);
        this.jPanel15.setLayout(new FlowLayout(0));
        this.jLabel12.setText("Run Queue Size maximum:");
        this.jPanel15.add(this.jLabel12);
        this.jTextField9.setEditable(false);
        this.jTextField9.setText(kSarConfig.solarisrqueuetrigger.toString());
        this.jTextField9.setMinimumSize(new Dimension(150, 22));
        this.jTextField9.setPreferredSize(new Dimension(150, 22));
        this.jPanel15.add(this.jTextField9);
        this.jPanel3.add(this.jPanel15);
        this.jPanel16.setLayout(new FlowLayout(0));
        this.jLabel13.setText("Page Scan maximum:");
        this.jPanel16.add(this.jLabel13);
        this.jTextField10.setEditable(false);
        this.jTextField10.setText(kSarConfig.solarispagescantrigger.toString());
        this.jTextField10.setMinimumSize(new Dimension(150, 22));
        this.jTextField10.setPreferredSize(new Dimension(150, 22));
        this.jPanel16.add(this.jTextField10);
        this.jPanel3.add(this.jPanel16);
        this.jScrollPane1.setViewportView(this.jPanel3);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.getContentPane().add((Component)this.jPanel1, "Center");
        this.okButton.setText("Ok");
        this.okButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                SolarisTrigger.this.okButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.okButton);
        this.cancelButton.setText("Cancel");
        this.cancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                SolarisTrigger.this.cancelButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.cancelButton);
        this.resetButton.setText("Reset");
        this.resetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                SolarisTrigger.this.resetButtonActionPerformed(evt);
            }
        });
        this.jPanel2.add(this.resetButton);
        this.getContentPane().add((Component)this.jPanel2, "South");
        this.pack();
    }

    private void cancelButtonActionPerformed(ActionEvent evt) {
        this.setVisible(true);
        this.dispose();
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        this.jTextField3.setText(kSarConfig.solariscpuidletrigger.toString());
        this.jTextField4.setText(kSarConfig.solariscpusystemtrigger.toString());
        this.jTextField5.setText(kSarConfig.solariscpuwiotrigger.toString());
        this.jTextField1.setText(kSarConfig.solariscpuusrtrigger.toString());
        this.jTextField2.setText(kSarConfig.solarisdiskbusytrigger.toString());
        this.jTextField6.setText(kSarConfig.solarisdiskavservtrigger.toString());
        this.jTextField7.setText(kSarConfig.solarisdiskavquetrigger.toString());
        this.jTextField8.setText(kSarConfig.solarisbufferrcachetrigger.toString());
        this.jTextField9.setText(kSarConfig.solarisrqueuetrigger.toString());
        this.jTextField10.setText(kSarConfig.solarispagescantrigger.toString());
    }

    private void okButtonActionPerformed(ActionEvent evt) {
        kSarConfig.solariscpuidletrigger = new Double(this.jTextField3.getText());
        kSarConfig.solariscpusystemtrigger = new Double(this.jTextField4.getText());
        kSarConfig.solariscpuwiotrigger = new Double(this.jTextField5.getText());
        kSarConfig.solariscpuusrtrigger = new Double(this.jTextField1.getText());
        kSarConfig.solarisdiskbusytrigger = new Double(this.jTextField2.getText());
        kSarConfig.solarisdiskavservtrigger = new Double(this.jTextField6.getText());
        kSarConfig.solarisdiskavquetrigger = new Double(this.jTextField7.getText());
        kSarConfig.solarisbufferrcachetrigger = new Double(this.jTextField8.getText());
        kSarConfig.solarisrqueuetrigger = new Double(this.jTextField9.getText());
        kSarConfig.solarispagescantrigger = new Double(this.jTextField10.getText());
        kSarConfig.writeDefault();
        this.setVisible(true);
        this.dispose();
    }

}

