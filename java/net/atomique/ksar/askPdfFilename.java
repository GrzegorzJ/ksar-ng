/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class askPdfFilename {
    kSar mysar;

    public askPdfFilename(kSar hissar) {
        this.mysar = hissar;
    }

    public String run() {
        int returnVal;
        String filename = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Export PDF");
        if (kSarConfig.lastExportDirectory != null) {
            chooser.setCurrentDirectory(kSarConfig.lastExportDirectory);
        }
        if ((returnVal = chooser.showSaveDialog(this.mysar.myUI)) == 0) {
            filename = chooser.getSelectedFile().getAbsolutePath();
            kSarConfig.lastExportDirectory = chooser.getSelectedFile();
            if (!kSarConfig.lastExportDirectory.isDirectory()) {
                kSarConfig.lastExportDirectory = kSarConfig.lastExportDirectory.getParentFile();
                kSarConfig.writeDefault();
            }
        }
        if (filename == null) {
            return null;
        }
        if (new File(filename).exists()) {
            Object[] choix = new String[]{"Yes", "No"};
            int resultat = JOptionPane.showOptionDialog(null, "Overwrite " + filename + " ?", "File Exist", 1, 3, null, choix, choix[1]);
            if (resultat != 0) {
                return null;
            }
        }
        return filename;
    }
}

