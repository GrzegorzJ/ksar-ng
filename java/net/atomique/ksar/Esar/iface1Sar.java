/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class iface1Sar
extends AllGraph {
    private TimeSeries t_rxpck;
    private TimeSeries t_txpck;
    private TimeSeries t_rxbyt;
    private TimeSeries t_txbyt;
    private TimeSeries t_ierr;
    private TimeSeries t_oerr;
    private TimeSeries t_util;
    private String ifName;
    private String ifOpt = new String("");

    public iface1Sar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Interface traffic");
        this.ifName = s1;
        this.t_rxpck = new TimeSeries((Comparable)((Object)"ipacket/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " ipacket/s", this.t_rxpck);
        this.t_txpck = new TimeSeries((Comparable)((Object)"opacket/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " opacket/s", this.t_txpck);
        this.t_rxbyt = new TimeSeries((Comparable)((Object)"ibits/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " ibits/s", this.t_rxbyt);
        this.t_txbyt = new TimeSeries((Comparable)((Object)"obits/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " obits/s", this.t_txbyt);
        this.t_ierr = new TimeSeries((Comparable)((Object)"ierror/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " ierror/s", this.t_ierr);
        this.t_oerr = new TimeSeries((Comparable)((Object)"oerror/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " oerror/s", this.t_oerr);
        this.t_util = new TimeSeries((Comparable)((Object)"%util"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " %util", this.t_util);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7) {
        this.t_rxpck.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_txpck.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_rxbyt.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_txbyt.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_ierr.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_oerr.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_util.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        ++this.number_of_sample;
    }

    public void setifOpt(String s) {
        this.ifOpt = s;
    }

    public XYDataset createpck() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxpck);
        graphcollection.addSeries(this.t_txpck);
        return graphcollection;
    }

    public XYDataset createbyt() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_rxbyt);
        graphcollection.addSeries(this.t_txbyt);
        return graphcollection;
    }

    public XYDataset createerr() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_ierr);
        graphcollection.addSeries(this.t_oerr);
        return graphcollection;
    }

    public XYDataset createutil() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_util);
        return graphcollection;
    }

    public String getcheckBoxTitle() {
        return "Interface " + this.ifName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARIFACE1", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " " + this.ifName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createpck();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("ipacket/opacket /s"), (XYItemRenderer)minichart1);
        XYDataset bytset = this.createbyt();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(bytset, null, (ValueAxis)new NumberAxis("ibits/obits /s"), (XYItemRenderer)minichart2);
        XYDataset cmpset = this.createerr();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(cmpset, null, (ValueAxis)new NumberAxis("ierr/oerr /s"), (XYItemRenderer)minichart3);
        XYDataset mcstset = this.createutil();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color7);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(mcstset, null, (ValueAxis)new NumberAxis("%util"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        return mychart;
    }
}

