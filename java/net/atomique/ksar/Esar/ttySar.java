/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ttySar
extends AllGraph {
    private final TimeSeries t_rawch;
    private final TimeSeries t_canch;
    private final TimeSeries t_outch;
    private final TimeSeries t_rcvin;
    private final TimeSeries t_xmtin;
    private final TimeSeries t_mdmin;
    private final TimeSeriesCollection tscollection;
    private final String cpuName;

    public ttySar(kSar hissar, String cpuID) {
        super(hissar);
        this.Title = "Tty for CPU " + cpuID;
        this.cpuName = cpuID;
        this.t_rawch = new TimeSeries((Comparable)((Object)"Rawch/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Rawch/s", this.t_rawch);
        this.t_canch = new TimeSeries((Comparable)((Object)"Canch/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Canch/s", this.t_canch);
        this.t_outch = new TimeSeries((Comparable)((Object)"Outch/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Outch/s", this.t_outch);
        this.t_rcvin = new TimeSeries((Comparable)((Object)"Rcvin/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Rcvin/s", this.t_rcvin);
        this.t_xmtin = new TimeSeries((Comparable)((Object)"Xmtin/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Xmtin/s", this.t_xmtin);
        this.t_mdmin = new TimeSeries((Comparable)((Object)"Mdmin/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Mdmin/s", this.t_mdmin);
        this.tscollection = new TimeSeriesCollection();
        this.tscollection.addSeries(this.t_rawch);
        this.tscollection.addSeries(this.t_canch);
        this.tscollection.addSeries(this.t_outch);
        this.tscollection.addSeries(this.t_rcvin);
        this.tscollection.addSeries(this.t_xmtin);
        this.tscollection.addSeries(this.t_mdmin);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init) {
        this.t_rawch.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_canch.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_outch.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        this.t_rcvin.add((RegularTimePeriod)now, (Number)val4Init, this.do_notify());
        this.t_xmtin.add((RegularTimePeriod)now, (Number)val5Init, this.do_notify());
        this.t_mdmin.add((RegularTimePeriod)now, (Number)val6Init, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARTTY", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.tscollection, null, (ValueAxis)new NumberAxis("per second"), (XYItemRenderer)minichart1);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

