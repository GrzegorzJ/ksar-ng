/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StackedXYAreaRenderer2
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimePeriod
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.time.TimeTableXYDataset
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

public class cpuSar
extends AllGraph {
    private final Trigger cpuidletrigger;
    private final Trigger cpusystemtrigger;
    private final Trigger cpuwiotrigger;
    private final Trigger cpuusrtrigger;
    private final TimeTableXYDataset stacked_used;
    private final TimeTableXYDataset stacked_wio;
    private final TimeSeries t_usr;
    private final TimeSeries t_sys;
    private final TimeSeries t_wio;
    private final TimeSeries t_idle;
    private final TimeSeries t_w_io;
    private final TimeSeries t_w_swap;
    private final TimeSeries t_w_pio;
    private final TimeSeriesCollection used_collection;
    private final TimeSeriesCollection idle_collection;
    private String cpuName = new String("");

    public cpuSar(kSar hissar, String cpuID) {
        super(hissar);
        this.Title = new String("CPU " + cpuID);
        this.cpuName = cpuID;
        this.t_usr = new TimeSeries((Comparable)((Object)"User"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " User", this.t_usr);
        this.t_sys = new TimeSeries((Comparable)((Object)"System"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " System", this.t_sys);
        this.t_wio = new TimeSeries((Comparable)((Object)"Waiting I/O"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Wait I/O", this.t_wio);
        this.t_idle = new TimeSeries((Comparable)((Object)"Idle"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " Idle", this.t_idle);
        this.t_w_io = new TimeSeries((Comparable)((Object)"%W in io"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %W in io", this.t_w_io);
        this.t_w_swap = new TimeSeries((Comparable)((Object)"%W in swap"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %W in swap", this.t_w_swap);
        this.t_w_pio = new TimeSeries((Comparable)((Object)"%Win pio"), (Class)Second.class);
        this.mysar.dispo.put(this.Title + " %Win pio", this.t_w_pio);
        this.stacked_used = new TimeTableXYDataset();
        this.stacked_wio = new TimeTableXYDataset();
        this.cpuidletrigger = new Trigger(this.mysar, this, "idle", this.t_idle, "down");
        this.cpusystemtrigger = new Trigger(this.mysar, this, "system", this.t_sys, "up");
        this.cpuwiotrigger = new Trigger(this.mysar, this, "wio", this.t_wio, "up");
        this.cpuusrtrigger = new Trigger(this.mysar, this, "usr", this.t_usr, "up");
        this.cpuidletrigger.setTriggerValue(kSarConfig.solariscpuidletrigger);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.solariscpusystemtrigger);
        this.cpuwiotrigger.setTriggerValue(kSarConfig.solariscpuwiotrigger);
        this.cpuusrtrigger.setTriggerValue(kSarConfig.solariscpuusrtrigger);
        this.used_collection = new TimeSeriesCollection();
        this.used_collection.addSeries(this.t_usr);
        this.used_collection.addSeries(this.t_sys);
        this.used_collection.addSeries(this.t_wio);
        this.idle_collection = new TimeSeriesCollection();
        this.idle_collection.addSeries(this.t_idle);
    }

    public void doclosetrigger() {
        this.cpuidletrigger.doclose();
        this.cpusystemtrigger.doclose();
        this.cpuwiotrigger.doclose();
        this.cpuusrtrigger.doclose();
    }

    public void add(Second now, Float usrInit, Float sysInit, Float wioInit, Float idleInit, Float w_IO, Float w_swap, Float w_pio) {
        this.t_usr.add((RegularTimePeriod)now, (Number)usrInit, this.do_notify());
        this.t_sys.add((RegularTimePeriod)now, (Number)sysInit, this.do_notify());
        this.t_wio.add((RegularTimePeriod)now, (Number)wioInit, this.do_notify());
        this.t_idle.add((RegularTimePeriod)now, (Number)idleInit, this.do_notify());
        this.t_w_io.add((RegularTimePeriod)now, (Number)w_IO, this.do_notify());
        this.t_w_swap.add((RegularTimePeriod)now, (Number)w_swap, this.do_notify());
        this.t_w_pio.add((RegularTimePeriod)now, (Number)w_pio, this.do_notify());
        if (this.mysar.showtrigger) {
            this.cpuidletrigger.doMarker(now, idleInit);
            this.cpusystemtrigger.doMarker(now, sysInit);
            this.cpuwiotrigger.doMarker(now, wioInit);
            this.cpuusrtrigger.doMarker(now, usrInit);
        }
        this.stacked_used.add((TimePeriod)now, (Number)usrInit, "User", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)sysInit, "System", this.do_notify());
        this.stacked_used.add((TimePeriod)now, (Number)wioInit, "Waiting I/O", this.do_notify());
        this.stacked_wio.add((TimePeriod)now, (Number)w_IO, "W in IO", this.do_notify());
        this.stacked_wio.add((TimePeriod)now, (Number)w_swap, "W in swap", this.do_notify());
        this.stacked_wio.add((TimePeriod)now, (Number)w_pio, "W in pio", this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARCPU", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYPlot subplot1;
        NumberAxis usedaxis = new NumberAxis("% used cpu");
        if (this.mysar.show100axiscpu) {
            usedaxis.setRange(0.0, 100.0);
        }
        if (this.mysar.showstackedcpu) {
            StackedXYAreaRenderer2 renderer = new StackedXYAreaRenderer2();
            renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
            renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
            renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
            subplot1 = new XYPlot((XYDataset)this.stacked_used, (ValueAxis)new DateAxis(null), (ValueAxis)usedaxis, (XYItemRenderer)renderer);
        } else {
            StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
            minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
            minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
            minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
            minichart1.setSeriesPaint(2, (Paint)kSarConfig.color2);
            subplot1 = new XYPlot((XYDataset)this.used_collection, null, (ValueAxis)usedaxis, (XYItemRenderer)minichart1);
        }
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.idle_collection, null, (ValueAxis)new NumberAxis("% idle cpu"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 3);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.cpuidletrigger.setTriggerValue(kSarConfig.solariscpuidletrigger);
        this.cpuidletrigger.tagMarker(subplot2);
        this.cpusystemtrigger.setTriggerValue(kSarConfig.solariscpusystemtrigger);
        this.cpusystemtrigger.tagMarker(subplot1);
        this.cpuwiotrigger.setTriggerValue(kSarConfig.solariscpuwiotrigger);
        this.cpuwiotrigger.tagMarker(subplot1);
        this.cpuusrtrigger.setTriggerValue(kSarConfig.solariscpuusrtrigger);
        this.cpuusrtrigger.tagMarker(subplot1);
        return this.mygraph;
    }
}

