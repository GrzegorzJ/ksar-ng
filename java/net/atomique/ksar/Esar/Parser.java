/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.Esar;

import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;

public class Parser {
    kSar mysar;
    Float val1;
    Float val2;
    Float val3;
    Float val4;
    Float val5;
    Float val6;
    Float val7;
    Float val8;
    Float val9;
    Float val10;
    Float val11;
    Float val12;
    Float val13;
    int heure = 0;
    int minute = 0;
    int seconde = 0;
    Second now = new Second(0, 0, 0, 1, 1, 1970);
    String statType = "none";
    int stattypenum = 0;
    int entreetype;
    int firstwastime;
    private squeueSar sarSQUEUE = null;
    private rqueueSar sarRQUEUE = null;
    private memSar sarMEM = null;
    private loadSar sarLOAD = null;
    private rawipSar sarRAWIP = null;
    private udpSar sarUDP = null;
    private tcpSar sarTCP = null;
    private rpctcpSar sarRPCTCP = null;
    private rpcudpSar sarRPCUDP = null;
    private rpcdtcpSar sarRPCDTCP = null;
    private rpcdudpSar sarRPCDUDP = null;
    private memorySar sarMEMORY = null;
    private nfsdv2Sar sarNFSDv2 = null;
    private nfsv2Sar sarNFSv2 = null;
    private nfsdv3Sar sarNFSDv3 = null;
    private nfsv3Sar sarNFSv3 = null;
    private ipSar sarIP = null;

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String first, StringTokenizer matcher) {
        this.val1 = null;
        this.val2 = null;
        this.val3 = null;
        this.val4 = null;
        this.val5 = null;
        this.val5 = null;
        this.val6 = null;
        this.val7 = null;
        this.val8 = null;
        this.val9 = null;
        this.val10 = null;
        this.val11 = null;
        this.val12 = null;
        this.val13 = null;
        if (thisLine.indexOf("DISK ID") > 0) {
            return 1;
        }
        if (thisLine.indexOf("namei/s") > 0) {
            this.statType = "namei/s";
            if (!this.mysar.hasfilenode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.filetreenode);
                }
                this.mysar.hasfilenode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("bread/s") > 0) {
            this.statType = "bread/s";
            if (!this.mysar.hasbuffernode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.buffertreenode);
                }
                this.mysar.hasbuffernode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("scall/s") > 0) {
            this.statType = "scall/s";
            if (!this.mysar.hasscallnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.scalltreenode);
                }
                this.mysar.hasscallnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("avqr") > 0) {
            this.statType = "avqr";
            if (!this.mysar.hasdisknode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                }
                this.mysar.hasdisknode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("pgout/s") > 0) {
            this.statType = "pgout/s";
            if (!this.mysar.haspaging1node) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.paging1treenode);
                }
                this.mysar.haspaging1node = true;
            }
            return 1;
        }
        if (thisLine.indexOf("sema/s") > 0) {
            this.statType = "sema/s";
            if (!this.mysar.hasmsgnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.msgtreenode);
                }
                this.mysar.hasmsgnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("ppgin/s") > 0) {
            this.statType = "ppgin/s";
            if (!this.mysar.haspaging2node) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.paging2treenode);
                }
                this.mysar.haspaging2node = true;
            }
            return 1;
        }
        if (thisLine.indexOf("runq-sz") > 0) {
            this.statType = "runq-sz";
            if (this.sarRQUEUE == null) {
                this.sarRQUEUE = new rqueueSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRQUEUE.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarRqueueSar", this.sarRQUEUE);
                this.sarRQUEUE.setGraphLink("EsarRqueueSar");
            }
            if (this.sarSQUEUE == null) {
                this.sarSQUEUE = new squeueSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarSQUEUE.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarSqueueSar", this.sarSQUEUE);
                this.sarSQUEUE.setGraphLink("EsarSqueueSar");
            }
            return 1;
        }
        if (thisLine.indexOf("freemem") > 0) {
            this.statType = "freemem";
            if (this.sarMEM == null) {
                this.sarMEM = new memSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarMEM.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarmemSar", this.sarMEM);
                this.sarMEM.setGraphLink("EsarmemSar");
            }
            return 1;
        }
        if (thisLine.indexOf("%w_io") > 0) {
            this.statType = "%w_io";
            if (!this.mysar.hascpunode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.cpustreenode);
                }
                this.mysar.hascpunode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("swpin/s") > 0) {
            this.statType = "swpin/s";
            if (!this.mysar.hasswapingnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.swapingtreenode);
                }
                this.mysar.hasswapingnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("rawch/s") > 0) {
            this.statType = "rawch/s";
            if (!this.mysar.hasttynode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.ttytreenode);
                }
                this.mysar.hasttynode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("nproc") > 0) {
            this.statType = "nproc";
            if (this.sarLOAD == null) {
                this.sarLOAD = new loadSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarLOAD.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarloadSar", this.sarLOAD);
                this.sarLOAD.setGraphLink("EsarloadSar");
            }
            return 1;
        }
        if (thisLine.indexOf("ipacket/s") > 0) {
            this.statType = "ipacket/s";
            if (!this.mysar.hasifnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.ifacetreenode);
                }
                this.mysar.hasifnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("bcstrcv/s") > 0) {
            this.statType = "bcstrcv/s";
            if (!this.mysar.hasifnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.ifacetreenode);
                }
                this.mysar.hasifnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("pset") > 0) {
            this.statType = "pset";
            if (!this.mysar.haspsetnode) {
                if (this.mysar.myUI != null) {
                    this.mysar.add2tree(this.mysar.graphtree, this.mysar.psettreenode);
                }
                this.mysar.haspsetnode = true;
            }
            return 1;
        }
        if (thisLine.indexOf("RAWIP") > 0) {
            this.statType = "RAWIP";
            if (this.sarRAWIP == null) {
                this.sarRAWIP = new rawipSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRAWIP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarrawipSar", this.sarRAWIP);
                this.sarRAWIP.setGraphLink("EsarrawipSar");
            }
            return 1;
        }
        if (thisLine.indexOf("UDP") > 0) {
            this.statType = "UDP";
            if (this.sarUDP == null) {
                this.sarUDP = new udpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarUDP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarudpSar", this.sarUDP);
                this.sarUDP.setGraphLink("EsarudpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("TCP") > 0) {
            this.statType = "TCP";
            if (this.sarTCP == null) {
                this.sarTCP = new tcpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarTCP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsartcpSar", this.sarTCP);
                this.sarTCP.setGraphLink("EsartcpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("RPCD(tcp)") > 0) {
            this.statType = "RPCD(tcp)";
            if (this.sarRPCDTCP == null) {
                this.sarRPCDTCP = new rpcdtcpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRPCDTCP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarrpcdtcpSar", this.sarRPCDTCP);
                this.sarRPCDTCP.setGraphLink("EsarrpcdtcpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("RPC(tcp)") > 0) {
            this.statType = "RPC(tcp)";
            if (this.sarRPCTCP == null) {
                this.sarRPCTCP = new rpctcpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRPCTCP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarrpctcpSar", this.sarRPCTCP);
                this.sarRPCTCP.setGraphLink("EsarrpctcpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("RPCD(udp)") > 0) {
            this.statType = "RPCD(udp)";
            if (this.sarRPCDUDP == null) {
                this.sarRPCDUDP = new rpcdudpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRPCDUDP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarrpcdudpSar", this.sarRPCDUDP);
                this.sarRPCDUDP.setGraphLink("EsarrpcdudpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("RPC(udp)") > 0) {
            this.statType = "RPC(udp)";
            if (this.sarRPCUDP == null) {
                this.sarRPCUDP = new rpcudpSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarRPCUDP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarrpcudpSar", this.sarRPCUDP);
                this.sarRPCUDP.setGraphLink("EsarrpcudpSar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFS v2   create") > 0) {
            this.statType = "NFSv21";
            if (this.sarNFSv2 == null) {
                this.sarNFSv2 = new nfsv2Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSv2.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsv2Sar", this.sarNFSv2);
                this.sarNFSv2.setGraphLink("Esarnfsv2Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFS v2  getattr") > 0) {
            this.statType = "NFSv22";
            if (this.sarNFSv2 == null) {
                this.sarNFSv2 = new nfsv2Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSv2.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsv2Sar", this.sarNFSv2);
                this.sarNFSv2.setGraphLink("Esarnfsv2Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFSD v2   create") > 0) {
            this.statType = "NFSDv21";
            if (this.sarNFSDv2 == null) {
                this.sarNFSDv2 = new nfsdv2Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSDv2.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsdv2Sar", this.sarNFSDv2);
                this.sarNFSDv2.setGraphLink("Esarnfsdv2Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFSD v2  getattr") > 0) {
            this.statType = "NFSDv22";
            if (this.sarNFSDv2 == null) {
                this.sarNFSDv2 = new nfsdv2Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSDv2.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsdv2Sar", this.sarNFSDv2);
                this.sarNFSDv2.setGraphLink("Esarnfsdv2Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFS v3   access") > 0) {
            this.statType = "NFSv31";
            if (this.sarNFSv3 == null) {
                this.sarNFSv3 = new nfsv3Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSv3.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsv3Sar", this.sarNFSv3);
                this.sarNFSv3.setGraphLink("Esarnfsv3Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFS v3   fsstat") > 0) {
            this.statType = "NFSv32";
            if (this.sarNFSv3 == null) {
                this.sarNFSv3 = new nfsv3Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSv3.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsv3Sar", this.sarNFSv3);
                this.sarNFSv3.setGraphLink("Esarnfsv3Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFSD v3   access") > 0) {
            this.statType = "NFSDv31";
            if (this.sarNFSDv3 == null) {
                this.sarNFSDv3 = new nfsdv3Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSDv3.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsdv3Sar", this.sarNFSDv3);
                this.sarNFSDv3.setGraphLink("Esarnfsdv3Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFSD v3   fsstat") > 0) {
            this.statType = "NFSDv32";
            if (this.sarNFSDv3 == null) {
                this.sarNFSDv3 = new nfsdv3Sar(this.mysar);
                if (this.mysar.myUI != null) {
                    if (!this.mysar.hasnfsnode) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.nfstreenode);
                        this.mysar.hasnfsnode = true;
                    }
                    this.sarNFSDv3.addtotree(this.mysar.nfstreenode);
                }
                this.mysar.pdfList.put("Esarnfsdv3Sar", this.sarNFSDv3);
                this.sarNFSDv3.setGraphLink("Esarnfsdv3Sar");
            }
            return 1;
        }
        if (thisLine.indexOf("NFS v4   access") > 0) {
            this.statType = "NFSv41";
            return 1;
        }
        if (thisLine.indexOf("NFS v4   commit") > 0) {
            this.statType = "NFSv42";
            return 1;
        }
        if (thisLine.indexOf("NFS v4     lock") > 0) {
            this.statType = "NFSv43";
            return 1;
        }
        if (thisLine.indexOf("NFSD v4   access") > 0) {
            this.statType = "NFSDv41";
            return 1;
        }
        if (thisLine.indexOf("NFSD v4   commit") > 0) {
            this.statType = "NFSDv42";
            return 1;
        }
        if (thisLine.indexOf("NFSD v4     lock") > 0) {
            this.statType = "NFSDv43";
            return 1;
        }
        if (thisLine.indexOf("Locked") > 0) {
            this.statType = "Locked";
            if (this.sarMEMORY == null) {
                this.sarMEMORY = new memorySar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarMEMORY.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsarmemorySar", this.sarMEMORY);
                this.sarMEMORY.setGraphLink("EsarmemorySar");
            }
            return 1;
        }
        if (thisLine.indexOf("IP inRcvs/s") > 0) {
            this.statType = "IP";
            if (this.sarIP == null) {
                this.sarIP = new ipSar(this.mysar);
                if (this.mysar.myUI != null) {
                    this.sarIP.addtotree(this.mysar.graphtree);
                }
                this.mysar.pdfList.put("EsaripSar", this.sarIP);
                this.sarIP.setGraphLink("EsaripSar");
            }
            return 1;
        }
        String[] sarTime = first.split(":");
        if (sarTime.length != 3) {
            return 1;
        }
        this.heure = Integer.parseInt(sarTime[0]);
        this.minute = Integer.parseInt(sarTime[1]);
        this.seconde = Integer.parseInt(sarTime[2]);
        this.now = new Second(this.seconde, this.minute, this.heure, this.mysar.day, this.mysar.month, this.mysar.year);
        if (this.mysar.statstart == null) {
            this.mysar.statstart = new String(this.now.toString());
            this.mysar.startofgraph = this.now;
        }
        if (!this.mysar.datefound.contains((Object)this.now)) {
            this.mysar.datefound.add(this.now);
        }
        if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
            this.mysar.lastever = this.now;
            this.mysar.statend = new String(this.mysar.lastever.toString());
            this.mysar.endofgraph = this.mysar.lastever;
        }
        try {
            if (this.statType.equals("%w_io")) {
                String cpuid = matcher.nextToken();
                cpuSar mysarcpu = (cpuSar)this.mysar.cpuSarList.get(cpuid + "-cpu");
                if (mysarcpu == null) {
                    mysarcpu = new cpuSar(this.mysar, cpuid);
                    this.mysar.cpuSarList.put(cpuid + "-cpu", mysarcpu);
                    this.mysar.pdfList.put(cpuid + "-cpu", mysarcpu);
                    mysarcpu.setGraphLink("cpu-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarcpu.addtotree(this.mysar.cpustreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                mysarcpu.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("namei/s")) {
                String cpuid = matcher.nextToken();
                fileSar mysarfile = (fileSar)this.mysar.fileSarList.get(cpuid + "-file");
                if (mysarfile == null) {
                    mysarfile = new fileSar(this.mysar, cpuid);
                    this.mysar.fileSarList.put(cpuid + "-file", mysarfile);
                    this.mysar.pdfList.put(cpuid + "-file", mysarfile);
                    mysarfile.setGraphLink("file-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarfile.addtotree(this.mysar.filetreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                mysarfile.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("scall/s")) {
                String cpuid = matcher.nextToken();
                syscallSar mysarscall = (syscallSar)this.mysar.scallSarList.get(cpuid + "-syscall");
                if (mysarscall == null) {
                    mysarscall = new syscallSar(this.mysar, cpuid);
                    this.mysar.scallSarList.put(cpuid + "-syscall", mysarscall);
                    this.mysar.pdfList.put(cpuid + "-syscall", mysarscall);
                    mysarscall.setGraphLink("syscall-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarscall.addtotree(this.mysar.scalltreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                mysarscall.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10);
                return 1;
            }
            if (this.statType.equals("bread/s")) {
                String cpuid = matcher.nextToken();
                bufferSar mysarbuffer = (bufferSar)this.mysar.bufferSarList.get(cpuid + "-buffer");
                if (mysarbuffer == null) {
                    mysarbuffer = new bufferSar(this.mysar, cpuid);
                    this.mysar.bufferSarList.put(cpuid + "-buffer", mysarbuffer);
                    this.mysar.pdfList.put(cpuid + "-buffer", mysarbuffer);
                    mysarbuffer.setGraphLink("buffer-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarbuffer.addtotree(this.mysar.buffertreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                mysarbuffer.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if (this.statType.equals("avqr")) {
                diskwaitSar mydiskwait = null;
                String disktag = matcher.nextToken();
                diskxferSar mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(disktag + "Esardiskxfer");
                if (mydiskxfer == null) {
                    diskName tmp = new diskName(disktag);
                    this.mysar.AlternateDiskName.put(disktag, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(disktag));
                    mydiskxfer = new diskxferSar(this.mysar, disktag, tmp);
                    this.mysar.disksSarList.put(disktag + "Esardiskxfer", mydiskxfer);
                    mydiskwait = new diskwaitSar(this.mysar, disktag, tmp);
                    this.mysar.disksSarList.put(disktag + "Esardiskwait", mydiskwait);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    this.mysar.pdfList.put(disktag + "Esardiskxfer", mydiskxfer);
                    mydiskxfer.addtotree(mydisk);
                    mydiskwait.addtotree(mydisk);
                    this.mysar.pdfList.put(disktag + "Esardiskwait", mydiskwait);
                    mydiskxfer.setGraphLink(disktag + "Esardiskxfer");
                    mydiskwait.setGraphLink(disktag + "Esardiskwait");
                    this.mysar.add2tree(this.mysar.diskstreenode, mydisk);
                } else {
                    mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(disktag + "Esardiskwait");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                mydiskxfer.add(this.now, this.val2, this.val3, this.val4, this.val5, this.val9);
                mydiskwait.add(this.now, this.val1, this.val6, this.val7, this.val8);
                return 1;
            }
            if (this.statType.equals("pgout/s")) {
                String cpuid = matcher.nextToken();
                paging1Sar mysarpaging1 = (paging1Sar)this.mysar.paging1SarList.get(cpuid + "-paging1");
                if (mysarpaging1 == null) {
                    mysarpaging1 = new paging1Sar(this.mysar, cpuid);
                    this.mysar.paging1SarList.put(cpuid + "-paging1", mysarpaging1);
                    this.mysar.pdfList.put(cpuid + "-paging1", mysarpaging1);
                    mysarpaging1.setGraphLink("paging1-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarpaging1.addtotree(this.mysar.paging1treenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                mysarpaging1.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("sema/s")) {
                String cpuid = matcher.nextToken();
                msgSar mysarmsg = (msgSar)this.mysar.msgSarList.get(cpuid + "-msg");
                if (mysarmsg == null) {
                    mysarmsg = new msgSar(this.mysar, cpuid);
                    this.mysar.msgSarList.put(cpuid + "-msg", mysarmsg);
                    this.mysar.pdfList.put(cpuid + "-msg", mysarmsg);
                    mysarmsg.setGraphLink("msg-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarmsg.addtotree(this.mysar.msgtreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                mysarmsg.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("ppgin/s")) {
                String cpuid = matcher.nextToken();
                paging2Sar mysarpaging2 = (paging2Sar)this.mysar.paging2SarList.get(cpuid + "-paging2");
                if (mysarpaging2 == null) {
                    mysarpaging2 = new paging2Sar(this.mysar, cpuid);
                    this.mysar.paging2SarList.put(cpuid + "-paging2", mysarpaging2);
                    this.mysar.pdfList.put(cpuid + "-paging2", mysarpaging2);
                    mysarpaging2.setGraphLink("paging2-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarpaging2.addtotree(this.mysar.paging2treenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mysarpaging2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("runq-sz")) {
                if (!matcher.hasMoreElements()) {
                    return 1;
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.sarRQUEUE.add(this.now, this.val1, this.val2);
                if (matcher.hasMoreElements()) {
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.sarSQUEUE.add(this.now, this.val3, this.val4);
                }
                return 1;
            }
            if (this.statType.equals("freemem")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarMEM.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("swpin/s")) {
                String cpuid = matcher.nextToken();
                swapingSar mysarswaping = (swapingSar)this.mysar.swapingSarList.get(cpuid + "-swaping");
                if (mysarswaping == null) {
                    mysarswaping = new swapingSar(this.mysar, cpuid);
                    this.mysar.swapingSarList.put(cpuid + "-swaping", mysarswaping);
                    this.mysar.pdfList.put(cpuid + "-swaping", mysarswaping);
                    mysarswaping.setGraphLink("swaping-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysarswaping.addtotree(this.mysar.swapingtreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                mysarswaping.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("rawch/s")) {
                String cpuid = matcher.nextToken();
                ttySar mysartty = (ttySar)this.mysar.ttySarList.get(cpuid + "-tty");
                if (mysartty == null) {
                    mysartty = new ttySar(this.mysar, cpuid);
                    this.mysar.ttySarList.put(cpuid + "-tty", mysartty);
                    this.mysar.pdfList.put(cpuid + "-tty", mysartty);
                    mysartty.setGraphLink("tty-" + cpuid);
                    if (this.mysar.myUI != null) {
                        mysartty.addtotree(this.mysar.ttytreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mysartty.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("nproc")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarLOAD.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("ipacket/s")) {
                String ifname = matcher.nextToken();
                iface1Sar mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(ifname + "-if1");
                if (mysarif1 == null) {
                    mysarif1 = new iface1Sar(this.mysar, ifname);
                    iface2Sar mysarif2 = new iface2Sar(this.mysar, ifname);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(ifname);
                    if (this.mysar.myUI != null) {
                        mysarif1.addtotree(myif);
                        mysarif2.addtotree(myif);
                    }
                    this.mysar.ifaceSarList.put(ifname + "-if1", mysarif1);
                    mysarif1.setGraphLink(ifname + "-if1");
                    this.mysar.ifaceSarList.put(ifname + "-if2", mysarif2);
                    mysarif2.setGraphLink(ifname + "-if2");
                    this.mysar.pdfList.put(ifname + "-if1", mysarif1);
                    this.mysar.pdfList.put(ifname + "-if2", mysarif2);
                    this.mysar.ifacetreenode.add(myif);
                } else {
                    iface2Sar mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(ifname + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                String tmpval7 = matcher.nextToken();
                this.val7 = "N/A".equals(tmpval7) ? new Float(0.0f) : new Float(tmpval7);
                mysarif1.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("bcstrcv/s")) {
                iface2Sar mysarif2;
                String ifname = matcher.nextToken();
                iface1Sar mysarif1 = (iface1Sar)this.mysar.ifaceSarList.get(ifname + "-if1");
                if (mysarif1 == null) {
                    mysarif1 = new iface1Sar(this.mysar, ifname);
                    mysarif2 = new iface2Sar(this.mysar, ifname);
                    DefaultMutableTreeNode myif = new DefaultMutableTreeNode(ifname);
                    if (this.mysar.myUI != null) {
                        mysarif1.addtotree(myif);
                        mysarif2.addtotree(myif);
                    }
                    this.mysar.ifaceSarList.put(ifname + "-if1", mysarif1);
                    mysarif1.setGraphLink(ifname + "-if1");
                    this.mysar.ifaceSarList.put(ifname + "-if2", mysarif2);
                    mysarif2.setGraphLink(ifname + "-if2");
                    this.mysar.pdfList.put(ifname + "-if1", mysarif1);
                    this.mysar.pdfList.put(ifname + "-if2", mysarif2);
                    this.mysar.ifacetreenode.add(myif);
                } else {
                    mysarif2 = (iface2Sar)this.mysar.ifaceSarList.get(ifname + "-if2");
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                mysarif2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("pset")) {
                String psetid = matcher.nextToken();
                psetSar mysarpset = (psetSar)this.mysar.psetSarList.get(psetid + "-pset");
                if (mysarpset == null) {
                    mysarpset = new psetSar(this.mysar, psetid);
                    this.mysar.psetSarList.put(psetid + "-pset", mysarpset);
                    this.mysar.pdfList.put(psetid + "-pset", mysarpset);
                    mysarpset.setGraphLink("pset-" + psetid);
                    if (this.mysar.myUI != null) {
                        mysarpset.addtotree(this.mysar.psettreenode);
                    }
                }
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                mysarpset.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("RAWIP")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarRAWIP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("UDP")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarUDP.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("IP")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarIP.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("TCP")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.sarTCP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if (this.statType.equals("RPC(udp)")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.sarRPCUDP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10);
                return 1;
            }
            if (this.statType.equals("RPC(tcp)")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.sarRPCTCP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10);
                return 1;
            }
            if (this.statType.equals("RPCD(tcp)")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarRPCDTCP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("RPCD(udp)")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarRPCDUDP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("Locked")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarMEMORY.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("NFSDv31")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.val11 = new Float(matcher.nextToken());
                this.val12 = new Float(matcher.nextToken());
                this.val13 = new Float(matcher.nextToken());
                this.sarNFSDv3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10, this.val11, this.val12, this.val13);
                return 1;
            }
            if (this.statType.equals("NFSDv32")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.sarNFSDv3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("NFSv31")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.val11 = new Float(matcher.nextToken());
                this.val12 = new Float(matcher.nextToken());
                this.val13 = new Float(matcher.nextToken());
                this.sarNFSv3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10, this.val11, this.val12, this.val13);
                return 1;
            }
            if (this.statType.equals("NFSv32")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.sarNFSv3.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9);
                return 1;
            }
            if (this.statType.equals("NFSDv21")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.sarNFSDv2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10);
                return 1;
            }
            if (this.statType.equals("NFSDv22")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarNFSDv2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("NFSv21")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.val9 = new Float(matcher.nextToken());
                this.val10 = new Float(matcher.nextToken());
                this.sarNFSv2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8, this.val9, this.val10);
                return 1;
            }
            if (this.statType.equals("NFSv22")) {
                this.val1 = new Float(matcher.nextToken());
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarNFSv2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            System.out.println("Esar unknwon line (" + this.statType + "): " + thisLine);
        }
        catch (SeriesException e) {
            System.out.println("Esar parser: " + (Object)e);
            return -1;
        }
        return 0;
    }
}

