/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class paging1Sar
extends AllGraph {
    private final Trigger pagescantrigger;
    private final TimeSeries t_pgfree;
    private final TimeSeries t_pgout;
    private final TimeSeries t_ppgout;
    private final TimeSeries t_pgscan;
    private final TimeSeries t_ufs;
    private final TimeSeriesCollection free_collection;
    private final TimeSeriesCollection out_collection;
    private final TimeSeriesCollection scan_collection;
    private final TimeSeriesCollection ufs_collection;
    private final String cpuName;

    public paging1Sar(kSar hissar, String cpuID) {
        super(hissar);
        this.Title = "Paging1 for CPU " + cpuID;
        this.cpuName = cpuID;
        this.t_pgfree = new TimeSeries((Comparable)((Object)"pgfree/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Page free/s", this.t_pgfree);
        this.t_pgout = new TimeSeries((Comparable)((Object)"pgout/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Page out/s", this.t_pgout);
        this.t_ppgout = new TimeSeries((Comparable)((Object)"ppgout/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Priority page out/s", this.t_ppgout);
        this.t_pgscan = new TimeSeries((Comparable)((Object)"pgscan/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " Page scan/s", this.t_pgscan);
        this.t_ufs = new TimeSeries((Comparable)((Object)"ufs_ipf/s"), (Class)Second.class);
        this.mysar.dispo.put("CPU " + cpuID + " ufs_ipf/s", this.t_ufs);
        this.pagescantrigger = new Trigger(this.mysar, this, "scan", this.t_pgscan, "up");
        this.pagescantrigger.setTriggerValue(kSarConfig.solarispagescantrigger);
        this.free_collection = new TimeSeriesCollection();
        this.free_collection.addSeries(this.t_pgfree);
        this.out_collection = new TimeSeriesCollection();
        this.out_collection.addSeries(this.t_pgout);
        this.out_collection.addSeries(this.t_ppgout);
        this.scan_collection = new TimeSeriesCollection();
        this.scan_collection.addSeries(this.t_pgscan);
        this.ufs_collection = new TimeSeriesCollection();
        this.ufs_collection.addSeries(this.t_ufs);
    }

    public void doclosetrigger() {
        this.pagescantrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init) {
        this.t_pgfree.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_pgout.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_ppgout.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        this.t_pgscan.add((RegularTimePeriod)now, (Number)val4Init, this.do_notify());
        this.t_ufs.add((RegularTimePeriod)now, (Number)val5Init, this.do_notify());
        if (this.mysar.showtrigger) {
            this.pagescantrigger.doMarker(now, val4Init);
        }
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARPAGING1", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.free_collection, null, (ValueAxis)new NumberAxis("pgfree /s"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.out_collection, null, (ValueAxis)new NumberAxis("pgout/s"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.scan_collection, null, (ValueAxis)new NumberAxis("pgscan/s"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.ufs_collection, null, (ValueAxis)new NumberAxis("ufs_ipf/s"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.pagescantrigger.setTriggerValue(kSarConfig.solarispagescantrigger);
        this.pagescantrigger.tagMarker(subplot3);
        return this.mygraph;
    }
}

