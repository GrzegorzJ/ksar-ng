/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class udpSar
extends AllGraph {
    private TimeSeries t_indgm;
    private TimeSeries t_inerr;
    private TimeSeries t_outdgm;
    private TimeSeries t_outerr;
    private String ioOpt = new String("");

    public udpSar(kSar hissar) {
        super(hissar);
        this.Title = new String("UDP");
        this.t_indgm = new TimeSeries((Comparable)((Object)"inDgms/s"), (Class)Second.class);
        this.mysar.dispo.put("udp inDgms/s", this.t_indgm);
        this.t_inerr = new TimeSeries((Comparable)((Object)"inErrs/s"), (Class)Second.class);
        this.mysar.dispo.put("udp inErrs/s", this.t_inerr);
        this.t_outdgm = new TimeSeries((Comparable)((Object)"outDgms/s"), (Class)Second.class);
        this.mysar.dispo.put("udp outDgms/s", this.t_outdgm);
        this.t_outerr = new TimeSeries((Comparable)((Object)"outErrs/s"), (Class)Second.class);
        this.mysar.dispo.put("udp outErrs/s", this.t_outerr);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_indgm.add((RegularTimePeriod)now, (Number)val1);
        this.t_inerr.add((RegularTimePeriod)now, (Number)val2);
        this.t_outdgm.add((RegularTimePeriod)now, (Number)val3);
        this.t_outerr.add((RegularTimePeriod)now, (Number)val4);
        ++this.number_of_sample;
    }

    public XYDataset createdgm() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_indgm);
        graphcollection.addSeries(this.t_outdgm);
        return graphcollection;
    }

    public XYDataset createerr() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_inerr);
        graphcollection.addSeries(this.t_outerr);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARUDP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        CombinedDomainXYPlot plot = null;
        XYDataset xydataset1 = this.createdgm();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("indgm/outdgm /s"), (XYItemRenderer)minichart1);
        XYDataset ipfrag = this.createerr();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(ipfrag, null, (ValueAxis)new NumberAxis("inerr/outerr /s"), (XYItemRenderer)minichart2);
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        return mychart;
    }
}

