/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class iface2Sar
extends AllGraph {
    private TimeSeries t_bcstrcv;
    private TimeSeries t_bcstxmt;
    private TimeSeries t_mcstrcv;
    private TimeSeries t_mcstxmt;
    private TimeSeries t_norcvbf;
    private TimeSeries t_noxmtbf;
    private TimeSeries t_coll;
    private String ifName;
    private String ifOpt = new String("");

    public iface2Sar(kSar hissar, String s1) {
        super(hissar);
        this.Title = new String("Interface misc");
        this.ifName = s1;
        this.t_bcstrcv = new TimeSeries((Comparable)((Object)"bcstrcv/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " bcstrcv/s", this.t_bcstrcv);
        this.t_bcstxmt = new TimeSeries((Comparable)((Object)"bcstxmt/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " bcstxmt/s", this.t_bcstxmt);
        this.t_mcstrcv = new TimeSeries((Comparable)((Object)"mcstrcv/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " mcstrcv/s", this.t_mcstrcv);
        this.t_mcstxmt = new TimeSeries((Comparable)((Object)"mcstxmt/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " mcstxmt/s", this.t_mcstxmt);
        this.t_norcvbf = new TimeSeries((Comparable)((Object)"norcvbf/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " norcvbf/s", this.t_norcvbf);
        this.t_noxmtbf = new TimeSeries((Comparable)((Object)"noxmtbf/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " noxmtbf/s", this.t_noxmtbf);
        this.t_coll = new TimeSeries((Comparable)((Object)"coll/s"), (Class)Second.class);
        this.mysar.dispo.put("IF " + s1 + " coll/s", this.t_coll);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7) {
        this.t_bcstrcv.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_bcstxmt.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_mcstrcv.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_mcstxmt.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_norcvbf.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_noxmtbf.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_coll.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        ++this.number_of_sample;
    }

    public void setifOpt(String s) {
        this.ifOpt = s;
    }

    public XYDataset createbcst() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_bcstrcv);
        graphcollection.addSeries(this.t_bcstxmt);
        return graphcollection;
    }

    public XYDataset createmcst() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_mcstrcv);
        graphcollection.addSeries(this.t_mcstrcv);
        return graphcollection;
    }

    public XYDataset createbuf() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_norcvbf);
        graphcollection.addSeries(this.t_noxmtbf);
        return graphcollection;
    }

    public XYDataset createcoll() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_coll);
        return graphcollection;
    }

    public String getcheckBoxTitle() {
        return "Interface " + this.ifName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "LINUXIFACE2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " " + this.ifName + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createbcst();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("broadcast /s"), (XYItemRenderer)minichart1);
        XYDataset dropset = this.createmcst();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(dropset, null, (ValueAxis)new NumberAxis("multicast /s"), (XYItemRenderer)minichart2);
        XYDataset collset = this.createbuf();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(collset, null, (ValueAxis)new NumberAxis("buffer /s"), (XYItemRenderer)minichart3);
        XYDataset mcstset = this.createcoll();
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color8);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot(mcstset, null, (ValueAxis)new NumberAxis("collision /s"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        return mychart;
    }
}

