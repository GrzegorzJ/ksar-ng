/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class psetSar
extends AllGraph {
    private TimeSeries t_ncpu;
    private TimeSeries t_ldavg1;
    private TimeSeries t_ldavg5;
    private TimeSeries t_ldavg15;
    private String psetName;

    public psetSar(kSar hissar, String psetID) {
        super(hissar);
        this.psetName = psetID;
        this.Title = "Load for Pset " + psetID;
        this.t_ldavg1 = new TimeSeries((Comparable)((Object)"load 1mn"), (Class)Second.class);
        this.mysar.dispo.put("Pset " + psetID + " Load 1mn", this.t_ldavg1);
        this.t_ldavg5 = new TimeSeries((Comparable)((Object)"load 5mn"), (Class)Second.class);
        this.mysar.dispo.put("Pset " + psetID + " Load 5mn", this.t_ldavg5);
        this.t_ldavg15 = new TimeSeries((Comparable)((Object)"load 15mn"), (Class)Second.class);
        this.mysar.dispo.put("Pset " + psetID + " Load 15mn", this.t_ldavg15);
        this.t_ncpu = new TimeSeries((Comparable)((Object)"ncpus"), (Class)Second.class);
        this.mysar.dispo.put("Pset " + psetID + " cpus", this.t_ncpu);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_ldavg1.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_ldavg5.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_ldavg15.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_ncpu.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        ++this.number_of_sample;
    }

    public XYDataset createncpu() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_ncpu);
        return graphcollection;
    }

    public XYDataset createload() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_ldavg1);
        graphcollection.addSeries(this.t_ldavg5);
        graphcollection.addSeries(this.t_ldavg15);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARPSET", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset1 = this.createncpu();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)new NumberAxis("ncpu"), (XYItemRenderer)minichart1);
        XYDataset loadset = this.createload();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(2, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(loadset, null, (ValueAxis)new NumberAxis("Load Average"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        return mychart;
    }
}

