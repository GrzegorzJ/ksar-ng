/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class memorySar
extends AllGraph {
    private TimeSeries t_kernel;
    private TimeSeries t_locked;
    private TimeSeries t_avail;
    private TimeSeries t_free;
    private String ioOpt = new String("");

    public memorySar(kSar hissar) {
        super(hissar);
        this.Title = new String("Memory");
        this.t_kernel = new TimeSeries((Comparable)((Object)"kernel"), (Class)Second.class);
        this.mysar.dispo.put("kernel mem", this.t_kernel);
        this.t_locked = new TimeSeries((Comparable)((Object)"locked"), (Class)Second.class);
        this.mysar.dispo.put("locked mem", this.t_locked);
        this.t_avail = new TimeSeries((Comparable)((Object)"available"), (Class)Second.class);
        this.mysar.dispo.put("avail mem", this.t_avail);
        this.t_free = new TimeSeries((Comparable)((Object)"%free"), (Class)Second.class);
        this.mysar.dispo.put("%free mem", this.t_free);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4) {
        this.t_free.add((RegularTimePeriod)now, (double)(val4.floatValue() / val1.floatValue() * 100.0f));
        this.t_kernel.add((RegularTimePeriod)now, (Number)val2);
        this.t_locked.add((RegularTimePeriod)now, (Number)val3);
        this.t_avail.add((RegularTimePeriod)now, (Number)val4);
        ++this.number_of_sample;
    }

    public XYDataset createused() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_kernel);
        graphcollection.addSeries(this.t_locked);
        return graphcollection;
    }

    public XYDataset createavail() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_avail);
        return graphcollection;
    }

    public XYDataset createfree() {
        TimeSeriesCollection graphcollection = new TimeSeriesCollection();
        graphcollection.addSeries(this.t_free);
        return graphcollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARMEMORY", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        CombinedDomainXYPlot plot = null;
        NumberAxis numberaxis1 = new NumberAxis("Used");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1024);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYDataset xydataset1 = this.createused();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot(xydataset1, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        XYDataset memavail = this.createavail();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis2 = new NumberAxis("available");
        IEEE1541Number decimalformat2 = new IEEE1541Number(1024);
        numberaxis2.setNumberFormatOverride((NumberFormat)decimalformat2);
        XYPlot subplot2 = new XYPlot(memavail, null, (ValueAxis)numberaxis2, (XYItemRenderer)minichart2);
        XYDataset memfree = this.createfree();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(memfree, null, (ValueAxis)new NumberAxis("% free"), (XYItemRenderer)minichart3);
        plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        return mychart;
    }
}

