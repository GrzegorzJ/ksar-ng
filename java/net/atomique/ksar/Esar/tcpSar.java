/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class tcpSar
extends AllGraph {
    private final TimeSeries t_actvOpens;
    private final TimeSeries t_atmptFails;
    private final TimeSeries t_currEstab;
    private final TimeSeries t_estabRsts;
    private final TimeSeries t_hlfOpenDrp;
    private final TimeSeries t_listenDrop;
    private final TimeSeries t_listDropQ0;
    private final TimeSeries t_passvOpens;
    private final TimeSeriesCollection in_collection;
    private final TimeSeriesCollection out_collection;
    private final TimeSeriesCollection lst_collection;
    private final TimeSeriesCollection cur_collection;

    public tcpSar(kSar hissar) {
        super(hissar);
        this.Title = "TCP";
        this.t_actvOpens = new TimeSeries((Comparable)((Object)"actvOpens/s"), (Class)Second.class);
        this.mysar.dispo.put("actvOpens/s", this.t_actvOpens);
        this.t_atmptFails = new TimeSeries((Comparable)((Object)"atmptFails/s"), (Class)Second.class);
        this.mysar.dispo.put("atmptFails/s", this.t_atmptFails);
        this.t_currEstab = new TimeSeries((Comparable)((Object)"currEstab"), (Class)Second.class);
        this.mysar.dispo.put("currEstab", this.t_currEstab);
        this.t_estabRsts = new TimeSeries((Comparable)((Object)"estabRsts/s"), (Class)Second.class);
        this.mysar.dispo.put("estabRsts/s", this.t_estabRsts);
        this.t_hlfOpenDrp = new TimeSeries((Comparable)((Object)"hlfOpenDrp/s"), (Class)Second.class);
        this.mysar.dispo.put("hlfOpenDrp/s", this.t_hlfOpenDrp);
        this.t_listenDrop = new TimeSeries((Comparable)((Object)"istenDrop/s"), (Class)Second.class);
        this.mysar.dispo.put("listenDrop/s", this.t_listenDrop);
        this.t_listDropQ0 = new TimeSeries((Comparable)((Object)"listenDropQ0/s"), (Class)Second.class);
        this.mysar.dispo.put("listenDropQ0/s", this.t_listDropQ0);
        this.t_passvOpens = new TimeSeries((Comparable)((Object)"passvOpens/s"), (Class)Second.class);
        this.mysar.dispo.put("passvOpens/s", this.t_passvOpens);
        this.in_collection = new TimeSeriesCollection();
        this.in_collection.addSeries(this.t_passvOpens);
        this.in_collection.addSeries(this.t_atmptFails);
        this.out_collection = new TimeSeriesCollection();
        this.out_collection.addSeries(this.t_actvOpens);
        this.out_collection.addSeries(this.t_estabRsts);
        this.lst_collection = new TimeSeriesCollection();
        this.lst_collection.addSeries(this.t_hlfOpenDrp);
        this.lst_collection.addSeries(this.t_listenDrop);
        this.lst_collection.addSeries(this.t_listDropQ0);
        this.cur_collection = new TimeSeriesCollection();
        this.cur_collection.addSeries(this.t_currEstab);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8) {
        this.t_actvOpens.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_atmptFails.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_currEstab.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_estabRsts.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_hlfOpenDrp.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_listenDrop.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_listDropQ0.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_passvOpens.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARTCP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.in_collection, null, (ValueAxis)new NumberAxis("in"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.out_collection, null, (ValueAxis)new NumberAxis("out"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setSeriesPaint(2, (Paint)kSarConfig.color7);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.lst_collection, null, (ValueAxis)new NumberAxis("queue"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color8);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.cur_collection, null, (ValueAxis)new NumberAxis("current"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

