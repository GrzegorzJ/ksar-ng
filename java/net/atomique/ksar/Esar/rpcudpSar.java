/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class rpcudpSar
extends AllGraph {
    private final TimeSeries t_badcalls;
    private final TimeSeries t_badverfs;
    private final TimeSeries t_badxids;
    private final TimeSeries t_calls;
    private final TimeSeries t_cantsend;
    private final TimeSeries t_retrans;
    private final TimeSeries t_newcreds;
    private final TimeSeries t_nomem;
    private final TimeSeries t_timeouts;
    private final TimeSeries t_timers;
    private final TimeSeriesCollection bad_collection;
    private final TimeSeriesCollection call_collection;
    private final TimeSeriesCollection time_collection;
    private final TimeSeriesCollection misc_collection;

    public rpcudpSar(kSar hissar) {
        super(hissar);
        this.Title = "RPC(udp)";
        this.t_badcalls = new TimeSeries((Comparable)((Object)"badcalls/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) badcalls/s", this.t_badcalls);
        this.t_badverfs = new TimeSeries((Comparable)((Object)"badverfs/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) badverfs/s", this.t_badverfs);
        this.t_badxids = new TimeSeries((Comparable)((Object)"badxids/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) badxids/s", this.t_badxids);
        this.t_calls = new TimeSeries((Comparable)((Object)"calls/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) calls/s", this.t_calls);
        this.t_cantsend = new TimeSeries((Comparable)((Object)"cantsend/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) cantsend/s", this.t_cantsend);
        this.t_retrans = new TimeSeries((Comparable)((Object)"retrans/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) retrans/s", this.t_retrans);
        this.t_newcreds = new TimeSeries((Comparable)((Object)"newcreds/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) newcreds/s", this.t_newcreds);
        this.t_nomem = new TimeSeries((Comparable)((Object)"nomem/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) nomem/s", this.t_nomem);
        this.t_timeouts = new TimeSeries((Comparable)((Object)"timeouts/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) timeouts/s", this.t_timeouts);
        this.t_timers = new TimeSeries((Comparable)((Object)"timers/s"), (Class)Second.class);
        this.mysar.dispo.put("RPC(udp) timers/s", this.t_timers);
        this.bad_collection = new TimeSeriesCollection();
        this.bad_collection.addSeries(this.t_badcalls);
        this.bad_collection.addSeries(this.t_badverfs);
        this.bad_collection.addSeries(this.t_badxids);
        this.call_collection = new TimeSeriesCollection();
        this.call_collection.addSeries(this.t_calls);
        this.time_collection = new TimeSeriesCollection();
        this.time_collection.addSeries(this.t_timeouts);
        this.time_collection.addSeries(this.t_timers);
        this.misc_collection = new TimeSeriesCollection();
        this.misc_collection.addSeries(this.t_newcreds);
        this.misc_collection.addSeries(this.t_retrans);
        this.misc_collection.addSeries(this.t_cantsend);
        this.misc_collection.addSeries(this.t_nomem);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9, Float val10) {
        this.t_badcalls.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_badverfs.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_badxids.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_calls.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_cantsend.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_retrans.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_newcreds.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_nomem.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_timeouts.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        this.t_timers.add((RegularTimePeriod)now, (Number)val10, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARTCP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.bad_collection, null, (ValueAxis)new NumberAxis("bad"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.call_collection, null, (ValueAxis)new NumberAxis("calls"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setSeriesPaint(2, (Paint)kSarConfig.color7);
        minichart3.setSeriesPaint(3, (Paint)kSarConfig.color8);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.misc_collection, null, (ValueAxis)new NumberAxis("misc"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color9);
        minichart4.setSeriesPaint(1, (Paint)kSarConfig.color10);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.time_collection, null, (ValueAxis)new NumberAxis("time"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

