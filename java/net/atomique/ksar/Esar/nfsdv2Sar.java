/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Esar;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class nfsdv2Sar
extends AllGraph {
    private final TimeSeries t_create;
    private final TimeSeries t_link;
    private final TimeSeries t_lookup;
    private final TimeSeries t_mkdir;
    private final TimeSeries t_read;
    private final TimeSeries t_readdir;
    private final TimeSeries t_readlnk;
    private final TimeSeries t_remove;
    private final TimeSeries t_rename;
    private final TimeSeries t_rmdir;
    private final TimeSeries t_symlnk;
    private final TimeSeries t_write;
    private final TimeSeries t_getattr;
    private final TimeSeries t_null;
    private final TimeSeries t_root;
    private final TimeSeries t_setattr;
    private final TimeSeries t_statfs;
    private final TimeSeries t_wrcache;
    private final TimeSeriesCollection rw_collection;
    private final TimeSeriesCollection fsr_collection;
    private final TimeSeriesCollection fsw_collection;
    private final TimeSeriesCollection proto_collection;

    public nfsdv2Sar(kSar hissar) {
        super(hissar);
        this.Title = "NFSD v2";
        this.t_create = new TimeSeries((Comparable)((Object)"create/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 create/s", this.t_create);
        this.t_link = new TimeSeries((Comparable)((Object)"link/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 link/s", this.t_link);
        this.t_lookup = new TimeSeries((Comparable)((Object)"lookup/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 lookup/s", this.t_lookup);
        this.t_mkdir = new TimeSeries((Comparable)((Object)"mkdir/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 mkdir/s", this.t_mkdir);
        this.t_read = new TimeSeries((Comparable)((Object)"read/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 read/s", this.t_read);
        this.t_readdir = new TimeSeries((Comparable)((Object)"readdir/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 readdir/s", this.t_readdir);
        this.t_readlnk = new TimeSeries((Comparable)((Object)"readlnk/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 readlnk/s", this.t_readlnk);
        this.t_remove = new TimeSeries((Comparable)((Object)"remove/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 remove/s", this.t_remove);
        this.t_rename = new TimeSeries((Comparable)((Object)"rename/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 rename/s", this.t_rename);
        this.t_rmdir = new TimeSeries((Comparable)((Object)"rmdir/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 rmdir/s", this.t_rmdir);
        this.t_symlnk = new TimeSeries((Comparable)((Object)"symlnk/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 symlnk/s", this.t_symlnk);
        this.t_write = new TimeSeries((Comparable)((Object)"write/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 write/s", this.t_write);
        this.t_getattr = new TimeSeries((Comparable)((Object)"getattr/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 getattr/s", this.t_getattr);
        this.t_null = new TimeSeries((Comparable)((Object)"null/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 null/s", this.t_null);
        this.t_root = new TimeSeries((Comparable)((Object)"root/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 root/s", this.t_root);
        this.t_setattr = new TimeSeries((Comparable)((Object)"setattr/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 setattr/s", this.t_setattr);
        this.t_statfs = new TimeSeries((Comparable)((Object)"statfs/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 statfs/s", this.t_statfs);
        this.t_wrcache = new TimeSeries((Comparable)((Object)"wrcache/s"), (Class)Second.class);
        this.mysar.dispo.put("NFSD v2 wrcache/s", this.t_wrcache);
        this.rw_collection = new TimeSeriesCollection();
        this.rw_collection.addSeries(this.t_read);
        this.rw_collection.addSeries(this.t_write);
        this.fsr_collection = new TimeSeriesCollection();
        this.fsr_collection.addSeries(this.t_lookup);
        this.fsr_collection.addSeries(this.t_readlnk);
        this.fsr_collection.addSeries(this.t_getattr);
        this.fsr_collection.addSeries(this.t_readdir);
        this.fsw_collection = new TimeSeriesCollection();
        this.fsw_collection.addSeries(this.t_create);
        this.fsw_collection.addSeries(this.t_link);
        this.fsw_collection.addSeries(this.t_mkdir);
        this.fsw_collection.addSeries(this.t_remove);
        this.fsw_collection.addSeries(this.t_rename);
        this.fsw_collection.addSeries(this.t_rmdir);
        this.fsw_collection.addSeries(this.t_symlnk);
        this.fsw_collection.addSeries(this.t_setattr);
        this.proto_collection = new TimeSeriesCollection();
        this.proto_collection.addSeries(this.t_null);
        this.proto_collection.addSeries(this.t_statfs);
        this.proto_collection.addSeries(this.t_root);
        this.proto_collection.addSeries(this.t_wrcache);
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6, Float val7, Float val8, Float val9, Float val10) {
        this.t_create.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_link.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_mkdir.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_read.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_readdir.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_remove.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
        this.t_rename.add((RegularTimePeriod)now, (Number)val7, this.do_notify());
        this.t_rmdir.add((RegularTimePeriod)now, (Number)val8, this.do_notify());
        this.t_symlnk.add((RegularTimePeriod)now, (Number)val9, this.do_notify());
        this.t_write.add((RegularTimePeriod)now, (Number)val10, this.do_notify());
        ++this.number_of_sample;
    }

    public void add(Second now, Float val1, Float val2, Float val3, Float val4, Float val5, Float val6) {
        this.t_getattr.add((RegularTimePeriod)now, (Number)val1, this.do_notify());
        this.t_null.add((RegularTimePeriod)now, (Number)val2, this.do_notify());
        this.t_root.add((RegularTimePeriod)now, (Number)val3, this.do_notify());
        this.t_setattr.add((RegularTimePeriod)now, (Number)val4, this.do_notify());
        this.t_statfs.add((RegularTimePeriod)now, (Number)val5, this.do_notify());
        this.t_wrcache.add((RegularTimePeriod)now, (Number)val6, this.do_notify());
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "ESARNFSDV2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(2, (Paint)kSarConfig.color3);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.rw_collection, null, (ValueAxis)new NumberAxis("data"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart2.setSeriesPaint(1, (Paint)kSarConfig.color5);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.fsr_collection, null, (ValueAxis)new NumberAxis("fs read"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color5);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color6);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.fsw_collection, null, (ValueAxis)new NumberAxis("fs write"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color7);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.proto_collection, null, (ValueAxis)new NumberAxis("protocol"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

