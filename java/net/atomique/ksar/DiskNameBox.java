/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class DiskNameBox {
    kSar mysar = null;
    kSarUI myUI = null;

    public DiskNameBox(kSarUI hisUI, kSar hissar) {
        this.mysar = hissar;
        this.myUI = hisUI;
        JPanel panel0 = new JPanel();
        panel0.setLayout(new BoxLayout(panel0, 3));
        JPanel panel1 = new JPanel();
        JPanel panel3 = new JPanel(new GridLayout(1, 3));
        JButton OkButton = new JButton("Ok");
        JButton CancelButton = new JButton("Cancel");
        JButton ResetButton = new JButton("Reset");
        JPanel panelname = new JPanel(new GridLayout(0, 1));
        JPanel panelvalue = new JPanel(new GridLayout(0, 1));
        JPanel panelinfo = new JPanel(new BorderLayout());
        panelinfo.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panelinfo.add((Component)panelname, "Center");
        panelinfo.add((Component)panelvalue, "After");
        JScrollPane jscroll = new JScrollPane(panelinfo);
        jscroll.setPreferredSize(new Dimension(300, 200));
        jscroll.setVerticalScrollBarPolicy(20);
        jscroll.setHorizontalScrollBarPolicy(31);
        panel1.add(jscroll);
        panel3.add(OkButton);
        panel3.add(CancelButton);
        panel3.add(ResetButton);
        panel0.add(panel1);
        panel0.add(panel3);
        final Frame dialog = new Frame("");
        dialog.setTitle("Disk Name");
        this.triggerentries(panelname, panelvalue);
        OkButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                boolean tosave = false;
                StringBuffer tmp = new StringBuffer("");
                for (String key : DiskNameBox.this.mysar.AlternateDiskName.keySet()) {
                    String myrec;
                    diskName value = DiskNameBox.this.mysar.AlternateDiskName.get(key);
                    if (value.fieldtouch() == 1) {
                        value.setTitle(value.getfieldtext());
                        tosave = true;
                    }
                    if ((myrec = value.showrecord()) == null) continue;
                    tmp.append(myrec + "!");
                }
                if (tmp.length() > 1 && tosave) {
                    kSarConfig.writeSpecial("ADISK:" + DiskNameBox.this.mysar.hostName, tmp.toString());
                }
                DiskNameBox.this.mysar.myUI.refreshGraph();
                dialog.setVisible(false);
                DiskNameBox.this.mysar.refreshdisktree();
                dialog.dispose();
            }
        });
        CancelButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
                dialog.dispose();
            }
        });
        ResetButton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                for (String key : DiskNameBox.this.mysar.AlternateDiskName.keySet()) {
                    diskName value = DiskNameBox.this.mysar.AlternateDiskName.get(key);
                    value.resetfield();
                }
            }
        });
        dialog.add(panel0);
        dialog.pack();
        dialog.setVisible(true);
    }

    private void triggerentries(JPanel namepanel, JPanel valuepanel) {
        for (String key : this.mysar.AlternateDiskName.keySet()) {
            diskName value = this.mysar.AlternateDiskName.get(key);
            JLabel labeltmp = new JLabel(key + " ", 4);
            labeltmp.setLabelFor(value.getField());
            valuepanel.add(value.getField());
            namepanel.add(labeltmp);
        }
    }

    private void triggerentry(JPanel namepanel, JPanel valuepanel, String label, String value) {
        JTextField jtext = new JTextField(value, 10);
        JLabel jlabel = new JLabel(label + ": ", 4);
        jlabel.setLabelFor(jtext);
        valuepanel.add(jtext);
        namepanel.add(jlabel);
    }

}

