/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import net.atomique.ksar.Linux.PidCpuSar;
import net.atomique.ksar.Linux.PidIOSar;
import net.atomique.ksar.Linux.PidMemSar;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class kSarUI
extends JInternalFrame {
    public static final long serialVersionUID = 501;
    public JMenuItem addtoauto;
    public JPanel chartContainer;
    private JCheckBoxMenuItem chkbox_cpu100xaxis;
    public JCheckBoxMenuItem chkbox_cpuused;
    public JCheckBoxMenuItem chkbox_stackintr;
    public JMenuItem disknamemenu;
    private JPanel displayPanel;
    public JMenuItem exportcsvmenu;
    public JMenuItem exportjpgmenu;
    public JMenuItem exportpdfmenu;
    public JMenuItem exportpngmenu;
    public JMenuItem exporttxtmenu;
    private JCheckBoxMenuItem jCheckBoxMenuItem1;
    private JMenu jMenu1;
    private JMenu jMenu2;
    private JMenuBar jMenuBar1;
    private JMenuItem jMenuItem11;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;
    private JSeparator jSeparator2;
    private JSeparator jSeparator3;
    private JSeparator jSeparator4;
    private JSeparator jSeparator5;
    private JSplitPane jSplitPane1;
    private JTree jTree1;
    public JCheckBoxMenuItem memusedbufadj;
    private JMenuItem menuloadcmd;
    private JMenuItem menuloadssh;
    private JMenuItem menuloadtxt;
    public JCheckBoxMenuItem menushowstackedmem;
    private JMenuItem pagesizemenu;
    public JButton redobutton;
    public JMenuItem selecttimemenu;
    public JCheckBoxMenuItem showtriggermenu;
    public kSar mysar = null;
    AllGraph obj2 = null;
    static final String nonesel = "No type selected";

    public kSarUI(kSar hissar) {
        this.mysar = hissar;
        this.initComponents();
        this.showtriggermenu.setSelected(this.mysar.showtrigger);
        this.jTree1.setToolTipText("");
    }

    private String tooltipsfornode(TreePath curPath) {
        Object obj1;
        DefaultMutableTreeNode defaultmutabletreenode;
        String retval = null;
        String tempid = null;
        Object obj = curPath.getLastPathComponent();
        if (obj != null && (obj1 = (defaultmutabletreenode = (DefaultMutableTreeNode)obj).getUserObject()) instanceof GraphDescription) {
            GraphDescription graphdescription = (GraphDescription)obj1;
            this.obj2 = graphdescription.getobjectPointer();
            if (this.obj2 instanceof PidCpuSar && this.mysar.pslist != null) {
                tempid = ((PidCpuSar)this.obj2).getMypid();
            }
            if (this.obj2 instanceof PidIOSar && this.mysar.pslist != null) {
                tempid = ((PidIOSar)this.obj2).getMypid();
            }
            if (this.obj2 instanceof PidMemSar && this.mysar.pslist != null) {
                tempid = ((PidMemSar)this.obj2).getMypid();
            }
            if (tempid != null) {
                retval = this.mysar.pslist.listproc.get(tempid).getCommand();
            }
            if (retval == null) {
                return "";
            }
            return retval;
        }
        return "";
    }

    private void initComponents() {
        this.jTree1 = new JTree(new DefaultTreeModel(this.mysar.graphtree)){
            public static final long serialVersionUID = 501;

            public String getToolTipText(MouseEvent evt) {
                if (this.getRowForLocation(evt.getX(), evt.getY()) == -1) {
                    return null;
                }
                TreePath curPath = this.getPathForLocation(evt.getX(), evt.getY());
                return kSarUI.this.tooltipsfornode(curPath);
            }
        };
        this.jSplitPane1 = new JSplitPane();
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane(this.jTree1);
        this.redobutton = new JButton();
        this.displayPanel = new JPanel();
        this.chartContainer = new JPanel();
        this.jMenuBar1 = new JMenuBar();
        this.jMenu1 = new JMenu();
        this.menuloadtxt = new JMenuItem();
        this.menuloadssh = new JMenuItem();
        this.menuloadcmd = new JMenuItem();
        this.jSeparator1 = new JSeparator();
        this.exportpdfmenu = new JMenuItem();
        this.exportjpgmenu = new JMenuItem();
        this.exportpngmenu = new JMenuItem();
        this.exportcsvmenu = new JMenuItem();
        this.exporttxtmenu = new JMenuItem();
        this.jSeparator2 = new JSeparator();
        this.selecttimemenu = new JMenuItem();
        this.jSeparator5 = new JSeparator();
        this.addtoauto = new JMenuItem();
        this.jMenu2 = new JMenu();
        this.disknamemenu = new JMenuItem();
        this.showtriggermenu = new JCheckBoxMenuItem();
        this.jSeparator3 = new JSeparator();
        this.pagesizemenu = new JMenuItem();
        this.jCheckBoxMenuItem1 = new JCheckBoxMenuItem();
        this.chkbox_cpuused = new JCheckBoxMenuItem();
        this.chkbox_cpu100xaxis = new JCheckBoxMenuItem();
        this.memusedbufadj = new JCheckBoxMenuItem();
        this.menushowstackedmem = new JCheckBoxMenuItem();
        this.jSeparator4 = new JSeparator();
        this.chkbox_stackintr = new JCheckBoxMenuItem();
        this.jMenuItem11 = new JMenuItem();
        this.jTree1.addTreeSelectionListener(new TreeSelectionListener(){

            public void valueChanged(TreeSelectionEvent evt) {
                kSarUI.this.jTree1ValueChanged(evt);
            }
        });
        this.setClosable(true);
        this.setDefaultCloseOperation(0);
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setResizable(true);
        this.setDoubleBuffered(true);
        this.addInternalFrameListener(new InternalFrameListener(){

            public void internalFrameOpened(InternalFrameEvent evt) {
            }

            public void internalFrameClosing(InternalFrameEvent evt) {
                kSarUI.this.formInternalFrameClosing(evt);
            }

            public void internalFrameClosed(InternalFrameEvent evt) {
            }

            public void internalFrameIconified(InternalFrameEvent evt) {
            }

            public void internalFrameDeiconified(InternalFrameEvent evt) {
            }

            public void internalFrameActivated(InternalFrameEvent evt) {
            }

            public void internalFrameDeactivated(InternalFrameEvent evt) {
            }
        });
        this.jPanel1.setLayout(new BorderLayout());
        this.jScrollPane1.setPreferredSize(new Dimension(156, 4));
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.redobutton.setText("Redo");
        this.redobutton.setEnabled(false);
        this.redobutton.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.redobuttonActionPerformed(evt);
            }
        });
        this.jPanel1.add((Component)this.redobutton, "South");
        this.jSplitPane1.setLeftComponent(this.jPanel1);
        this.displayPanel.setLayout(new BorderLayout());
        this.chartContainer.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.chartContainer.setLayout(new BorderLayout());
        this.displayPanel.add((Component)this.chartContainer, "Center");
        this.jSplitPane1.setRightComponent(this.displayPanel);
        this.getContentPane().add((Component)this.jSplitPane1, "Center");
        this.jMenuBar1.setMinimumSize(new Dimension(90, 91));
        this.jMenuBar1.setName("uimenu");
        this.jMenuBar1.setRequestFocusEnabled(false);
        this.jMenu1.setText("Data");
        this.menuloadtxt.setText("Load from text file...");
        this.menuloadtxt.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.menuloadtxtActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.menuloadtxt);
        this.menuloadssh.setText("Launch SSH command...");
        this.menuloadssh.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.menuloadsshActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.menuloadssh);
        this.menuloadcmd.setText("Run local command...");
        this.menuloadcmd.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.menuloadcmdActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.menuloadcmd);
        this.jMenu1.add(this.jSeparator1);
        this.exportpdfmenu.setText("Export to PDF...");
        this.exportpdfmenu.setEnabled(false);
        this.exportpdfmenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.exportpdfmenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.exportpdfmenu);
        this.exportjpgmenu.setText("Export to JPG...");
        this.exportjpgmenu.setEnabled(false);
        this.exportjpgmenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.exportjpgmenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.exportjpgmenu);
        this.exportpngmenu.setText("Export to PNG...");
        this.exportpngmenu.setEnabled(false);
        this.exportpngmenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.exportpngmenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.exportpngmenu);
        this.exportcsvmenu.setText("Export to CSV...");
        this.exportcsvmenu.setEnabled(false);
        this.exportcsvmenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.exportcsvmenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.exportcsvmenu);
        this.exporttxtmenu.setText("Export to TXT...");
        this.exporttxtmenu.setEnabled(false);
        this.exporttxtmenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.exporttxtmenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.exporttxtmenu);
        this.jMenu1.add(this.jSeparator2);
        this.selecttimemenu.setText("Select time range...");
        this.selecttimemenu.setEnabled(false);
        this.selecttimemenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.selecttimemenuActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.selecttimemenu);
        this.jMenu1.add(this.jSeparator5);
        this.addtoauto.setText("Add to automatic");
        this.addtoauto.setEnabled(false);
        this.addtoauto.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.addtoautoActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.addtoauto);
        this.jMenuBar1.add(this.jMenu1);
        this.jMenu2.setText("Options");
        this.disknamemenu.setText("Disk Name");
        this.disknamemenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.disknamemenuActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.disknamemenu);
        this.showtriggermenu.setText("Show Trigger");
        this.showtriggermenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.showtriggermenuActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.showtriggermenu);
        this.jMenu2.add(this.jSeparator3);
        this.pagesizemenu.setText("Memory pagesize");
        this.pagesizemenu.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.pagesizemenuActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.pagesizemenu);
        this.jCheckBoxMenuItem1.setSelected(true);
        this.jCheckBoxMenuItem1.setText("Export zero'ed disk");
        this.jCheckBoxMenuItem1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.jCheckBoxMenuItem1);
        this.chkbox_cpuused.setText("Show CPU used stacked");
        this.chkbox_cpuused.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.chkbox_cpuusedActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.chkbox_cpuused);
        this.chkbox_cpu100xaxis.setText("Show CPU axis 0-100");
        this.chkbox_cpu100xaxis.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.chkbox_cpu100xaxisActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.chkbox_cpu100xaxis);
        this.memusedbufadj.setText("Show memused (buffers adjusted)");
        this.memusedbufadj.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.memusedbufadjActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.memusedbufadj);
        this.menushowstackedmem.setSelected(true);
        this.menushowstackedmem.setText("Show Memory stacked");
        this.menushowstackedmem.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.menushowstackedmemActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.menushowstackedmem);
        this.jMenu2.add(this.jSeparator4);
        this.chkbox_stackintr.setText("Show Interrupt stacked");
        this.chkbox_stackintr.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.chkbox_stackintrActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.chkbox_stackintr);
        this.jMenuItem11.setText("Add Personal graph");
        this.jMenuItem11.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
                kSarUI.this.jMenuItem11ActionPerformed(evt);
            }
        });
        this.jMenu2.add(this.jMenuItem11);
        this.jMenuBar1.add(this.jMenu2);
        this.setJMenuBar(this.jMenuBar1);
        this.pack();
    }

    private void menuloadcmdActionPerformed(ActionEvent evt) {
        this.mysar.do_localcommand(null);
    }

    private void menuloadtxtActionPerformed(ActionEvent evt) {
        this.mysar.do_fileread(null);
    }

    public void refreshGraph() {
        if (this.obj2 != null) {
            this.chartContainer.removeAll();
            if (this.mysar.isparsing) {
                this.chartContainer.add(this.obj2.run(null, null));
            } else {
                this.chartContainer.add(this.obj2.run(this.mysar.startofgraph, this.mysar.endofgraph));
            }
            this.displayPanel.validate();
        }
    }

    public void changeintoherframe(String s) {
        if (!"none".equals(s)) {
            this.traverse(this.jTree1, s);
        }
    }

    public void traverse(JTree tree, String s) {
        TreeModel model = tree.getModel();
        if (model != null) {
            Object root = model.getRoot();
            if ("root".equals(s)) {
                this.jTree1.setSelectionPath(new TreePath((DefaultMutableTreeNode)root));
            }
            this.walk(model, root, s);
        }
    }

    private void walk(TreeModel model, Object o, String s) {
        int cc = model.getChildCount(o);
        for (int i = 0; i < cc; ++i) {
            Object child = model.getChild(o, i);
            if (model.isLeaf(child)) {
                AllGraph obj3;
                GraphDescription graphdescription;
                DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)child;
                Object obj1 = defaultmutabletreenode.getUserObject();
                if (!(obj1 instanceof GraphDescription) || (obj3 = (graphdescription = (GraphDescription)obj1).getobjectPointer()).getGraphLink() == null || !obj3.getGraphLink().equals(s)) continue;
                this.jTree1.setSelectionPath(new TreePath(defaultmutabletreenode.getPath()));
                continue;
            }
            this.walk(model, child, s);
        }
    }

    private JPanel setInfoPanel() {
        StringBuffer myinfo = new StringBuffer("\n");
        JPanel jpanel = new JPanel(new BorderLayout());
        JPanel panelinfo = new JPanel(new BorderLayout());
        panelinfo.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        myinfo.append("kSar Info:\n\n");
        if (this.mysar.myOS != null) {
            myinfo.append(this.mysar.myOS.getOSInfo());
        }
        myinfo.append(this.mysar.calendarinfo());
        if (this.mysar.DetectedBounds.size() > 0) {
            myinfo.append("\n Detected bottlenecks:\n");
            for (String key : this.mysar.DetectedBounds.keySet()) {
                AllGraph value = this.mysar.DetectedBounds.get(key);
                myinfo.append(" " + key + "\n");
            }
            myinfo.append("\n");
        }
        JTextArea ksarinfo = new JTextArea(myinfo.toString());
        ksarinfo.setEditable(false);
        panelinfo.add(ksarinfo);
        JScrollPane jpanel1 = new JScrollPane(panelinfo);
        jpanel1.setVerticalScrollBarPolicy(20);
        jpanel1.setHorizontalScrollBarPolicy(30);
        jpanel.add(jpanel1);
        return jpanel;
    }

    private JPanel setEmptyPanel() {
        JPanel jpanel = new JPanel(new FlowLayout());
        jpanel.add(new JLabel("No type selected"));
        return jpanel;
    }

    private void jTree1ValueChanged(TreeSelectionEvent evt) {
        TreePath treepath = evt.getPath();
        Object obj = treepath.getLastPathComponent();
        if (this.obj2 != null && this.obj2 instanceof AllGraph) {
            this.obj2.setnotifygraph(false);
        }
        if (obj != null) {
            DefaultMutableTreeNode defaultmutabletreenode = (DefaultMutableTreeNode)obj;
            Object obj1 = defaultmutabletreenode.getUserObject();
            if (obj1 instanceof GraphDescription) {
                GraphDescription graphdescription = (GraphDescription)obj1;
                this.obj2 = graphdescription.getobjectPointer();
                this.mysar.mydesktop.showgraphselected(this.obj2.getGraphLink(), this);
                this.obj2.setnotifygraph(true);
                this.refreshGraph();
            } else {
                if (treepath.getParentPath() == null) {
                    this.obj2 = null;
                    this.chartContainer.removeAll();
                    this.chartContainer.add(this.setInfoPanel());
                    this.displayPanel.validate();
                    this.mysar.mydesktop.showgraphselected("root", this);
                    return;
                }
                this.obj2 = null;
                this.chartContainer.removeAll();
                this.chartContainer.add(this.setEmptyPanel());
                this.displayPanel.validate();
                this.mysar.mydesktop.showgraphselected("none", this);
            }
        }
    }

    private void chkbox_cpuusedActionPerformed(ActionEvent evt) {
        this.mysar.showstackedcpu = this.chkbox_cpuused.getState();
        this.refreshGraph();
    }

    private void chkbox_cpu100xaxisActionPerformed(ActionEvent evt) {
        this.mysar.show100axiscpu = this.chkbox_cpu100xaxis.getState();
        this.refreshGraph();
    }

    private void jMenuItem11ActionPerformed(ActionEvent evt) {
        OtherGraph tmp = new OtherGraph(this.mysar);
        tmp.addtotree(this.mysar.graphtree);
        this.mysar.pdfList.put("xXx", tmp);
        tmp.setGraphLink("xXx");
    }

    private void menuloadsshActionPerformed(ActionEvent evt) {
        this.mysar.do_sshread(null);
    }

    private void memusedbufadjActionPerformed(ActionEvent evt) {
        this.mysar.showmemusedbuffersadjusted = this.memusedbufadj.getState();
        this.refreshGraph();
    }

    private void selecttimemenuActionPerformed(ActionEvent evt) {
        CalendarSelection t = new CalendarSelection(this.mysar);
        t.setVisible(true);
    }

    private void jCheckBoxMenuItem1ActionPerformed(ActionEvent evt) {
        this.mysar.showemptydisk = this.jCheckBoxMenuItem1.getState();
        this.refreshGraph();
    }

    private void exportpdfmenuActionPerformed(ActionEvent evt) {
        askPdfFilename u = new askPdfFilename(this.mysar);
        String pdffilename = u.run();
        if (pdffilename == null) {
            return;
        }
        GraphSelection t = new GraphSelection(this.mysar, this.mysar.pdfList, pdffilename, "PDF");
    }

    private void exportjpgmenuActionPerformed(ActionEvent evt) {
        askImageBasename u = new askImageBasename(this.mysar);
        String pdffilename = u.run();
        if (pdffilename == null) {
            return;
        }
        GraphSelection t = new GraphSelection(this.mysar, this.mysar.pdfList, pdffilename, "JPG");
    }

    private void exportpngmenuActionPerformed(ActionEvent evt) {
        askImageBasename u = new askImageBasename(this.mysar);
        String pdffilename = u.run();
        if (pdffilename == null) {
            return;
        }
        GraphSelection t = new GraphSelection(this.mysar, this.mysar.pdfList, pdffilename, "PNG");
    }

    private void menushowstackedmemActionPerformed(ActionEvent evt) {
        this.mysar.showstackedmem = this.menushowstackedmem.getState();
        this.refreshGraph();
    }

    private void exportcsvmenuActionPerformed(ActionEvent evt) {
        askCsvFilename u = new askCsvFilename(this.mysar);
        String csvfilename = u.run();
        if (csvfilename == null) {
            return;
        }
        this.mysar.outputCsv(csvfilename);
    }

    private void showtriggermenuActionPerformed(ActionEvent evt) {
        this.mysar.showtrigger = this.showtriggermenu.getState();
        this.refreshGraph();
    }

    private void pagesizemenuActionPerformed(ActionEvent evt) {
        this.askforsolarispagesize();
    }

    private void disknamemenuActionPerformed(ActionEvent evt) {
        DiskNameBox disknameBox = new DiskNameBox(this, this.mysar);
    }

    private void addtoautoActionPerformed(ActionEvent evt) {
        if ("Empty".equals(this.mysar.reload_command)) {
            return;
        }
        String shortcut_desc = JOptionPane.showInputDialog("Enter a description for this shortcut ", (Object)"");
        if (shortcut_desc == null) {
            return;
        }
        if (this.mysar.reload_command.indexOf("file://") == 0) {
            kSarConfig.shortcut_window_list.put(this.mysar.reload_command, shortcut_desc);
            this.mysar.mydesktop.add_shortcut(shortcut_desc);
            kSarConfig.writeDefault();
        }
        if (this.mysar.reload_command.indexOf("cmd://") == 0) {
            kSarConfig.shortcut_window_list.put(this.mysar.reload_command, shortcut_desc);
            this.mysar.mydesktop.add_shortcut(shortcut_desc);
            kSarConfig.writeDefault();
        }
        if (this.mysar.reload_command.indexOf("ssh://") == 0) {
            String host;
            int port = 22;
            String tmpcmd = this.mysar.reload_command.substring(6);
            String[] cmd_splitted = tmpcmd.split("@", 2);
            if (cmd_splitted.length != 2) {
                return;
            }
            String[] user_part = cmd_splitted[0].split(":", 2);
            String user = user_part.length == 2 ? user_part[0] : cmd_splitted[0];
            String[] cmd_part = cmd_splitted[1].split("/", 2);
            if (cmd_part.length != 2) {
                return;
            }
            String[] host_part = cmd_part[0].split(":", 2);
            if (host_part.length == 2) {
                host = host_part[0];
                try {
                    port = Integer.parseInt(host_part[1]);
                }
                catch (NumberFormatException e) {
                    return;
                }
            } else {
                host = host_part[0];
            }
            String cmd = cmd_part[1];
            String ret = port != 22 ? "ssh://" + user + "@" + host + ":" + port + "/" + cmd : "ssh://" + user + "@" + host + "/" + cmd;
            kSarConfig.shortcut_window_list.put(ret, shortcut_desc);
            this.mysar.mydesktop.add_shortcut(shortcut_desc);
            kSarConfig.writeDefault();
        }
    }

    private void redobuttonActionPerformed(ActionEvent evt) {
        this.redobutton.setEnabled(false);
        if ("Redo".equals(this.redobutton.getText()) && this.mysar.parse_mission(this.mysar.reload_command)) {
            this.mysar.do_mission(this.mysar.reload_command);
        }
        if ("Stop".equals(this.redobutton.getText())) {
            this.mysar.command_interrupted = true;
            try {
                if (this.mysar.launched_command != null) {
                    this.mysar.launched_command.interrupt();
                    this.mysar.launched_command.join(100);
                }
            }
            catch (InterruptedException iee) {
                iee.printStackTrace();
            }
            this.mysar.launched_command = null;
            this.mysar.changemenu(true);
        }
    }

    private void save_tempfile() {
        Object[] options;
        int foo;
        if (this.mysar.tmpfile != null && this.mysar.tmpfile.length() > 0 && (foo = JOptionPane.showOptionDialog(this, "Would you like to save the sar file ?", "Warning", -1, 2, null, options = new String[]{"yes", "no"}, options[0])) == 0) {
            askSaveFilename tmp = new askSaveFilename(this.mysar);
            String savetostr = tmp.run();
            if (savetostr != null) {
                File saveto = new File(savetostr);
                SaveSar t = null;
                try {
                    t = new SaveSar(this.mysar.tmpfile, saveto);
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                t = null;
            }
            tmp = null;
        }
    }

    private void formInternalFrameClosing(InternalFrameEvent evt) {
        this.mysar.command_interrupted = true;
        if (this.mysar.launched_command != null) {
            try {
                this.mysar.launched_command.interrupt();
                this.mysar.launched_command.join(100);
            }
            catch (InterruptedException iee) {
                iee.printStackTrace();
            }
        }
        this.mysar.launched_command = null;
        this.save_tempfile();
        this.dispose();
        this.mysar.cleanup_temp();
        this.mysar.resetInfo();
        this.mysar.myUI = null;
        this.mysar = null;
    }

    private void exporttxtmenuActionPerformed(ActionEvent evt) {
        this.save_tempfile();
    }

    private void chkbox_stackintrActionPerformed(ActionEvent evt) {
        this.mysar.showstackedintrlist = this.chkbox_stackintr.getState();
        this.refreshGraph();
    }

    public int exportPng(String pdffilename, Map<String, AllGraph> pdfList) {
        JPanel panel0 = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JProgressBar pbar = new JProgressBar();
        pbar.setMinimum(0);
        pbar.setMaximum(pdfList.size());
        pbar.setStringPainted(true);
        JLabel mytitre = new JLabel("Exporting: ");
        panel1.add(mytitre);
        panel2.add(pbar);
        panel0.add(panel1);
        panel0.add(panel2);
        JDialog mydial = new JDialog();
        mydial.setDefaultCloseOperation(2);
        mydial.setContentPane(panel0);
        mydial.setSize(250, 80);
        mydial.pack();
        mydial.setLocationRelativeTo(this);
        mydial.setVisible(true);
        PngExport t = new PngExport(pdffilename, pdfList, pbar, mydial, this.mysar, kSarConfig.imagehtml, kSarConfig.imageheight, kSarConfig.imagewidth);
        Thread th = new Thread(t);
        th.start();
        return 1;
    }

    public void askforsolarispagesize() {
        String tmp = JOptionPane.showInputDialog("Size of pagesize:\n(use -1 for graphing block value)\nuse /usr/bin/pagesize to known it", (Object)new Integer(this.mysar.solarispagesize).toString());
        if (tmp != null) {
            try {
                Integer i = new Integer(tmp);
                this.mysar.solarispagesize = i;
                kSarConfig.writeSpecial("PGSZ:" + this.mysar.hostName, i.toString());
                this.refreshGraph();
            }
            catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Can find pagesize", "PageSize error", 0);
            }
        }
    }

    public int exportJpg(String pdffilename, Map<String, AllGraph> pdfList) {
        JPanel panel0 = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JProgressBar pbar = new JProgressBar();
        pbar.setMinimum(0);
        pbar.setMaximum(pdfList.size());
        pbar.setStringPainted(true);
        JLabel mytitre = new JLabel("Exporting: ");
        panel1.add(mytitre);
        panel2.add(pbar);
        panel0.add(panel1);
        panel0.add(panel2);
        JDialog mydial = new JDialog();
        mydial.setDefaultCloseOperation(2);
        mydial.setContentPane(panel0);
        mydial.setSize(250, 80);
        mydial.pack();
        mydial.setLocationRelativeTo(this);
        mydial.setVisible(true);
        JpgExport t = new JpgExport(pdffilename, pdfList, pbar, mydial, this.mysar, kSarConfig.imagehtml, kSarConfig.imageheight, kSarConfig.imagewidth);
        Thread th = new Thread(t);
        th.start();
        return 1;
    }

    public int exportPdf(String pdffilename, Map<String, AllGraph> pdfList) {
        JPanel panel0 = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JProgressBar pbar = new JProgressBar();
        pbar.setMinimum(0);
        pbar.setMaximum(pdfList.size());
        pbar.setStringPainted(true);
        JLabel mytitre = new JLabel("Exporting: ");
        panel1.add(mytitre);
        panel2.add(pbar);
        panel0.add(panel1);
        panel0.add(panel2);
        JDialog mydial = new JDialog();
        mydial.setDefaultCloseOperation(2);
        mydial.setContentPane(panel0);
        mydial.setSize(250, 80);
        mydial.pack();
        mydial.setLocationRelativeTo(this);
        mydial.setVisible(true);
        PdfExport t = new PdfExport(pdffilename, pdfList, pbar, mydial, this.mysar);
        Thread th = new Thread(t);
        th.start();
        return 1;
    }

    public void add2tree(DefaultMutableTreeNode parent, DefaultMutableTreeNode newNode) {
        DefaultTreeModel model = (DefaultTreeModel)this.jTree1.getModel();
        model.insertNodeInto(newNode, parent, parent.getChildCount());
    }

    public void remove2tree(DefaultMutableTreeNode oldNode) {
        DefaultTreeModel model = (DefaultTreeModel)this.jTree1.getModel();
        model.removeNodeFromParent(oldNode);
    }

    public void home2tree() {
        this.jTree1.setSelectionRow(0);
        this.jTree1.expandRow(0);
    }

    public void reset2tree() {
        DefaultTreeModel model = (DefaultTreeModel)this.jTree1.getModel();
        if (model.getChildCount(model.getRoot()) >= 1) {
            this.mysar.graphtree.removeAllChildren();
            model.reload(this.mysar.graphtree);
        }
        this.jTree1.expandRow(0);
    }

}

