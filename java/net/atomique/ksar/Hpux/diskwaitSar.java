/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Hpux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class diskwaitSar
extends AllGraph {
    private Trigger diskbusytrigger;
    private Trigger diskavquetrigger;
    private TimeSeries t_avque;
    private TimeSeries t_avwait;
    private TimeSeries t_busy;
    private String mydiskName;
    private diskName optdisk;

    public diskwaitSar(kSar hissar, String s1, diskName diskopt) {
        super(hissar);
        this.datain = 0;
        this.Title = new String("Disk wait");
        this.mydiskName = s1;
        this.optdisk = diskopt;
        this.t_avque = new TimeSeries((Comparable)((Object)"avque"), (Class)Second.class);
        this.t_avwait = new TimeSeries((Comparable)((Object)"avwait"), (Class)Second.class);
        this.t_busy = new TimeSeries((Comparable)((Object)"%busy"), (Class)Second.class);
        this.diskbusytrigger = new Trigger(this.mysar, this, "%busy", this.t_busy, "up");
        this.diskbusytrigger.setTriggerValue(kSarConfig.hpuxdiskbusytrigger);
        this.diskavquetrigger = new Trigger(this.mysar, this, "avque", this.t_avque, "up");
        this.diskavquetrigger.setTriggerValue(kSarConfig.hpuxdiskavquetrigger);
    }

    public void doclosetrigger() {
        this.diskbusytrigger.doclose();
        this.diskavquetrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init) {
        Float newval;
        Number tmpInt;
        Float zerof = new Float(0.0f);
        if ((val1Init != zerof || val2Init != zerof || val3Init != zerof) && this.datain == 0) {
            this.datain = 1;
        }
        if ((tmpInt = this.t_avque.getValue((RegularTimePeriod)now)) != null) {
            newval = new Float(tmpInt.floatValue() + val1Init.floatValue());
            this.t_avque.update((RegularTimePeriod)now, (Number)newval);
            this.diskavquetrigger.doMarker(now, newval);
        } else {
            this.t_avque.add((RegularTimePeriod)now, (Number)val1Init);
            this.diskavquetrigger.doMarker(now, val1Init);
        }
        tmpInt = this.t_avwait.getValue((RegularTimePeriod)now);
        if (tmpInt != null) {
            newval = new Float((tmpInt.floatValue() + val2Init.floatValue()) / 2.0f);
            this.t_avwait.update((RegularTimePeriod)now, (Number)newval);
        } else {
            this.t_avwait.add((RegularTimePeriod)now, (double)val2Init.floatValue());
        }
        tmpInt = this.t_busy.getValue((RegularTimePeriod)now);
        if (tmpInt != null) {
            newval = new Float((tmpInt.floatValue() + val3Init.floatValue()) / 2.0f);
            this.t_busy.update((RegularTimePeriod)now, (Number)newval);
            this.diskbusytrigger.doMarker(now, newval);
        } else {
            this.t_busy.add((RegularTimePeriod)now, (Number)val3Init);
            this.diskbusytrigger.doMarker(now, val3Init);
        }
    }

    public XYDataset createavque() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_avque);
        return timeseriescollection;
    }

    public XYDataset createavwait() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_avwait);
        return timeseriescollection;
    }

    public XYDataset createbusy() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.t_busy);
        return timeseriescollection;
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.mydiskName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "HPUXDISKWAIT", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " on " + this.optdisk.showTitle() + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset avqueset = this.createavque();
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot(avqueset, null, (ValueAxis)new NumberAxis("avque"), (XYItemRenderer)minichart1);
        XYDataset avwaitset = this.createavwait();
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot(avwaitset, null, (ValueAxis)new NumberAxis("avwait"), (XYItemRenderer)minichart2);
        XYDataset busyset = this.createbusy();
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot(busyset, null, (ValueAxis)new NumberAxis("%busy"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.diskbusytrigger.setTriggerValue(kSarConfig.hpuxdiskbusytrigger);
        this.diskbusytrigger.tagMarker(subplot3);
        this.diskavquetrigger.setTriggerValue(kSarConfig.hpuxdiskavquetrigger);
        this.diskavquetrigger.tagMarker(subplot1);
        return mychart;
    }
}

