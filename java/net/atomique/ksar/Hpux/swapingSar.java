/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Hpux;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class swapingSar
extends AllGraph {
    private TimeSeries swpin;
    private TimeSeries bswin;
    private TimeSeries swpot;
    private TimeSeries bswot;
    private TimeSeries pswch;

    public swapingSar(kSar hissar) {
        super(hissar);
        this.Title = new String("Swapping");
        this.swpin = new TimeSeries((Comparable)((Object)"LWP in"), (Class)Second.class);
        this.bswin = new TimeSeries((Comparable)((Object)"pages in"), (Class)Second.class);
        this.swpot = new TimeSeries((Comparable)((Object)"LWP out"), (Class)Second.class);
        this.bswot = new TimeSeries((Comparable)((Object)"pages out"), (Class)Second.class);
        this.pswch = new TimeSeries((Comparable)((Object)"LWP switch"), (Class)Second.class);
    }

    public void add(Second now, Float swpinInt, Float bswinInt, Float swpotInt, Float bswotInt, Float pswchInt) {
        this.swpin.add((RegularTimePeriod)now, (Number)swpinInt);
        this.bswin.add((RegularTimePeriod)now, (double)(bswinInt.floatValue() * 512.0f));
        this.swpot.add((RegularTimePeriod)now, (Number)swpotInt);
        this.bswot.add((RegularTimePeriod)now, (double)(bswotInt.floatValue() * 512.0f));
        this.pswch.add((RegularTimePeriod)now, (Number)pswchInt);
    }

    public XYDataset create() {
        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        timeseriescollection.addSeries(this.swpin);
        timeseriescollection.addSeries(this.bswin);
        timeseriescollection.addSeries(this.swpot);
        timeseriescollection.addSeries(this.bswot);
        timeseriescollection.addSeries(this.pswch);
        return timeseriescollection;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "HPUXSWAP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        XYDataset xydataset = this.create();
        JFreeChart mychart = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)xydataset, (boolean)true, (boolean)true, (boolean)false);
        this.setbackgroundimage(mychart);
        XYPlot xyplot = (XYPlot)mychart.getPlot();
        XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        xylineandshaperenderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        xylineandshaperenderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        xylineandshaperenderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        xylineandshaperenderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        xylineandshaperenderer.setSeriesPaint(3, (Paint)kSarConfig.color4);
        xylineandshaperenderer.setSeriesPaint(4, (Paint)kSarConfig.color5);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }
}

