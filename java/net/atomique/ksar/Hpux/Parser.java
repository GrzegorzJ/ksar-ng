/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.Hpux;

import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;

public class Parser {
    kSar mysar;
    Float val1;
    Float val2;
    Float val3;
    Float val4;
    Float val5;
    Float val6;
    Float val7;
    Float val8;
    Float val9;
    Float val10;
    Float val11;
    int heure = 0;
    int minute = 0;
    int seconde = 0;
    Second now = new Second(0, 0, 0, 1, 1, 1970);
    String lastHeader;
    String statType = "none";
    int firstwastime;
    cpuSar sarCPU = null;
    swapingSar sarSWAP = null;
    syscallSar sarSYSCALL = null;
    fileSar sarFILE = null;
    msgSar sarMSG = null;
    ttySar sarTTY = null;
    bufferSar sarBUFFER = null;
    squeueSar sarSQUEUE = null;
    rqueueSar sarRQUEUE = null;

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String first, StringTokenizer matcher) {
        String[] sarTime;
        boolean headerFound = false;
        if (thisLine.indexOf("%usr") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("device") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("runq-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("bread/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("swpin/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("scall/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("iget/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rawch/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("proc-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("msg/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("atch/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pgout/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("freemem") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("sml_mem") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if ((sarTime = first.split(":")).length != 3 && !this.statType.equals("device")) {
            return 1;
        }
        if (sarTime.length == 3) {
            this.heure = Integer.parseInt(sarTime[0]);
            this.minute = Integer.parseInt(sarTime[1]);
            this.seconde = Integer.parseInt(sarTime[2]);
            this.now = new Second(this.seconde, this.minute, this.heure, this.mysar.day, this.mysar.month, this.mysar.year);
            if (this.mysar.statstart == null) {
                this.mysar.statstart = new String(this.now.toString());
                this.mysar.startofgraph = this.now;
            }
            if (!this.mysar.datefound.contains((Object)this.now)) {
                this.mysar.datefound.add(this.now);
            }
            if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
                this.mysar.lastever = this.now;
                this.mysar.statend = new String(this.mysar.lastever.toString());
                this.mysar.endofgraph = this.mysar.lastever;
            }
            this.firstwastime = 1;
        } else {
            this.firstwastime = 0;
        }
        if (!matcher.hasMoreElements()) {
            return 1;
        }
        this.lastHeader = matcher.nextToken();
        if (headerFound) {
            if (this.lastHeader.equals(this.statType)) {
                headerFound = false;
                return 1;
            }
            this.statType = this.lastHeader;
            if (this.lastHeader.equals("%usr")) {
                if (this.sarCPU == null) {
                    this.sarCPU = new cpuSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarCPU.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxcpuSar", this.sarCPU);
                    this.sarCPU.setGraphLink("HpuxcpuSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("device")) {
                if (!this.mysar.hasdisknode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                    }
                    this.mysar.hasdisknode = true;
                }
                return 1;
            }
            if (this.lastHeader.equals("runq-sz")) {
                if (this.sarRQUEUE == null) {
                    this.sarRQUEUE = new rqueueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarRQUEUE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxRqueueSar", this.sarRQUEUE);
                    this.sarRQUEUE.setGraphLink("HpuxRqueueSar");
                }
                if (this.sarSQUEUE == null) {
                    this.sarSQUEUE = new squeueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSQUEUE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxSqueueSar", this.sarSQUEUE);
                    this.sarSQUEUE.setGraphLink("HpuxSqueueSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("bread/s")) {
                if (this.sarBUFFER == null) {
                    this.sarBUFFER = new bufferSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarBUFFER.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxbufferSar", this.sarBUFFER);
                    this.sarBUFFER.setGraphLink("HpuxbufferSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("swpin/s")) {
                if (this.sarSWAP == null) {
                    this.sarSWAP = new swapingSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSWAP.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxswapingSar", this.sarSWAP);
                    this.sarSWAP.setGraphLink("HpuxswapSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("scall/s")) {
                if (this.sarSYSCALL == null) {
                    this.sarSYSCALL = new syscallSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSYSCALL.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("syscalSar", this.sarSYSCALL);
                    this.sarSYSCALL.setGraphLink("HpuxsyscalSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("iget/s")) {
                if (this.sarFILE == null) {
                    this.sarFILE = new fileSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarFILE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxfileSar", this.sarFILE);
                    this.sarFILE.setGraphLink("HpuxfileSar");
                }
                return 1;
            }
            if (this.lastHeader.equals("rawch/s")) {
                if (this.sarTTY == null) {
                    this.sarTTY = new ttySar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarTTY.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxttySar", this.sarTTY);
                    this.sarTTY.setGraphLink("HpuxttySar");
                }
                return 1;
            }
            if (this.lastHeader.equals("proc-sz")) {
                return 1;
            }
            if (this.lastHeader.equals("msg/s")) {
                if (this.sarMSG == null) {
                    this.sarMSG = new msgSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarMSG.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("HpuxmsgSar", this.sarMSG);
                    this.sarMSG.setGraphLink("HpuxmsgSar");
                }
                return 1;
            }
            headerFound = false;
            return 1;
        }
        try {
            if (this.statType.equals("%usr")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarCPU.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if (this.statType.equals("swpin/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarSWAP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if (this.statType.equals("scall/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarSYSCALL.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if (this.statType.equals("iget/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarFILE.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if (this.statType.equals("msg/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarMSG.add(this.now, this.val1, this.val2);
                return 1;
            }
            if (this.statType.equals("rawch/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarTTY.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if (this.statType.equals("bread/s")) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.sarBUFFER.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if (this.statType.equals("runq-sz")) {
                if (!matcher.hasMoreElements()) {
                    return 1;
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarRQUEUE.add(this.now, this.val1, this.val2);
                if (matcher.hasMoreElements()) {
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.sarSQUEUE.add(this.now, this.val3, this.val4);
                }
                return 1;
            }
            if (this.statType.equals("device")) {
                diskwaitSar mydiskwait;
                diskxferSar mydiskxfer;
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                if (this.firstwastime == 1) {
                    diskwaitSar mydiskwait2;
                    diskxferSar mydiskxfer2;
                    if (!this.mysar.disksSarList.containsKey(this.lastHeader + "Hpuxxfer")) {
                        diskName tmp = new diskName(this.lastHeader);
                        this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                        tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                        mydiskxfer2 = new diskxferSar(this.mysar, this.lastHeader, tmp);
                        this.mysar.disksSarList.put(this.lastHeader + "Hpuxxfer", mydiskxfer2);
                        mydiskwait2 = new diskwaitSar(this.mysar, this.lastHeader, tmp);
                        this.mysar.disksSarList.put(this.lastHeader + "Hpuxwait", mydiskwait2);
                        DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                        mydiskxfer2.addtotree(mydisk);
                        mydiskwait2.addtotree(mydisk);
                        this.mysar.pdfList.put(this.lastHeader + "Hpuxxfer", mydiskxfer2);
                        this.mysar.pdfList.put(this.lastHeader + "Hpuxwait", mydiskwait2);
                        mydiskxfer2.setGraphLink(this.lastHeader + "Hpuxxfer");
                        mydiskwait2.setGraphLink(this.lastHeader + "Hpuxwait");
                        this.mysar.add2tree(this.mysar.diskstreenode, mydisk);
                    } else {
                        mydiskxfer2 = (diskxferSar)this.mysar.disksSarList.get(this.lastHeader + "Hpuxxfer");
                        mydiskwait2 = (diskwaitSar)this.mysar.disksSarList.get(this.lastHeader + "Hpuxwait");
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.val5 = new Float(matcher.nextToken());
                    this.val6 = new Float(matcher.nextToken());
                    mydiskxfer2.add(this.now, this.val4, this.val3, this.val6);
                    mydiskwait2.add(this.now, this.val2, this.val5, this.val1);
                    return 1;
                }
                if (!this.mysar.disksSarList.containsKey(first + "Hpuxxfer")) {
                    diskName tmp = new diskName(first);
                    this.mysar.AlternateDiskName.put(first, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(first));
                    mydiskxfer = new diskxferSar(this.mysar, first, tmp);
                    this.mysar.disksSarList.put(first + "Hpuxxfer", mydiskxfer);
                    mydiskwait = new diskwaitSar(this.mysar, first, tmp);
                    this.mysar.disksSarList.put(first + "Hpuxwait", mydiskwait);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    mydiskxfer.addtotree(mydisk);
                    mydiskwait.addtotree(mydisk);
                    this.mysar.pdfList.put(first + "Hpuxxfer", mydiskxfer);
                    this.mysar.pdfList.put(first + "Hpuxwait", mydiskwait);
                    mydiskxfer.setGraphLink(first + "Hpuxxfer");
                    mydiskwait.setGraphLink(first + "Hpuxwait");
                    this.mysar.add2tree(this.mysar.diskstreenode, mydisk);
                } else {
                    mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(first + "Hpuxxfer");
                    mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(first + "Hpuxwait");
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mydiskxfer.add(this.now, this.val4, this.val3, this.val6);
                mydiskwait.add(this.now, this.val2, this.val5, this.val1);
                return 1;
            }
        }
        catch (SeriesException e) {
            System.out.println("Hpux parser: " + (Object)e);
            return -1;
        }
        return 0;
    }
}

