/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

public class SaveSar {
    public SaveSar(File in, File out) throws IOException {
        FileChannel inChannel = new FileInputStream(in).getChannel();
        FileChannel outChannel = new FileOutputStream(out).getChannel();
        try {
            int maxCount = 67076096;
            long size = inChannel.size();
            for (long position = 0; position < size; position += inChannel.transferTo((long)position, (long)((long)maxCount), (WritableByteChannel)outChannel)) {
            }
        }
        catch (IOException e) {
            throw e;
        }
        finally {
            if (inChannel != null) {
                inChannel.close();
            }
            if (outChannel != null) {
                outChannel.close();
            }
        }
    }
}

