/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

public class IEEE1541Number
extends NumberFormat {
    private static final long serialVersionUID = 5;
    int kilo = 0;

    public IEEE1541Number() {
    }

    public IEEE1541Number(int value) {
        this.kilo = value;
    }

    public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
        if (this.kilo == 0) {
            return toAppendTo.append(number);
        }
        if (number * (double)this.kilo < 1024.0) {
            return toAppendTo.append(number);
        }
        if (number * (double)this.kilo < 1048576.0) {
            DecimalFormat formatter = new DecimalFormat("#,##0.0");
            toAppendTo.append(formatter.format(number / 1024.0)).append(" KB");
            return toAppendTo;
        }
        if (number * (double)this.kilo < 1.073741824E9) {
            DecimalFormat formatter = new DecimalFormat("#,##0.0");
            toAppendTo.append(formatter.format(number * (double)this.kilo / 1048576.0)).append(" MB");
            return toAppendTo;
        }
        DecimalFormat formatter = new DecimalFormat("#,##0.0");
        toAppendTo.append(formatter.format(number * (double)this.kilo / 1.073741824E9)).append(" GB");
        return toAppendTo;
    }

    public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
        return this.format((double)(number * (long)this.kilo), toAppendTo, pos);
    }

    public Number parse(String source, ParsePosition parsePosition) {
        return null;
    }
}

