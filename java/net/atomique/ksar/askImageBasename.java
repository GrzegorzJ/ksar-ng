/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import javax.swing.JFileChooser;

public class askImageBasename {
    kSar mysar;

    public askImageBasename(kSar hissar) {
        this.mysar = hissar;
    }

    public String run() {
        int returnVal;
        String filename = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose a basename");
        if (kSarConfig.lastExportDirectory != null) {
            chooser.setCurrentDirectory(kSarConfig.lastExportDirectory);
        }
        if ((returnVal = chooser.showSaveDialog(this.mysar.myUI)) == 0) {
            filename = chooser.getSelectedFile().getAbsolutePath();
            kSarConfig.lastExportDirectory = chooser.getSelectedFile();
            if (!kSarConfig.lastExportDirectory.isDirectory()) {
                kSarConfig.lastExportDirectory = kSarConfig.lastExportDirectory.getParentFile();
                kSarConfig.writeDefault();
            }
        }
        if (filename == null) {
            return null;
        }
        return filename;
    }
}

