/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class diskxferSar
extends AllGraph {
    private final Trigger diskavservtrigger;
    private final TimeSeries t_xfer;
    private final TimeSeries t_rw;
    private final TimeSeries t_avserv;
    private final String mydiskName;
    private final diskName optdisk;
    private final TimeSeriesCollection xfer_collection;
    private final TimeSeriesCollection rw_collection;
    private final TimeSeriesCollection avserv_collection;

    public diskxferSar(kSar hissar, String diskname, diskName diskopt) {
        super(hissar);
        this.Title = "Disk Transfer";
        this.datain = 0;
        this.mydiskName = diskname;
        this.optdisk = diskopt;
        this.t_xfer = new TimeSeries((Comparable)((Object)"block/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + this.mydiskName + " block/s", this.t_xfer);
        this.t_rw = new TimeSeries((Comparable)((Object)"read+write/s"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + this.mydiskName + " read+write/s", this.t_rw);
        this.t_avserv = new TimeSeries((Comparable)((Object)"avserv/ms"), (Class)Second.class);
        this.mysar.dispo.put("Disk " + this.mydiskName + "avserv/ms", this.t_avserv);
        this.diskavservtrigger = new Trigger(this.mysar, this, "avserv", this.t_avserv, "up");
        this.diskavservtrigger.setTriggerValue(kSarConfig.solarisdiskavservtrigger);
        this.xfer_collection = new TimeSeriesCollection();
        this.xfer_collection.addSeries(this.t_xfer);
        this.rw_collection = new TimeSeriesCollection();
        this.rw_collection.addSeries(this.t_rw);
        this.avserv_collection = new TimeSeriesCollection();
        this.avserv_collection.addSeries(this.t_avserv);
    }

    public void doclosetrigger() {
        this.diskavservtrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init) {
        Number tmpInt;
        Float zerof = new Float(0.0f);
        if (!(val1Init.equals(zerof) && val2Init.equals(zerof) && val3Init.equals(zerof) || this.datain != 0)) {
            this.datain = 1;
        }
        if ((tmpInt = this.t_xfer.getValue((RegularTimePeriod)now)) == null) {
            this.t_xfer.add((RegularTimePeriod)now, (double)(val1Init.floatValue() * 512.0f), this.do_notify());
        } else {
            this.t_xfer.update((RegularTimePeriod)now, (Number)new Float(val1Init.floatValue() * 512.0f + tmpInt.floatValue()));
        }
        tmpInt = this.t_rw.getValue((RegularTimePeriod)now);
        if (tmpInt == null) {
            this.t_rw.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        } else {
            this.t_rw.update((RegularTimePeriod)now, (Number)new Float(tmpInt.floatValue() + val2Init.floatValue()));
        }
        tmpInt = this.t_avserv.getValue((RegularTimePeriod)now);
        if (tmpInt == null) {
            this.t_avserv.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
            if (this.mysar.showtrigger) {
                this.diskavservtrigger.doMarker(now, val3Init);
            }
        } else {
            this.t_avserv.update((RegularTimePeriod)now, (Number)new Float((tmpInt.floatValue() + val3Init.floatValue()) / 2.0f));
            if (this.mysar.showtrigger) {
                this.diskavservtrigger.doMarker(now, tmpInt);
            }
        }
        ++this.number_of_sample;
    }

    public String getcheckBoxTitle() {
        return "Disk " + this.mydiskName;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISDISKXFER", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public String getGraphTitle() {
        return this.Title + " on " + this.optdisk.showTitle() + " for " + this.mysar.hostName;
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis1 = new NumberAxis("byte/s");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot1 = new XYPlot((XYDataset)this.xfer_collection, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.rw_collection, null, (ValueAxis)new NumberAxis("read+write/s"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.avserv_collection, null, (ValueAxis)new NumberAxis("avserv/ms"), (XYItemRenderer)minichart3);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        this.diskavservtrigger.setTriggerValue(kSarConfig.solarisdiskavservtrigger);
        this.diskavservtrigger.tagMarker(subplot3);
        return this.mygraph;
    }
}

