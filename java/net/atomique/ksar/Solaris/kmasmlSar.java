/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class kmasmlSar
extends AllGraph {
    private final TimeSeries t_mem;
    private final TimeSeries t_alloc;
    private final TimeSeries t_failed;
    private final TimeSeriesCollection used_collection;
    private final TimeSeriesCollection failed_collection;

    public kmasmlSar(kSar hissar) {
        super(hissar);
        this.Title = "Kernel small page";
        this.t_mem = new TimeSeries((Comparable)((Object)"Reserved"), (Class)Second.class);
        this.mysar.dispo.put("Kernel small page Reserved", this.t_mem);
        this.t_alloc = new TimeSeries((Comparable)((Object)"Allocated"), (Class)Second.class);
        this.mysar.dispo.put("Kernel small page Allocated", this.t_alloc);
        this.t_failed = new TimeSeries((Comparable)((Object)"Failed"), (Class)Second.class);
        this.mysar.dispo.put("Kernel small page Failed", this.t_failed);
        this.used_collection = new TimeSeriesCollection();
        this.used_collection.addSeries(this.t_mem);
        this.used_collection.addSeries(this.t_alloc);
        this.failed_collection = new TimeSeriesCollection();
        this.failed_collection.addSeries(this.t_failed);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init) {
        this.t_mem.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_alloc.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_failed.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISKMASML", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis1 = new NumberAxis("Reserved/Allocated");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot1 = new XYPlot((XYDataset)this.used_collection, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.failed_collection, null, (ValueAxis)new NumberAxis("Failed"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 3);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

