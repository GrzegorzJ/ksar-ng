/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ttySar
extends AllGraph {
    private final TimeSeries t_rawch;
    private final TimeSeries t_canch;
    private final TimeSeries t_outch;
    private final TimeSeries t_rcvin;
    private final TimeSeries t_xmtin;
    private final TimeSeries t_mdmin;
    private final TimeSeriesCollection tscollection;

    public ttySar(kSar hissar) {
        super(hissar);
        this.Title = "Tty";
        this.t_rawch = new TimeSeries((Comparable)((Object)"Rawch/s"), (Class)Second.class);
        this.mysar.dispo.put("Rawch/s", this.t_rawch);
        this.t_canch = new TimeSeries((Comparable)((Object)"Canch/s"), (Class)Second.class);
        this.mysar.dispo.put("Canch/s", this.t_canch);
        this.t_outch = new TimeSeries((Comparable)((Object)"Outch/s"), (Class)Second.class);
        this.mysar.dispo.put("Outch/s", this.t_outch);
        this.t_rcvin = new TimeSeries((Comparable)((Object)"Rcvin/s"), (Class)Second.class);
        this.mysar.dispo.put("Rcvin/s", this.t_rcvin);
        this.t_xmtin = new TimeSeries((Comparable)((Object)"Xmtin/s"), (Class)Second.class);
        this.mysar.dispo.put("Xmtin/s", this.t_xmtin);
        this.t_mdmin = new TimeSeries((Comparable)((Object)"Mdmin/s"), (Class)Second.class);
        this.mysar.dispo.put("Mdmin/s", this.t_mdmin);
        this.tscollection = new TimeSeriesCollection();
        this.tscollection.addSeries(this.t_rawch);
        this.tscollection.addSeries(this.t_canch);
        this.tscollection.addSeries(this.t_outch);
        this.tscollection.addSeries(this.t_rcvin);
        this.tscollection.addSeries(this.t_xmtin);
        this.tscollection.addSeries(this.t_mdmin);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init) {
        this.t_rawch.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_canch.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_outch.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        this.t_rcvin.add((RegularTimePeriod)now, (Number)val4Init, this.do_notify());
        this.t_xmtin.add((RegularTimePeriod)now, (Number)val5Init, this.do_notify());
        this.t_mdmin.add((RegularTimePeriod)now, (Number)val6Init, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISTTY", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        this.mygraph = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)this.tscollection, (boolean)true, (boolean)true, (boolean)false);
        this.setbackgroundimage(this.mygraph);
        XYPlot xyplot = (XYPlot)this.mygraph.getPlot();
        XYLineAndShapeRenderer myrenderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        myrenderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        myrenderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        myrenderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        myrenderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        myrenderer.setSeriesPaint(3, (Paint)kSarConfig.color4);
        myrenderer.setSeriesPaint(4, (Paint)kSarConfig.color5);
        myrenderer.setSeriesPaint(5, (Paint)kSarConfig.color6);
        myrenderer.setSeriesPaint(6, (Paint)kSarConfig.color7);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

