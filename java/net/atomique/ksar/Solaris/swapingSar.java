/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class swapingSar
extends AllGraph {
    private final TimeSeries swpin;
    private final TimeSeries bswin;
    private final TimeSeries swpot;
    private final TimeSeries bswot;
    private final TimeSeries pswch;
    private final TimeSeriesCollection ts_collection;

    public swapingSar(kSar hissar) {
        super(hissar);
        this.Title = "Swapping";
        this.swpin = new TimeSeries((Comparable)((Object)"LWP in"), (Class)Second.class);
        this.mysar.dispo.put("LWP swap in", this.swpin);
        this.bswin = new TimeSeries((Comparable)((Object)"pages in"), (Class)Second.class);
        this.mysar.dispo.put("Swap page in", this.bswin);
        this.swpot = new TimeSeries((Comparable)((Object)"LWP out"), (Class)Second.class);
        this.mysar.dispo.put("LWP swap out", this.swpot);
        this.bswot = new TimeSeries((Comparable)((Object)"pages out"), (Class)Second.class);
        this.mysar.dispo.put("Swap page out", this.bswot);
        this.pswch = new TimeSeries((Comparable)((Object)"LWP switch"), (Class)Second.class);
        this.mysar.dispo.put("LWP switch", this.pswch);
        this.ts_collection = new TimeSeriesCollection();
        this.ts_collection.addSeries(this.swpin);
        this.ts_collection.addSeries(this.bswin);
        this.ts_collection.addSeries(this.swpot);
        this.ts_collection.addSeries(this.bswot);
        this.ts_collection.addSeries(this.pswch);
    }

    public void add(Second now, Float swpinInt, Float bswinInt, Float swpotInt, Float bswotInt, Float pswchInt) {
        this.swpin.add((RegularTimePeriod)now, (Number)swpinInt, this.do_notify());
        this.bswin.add((RegularTimePeriod)now, (double)(bswinInt.floatValue() * 512.0f), this.do_notify());
        this.swpot.add((RegularTimePeriod)now, (Number)swpotInt, this.do_notify());
        this.bswot.add((RegularTimePeriod)now, (double)(bswotInt.floatValue() * 512.0f), this.do_notify());
        this.pswch.add((RegularTimePeriod)now, (Number)pswchInt, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISSWAP", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        this.mygraph = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)this.ts_collection, (boolean)true, (boolean)true, (boolean)false);
        this.setbackgroundimage(this.mygraph);
        XYPlot xyplot = (XYPlot)this.mygraph.getPlot();
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        renderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        renderer.setSeriesPaint(3, (Paint)kSarConfig.color4);
        renderer.setSeriesPaint(4, (Paint)kSarConfig.color5);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

