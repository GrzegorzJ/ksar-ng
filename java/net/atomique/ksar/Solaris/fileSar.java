/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.ChartFactory
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class fileSar
extends AllGraph {
    private final TimeSeries iget;
    private final TimeSeries namei;
    private final TimeSeries dirbk;
    private final TimeSeriesCollection ts_collection;

    public fileSar(kSar hissar) {
        super(hissar);
        this.Title = "File";
        this.iget = new TimeSeries((Comparable)((Object)"iget"), (Class)Second.class);
        this.mysar.dispo.put("iget", this.iget);
        this.namei = new TimeSeries((Comparable)((Object)"namei"), (Class)Second.class);
        this.mysar.dispo.put("namei", this.namei);
        this.dirbk = new TimeSeries((Comparable)((Object)"dirbk"), (Class)Second.class);
        this.mysar.dispo.put("dirbk", this.dirbk);
        this.ts_collection = new TimeSeriesCollection();
        this.ts_collection.addSeries(this.iget);
        this.ts_collection.addSeries(this.namei);
        this.ts_collection.addSeries(this.dirbk);
    }

    public void add(Second now, Float val1Int, Float val2Int, Float val3Int) {
        this.iget.add((RegularTimePeriod)now, (Number)val1Int, this.do_notify());
        this.namei.add((RegularTimePeriod)now, (Number)val2Int, this.do_notify());
        this.dirbk.add((RegularTimePeriod)now, (Number)val3Int, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISFILE", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        this.mygraph = ChartFactory.createTimeSeriesChart((String)this.getGraphTitle(), (String)"", (String)"per second", (XYDataset)this.ts_collection, (boolean)true, (boolean)true, (boolean)false);
        this.setbackgroundimage(this.mygraph);
        XYPlot xyplot = (XYPlot)this.mygraph.getPlot();
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)xyplot.getRenderer();
        renderer.setSeriesPaint(0, (Paint)kSarConfig.color1);
        renderer.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        renderer.setSeriesPaint(1, (Paint)kSarConfig.color2);
        renderer.setSeriesPaint(2, (Paint)kSarConfig.color3);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

