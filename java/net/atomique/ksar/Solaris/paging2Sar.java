/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class paging2Sar
extends AllGraph {
    private final TimeSeries t_atch;
    private final TimeSeries t_pgin;
    private final TimeSeries t_ppgin;
    private final TimeSeries t_pflt;
    private final TimeSeries t_vflt;
    private final TimeSeries t_slock;
    private final TimeSeriesCollection atch_collection;
    private final TimeSeriesCollection pgin_collection;
    private final TimeSeriesCollection flt_collection;
    private final TimeSeriesCollection lock_collection;

    public paging2Sar(kSar hissar) {
        super(hissar);
        this.Title = "Paging2";
        this.t_atch = new TimeSeries((Comparable)((Object)"acth/s"), (Class)Second.class);
        this.mysar.dispo.put("Page Attach/s", this.t_atch);
        this.t_pgin = new TimeSeries((Comparable)((Object)"pgin/s"), (Class)Second.class);
        this.mysar.dispo.put("Page In/s", this.t_pgin);
        this.t_ppgin = new TimeSeries((Comparable)((Object)"ppgin/s"), (Class)Second.class);
        this.mysar.dispo.put("Priority page in/s", this.t_ppgin);
        this.t_pflt = new TimeSeries((Comparable)((Object)"pflt/s"), (Class)Second.class);
        this.mysar.dispo.put("Page fault/s", this.t_pflt);
        this.t_vflt = new TimeSeries((Comparable)((Object)"vflt/s"), (Class)Second.class);
        this.mysar.dispo.put("Valid Page fault/s", this.t_vflt);
        this.t_slock = new TimeSeries((Comparable)((Object)"slock/s"), (Class)Second.class);
        this.mysar.dispo.put("Page software lock/s", this.t_slock);
        this.atch_collection = new TimeSeriesCollection();
        this.atch_collection.addSeries(this.t_atch);
        this.pgin_collection = new TimeSeriesCollection();
        this.pgin_collection.addSeries(this.t_pgin);
        this.pgin_collection.addSeries(this.t_ppgin);
        this.flt_collection = new TimeSeriesCollection();
        this.flt_collection.addSeries(this.t_pflt);
        this.flt_collection.addSeries(this.t_vflt);
        this.lock_collection = new TimeSeriesCollection();
        this.lock_collection.addSeries(this.t_slock);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init) {
        this.t_atch.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_pgin.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_ppgin.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        this.t_pflt.add((RegularTimePeriod)now, (Number)val4Init, this.do_notify());
        this.t_vflt.add((RegularTimePeriod)now, (Number)val5Init, this.do_notify());
        this.t_slock.add((RegularTimePeriod)now, (Number)val6Init, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISPAGING2", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot1 = new XYPlot((XYDataset)this.atch_collection, null, (ValueAxis)new NumberAxis("atch/s"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.pgin_collection, null, (ValueAxis)new NumberAxis("pgin/ppgin /s"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color5);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.flt_collection, null, (ValueAxis)new NumberAxis("pflt/vflt /s"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color6);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.lock_collection, null, (ValueAxis)new NumberAxis("slock/s"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return mychart;
    }
}

