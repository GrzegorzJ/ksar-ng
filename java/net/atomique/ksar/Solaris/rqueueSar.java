/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.Trigger;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class rqueueSar
extends AllGraph {
    private final Trigger rqueuetrigger;
    private final TimeSeries t_runqsz;
    private final TimeSeries t_runqocc;
    private final TimeSeriesCollection collectionrunq1;
    private final TimeSeriesCollection collectionrunq2;

    public rqueueSar(kSar hissar) {
        super(hissar);
        this.Title = "Run Queue";
        this.t_runqsz = new TimeSeries((Comparable)((Object)"runq-sz"), (Class)Second.class);
        this.mysar.dispo.put("Run Queue Size", this.t_runqsz);
        this.t_runqocc = new TimeSeries((Comparable)((Object)"runqocc"), (Class)Second.class);
        this.mysar.dispo.put("Run Queue Occupied", this.t_runqocc);
        this.rqueuetrigger = new Trigger(this.mysar, this, "Size", this.t_runqsz, "up");
        this.rqueuetrigger.setTriggerValue(kSarConfig.solarisrqueuetrigger);
        this.collectionrunq1 = new TimeSeriesCollection();
        this.collectionrunq1.addSeries(this.t_runqsz);
        this.collectionrunq2 = new TimeSeriesCollection();
        this.collectionrunq2.addSeries(this.t_runqocc);
    }

    public void doclosetrigger() {
        this.rqueuetrigger.doclose();
    }

    public void add(Second now, Float val1Init, Float val2Init) {
        this.t_runqsz.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_runqocc.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        if (this.mysar.showtrigger) {
            this.rqueuetrigger.doMarker(now, val1Init);
        }
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISRQUEUE", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        XYPlot subplot1 = new XYPlot((XYDataset)this.collectionrunq1, null, (ValueAxis)new NumberAxis("Size"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.collectionrunq2, null, (ValueAxis)new NumberAxis("%occ"), (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        JFreeChart mychart = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)mychart.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        if (this.setbackgroundimage(mychart) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        this.rqueuetrigger.setTriggerValue(kSarConfig.solarisrqueuetrigger);
        this.rqueuetrigger.tagMarker(subplot1);
        return mychart;
    }
}

