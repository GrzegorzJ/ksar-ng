/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import java.text.NumberFormat;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.IEEE1541Number;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class memSar
extends AllGraph {
    private final TimeSeries t_freemem;
    private final TimeSeries t_freeswap;
    private final TimeSeriesCollection collectionmem;
    private final TimeSeriesCollection collectionswap;

    public memSar(kSar hissar) {
        super(hissar);
        this.Title = "Memory usage";
        this.t_freemem = new TimeSeries((Comparable)((Object)"Free memory"), (Class)Second.class);
        this.mysar.dispo.put("Free memory", this.t_freemem);
        this.t_freeswap = new TimeSeries((Comparable)((Object)"Free swap"), (Class)Second.class);
        this.mysar.dispo.put("Free swap", this.t_freeswap);
        this.collectionmem = new TimeSeriesCollection();
        this.collectionmem.addSeries(this.t_freemem);
        this.collectionswap = new TimeSeriesCollection();
        this.collectionswap.addSeries(this.t_freeswap);
    }

    public void add(Second now, Float memInit, Float swapInit) {
        this.t_freemem.add((RegularTimePeriod)now, (Number)memInit, this.do_notify());
        this.t_freeswap.add((RegularTimePeriod)now, (double)(swapInit.floatValue() * 512.0f), this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISMEM", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        NumberAxis numberaxis1 = new NumberAxis("freswap");
        IEEE1541Number decimalformat1 = new IEEE1541Number(1);
        numberaxis1.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot1 = new XYPlot((XYDataset)this.collectionswap, null, (ValueAxis)numberaxis1, (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color2);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        NumberAxis numberaxis2 = new NumberAxis("freemem");
        decimalformat1 = this.mysar.solarispagesize == -1 ? new IEEE1541Number(0) : new IEEE1541Number(this.mysar.solarispagesize);
        numberaxis2.setNumberFormatOverride((NumberFormat)decimalformat1);
        XYPlot subplot2 = new XYPlot((XYDataset)this.collectionmem, null, (ValueAxis)numberaxis2, (XYItemRenderer)minichart2);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

