/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.data.general.SeriesException
 *  org.jfree.data.time.Second
 */
package net.atomique.ksar.Solaris;

import java.util.StringTokenizer;
import javax.swing.tree.DefaultMutableTreeNode;

import net.atomique.ksar.diskName;
import net.atomique.ksar.kSar;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Second;

public class Parser {
    private final kSar mysar;
    private Float val1;
    private Float val2;
    private Float val3;
    private Float val4;
    private Float val5;
    private Float val6;
    private Float val7;
    private Float val8;
    private int heure = 0;
    private int minute = 0;
    private int seconde = 0;
    private Second now = new Second(0, 0, 0, 1, 1, 1970);
    private String lastHeader;
    private String statType = "none";
    private int firstwastime;
    private cpuSar sarCPU = null;
    private swapingSar sarSWAP = null;
    private memSar sarMEM = null;
    private syscallSar sarSYSCALL = null;
    private fileSar sarFILE = null;
    private msgSar sarMSG = null;
    private paging1Sar sarPAGING1 = null;
    private paging2Sar sarPAGING2 = null;
    private ttySar sarTTY = null;
    private bufferSar sarBUFFER = null;
    private squeueSar sarSQUEUE = null;
    private rqueueSar sarRQUEUE = null;
    private kmasmlSar sarKMASML = null;
    private kmalgSar sarKMALG = null;
    private kmaovzSar sarKMAOVZ = null;

    public Parser(kSar hissar) {
        this.mysar = hissar;
    }

    public int parse(String thisLine, String first, StringTokenizer matcher) {
        String[] sarTime;
        boolean headerFound = false;
        if (thisLine.indexOf("%usr") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("device") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("runq-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("bread/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("swpin/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("scall/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("iget/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("rawch/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("proc-sz") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("msg/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("atch/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("pgout/s") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("freemem") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if (thisLine.indexOf("sml_mem") > 0) {
            headerFound = true;
            this.mysar.underaverage = 0;
        }
        if ((sarTime = first.split(":")).length != 3 && !"device".equals(this.statType)) {
            return 1;
        }
        if (sarTime.length == 3) {
            this.heure = Integer.parseInt(sarTime[0]);
            this.minute = Integer.parseInt(sarTime[1]);
            this.seconde = Integer.parseInt(sarTime[2]);
            this.now = new Second(this.seconde, this.minute, this.heure, this.mysar.day, this.mysar.month, this.mysar.year);
            if (this.mysar.statstart == null) {
                this.mysar.statstart = new String(this.now.toString());
                this.mysar.startofgraph = this.now;
            }
            if (!this.mysar.datefound.contains((Object)this.now)) {
                this.mysar.datefound.add(this.now);
            }
            if (this.now.compareTo((Object)this.mysar.lastever) > 0) {
                this.mysar.lastever = this.now;
                this.mysar.statend = new String(this.mysar.lastever.toString());
                this.mysar.endofgraph = this.mysar.lastever;
            }
            this.firstwastime = 1;
        } else {
            this.firstwastime = 0;
        }
        if (!matcher.hasMoreElements()) {
            return 1;
        }
        this.lastHeader = matcher.nextToken();
        if (headerFound) {
            if (this.lastHeader.equals(this.statType)) {
                headerFound = false;
                return 1;
            }
            this.statType = this.lastHeader;
            if ("%usr".equals(this.lastHeader)) {
                if (this.sarCPU == null) {
                    this.sarCPU = new cpuSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarCPU.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolariscpuSar", this.sarCPU);
                    this.sarCPU.setGraphLink("SolariscpuSar");
                }
                return 1;
            }
            if ("device".equals(this.lastHeader)) {
                if (!this.mysar.hasdisknode) {
                    if (this.mysar.myUI != null) {
                        this.mysar.add2tree(this.mysar.graphtree, this.mysar.diskstreenode);
                    }
                    this.mysar.hasdisknode = true;
                }
                return 1;
            }
            if ("runq-sz".equals(this.lastHeader)) {
                if (this.sarRQUEUE == null) {
                    this.sarRQUEUE = new rqueueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarRQUEUE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarisRqueueSar", this.sarRQUEUE);
                    this.sarRQUEUE.setGraphLink("SolarisRqueueSar");
                }
                if (this.sarSQUEUE == null) {
                    this.sarSQUEUE = new squeueSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSQUEUE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarisSqueueSar", this.sarSQUEUE);
                    this.sarSQUEUE.setGraphLink("SolarisSqueueSar");
                }
                return 1;
            }
            if ("bread/s".equals(this.lastHeader)) {
                if (this.sarBUFFER == null) {
                    this.sarBUFFER = new bufferSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarBUFFER.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarisbufferSar", this.sarBUFFER);
                    this.sarBUFFER.setGraphLink("SolarisbufferSar");
                }
                return 1;
            }
            if ("swpin/s".equals(this.lastHeader)) {
                if (this.sarSWAP == null) {
                    this.sarSWAP = new swapingSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSWAP.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarisswapingSar", this.sarSWAP);
                    this.sarSWAP.setGraphLink("SolarisswapSar");
                }
                return 1;
            }
            if ("scall/s".equals(this.lastHeader)) {
                if (this.sarSYSCALL == null) {
                    this.sarSYSCALL = new syscallSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarSYSCALL.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarissyscalSar", this.sarSYSCALL);
                    this.sarSYSCALL.setGraphLink("SolarissyscalSar");
                }
                return 1;
            }
            if ("iget/s".equals(this.lastHeader)) {
                if (this.sarFILE == null) {
                    this.sarFILE = new fileSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarFILE.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarisfileSar", this.sarFILE);
                    this.sarFILE.setGraphLink("SolarisfileSar");
                }
                return 1;
            }
            if ("rawch/s".equals(this.lastHeader)) {
                if (this.sarTTY == null) {
                    this.sarTTY = new ttySar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarTTY.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolaristtySar", this.sarTTY);
                    this.sarTTY.setGraphLink("SolaristtySar");
                }
                return 1;
            }
            if ("proc-sz".equals(this.lastHeader)) {
                return 1;
            }
            if ("msg/s".equals(this.lastHeader)) {
                if (this.sarMSG == null) {
                    this.sarMSG = new msgSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarMSG.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarismsgSar", this.sarMSG);
                    this.sarMSG.setGraphLink("SolarismsgSar");
                }
                return 1;
            }
            if ("atch/s".equals(this.lastHeader)) {
                if (this.sarPAGING2 == null) {
                    this.sarPAGING2 = new paging2Sar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPAGING2.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarispagingSar2", this.sarPAGING2);
                    this.sarPAGING2.setGraphLink("SolarispagingSar2");
                }
                return 1;
            }
            if ("pgout/s".equals(this.lastHeader)) {
                if (this.sarPAGING1 == null) {
                    this.sarPAGING1 = new paging1Sar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarPAGING1.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarispagingSar1", this.sarPAGING1);
                    this.sarPAGING2.setGraphLink("SolarispagingSar1");
                }
                return 1;
            }
            if ("freemem".equals(this.lastHeader)) {
                if (this.sarMEM == null) {
                    this.sarMEM = new memSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarMEM.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolarismemSar", this.sarMEM);
                    this.sarMEM.setGraphLink("SolarismemSar");
                }
                return 1;
            }
            if ("sml_mem".equals(this.lastHeader)) {
                if (this.sarKMASML == null) {
                    this.sarKMASML = new kmasmlSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKMASML.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolariskmasmlSar", this.sarKMASML);
                    this.sarKMASML.setGraphLink("SolariskmasmlSar");
                }
                if (this.sarKMALG == null) {
                    this.sarKMALG = new kmalgSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKMALG.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolariskmalgSar", this.sarKMALG);
                    this.sarKMALG.setGraphLink("SolariskmalgSar");
                }
                if (this.sarKMAOVZ == null) {
                    this.sarKMAOVZ = new kmaovzSar(this.mysar);
                    if (this.mysar.myUI != null) {
                        this.sarKMAOVZ.addtotree(this.mysar.graphtree);
                    }
                    this.mysar.pdfList.put("SolariskmaovzSar", this.sarKMAOVZ);
                    this.sarKMAOVZ.setGraphLink("SolariskmaovzSar");
                }
                return 1;
            }
            headerFound = false;
            return 1;
        }
        try {
            if ("%usr".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.sarCPU.add(this.now, this.val1, this.val2, this.val3, this.val4);
                return 1;
            }
            if ("swpin/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarSWAP.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if ("freemem".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarMEM.add(this.now, this.val1, this.val2);
                return 1;
            }
            if ("scall/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.sarSYSCALL.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7);
                return 1;
            }
            if ("iget/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.sarFILE.add(this.now, this.val1, this.val2, this.val3);
                return 1;
            }
            if ("msg/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarMSG.add(this.now, this.val1, this.val2);
                return 1;
            }
            if ("pgout/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.sarPAGING1.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5);
                return 1;
            }
            if ("proc-sz".equals(this.statType)) {
                return 1;
            }
            if ("atch/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarPAGING2.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if ("rawch/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.sarTTY.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6);
                return 1;
            }
            if ("bread/s".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.sarBUFFER.add(this.now, this.val1, this.val2, this.val3, this.val4, this.val5, this.val6, this.val7, this.val8);
                return 1;
            }
            if ("runq-sz".equals(this.statType)) {
                if (!matcher.hasMoreElements()) {
                    return 1;
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.sarRQUEUE.add(this.now, this.val1, this.val2);
                if (matcher.hasMoreElements()) {
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.sarSQUEUE.add(this.now, this.val3, this.val4);
                }
                return 1;
            }
            if ("sml_mem".equals(this.statType)) {
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                this.val7 = new Float(matcher.nextToken());
                this.val8 = new Float(matcher.nextToken());
                this.sarKMASML.add(this.now, this.val1, this.val2, this.val3);
                this.sarKMALG.add(this.now, this.val4, this.val5, this.val6);
                this.sarKMAOVZ.add(this.now, this.val7, this.val8);
                return 1;
            }
            if ("device".equals(this.statType)) {
                if (this.mysar.underaverage == 1) {
                    return 1;
                }
                diskxferSar mydiskxfer = null;
                diskwaitSar mydiskwait = null;
                if (this.firstwastime == 1) {
                    mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(this.lastHeader + "Solarisxfer");
                    if (mydiskxfer == null) {
                        diskName tmp = new diskName(this.lastHeader);
                        this.mysar.AlternateDiskName.put(this.lastHeader, tmp);
                        tmp.setTitle(this.mysar.Adiskname.get(this.lastHeader));
                        mydiskxfer = new diskxferSar(this.mysar, this.lastHeader, tmp);
                        this.mysar.disksSarList.put(this.lastHeader + "Solarisxfer", mydiskxfer);
                        mydiskwait = new diskwaitSar(this.mysar, this.lastHeader, tmp);
                        this.mysar.disksSarList.put(this.lastHeader + "Solariswait", mydiskwait);
                        DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                        this.mysar.pdfList.put(this.lastHeader + "Solarisxfer", mydiskxfer);
                        mydiskxfer.addtotree(mydisk);
                        mydiskwait.addtotree(mydisk);
                        this.mysar.pdfList.put(this.lastHeader + "Solariswait", mydiskwait);
                        mydiskxfer.setGraphLink(this.lastHeader + "Solarisxfer");
                        mydiskwait.setGraphLink(this.lastHeader + "Solariswait");
                        this.mysar.add2tree(this.mysar.diskstreenode, mydisk);
                    } else {
                        mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(this.lastHeader + "Solariswait");
                    }
                    this.val1 = new Float(matcher.nextToken());
                    this.val2 = new Float(matcher.nextToken());
                    this.val3 = new Float(matcher.nextToken());
                    this.val4 = new Float(matcher.nextToken());
                    this.val5 = new Float(matcher.nextToken());
                    this.val6 = new Float(matcher.nextToken());
                    mydiskxfer.add(this.now, this.val4, this.val3, this.val6);
                    mydiskwait.add(this.now, this.val2, this.val5, this.val1);
                    return 1;
                }
                mydiskxfer = (diskxferSar)this.mysar.disksSarList.get(first + "Solarisxfer");
                if (mydiskxfer == null) {
                    diskName tmp = new diskName(first);
                    this.mysar.AlternateDiskName.put(first, tmp);
                    tmp.setTitle(this.mysar.Adiskname.get(first));
                    mydiskxfer = new diskxferSar(this.mysar, first, tmp);
                    this.mysar.disksSarList.put(first + "Solarisxfer", mydiskxfer);
                    mydiskwait = new diskwaitSar(this.mysar, first, tmp);
                    this.mysar.disksSarList.put(first + "Solariswait", mydiskwait);
                    DefaultMutableTreeNode mydisk = new DefaultMutableTreeNode(tmp.showTitle());
                    mydiskxfer.addtotree(mydisk);
                    mydiskwait.addtotree(mydisk);
                    this.mysar.pdfList.put(first + "Solarisxfer", mydiskxfer);
                    this.mysar.pdfList.put(first + "Solariswait", mydiskwait);
                    mydiskxfer.setGraphLink(first + "Solarisxfer");
                    mydiskwait.setGraphLink(first + "Solariswait");
                    this.mysar.add2tree(this.mysar.diskstreenode, mydisk);
                } else {
                    mydiskwait = (diskwaitSar)this.mysar.disksSarList.get(first + "Solariswait");
                }
                this.val1 = new Float(this.lastHeader);
                this.val2 = new Float(matcher.nextToken());
                this.val3 = new Float(matcher.nextToken());
                this.val4 = new Float(matcher.nextToken());
                this.val5 = new Float(matcher.nextToken());
                this.val6 = new Float(matcher.nextToken());
                mydiskxfer.add(this.now, this.val4, this.val3, this.val6);
                mydiskwait.add(this.now, this.val2, this.val5, this.val1);
                return 1;
            }
        }
        catch (SeriesException e) {
            System.out.println("Solaris parser: " + (Object)e);
            return -1;
        }
        return 0;
    }
}

