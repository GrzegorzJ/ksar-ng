/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.jfree.chart.JFreeChart
 *  org.jfree.chart.axis.DateAxis
 *  org.jfree.chart.axis.NumberAxis
 *  org.jfree.chart.axis.ValueAxis
 *  org.jfree.chart.plot.CombinedDomainXYPlot
 *  org.jfree.chart.plot.Plot
 *  org.jfree.chart.plot.PlotOrientation
 *  org.jfree.chart.plot.XYPlot
 *  org.jfree.chart.renderer.xy.StandardXYItemRenderer
 *  org.jfree.chart.renderer.xy.XYItemRenderer
 *  org.jfree.data.time.RegularTimePeriod
 *  org.jfree.data.time.Second
 *  org.jfree.data.time.TimeSeries
 *  org.jfree.data.time.TimeSeriesCollection
 *  org.jfree.data.xy.XYDataset
 */
package net.atomique.ksar.Solaris;

import java.awt.Paint;
import java.awt.Stroke;
import javax.swing.tree.DefaultMutableTreeNode;
import net.atomique.ksar.AllGraph;
import net.atomique.ksar.GraphDescription;
import net.atomique.ksar.kSar;
import net.atomique.ksar.kSarConfig;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class syscallSar
extends AllGraph {
    private final TimeSeries t_scall;
    private final TimeSeries t_sread;
    private final TimeSeries t_swrit;
    private final TimeSeries t_fork;
    private final TimeSeries t_exec;
    private final TimeSeries t_rchar;
    private final TimeSeries t_wchar;
    private final TimeSeriesCollection rw_collection;
    private final TimeSeriesCollection call_collection;
    private final TimeSeriesCollection fork_collection;
    private final TimeSeriesCollection char_collection;

    public syscallSar(kSar hissar) {
        super(hissar);
        this.Title = "Syscalls";
        this.t_scall = new TimeSeries((Comparable)((Object)"Syscall/s"), (Class)Second.class);
        this.mysar.dispo.put("Syscall/s", this.t_scall);
        this.t_sread = new TimeSeries((Comparable)((Object)"Read/s"), (Class)Second.class);
        this.mysar.dispo.put("Read/s", this.t_sread);
        this.t_swrit = new TimeSeries((Comparable)((Object)"Write/s"), (Class)Second.class);
        this.mysar.dispo.put("Write/s", this.t_swrit);
        this.t_fork = new TimeSeries((Comparable)((Object)"Fork/s"), (Class)Second.class);
        this.mysar.dispo.put("Fork/s", this.t_fork);
        this.t_exec = new TimeSeries((Comparable)((Object)"Exec/s"), (Class)Second.class);
        this.mysar.dispo.put("Exec/s", this.t_exec);
        this.t_rchar = new TimeSeries((Comparable)((Object)"Rchar/s"), (Class)Second.class);
        this.mysar.dispo.put("Rchar/s", this.t_rchar);
        this.t_wchar = new TimeSeries((Comparable)((Object)"Wchar/s"), (Class)Second.class);
        this.mysar.dispo.put("Wchar/s", this.t_wchar);
        this.rw_collection = new TimeSeriesCollection();
        this.rw_collection.addSeries(this.t_sread);
        this.rw_collection.addSeries(this.t_swrit);
        this.call_collection = new TimeSeriesCollection();
        this.call_collection.addSeries(this.t_scall);
        this.fork_collection = new TimeSeriesCollection();
        this.fork_collection.addSeries(this.t_fork);
        this.fork_collection.addSeries(this.t_exec);
        this.char_collection = new TimeSeriesCollection();
        this.char_collection.addSeries(this.t_rchar);
        this.char_collection.addSeries(this.t_wchar);
    }

    public void add(Second now, Float val1Init, Float val2Init, Float val3Init, Float val4Init, Float val5Init, Float val6Init, Float val7Init) {
        this.t_scall.add((RegularTimePeriod)now, (Number)val1Init, this.do_notify());
        this.t_sread.add((RegularTimePeriod)now, (Number)val2Init, this.do_notify());
        this.t_swrit.add((RegularTimePeriod)now, (Number)val3Init, this.do_notify());
        this.t_fork.add((RegularTimePeriod)now, (Number)val4Init, this.do_notify());
        this.t_exec.add((RegularTimePeriod)now, (Number)val5Init, this.do_notify());
        this.t_rchar.add((RegularTimePeriod)now, (Number)val6Init, this.do_notify());
        this.t_wchar.add((RegularTimePeriod)now, (Number)val7Init, this.do_notify());
        ++this.number_of_sample;
    }

    public void addtotree(DefaultMutableTreeNode myroot) {
        this.mynode = new DefaultMutableTreeNode(new GraphDescription(this, "SOLARISSYSCALL", this.Title, null));
        this.mysar.add2tree(myroot, this.mynode);
    }

    public JFreeChart makegraph(Second g_start, Second g_end) {
        StandardXYItemRenderer minichart1 = new StandardXYItemRenderer();
        minichart1.setSeriesPaint(0, (Paint)kSarConfig.color1);
        minichart1.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        minichart1.setSeriesPaint(1, (Paint)kSarConfig.color2);
        XYPlot subplot1 = new XYPlot((XYDataset)this.rw_collection, null, (ValueAxis)new NumberAxis("read/write /s"), (XYItemRenderer)minichart1);
        StandardXYItemRenderer minichart2 = new StandardXYItemRenderer();
        minichart2.setSeriesPaint(0, (Paint)kSarConfig.color3);
        minichart2.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot2 = new XYPlot((XYDataset)this.call_collection, null, (ValueAxis)new NumberAxis("syscall/s"), (XYItemRenderer)minichart2);
        StandardXYItemRenderer minichart3 = new StandardXYItemRenderer();
        minichart3.setSeriesPaint(0, (Paint)kSarConfig.color4);
        minichart3.setSeriesPaint(1, (Paint)kSarConfig.color5);
        minichart3.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot3 = new XYPlot((XYDataset)this.fork_collection, null, (ValueAxis)new NumberAxis("fork/exec /s"), (XYItemRenderer)minichart3);
        StandardXYItemRenderer minichart4 = new StandardXYItemRenderer();
        minichart4.setSeriesPaint(0, (Paint)kSarConfig.color6);
        minichart4.setSeriesPaint(1, (Paint)kSarConfig.color7);
        minichart4.setBaseStroke((Stroke)kSarConfig.DEFAULT_STROKE);
        XYPlot subplot4 = new XYPlot((XYDataset)this.char_collection, null, (ValueAxis)new NumberAxis("rchar/wchar /s"), (XYItemRenderer)minichart4);
        CombinedDomainXYPlot plot = new CombinedDomainXYPlot((ValueAxis)new DateAxis(""));
        plot.add(subplot1, 1);
        plot.add(subplot2, 1);
        plot.add(subplot3, 1);
        plot.add(subplot4, 1);
        plot.setOrientation(PlotOrientation.VERTICAL);
        this.mygraph = new JFreeChart(this.getGraphTitle(), kSarConfig.DEFAULT_FONT, (Plot)plot, true);
        if (this.setbackgroundimage(this.mygraph) == 1) {
            subplot1.setBackgroundPaint(null);
            subplot2.setBackgroundPaint(null);
            subplot3.setBackgroundPaint(null);
            subplot4.setBackgroundPaint(null);
        }
        if (g_start != null) {
            DateAxis dateaxis1 = (DateAxis)this.mygraph.getXYPlot().getDomainAxis();
            dateaxis1.setRange(g_start.getStart(), g_end.getEnd());
        }
        return this.mygraph;
    }
}

