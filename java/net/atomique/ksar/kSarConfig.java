/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class kSarConfig {
    private static kSarConfig instance = new kSarConfig();
    private static Preferences myPref;
    private static boolean loaded;
    public static File lastReadDirectory;
    public static File lastExportDirectory;
    public static ArrayList<String> sshconnectionmap;
    public static ArrayList<String> sshconnectioncmd;
    public static HashMap<String, String> shortcut_window_list;
    public static ArrayList<String> startup_windows_list;
    public static HashMap<String, String> association_list;
    public static boolean ssh_stricthostchecking;
    public static HashMap<String, String> shortcut_window_processlist;
    public static Double solariscpuidletrigger;
    public static Double solariscpusystemtrigger;
    public static Double solariscpuwiotrigger;
    public static Double solariscpuusrtrigger;
    public static Double solarisbufferrcachetrigger;
    public static Double solarisdiskbusytrigger;
    public static Double solarisdiskavquetrigger;
    public static Double solarisdiskavservtrigger;
    public static Double solarisrqueuetrigger;
    public static Double solarispagescantrigger;
    public static Double hpuxcpuidletrigger;
    public static Double hpuxcpusystemtrigger;
    public static Double hpuxcpuwiotrigger;
    public static Double hpuxcpuusrtrigger;
    public static Double hpuxbufferrcachetrigger;
    public static Double hpuxdiskbusytrigger;
    public static Double hpuxdiskavquetrigger;
    public static Double hpuxdiskavservtrigger;
    public static Double hpuxrqueuetrigger;
    public static Double aixcpuidletrigger;
    public static Double aixcpusystemtrigger;
    public static Double aixcpuwiotrigger;
    public static Double aixcpuusrtrigger;
    public static Double aixbufferrcachetrigger;
    public static Double aixdiskbusytrigger;
    public static Double aixdiskavquetrigger;
    public static Double aixdiskavservtrigger;
    public static Double aixrqueuetrigger;
    public static Double linuxcpuidletrigger;
    public static Double linuxcpusystemtrigger;
    public static Double linuxcpuwiotrigger;
    public static Double linuxcpuusrtrigger;
    public static boolean linuxhack;
    public static String pdfindexpage;
    public static String pdfbottomleft;
    public static String pdfupperright;
    public static int imagewidth;
    public static int imageheight;
    public static boolean imagehtml;
    public static File sshidentity;
    public static File background_image;
    static StringBuffer tmpbuf;
    static boolean hasUI;
    static final String prefsave_err = "There was a problem while saving your preferences";
    public static String unified_user;
    public static String unified_host;
    private static String pref_to_flush;
    public static boolean tile_at_startup;
    static String tempstr;
    static String landf;
    public static Color color1;
    public static Color color2;
    public static Color color3;
    public static Color color4;
    public static Color color5;
    public static Color color6;
    public static Color color7;
    public static Color color8;
    public static Color color9;
    public static Color color10;
    public static Color color11;
    public static int alwaysrefresh;
    public static int somerefresh;
    public static int somerefresh_time;
    public static int lessrefresh;
    public static int lessrefresh_time;
    public static int norefresh;
    public static String tempdir;
    public static final Font DEFAULT_FONT;
    public static final BasicStroke DEFAULT_STROKE;

    public static kSarConfig getInstance() {
        return instance;
    }

    kSarConfig() {
        if (!loaded) {
            myPref = Preferences.userNodeForPackage(kSar.class);
            loaded = true;
            kSarConfig.loadDefault();
        }
    }

    public static void flush_prefs(String pref) {
        if (pref == null) {
            return;
        }
        pref_to_flush = pref;
    }

    public static void clear_all() {
        try {
            myPref.clear();
            myPref.flush();
        }
        catch (BackingStoreException e) {
            if (hasUI) {
                JOptionPane.showMessageDialog(null, "There was a problem while saving your preferences", "Config error", 0);
            }
            System.err.println("There was a problem while saving your preferences");
        }
        loaded = false;
        kSarConfig.loadDefault();
    }

    public static void loadDefault() {
        String mybackimage;
        String mylastexportdirectory;
        tempdir = System.getProperty("java.io.tmpdir");
        if (!tempdir.endsWith("/") && !tempdir.endsWith("\\")) {
            tempdir = tempdir + System.getProperty("file.separator");
        }
        Integer version = myPref.getInt("version", 0);
        unified_user = myPref.get("UnifiedUser", null);
        unified_host = myPref.get("UnifiedHost", null);
        String mylastReadDirectory = myPref.get("lastReadDirectory", null);
        if (mylastReadDirectory != null) {
            lastReadDirectory = new File(mylastReadDirectory);
        }
        if ((mylastexportdirectory = myPref.get("lastExportDirectory", null)) != null) {
            lastExportDirectory = new File(mylastexportdirectory);
        }
        sshconnectionmap = new ArrayList();
        String SSHConnectionList = myPref.get("SSHConnectionList", null);
        if (SSHConnectionList != null && version > 0) {
            String[] tmp = SSHConnectionList.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                sshconnectionmap.add(tmp[i]);
            }
        }
        sshconnectioncmd = new ArrayList();
        String SSHCommandList = myPref.get("SSHCommandList", null);
        if (SSHCommandList != null && version > 0) {
            String[] tmp = SSHCommandList.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                sshconnectioncmd.add(tmp[i]);
            }
        }
        shortcut_window_list = new HashMap();
        String shortcutwindowtemp = myPref.get("Shortcut", null);
        if (shortcutwindowtemp != null) {
            String[] tmp = shortcutwindowtemp.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                String tmpstr = tmp[i];
                String[] temp = tmpstr.split("\u221e");
                shortcut_window_list.put(temp[0], temp[1]);
            }
        }
        ssh_stricthostchecking = myPref.getBoolean("stricthost", true);
        shortcut_window_processlist = new HashMap();
        String shortcut_window_processlisttemp = myPref.get("Processlist", null);
        if (shortcut_window_processlisttemp != null) {
            String[] tmp = shortcut_window_processlisttemp.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                String tmpstr = tmp[i];
                String[] temp = tmpstr.split("\u221e");
                shortcut_window_processlist.put(temp[0], temp[1]);
            }
        }
        startup_windows_list = new ArrayList();
        String startupList = myPref.get("Startup", null);
        if (startupList != null) {
            String[] tmp = startupList.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                startup_windows_list.add(tmp[i]);
            }
        }
        association_list = new HashMap();
        String associationList = myPref.get("Association", null);
        if (associationList != null) {
            String[] tmp = associationList.split("\u00a7");
            for (int i = 0; i < tmp.length; ++i) {
                String tmpstr = tmp[i];
                String[] temp = tmpstr.split("\u221e");
                association_list.put(temp[0], temp[1]);
            }
        }
        solariscpuidletrigger = new Double(myPref.getDouble("SolarisCpuIdleTrig", 0.0));
        solariscpusystemtrigger = new Double(myPref.getDouble("SolarisCpuSystemTrig", 50.0));
        solariscpuwiotrigger = new Double(myPref.getDouble("SolarisCpuWioTrig", 25.0));
        solariscpuusrtrigger = new Double(myPref.getDouble("SolarisCpuUsrTrig", 50.0));
        solarisbufferrcachetrigger = new Double(myPref.getDouble("SolarisBufferRcacheTrig", 95.0));
        solarisdiskbusytrigger = new Double(myPref.getDouble("SolarisDiskBusyTrig", 100.0));
        solarisrqueuetrigger = new Double(myPref.getDouble("SolarisRqueueTrig", 2.0));
        solarispagescantrigger = new Double(myPref.getDouble("SolarisPageScanTrig", 100.0));
        solarisdiskavservtrigger = new Double(myPref.getDouble("SolarisDiskAvservTrig", 30.0));
        solarisdiskavquetrigger = new Double(myPref.getDouble("SolarisDiskAvqueTrig", 10.0));
        hpuxcpuidletrigger = new Double(myPref.getDouble("HpuxCpuIdleTrig", 0.0));
        hpuxcpusystemtrigger = new Double(myPref.getDouble("HpuxCpuSystemTrig", 50.0));
        hpuxcpuwiotrigger = new Double(myPref.getDouble("HpuxCpuWioTrig", 25.0));
        hpuxcpuusrtrigger = new Double(myPref.getDouble("HpuxCpuUsrTrig", 50.0));
        hpuxbufferrcachetrigger = new Double(myPref.getDouble("HpuxBufferRcacheTrig", 95.0));
        hpuxdiskbusytrigger = new Double(myPref.getDouble("HpuxDiskBusyTrig", 100.0));
        hpuxrqueuetrigger = new Double(myPref.getDouble("HpuxRqueueTrig", 2.0));
        hpuxdiskavservtrigger = new Double(myPref.getDouble("HpuxDiskAvservTrig", 30.0));
        hpuxdiskavquetrigger = new Double(myPref.getDouble("HpuxDiskAvqueTrig", 10.0));
        aixcpuidletrigger = new Double(myPref.getDouble("AIXCpuIdleTrig", 0.0));
        aixcpusystemtrigger = new Double(myPref.getDouble("AIXCpuSystemTrig", 50.0));
        aixcpuwiotrigger = new Double(myPref.getDouble("AIXCpuWioTrig", 25.0));
        aixcpuusrtrigger = new Double(myPref.getDouble("AIXCpuUsrTrig", 50.0));
        aixbufferrcachetrigger = new Double(myPref.getDouble("AIXBufferRcacheTrig", 95.0));
        aixdiskbusytrigger = new Double(myPref.getDouble("AIXDiskBusyTrig", 100.0));
        aixrqueuetrigger = new Double(myPref.getDouble("AIXRqueueTrig", 2.0));
        aixdiskavservtrigger = new Double(myPref.getDouble("AIXDiskAvservTrig", 30.0));
        aixdiskavquetrigger = new Double(myPref.getDouble("AIXDiskAvqueTrig", 10.0));
        linuxcpuidletrigger = new Double(myPref.getDouble("LinuxCpuIdleTrig", 0.0));
        linuxcpusystemtrigger = new Double(myPref.getDouble("LinuxCpuSystemTrig", 50.0));
        linuxcpuwiotrigger = new Double(myPref.getDouble("LinuxCpuWioTrig", 25.0));
        linuxcpuusrtrigger = new Double(myPref.getDouble("LinuxCpuUsrTrig", 25.0));
        pdfindexpage = myPref.get("PDFindexpage", "");
        pdfbottomleft = myPref.get("PDFbottomleft", "kSar-" + VersionNumber.getVersionNumber());
        pdfupperright = myPref.get("PDFupperright", "");
        imagewidth = myPref.getInt("ImageWidth", 800);
        imageheight = myPref.getInt("ImageHeight", 600);
        imagehtml = myPref.getBoolean("ImageHTML", false);
        String mysshidentity = myPref.get("SSHIdentify", null);
        if (mysshidentity != null) {
            sshidentity = new File(mysshidentity);
        }
        if ((mybackimage = myPref.get("BackgroundImage", null)) != null) {
            background_image = new File(mybackimage);
        }
        tile_at_startup = myPref.getBoolean("StartTile", false);
        landf = myPref.get("landf", UIManager.getLookAndFeel().getName());
        Integer tmpcolor = myPref.getInt("color1", Color.BLUE.getRGB());
        color1 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color2", Color.GREEN.getRGB());
        color2 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color3", Color.RED.getRGB());
        color3 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color4", Color.BLACK.getRGB());
        color4 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color5", Color.MAGENTA.getRGB());
        color5 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color6", Color.YELLOW.getRGB());
        color6 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color7", Color.CYAN.getRGB());
        color7 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color8", Color.GRAY.getRGB());
        color8 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color9", Color.ORANGE.getRGB());
        color9 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color10", Color.PINK.getRGB());
        color10 = new Color(tmpcolor);
        tmpcolor = myPref.getInt("color11", Color.DARK_GRAY.getRGB());
        color11 = new Color(tmpcolor);
        alwaysrefresh = myPref.getInt("alwaysrefresh", 100);
        somerefresh = myPref.getInt("somerefresh", 500);
        lessrefresh = myPref.getInt("lessrefresh", 1000);
        norefresh = myPref.getInt("norefresh", 5000);
        somerefresh_time = myPref.getInt("somerefresh_time", 10);
        lessrefresh_time = myPref.getInt("lessrefresh_time", 100);
    }

    public static void writeDefault() {
        String value;
        if (!loaded) {
            myPref = Preferences.userNodeForPackage(kSar.class);
            loaded = true;
        }
        myPref.putInt("version", VersionNumber.getVersionNumberint());
        if (lastReadDirectory != null) {
            myPref.put("lastReadDirectory", lastReadDirectory.toString());
        }
        if (lastExportDirectory != null) {
            myPref.put("lastExportDirectory", lastExportDirectory.toString());
        }
        if (unified_user != null) {
            myPref.put("UnifiedUser", unified_user);
        }
        if (unified_host != null) {
            myPref.put("UnifiedHost", unified_host);
        }
        tmpbuf = new StringBuffer();
        Iterator<String> it = sshconnectioncmd.iterator();
        while (it.hasNext()) {
            tmpbuf.append(it.next() + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("SSHCommandList", tmpbuf.toString());
        }
        tmpbuf = new StringBuffer();
        for (String key22 : shortcut_window_processlist.keySet()) {
            value = shortcut_window_processlist.get(key22);
            tmpbuf.append(key22 + "\u221e" + value + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("Processlist", tmpbuf.toString());
        } else {
            myPref.remove("Processlist");
        }
        tmpbuf = new StringBuffer();
        for (String key22 : shortcut_window_list.keySet()) {
            value = shortcut_window_list.get(key22);
            tmpbuf.append(key22 + "\u221e" + value + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("Shortcut", tmpbuf.toString());
        } else {
            myPref.remove("Shortcut");
        }
        tmpbuf = new StringBuffer();
        for (String key22 : association_list.keySet()) {
            value = association_list.get(key22);
            tmpbuf.append(key22 + "\u221e" + value + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("Association", tmpbuf.toString());
        } else {
            myPref.remove("Association");
        }
        tmpbuf = new StringBuffer();
        it = startup_windows_list.iterator();
        while (it.hasNext()) {
            tmpbuf.append(it.next() + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("Startup", tmpbuf.toString());
        } else {
            myPref.remove("Startup");
        }
        tmpbuf = new StringBuffer();
        it = sshconnectionmap.iterator();
        while (it.hasNext()) {
            tmpbuf.append(it.next() + "\u00a7");
        }
        if (tmpbuf.length() > 1) {
            myPref.put("SSHConnectionList", tmpbuf.toString());
        }
        myPref.putDouble("SolarisCpuIdleTrig", solariscpuidletrigger);
        myPref.putDouble("SolarisCpuSystemTrig", solariscpusystemtrigger);
        myPref.putDouble("SolarisCpuWioTrig", solariscpuwiotrigger);
        myPref.putDouble("SolarisCpuusrTrig", solariscpuusrtrigger);
        myPref.putDouble("SolarisBufferRcacheTrig", solarisbufferrcachetrigger);
        myPref.putDouble("SolarisDiskBusyTrig", solarisdiskbusytrigger);
        myPref.putDouble("SolarisRqueueTrig", solarisrqueuetrigger);
        myPref.putDouble("SolarisPageScanTrig", solarispagescantrigger);
        myPref.putDouble("SolarisDiskAvservTrig", solarisdiskavservtrigger);
        myPref.putDouble("SolarisDiskAvqueTrig", solarisdiskavquetrigger);
        myPref.putDouble("HpuxCpuIdleTrig", hpuxcpuidletrigger);
        myPref.putDouble("HpuxCpuSystemTrig", hpuxcpusystemtrigger);
        myPref.putDouble("HpuxCpuWioTrig", hpuxcpuwiotrigger);
        myPref.putDouble("HpuxCpuusrTrig", hpuxcpuusrtrigger);
        myPref.putDouble("HpuxBufferRcacheTrig", hpuxbufferrcachetrigger);
        myPref.putDouble("HpuxDiskBusyTrig", hpuxdiskbusytrigger);
        myPref.putDouble("HpuxRqueueTrig", hpuxrqueuetrigger);
        myPref.putDouble("HpuxDiskAvservTrig", hpuxdiskavservtrigger);
        myPref.putDouble("HpuxDiskAvqueTrig", hpuxdiskavquetrigger);
        myPref.putDouble("AIXCpuIdleTrig", aixcpuidletrigger);
        myPref.putDouble("AIXCpuSystemTrig", aixcpusystemtrigger);
        myPref.putDouble("AIXCpuWioTrig", aixcpuwiotrigger);
        myPref.putDouble("AIXCpuUsrTrig", aixcpuusrtrigger);
        myPref.putDouble("AIXBufferRcacheTrig", aixbufferrcachetrigger);
        myPref.putDouble("AIXDiskBusyTrig", aixdiskbusytrigger);
        myPref.putDouble("AIXRqueueTrig", aixrqueuetrigger);
        myPref.putDouble("AIXDiskAvservTrig", aixdiskavservtrigger);
        myPref.putDouble("AIXDiskAvqueTrig", aixdiskavquetrigger);
        myPref.putDouble("LinuxCpuIdleTrig", linuxcpuidletrigger);
        myPref.putDouble("LinuxCpuSystemTrig", linuxcpusystemtrigger);
        myPref.putDouble("LinuxCpuWioTrig", linuxcpuwiotrigger);
        myPref.putDouble("LinuxCpuUsrTrig", linuxcpuusrtrigger);
        if (!pdfbottomleft.equals("kSar-" + VersionNumber.getVersionNumber())) {
            myPref.put("PDFbottomleft", pdfbottomleft);
        }
        myPref.put("PDFindexpage", pdfindexpage);
        myPref.put("PDFupperright", pdfupperright);
        myPref.putInt("ImageWidth", imagewidth);
        myPref.putInt("ImageHeight", imageheight);
        myPref.putBoolean("ImageHTML", imagehtml);
        if (sshidentity != null) {
            myPref.put("SSHIdentify", sshidentity.toString());
        } else {
            myPref.remove("SSHIdentify");
        }
        if (background_image != null) {
            myPref.put("BackgroundImage", background_image.toString());
        } else {
            myPref.remove("BackgroundImage");
        }
        myPref.putBoolean("StartTile", tile_at_startup);
        myPref.putBoolean("stricthost", ssh_stricthostchecking);
        myPref.putInt("color1", color1.getRGB());
        myPref.putInt("color2", color2.getRGB());
        myPref.putInt("color3", color3.getRGB());
        myPref.putInt("color4", color4.getRGB());
        myPref.putInt("color5", color5.getRGB());
        myPref.putInt("color6", color6.getRGB());
        myPref.putInt("color7", color7.getRGB());
        myPref.putInt("color8", color8.getRGB());
        myPref.putInt("color9", color9.getRGB());
        myPref.putInt("color10", color10.getRGB());
        myPref.putInt("color11", color11.getRGB());
        myPref.putInt("alwaysrefresh", alwaysrefresh);
        myPref.putInt("somerefresh", somerefresh);
        myPref.putInt("lessrefresh", lessrefresh);
        myPref.putInt("norefresh", norefresh);
        myPref.putInt("somerefresh_time", somerefresh_time);
        myPref.putInt("lessrefresh_time", lessrefresh_time);
        myPref.put("landf", landf);
        if (pref_to_flush != null) {
            myPref.remove(pref_to_flush);
        }
        try {
            myPref.flush();
        }
        catch (BackingStoreException e) {
            if (hasUI) {
                JOptionPane.showMessageDialog(null, "There was a problem while saving your preferences", "Config error", 0);
            }
            System.err.println("There was a problem while saving your preferences");
        }
    }

    public static String getBackground_image() {
        if (background_image == null) {
            return "";
        }
        return background_image.getAbsolutePath();
    }

    public static String getSSHidentity() {
        if (sshidentity == null) {
            return "";
        }
        return sshidentity.getAbsolutePath();
    }

    public static String readSpecial(String key) {
        String value = myPref.get(key, null);
        StringBuffer temp = new StringBuffer();
        if (value == null) {
            return null;
        }
        if ("__SPLITVALUE__".equals(value)) {
            String tmp;
            int idx = 0;
            while ((tmp = myPref.get("__" + idx + "_" + key, null)) != null) {
                temp.append(tmp);
                ++idx;
            }
            return temp.toString();
        }
        return myPref.get(key, null);
    }

    public static void writeSpecial(String key, String value) {
        if (value == null) {
            return;
        }
        int value_size = value.length();
        int idx = 0;
        if (value_size > 8000) {
            int cur_pos = 0;
            while (value_size - cur_pos > 8000) {
                myPref.put("__" + idx + "__" + key, value.substring(cur_pos, cur_pos + 8000));
                ++idx;
                cur_pos += 8000;
            }
            myPref.put("__" + idx + "_" + key, value.substring(cur_pos, value_size));
            myPref.put(key, "__SPLITVALUE__");
        } else {
            myPref.put(key, value);
        }
        try {
            myPref.flush();
        }
        catch (BackingStoreException e) {
            if (hasUI) {
                JOptionPane.showMessageDialog(null, "There was a problem while saving your preferences", "Config error", 0);
            }
            System.err.println("There was a problem while saving your preferences");
        }
    }

    static {
        tmpbuf = null;
        hasUI = false;
        pref_to_flush = null;
        tempstr = null;
        DEFAULT_FONT = new Font("SansSerif", 1, 18);
        DEFAULT_STROKE = new BasicStroke(1.0f);
    }
}

