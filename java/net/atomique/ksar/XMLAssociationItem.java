/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

public class XMLAssociationItem {
    String description = null;
    String command = null;
    String processlist = null;
    String errormsg = null;

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getProcesslist() {
        return this.processlist;
    }

    public void setProcesslist(String processlist) {
        this.processlist = processlist;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public boolean valid() {
        if (this.description == null) {
            this.errormsg = new String("Missing description on association");
            return false;
        }
        if (this.command == null) {
            this.errormsg = new String("Missing command on association (" + this.description + ")");
            return false;
        }
        if (this.command.length() == 0) {
            this.errormsg = new String("Missing command on association (" + this.description + ")");
            return false;
        }
        return true;
    }

    public String toString() {
        if (!this.valid()) {
            return "";
        }
        return "cmd://" + this.command;
    }
}

