/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.atomique.ksar.XMLAssociationItem;
import net.atomique.ksar.XMLShortcutItem;
import net.atomique.ksar.kSarConfig;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLConfig
extends DefaultHandler {
    public boolean beenparse = false;
    public ArrayList<XMLShortcutItem> shortcutlist = null;
    public ArrayList<XMLAssociationItem> associationlist = null;
    private XMLShortcutItem cur_shortcut = null;
    private XMLAssociationItem cur_association = null;
    private String tempval;
    private boolean inshortcut = false;
    private boolean inassociation = false;

    public XMLConfig(String filename, boolean replace) {
        boolean added = false;
        this.load_document(filename);
        if (replace) {
            kSarConfig.shortcut_window_list.clear();
            kSarConfig.association_list.clear();
            kSarConfig.startup_windows_list.clear();
            kSarConfig.shortcut_window_processlist.clear();
        }
        for (XMLShortcutItem tmp : this.shortcutlist) {
            added = true;
            kSarConfig.shortcut_window_list.put(tmp.toString(), tmp.getDescription());
            if (tmp.getStartup()) {
                kSarConfig.startup_windows_list.add(tmp.getDescription());
            }
            if (!tmp.hasprocesslist()) continue;
            kSarConfig.shortcut_window_processlist.put(tmp.toString(), tmp.getProcesslistCommand());
        }
        if (added) {
            kSarConfig.writeDefault();
        }
    }

    private void load_document(String xmlfile) {
        SAXParserFactory fabric = null;
        SAXParser parser = null;
        try {
            fabric = SAXParserFactory.newInstance();
            parser = fabric.newSAXParser();
            parser.parse(xmlfile, (DefaultHandler)this);
        }
        catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
        catch (SAXException ex) {
            ex.printStackTrace();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.tempval = new String(ch, start, length);
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("ksar-config".equals(qName)) {
            this.shortcutlist = new ArrayList();
            this.associationlist = new ArrayList();
        }
        if ("shortcut".equals(qName)) {
            this.cur_shortcut = new XMLShortcutItem();
            this.inshortcut = true;
        }
        if ("association".equals(qName)) {
            this.cur_association = new XMLAssociationItem();
            this.inassociation = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("ksar-config".equals(qName)) {
            this.beenparse = true;
        }
        if ("shortcut".equals(qName)) {
            if (this.cur_shortcut.valid()) {
                this.shortcutlist.add(this.cur_shortcut);
            } else {
                System.err.println("Err: " + this.cur_shortcut.getErrormsg());
                this.cur_shortcut = null;
            }
            this.inshortcut = false;
        }
        if ("association".equals(qName)) {
            if (this.cur_association.valid()) {
                this.associationlist.add(this.cur_association);
            } else {
                System.err.println("Err: " + this.cur_association.getErrormsg());
                this.cur_association = null;
            }
            this.inassociation = false;
        }
        if (this.inshortcut) {
            if ("description".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setDescription(this.tempval);
            }
            if ("type".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setType(this.tempval);
            }
            if ("host".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setHost(this.tempval);
            }
            if ("login".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setLogin(this.tempval);
            }
            if ("filename".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setFilename(this.tempval);
            }
            if ("command".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setCommand(this.tempval);
            }
            if ("startup".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setStartup(this.tempval);
            }
            if ("processlist".equals(qName) && this.cur_shortcut != null) {
                this.cur_shortcut.setProcesslist(this.tempval);
            }
        }
        if (this.inassociation) {
            if ("description".equals(qName) && this.cur_association != null) {
                this.cur_association.setDescription(this.tempval);
            }
            if ("command".equals(qName) && this.cur_association != null) {
                this.cur_association.setCommand(this.tempval);
            }
            if ("processlist".equals(qName) && this.cur_association != null) {
                this.cur_association.setProcesslist(this.tempval);
            }
        }
    }
}

