/*
 * Decompiled with CFR 0_115.
 */
package net.atomique.ksar;

public class XMLShortcutItem {
    String description = null;
    String type = null;
    String host = null;
    String login = null;
    String command = null;
    String filename = null;
    String errormsg = null;
    String processlist = null;
    boolean openatstartup = false;

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setStartup(String startup) {
        if ("yes".equals(startup)) {
            this.openatstartup = true;
        }
    }

    public String getProcesslist() {
        return this.processlist;
    }

    public boolean hasprocesslist() {
        if (this.processlist == null) {
            return false;
        }
        return true;
    }

    public void setProcesslist(String processlist) {
        this.processlist = processlist;
    }

    public boolean getStartup() {
        return this.openatstartup;
    }

    public boolean valid() {
        if (this.type == null) {
            return false;
        }
        if (this.description == null) {
            this.errormsg = new String("Missing description on shortcut");
            return false;
        }
        if ("file".equals(this.type)) {
            if (this.filename == null) {
                this.errormsg = new String("Missing filename on file shortcut (" + this.description + ")");
                return false;
            }
            return true;
        }
        if ("command".equals(this.type)) {
            if (this.command == null) {
                this.errormsg = new String("Missing command on command shortcut (" + this.description + ")");
                return false;
            }
            return true;
        }
        if ("ssh".equals(this.type)) {
            if (this.host == null) {
                this.errormsg = new String("Missing host info on ssh shortcut (" + this.description + ")");
                return false;
            }
            if (this.login == null) {
                this.errormsg = new String("Missing login info on ssh shortcut (" + this.description + ")");
                return false;
            }
            if (this.command == null) {
                this.errormsg = new String("Missing command on ssh shortcut (" + this.description + ")");
                return false;
            }
            return true;
        }
        this.errormsg = new String("type of shortcut unknown :" + this.type + " (" + this.description + ")");
        return false;
    }

    public String toString() {
        if (!this.valid()) {
            return "";
        }
        if ("ssh".equals(this.type)) {
            return "ssh://" + this.login + "@" + this.host + "/" + this.command;
        }
        if ("file".equals(this.type)) {
            return "file://" + this.filename;
        }
        if ("command".equals(this.type)) {
            return "cmd://" + this.command;
        }
        return "";
    }

    public String getProcesslistCommand() {
        if (this.processlist == null) {
            return null;
        }
        if (!this.valid()) {
            return null;
        }
        if ("ssh".equals(this.type)) {
            return "ssh://" + this.login + "@" + this.host + "/" + this.processlist;
        }
        if ("command".equals(this.type)) {
            return "cmd://" + this.processlist;
        }
        return null;
    }
}

